Установка sphinx (в зависимости от пакетного менеджера)
    sudo apt-get install sphinxsearch

Далее помещяем sphinx.conf файл в /etc/sphinxsearch
Вставляем туда данные для подключения к базе

Далее нужно поместить словарь (ru.pak) по пути /var/lib/sphinxsearch/dictionaries

Тестовый запуск

/usr/bin/indexer --all --verbose

В случае успешного индексирования получим примерно вот такие строки:

Sphinx 2.2.10-id64-release (2c212e0)
Copyright (c) 2001-2015, Andrew Aksyonoff
Copyright (c) 2008-2015, Sphinx Technologies Inc (http://sphinxsearch.com)

using config file '/path/to/config/sphinx.conf'...
indexing index 'postsIndex'...
collected 500 docs, 0.1 MB
collected 1500 attr values
sorted 0.0 Mvalues, 100.0% done
sorted 0.0 Mhits, 100.0% done
total 500 docs, 129885 bytes
total 0.049 sec, 2629570 bytes/sec, 10122.68 docs/sec
indexing index 'categoriesIndex'...
collected 30 docs, 0.0 MB
sorted 0.0 Mhits, 100.0% done
total 30 docs, 451 bytes
total 0.007 sec, 57554 bytes/sec, 3828.48 docs/sec
total 1509 reads, 0.000 sec, 0.1 kb/call avg, 0.0 msec/call avg
total 26 writes, 0.000 sec, 9.9 kb/call avg, 0.0 msec/call avg



Запускаем поискового демона:

searchd --config /etc/sphinxsearch/sphinx.conf

(Для боевого сайта необходимо настроить автозапуск)


В случае успешного запуска должны увидеть примерно вот такой вывод:

Sphinx 2.1.3-dev (r4319)
Copyright (c) 2001-2013, Andrew Aksyonoff
Copyright (c) 2008-2013, Sphinx Technologies Inc (http://sphinxsearch.com)

using config file '/usr/local/sphinx/etc/sphinx.conf'...
listening on all interfaces, port=9312
listening on all interfaces, port=9306
precaching index 'mysqlbasename'
precached 1 indexes in 0.000 sec


Коннектимся к демону по SQL протоколу что бы проверить его работоспособность:

mysql -h 127.0.0.1 -P 9306
Теперь можно использовать консольный mysql клиент.


Добавляем в системный планировщик заданий (крон, crontab) запуск индексатора через каждые сутки:

crontab -e
# добавлем в крон следующую строку:
0 0 * * * /usr/bin/indexer --rotate --config /etc/sphinxsearch/sphinx.conf --all







Не плохой ресурс http://blog-programmista.ru/post/19-sphinx-ustanovka-i-nastrojka.html


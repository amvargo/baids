# Инструкция по переносу

1. Необходим сервер как минимум на виртуализации KVM с 4 ядрами ЦП, и 4 Гб оперативы
2. ПО установленное и настроенное на сервере (если не указана версия ПО то ставится версия предоставляемая дистрибутивом Debian 10)
- ОС: GNU Linux Debian 10 (64)
- nginx (стандартная настройка для laravel, настройка переадресаций и доменов)
- memcached
- mariadb-server (при необходимости перенести БД в postgres)
- php php-fpm php-curl php-dom php-json php-memcached php-mysql (php версия 7.3)
- composer
- supervisor - для запуска очереди laravel и laravel-echo-server
- redis-server - нужен для очереди
- sphinxsearch - для быстрого поиска инструкция тут sphinx/README.md
- nodejs npm 
- laravel-echo-server ```npm install laravel-echo-server -g``` - эхо сервер
3. С помощью git клонировать репозиторий проекта в ```/var/www```, произвести развертывание проекта laravel, 
файл .env можно взять с dev сервера либо заполенить из .env.example
3.1 Собираем фронтенд ```npm install``` затем ```npm run prod```
4. БД: импортируем дамп либо создаем миграциями
5. настраиваем сфинкс по инструкции sphinx/README.md
6. запускаем очередь ларавел с помощью supervisor https://laravel.com/docs/7.x/queues#supervisor-configuration
7. запускаем laravel-echo-server с помощью supervisor https://github.com/tlaverdure/laravel-echo-server
8. Проверяем

9. не забываем включить крон ```* * * * * cd /var/www/bankrotstvo/ && php artisan schedule:run >> /dev/null 2>&1```
10. команды для парсера если нужны то описаны тут app/Console/Commands/readme.md


# post install
`npm install -g laravel-echo-server`

`php artisan storage:link`

? `composer require pusher/pusher-php-server "~4.0"`

? `composer require predis/predis`

? `npm install --save socket.io-client`

? `composer require guzzlehttp/guzzle:~6.0`


# Сервис получения сведений из ЕФРСФДЮЛ (parser)

https://fedresurs.ru/helps/Sfacts/Fedresurs.MessagesService.pdf

## 2.4 Ограничения
Максимальное допустимое количество одновременных запросов к Сервису с одного IP адреса равняется 8 в
секунду.

## 3 Метод получения авторизационного токена – v1/auth
Токен действителен в течение 12 часов

> POST запрос в теле которого содержится JSON. 
```
{
    "login": "userLogin",
    "passwordHash":
    "D1087DC2C1DBAD08204353C94580E1784B04895D7F8F7C24FD20E9F495E8468429" 
}
```

- В случае успешного выполнения запроса: JWT
- В случае возникновения ошибки: code, message

## 3.2.3 Список возможных ошибок
- 1000: Не заполнен обязательный параметр запроса
- 1003: Значение в password_hash не является хешем
- 2000: В системе нет записи с такими значениями логина и пароля
- 2001: Срок действия учетной записи истек или не наступил

## 4 Метод получения списка опубликованных сообщений – v1/messages
Метод предназначен для получения списка сообщений согласно условиям, указанным в запросе


### 4.1.2 Пример тела запроса
На адрес <url Сервиса указанного в п.2.3>/v1/messages, отправляется GET запрос с параметрами.
Параметры описаны в пункте 4.1.1.
Пример:
```
<url Сервиса>/v1/messages?
                messageTypes=AnyOther
                &messageTypes= CreditorIntentionGoToCourt
                &participant.type=Company
                &participant.code=1020100699684
                &limit=10
                &offset=0
```
## 5 Метод получения данных по сообщению – v1/messages/{guid}
Метод предназначен для получения сведений по конкретному сообщению.
GUIDы сообщений можно получить в методе получения списка опубликованных сообщений

### 5.1.2 Пример тела запроса
> На адрес <url Сервиса указанного в п.2.3>/v1/messages/, отправляется GET запрос с параметрами.
  Параметры описаны в пункте 5.1.1.
```
<url Сервиса>/v1/messages/2E150046733A4094A0A94BB16A7701B0
```

## 6 Метод получения приложенного к сообщению файла – v1/messagedocs/{guid}
Метод предназначен для получения файла, прикрепленного к сообщению.
GUIDы файлов можно получить в методе получения данных по сообщению


# OpenSSL и подпись файлов
- Для подписи файлов электронной подписью необходим сертификат, его надо положить 
по пути storage/app/signature/signer1/p12.pem 

- Из токена достаем сертификат пользователя с приватным ключем с помощью программы КриптоПро CSP. 
https://good-tender.ru/skopirovat-etsp
С помощью программы P12FromGostCSP конвертируем этот сертификат в понятный формат для openssl.

- Необходимо установить: Russian GOST crypto algorithms for OpenSSL. 
Статья по настройки: Сборка OpenSSL с поддержкой ГОСТ 2012
https://forum.nag.ru/index.php?/topic/150051-sborka-openssl-s-podderzhkoy-gost-2012/
Если не помогло то это должно решить проблему с алгоритмом 
https://forum.nag.ru/index.php?/topic/150051-sborka-openssl-s-podderzhkoy-gost-2012/
Либо эта https://habr.com/ru/company/aktiv-company/blog/467707/

- проверка алгоритма gost2012_512:
```openssl genpkey -engine gost -algorithm gost2012_512 -pkeyopt paramset:A -out client_key.pem```

## Пример использования

- Подпись письма:
```
openssl cms -sign -in doc1.txt -text -out mail.msg -signer signer1/p12.pem
```

- Подпись файла:
```
openssl cms -sign -in doc1.txt -signer ./signer1/p12.pem -out doc1.txt.sig -outform pem -nodetach
```

openssl cms verify example
To verify a signed message, run the following command:

```
openssl cms -verify -CAfile misterpki.com-chain.pem -in mail.msg -signer misterpki.com.crt -out signedtext.txt
```
https://www.misterpki.com/openssl-cms/



## Тестовая подпить
- php artisan test:task sign


cd projects/baids/www

php artisan down --retry=10 --message="Обновление..."
git reset --hard origin/dev
git pull origin dev

if [[ $? -ne 0 ]] ; then
  exit 1;
fi


cp .env.dev dev
composer install

php artisan cache:clear
php artisan config:clear
php artisan route:clear
php artisan view:clear

php artisan migrate
npm i
npm run dev
php artisan up


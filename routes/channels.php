<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

/*Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id ? $user : false;
});*/

use App\Role\UserRole;

Broadcast::channel('panel', function (\App\Models\User $user) {
    if ($user->hasAnyRoles([
        UserRole::ROLE_SUPPORT,
        UserRole::ROLE_CONTENT,
        UserRole::ROLE_STAFF,
    ])) {
        return ['id' => $user->id, 'name' => $user->name, 'full_name' => $user->full_name, 'roles' => $user->roles];
    }
    return false;
});

Broadcast::channel('parser', function (\App\Models\User $user) {
    if ($user->hasAnyRoles([
        UserRole::ROLE_SUPPORT,
        UserRole::ROLE_CONTENT,
        UserRole::ROLE_STAFF,
    ])) {
        return ['id' => $user->id, 'name' => $user->name, 'full_name' => $user->full_name, 'roles' => $user->roles];
    }
    return false;
});

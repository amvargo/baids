<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Role\UserRole;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Stevebauman\Location\Facades\Location;

Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

# site
Route::namespace('Site')->group(function () {
    Route::name('site.')->group(function () {
        Route::get('/', 'SiteController@index')->name('index');
        Route::get('/about', 'SiteController@about')->name('about');
        Route::get('/advertisers', 'SiteController@advertisers')->name('advertisers');
        Route::get('/contacts', 'SiteController@contacts')->name('contacts');
        Route::get('/commerce', 'SiteController@commerce')->name('commerce');
        Route::get('/payment', 'SiteController@payment')->name('payment');
        Route::get('/documents', 'SiteController@view')->name('documents');
        Route::get('/news', 'SiteController@news')->name('news');
        Route::get('/about-project', 'SiteController@aboutProject')->name('about-project');
        Route::get('/tariff', 'SiteController@tariff')->name('tariff');
        Route::get('/tariff-all', 'SiteController@tariffAll')->name('tariff-all');
        Route::post('/report', 'SiteController@lotReport')->name('report');

        // services
        Route::prefix('services')->group(function () {
            Route::name('service.')->group(function () {
                Route::get('/', 'ServiceController@index')->name('page');
                Route::post('/order', 'ServiceController@order')->name('order');
                Route::get('/{service}', 'ServiceController@show')->name('show');
            });
        });

        // questions
        Route::prefix('questions')->group(function () {
            Route::name('question.')->group(function () {
                Route::get('/', 'QuestionController@index')->name('page');
                Route::post('/', 'QuestionController@store')->name('store');
            });
        });

        // consulting
        Route::group(['middleware' => ['auth']], function () {
            Route::prefix('consulting')->group(function () {
                Route::name('consulting.')->group(function () {
                    Route::get('/', 'ConsultingController@index')->name('page');
                    Route::post('/', 'ConsultingController@store')->name('store');
                });
            });
        });

        // catalog
        Route::namespace('Catalog')->group(function () {
            Route::prefix('catalog')->group(function () {
                Route::name('catalog.')->group(function () {
                    Route::get('/', 'CatalogController@index')->name('index');
                    Route::get('/without-category', 'CatalogController@withoutCategory')->name('without');
                    Route::get('/{category}', 'CatalogController@show')->name('category.show');
                    Route::get('/lot/{lot}', 'LotController@show')->name('lot.show');
                });
            });
            Route::prefix('catalog-filter')->group(function () {
                Route::name('catalog-filter.')->group(function () {
                    Route::get('/', 'CatalogController@filterData')->name('index');
                    Route::get('/without-category', 'CatalogController@filterDataWithoutCategory')->name('filterDataWithoutCategory');
                    Route::get('/{category}', 'CatalogController@filterData')->name('filterData');
                });
            });
            Route::prefix('hot-lots')->group(function () {
                Route::name('hot-lots.')->group(function () {
                    Route::get('/', 'HotLotsController@index')->name('index');
                    Route::get('/{category}', 'HotLotsController@show')->name('category.show');
                    Route::get('/lot/{lot}', 'LotController@show')->name('lot.show');
                });
            });
            Route::prefix('hot-lots-filter')->group(function () {
                Route::name('hot-lots-filter.')->group(function () {
                    Route::get('/', 'HotLotsController@filterData')->name('index');
                    Route::get('/{category}', 'HotLotsController@filterData')->name('filterData');
                });
            });
        });

        // Advertising

        Route::prefix('advertising')->group(function() {
            Route::name('advertising.')->group(function() {
                Route::get('/', 'AdvertisingController@index')->name('index');
                Route::get('/show/{banner}', 'AdvertisingController@getEditBanner')->name('edit-banner');
                Route::post('/set/{banner}', 'AdvertisingController@setEditBanner')->name('set-banner');
                Route::post('/', 'AdvertisingController@create')->name('create');
                Route::post('/price', 'AdvertisingController@getPrice')->name('get-price');
                Route::post('/date-check', 'AdvertisingController@dateCheck')->name('date-check');
                Route::get('/get-banners', 'AdvertisingController@getBanners')->name('get-banners');
                Route::get('/new-payment/{payment}', 'AdvertisingController@getNewPayment')->name('new-payment-banner');
            });
        });


        // ajax api
        Route::namespace('Api')->group(function () {
            Route::prefix('api')->group(function () {
                Route::name('api.')->group(function () {
                    Route::post('/site/tariff-get', 'SiteController@tariffGet');

                    Route::post('/location/regions', 'LocationController@regions');
                    Route::get('/location/regions-get', 'LocationController@regionsGet');
                    Route::get('/location/get-session', 'LocationController@getSession');
                    Route::post('/location/set-session', 'LocationController@setSession');

                    Route::prefix('cabinet')->group(function () {
                        Route::name('cabinet.')->group(function () {
                            Route::post('/favorite', 'CabinetController@setFavorite');
                            Route::post('/publish_lot', 'CabinetController@publishLot');
                            Route::get('/user_mailings', 'CabinetController@userMailings');
                            Route::post('/user_mailings', 'CabinetController@updateUserMailing');
                            Route::delete('/user_mailings', 'CabinetController@deleteUserMailing');
                            Route::get('/data_for_mailings', 'CabinetController@dataForMailings');
                            Route::get('/mailing_lots', 'CabinetController@mailingLots');
                        });
                    });

                    Route::prefix('vehicle')->group(function () {
                        Route::name('vehicle.')->group(function () {
                            Route::get('/brands', 'VehicleController@brands');
                            Route::get('/spec-brands', 'VehicleController@specBrands');
                            Route::get('/models', 'VehicleController@models');
                            Route::get('/spec-models', 'VehicleController@specModels');
                        });
                    });

                    Route::prefix('catalog')->group(function () {
                        Route::name('catalog.')->group(function () {
                            Route::post('/trade_organizer_search_list/without-category', 'CatalogController@trade_organizer_search_list_without_category');
                            Route::post('/trade_organizer_search_list/{category}', 'CatalogController@trade_organizer_search_list');
                            Route::post('/debtor_search_list/without-category', 'CatalogController@debtor_search_list_without_category');
                            Route::post('/debtor_search_list/{category}', 'CatalogController@debtor_search_list');
                            Route::post('/trade_organizer_search_list', 'CatalogController@trade_organizer_search_list');
                            Route::post('/debtor_search_list', 'CatalogController@debtor_search_list');
                        });
                    });
                });
            });
        });

    });

    // cabinet
    Route::group(['middleware' => ['auth']], function () {
        Route::prefix('cabinet')->group(function () {
            Route::namespace('Cabinet')->group(function () {
                Route::name('cabinet.')->group(function () {
                    Route::get('/profile', 'ProfileController@index')->name('profile');
                    Route::put('/profile', 'ProfileController@store')->name('change-profile');
                    Route::put('/org-profile', 'ProfileController@store_org_profile')->name('change-org-profile');
                    Route::put('/password', 'ProfileController@store_password')->name('change-password');

                    Route::resource('/notifications', 'NotificationController')->only(['index', 'destroy']);
                    Route::put('/order-evaluation', 'NotificationController@saveOrderEvaluation')->name('order-evaluation');
                    Route::get('/mailings', 'NotificationController@mailings')->name('mailings');
                    Route::get('/favorites', 'FavoriteController@index')->name('favorites');
                    Route::get('/balance', 'PaymentController@index')->name('payments');
                    Route::get('/services', 'ServiceController@index')->name('services');
                    Route::get('/services/{payment}', 'ServiceController@getNewPayment')->name('new-payment-service');
                    Route::group(['middleware' => 'can:create,App\Models\TradeLot'], function () {
                        Route::get('/bidding', 'BiddingController@index')->name('bidding');
                    });
                    Route::group(['middleware' => 'can:create,App\Models\UserMailingLot'], function () {
                        Route::get('/data_for_lot_adding', 'BiddingController@dataForLotAdding');
                    });
                });
            });

            Route::name('cabinet.')->get('/banners', 'AdvertisingController@getUserTable')->name('banners');
        });
    });
});



# admin panel
Route::group(['middleware' => ['auth', 'role:' . UserRole::ROLE_SUPPORT]], function () {

    Route::prefix('panel')->group(function () {
        Route::namespace('Panel')->group(function () {
            Route::get('/', 'PanelController@index')->name('panel');
            Route::get('{page}', 'PanelController@index')
                ->where('page', '^(?!.*(api)).*');
        });
    });

    Route::prefix('panel/api')->group(function () {
        Route::namespace('Panel')->group(function () {

            # Messages
            Route::post('/ip-check', 'UserController@getIp')->name('ip-check');
            Route::get('/get-messages', 'MessageController@getMessages')->name('get-messages');
            Route::post('/create-message', 'MessageController@createMessage')->name('create-message');
            Route::delete('/delete-message/{id}', 'MessageController@deleteMessage')->name('delete-message');

            # Sliders
            Route::get('/sliders', 'SliderController@index')->name('sliders');
            Route::post('/sliders', 'SliderController@editOrCreate')->name('edit-or-create');
            Route::delete('/sliders/{id}', 'SliderController@delete')->name('delete');

            # Image
            Route::post('/image', 'ImageController@create')->name('create');
            Route::delete('/image', 'ImageController@delete')->name('delete');

            # Moderation
            Route::get('/moderation/user-org-trade', 'ModerationUserOrgTradeController@index')->name('get-moderation');
            Route::post('/moderation/user-org-trade/change-status', 'ModerationUserOrgTradeController@changeStatus')->name('change-status');
            Route::get('/moderation/user-org-trade/get-statuses', 'ModerationUserOrgTradeController@getStatuses')->name('get-statuses');

            Route::get('/moderation/banners', 'ModerationAdvertisingController@index');
            Route::post('/moderation/banners/change-status', 'ModerationAdvertisingController@changeStatus');
            Route::get('/moderation/banners/get-statuses', 'ModerationAdvertisingController@getStatuses');

            # SeoConfiguration
            Route::get('/seo-configurations', 'SeoConfigurationController@index')->name('get-seo-configuraions');
            Route::post('/seo-configurations', 'SeoConfigurationController@save')->name('save-seo-configuraions');
            Route::delete('/seo-configurations/{id}', 'SeoConfigurationController@delete')->name('delete-seo-configuraions');
            Route::get('/seo-get-element', 'SeoConfigurationController@getElement')->name('get-element-seo');

            # Option
            Route::get('/options', 'OptionsController@index')->name('get-options');
            Route::post('/options', 'OptionsController@save')->name('save-option');

            # dashboard
            Route::get('/dashboard', 'PanelController@dashboard');

            # resources
            Route::prefix('resource')->group(function () {
                Route::resource('users', 'UserController');
                Route::post('users/{id}', 'UserController@update');
                Route::resource('staff', 'StaffController');
                Route::resource('orders', 'OrderController');
                Route::resource('payments', 'PaymentController');
                Route::resource('questions', 'QuestionController');
                Route::resource('services', 'ServiceController');
                Route::resource('news', 'NewsController');
                Route::resource('about_project_section', 'AboutProjectController');
                Route::post('news-image/{id}', 'NewsController@imageUpload2');
                Route::delete('news-image/{id}', 'NewsController@imageDelete2');

                Route::post('service-image/{id}', 'ServiceController@imageUpload');
                Route::delete('service-image/{id}', 'ServiceController@imageDelete');
                Route::post('service-image2/{id}', 'ServiceController@imageUpload2');
                Route::delete('service-image2/{id}', 'ServiceController@imageDelete2');

                Route::namespace('Catalog')->group(function () {
                    Route::post('catalog-lot-images-load', 'LotController@imagesLoad');
                    Route::post('catalog-lot-images-delete', 'LotController@imagesDelete');
                    Route::resource('catalog-lots', 'LotController');
                    Route::resource('catalog-params', 'LotParamController');
                    Route::resource('catalog-categories', 'CategoryController');
                    Route::post('catalog-category-images-load', 'CategoryController@imagesLoad');
                    Route::post('catalog-category-images-delete', 'CategoryController@imagesDelete');
                    Route::get('/catalog/lists', 'CategoryController@lists');
                });

                Route::namespace('Consulting')->group(function () {
                    Route::resource('consulting_messages', 'MessageController');
                    Route::resource('consulting_categories', 'CategoryController');
                });

                // <table-items> models
                //Route::get('model/{model}', 'TableItemsController');
                Route::resource('model', 'TableItemsController');
                //Route::resource('model/VehicleMaker', 'TableItemsController');
            });

            # parser
            Route::prefix('command')->group(function () {
                Route::post('run-parsing-params', 'CommandController@parserParams');
            });

        });
    });

});

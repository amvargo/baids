<?php

use Illuminate\Database\Seeder;
use App\Models\Service;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Service::class, 10)->create()->each(function ($item) {
            //dd(factory(Service::class)->make()->toArray());
            $item->save(factory(Service::class)->make()->toArray());
        });
    }
}

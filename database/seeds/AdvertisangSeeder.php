<?php

use Illuminate\Database\Seeder;

class AdvertisangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = array(
            array(
                'title' => 'Реклама (главная стр.)',
                'description' => 'Горизонтальные и вертикальные баннеры на главной странице',
                'priority' => 10000,
                'published' => 1,
                'price' => 1000,
                'system_key' => 'advertising_general'
            ),
            array(
                'title' => 'Реклама (каталог стр.)',
                'description' => 'Горизонтальные и вертикальные баннеры на страницах каталога',
                'priority' => 11000,
                'published' => 1,
                'price' => 500,
                'system_key' => 'advertising_catalog'
            )
        );

        \DB::table('services')->insert($services);
    }
}

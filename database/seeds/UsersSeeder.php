<?php

use App\Models\Panel\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::find(1);
        if (!$admin) {
            $admin = new User();
            $admin->fill([
                'name' => 'Виталий',
                'last_name' => 'Калиноский',
                'middle_name' => 'Леонидович',
                'email' => 'kalinovsky@echo-company.ru',
                'password' => \Illuminate\Support\Facades\Hash::make('ww123')
            ]);
            $admin->roles = ['ROLE_ADMIN'];
            $admin->save();
        }

        factory(User::class, 59)->create()->each(function ($user) {
            //dd(factory(User::class)->make()->toArray());
            $user->save(factory(User::class)->make()->toArray());
        });

        $workers = User::where('roles', '=', null)->take(10)->get();
        $faker = Faker\Factory::create('ru_RU');
        foreach ($workers as $worker) {
            $worker->roles = [\App\Role\UserRole::ROLE_STAFF];
            $worker->phone = $faker->phoneNumber;
            $worker->appointment = $faker->randomElement([
                'Должность 1', 'Должность 2', 'Должность 3'
            ]);
            $worker->department = $faker->randomElement([
                'Отдел 1', 'Отдел 2', 'Отдел 3'
            ]);
            $worker->save();
        }


        $staff = User::where('roles', '=', null)->take(5)->get();
        foreach ($staff as $item) {
            $item->roles = [\App\Role\UserRole::ROLE_CONTENT];
            $item->phone = $faker->phoneNumber;
            $item->appointment = $faker->randomElement([
                'Редактор 1 отд.', 'Редактор 2 отд.', 'Редактор 3 отд.'
            ]);
            $item->department = $faker->randomElement([
                'Отдел контента 1', 'Отдел контента 2', 'Отдел контента 3'
            ]);
            $item->save();
        }
    }
}

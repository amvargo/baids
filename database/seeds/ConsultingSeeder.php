<?php

use App\Models\ConsultingCategory;
use App\Models\ConsultingMessage;
use Illuminate\Database\Seeder;

class ConsultingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ConsultingCategory::class, 12)->create()->each(function ($item) {
            //dd(factory(ConsultingCategory::class)->make()->toArray());
            $item->save(factory(ConsultingCategory::class)->make()->toArray());
        });
        factory(ConsultingMessage::class, 60)->create()->each(function ($item) {
            //dd(factory(ConsultingMessage::class)->make()->toArray());
            $item->save(factory(ConsultingMessage::class)->make()->toArray());
        });
    }
}

<?php

use App\Models\Question;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     * composer dump-autoload
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            UsersSeeder::class,
            QuestionsSeeder::class,
            ConsultingSeeder::class,
            ServicesSeeder::class,
            OrderSeeder::class,
            VehicleMakesTableSeeder::class,
            VehicleModelsTableSeeder::class,
            VehicleTypesTableSeeder::class,
            CatalogSeeder::class,
            AdvertisangSeeder::class
        ]);
    }
}

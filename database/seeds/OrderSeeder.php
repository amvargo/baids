<?php

use App\Models\Order;
use App\Models\Payment;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Order::class, 30)->create()->each(function ($question) {
            //dd(factory(Question::class)->make()->toArray());
            $question->save(factory(Order::class)->make()->toArray());
        });

        $orders = Order::take(10)->get();
        foreach ($orders as $order) {
            $payment = new Payment([
                'user_id' => $order->user_id,
                'staff_id' => $order->staff_id,
                'service_id' => $order->service_id,
                'amount' => rand(1, 200) * 200,
                'type' => 'success',
            ]);
            $payment->save();
            $order->payment = true;
            $order->save();
        }
    }
}

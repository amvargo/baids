<?php

use App\Models\Question;
use Illuminate\Database\Seeder;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Question::class, 60)->create()->each(function ($question) {
            //dd(factory(Question::class)->make()->toArray());
            $question->save(factory(Question::class)->make()->toArray());
        });
    }
}

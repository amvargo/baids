<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TradeLotsPriceReductionAuctionAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trade_lot_info', function (Blueprint $table) {
            $table->text('PriceReductionAuction')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trade_lot_info', function (Blueprint $table) {
            $table->dropColumn('PriceReductionAuction');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLotPubOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trade_lots', function (Blueprint $table) {
            $table->timestamp('deleted_at')->nullable();
            $table->boolean('published')->default(1);
        });
        Schema::create('trade_lot_trade_lot_classificator', function (Blueprint $table) {
            $table->bigInteger('trade_lot_classificator_id')->index('inx1');
            $table->bigInteger('trade_lot_id')->index('inx2');
        });
        Schema::create('trade_lot_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('trade_lot_id')->index();
            $table->smallInteger('sort')->default(100);
            $table->string('file', 128);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trade_lots', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
            $table->dropColumn('published');
        });
        Schema::dropIfExists('trade_lot_trade_lot_classificator');
        Schema::dropIfExists('trade_lot_images');
    }
}

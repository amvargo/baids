<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TradePlaceSiteColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fr_company_trade_organizer', function (Blueprint $table) {
            $table->string('site', 128)->nullable();
        });
        Schema::table('trade_lot_params', function (Blueprint $table) {
            $table->text('replace_table')->nullable();
        });
        Schema::table('user_payments', function (Blueprint $table) {
            $table->bigInteger('order_id')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fr_company_trade_organizer', function (Blueprint $table) {
            $table->dropColumn('site');
        });
        Schema::table('trade_lot_params', function (Blueprint $table) {
            $table->dropColumn('replace_table');
        });
        Schema::table('user_payments', function (Blueprint $table) {
            $table->dropColumn('order_id');
        });
    }
}

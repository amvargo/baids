<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserOrgTrades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->string('key', 128)->primary()->unique();
            $table->text('value')->nullable();
            $table->timestamps();
        });
        Schema::create('user_org_trades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->index();
            $table->string('sro', 15);
            $table->string('inn', 12);
            $table->string('reg_number', 30);
            $table->boolean('confirmed')->default(false);
            $table->timestamps();
        });
        Schema::create('user_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->string('title', 512);
            $table->text('body');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('user_notification_reads', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->unsignedInteger('user_notification_id')->nullable()->index();
            $table->boolean('is_read')->default(true);
            $table->timestamps();
            $table->unique(['user_id', 'user_notification_id']);
        });
        Schema::create('user_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->unsignedInteger('service_id')->nullable()->index();
            $table->text('info')->nullable();
            $table->string('title', 128);
            $table->float('amount', 15, 2)->nullable();
            $table->string('status', 32)->default('new');
            $table->timestamps();
        });
        Schema::table('trade_lots', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable()->index();
        });
        Schema::create('trade_lot_favorites', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->unsignedInteger('trade_lot_id')->nullable()->index();
            $table->timestamps();
        });

        // insert test notifications
        $items = [];
        for ($i = 0; $i < 20; $i++) {
            $items[] = [
                'user_id' => null,
                'title' => 'Тестовое общее уведомление ' . ($i + 1),
                'body' => 'Содержимое общего уведомления',
                'created_at' => now()->addDays(-100 + $i),
            ];
        }
        $items[] = [
            'user_id' => 1,
            'title' => 'Тестовое личное уведомление',
            'body' => 'Содержимое личного уведомления',
            'created_at' => now()->addDays(-4),
        ];
        DB::table('user_notifications')->insert($items);

        // insert test user_payments
        $items = [];
        for ($i = 0; $i < 20; $i++) {
            $items[] = [
                'user_id' => 1,
                'service_id' => 1,
                'info' => json_encode(["type" => "Visa Classic"]),
                'title' => 'Тестовый платеж ' . ($i + 1),
                'status' => 'new',
                'amount' => rand(3, 17) * 100,
                'created_at' => now()->addDays(-30 + $i),
            ];
        }
        DB::table('user_payments')->insert($items);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('options');
        Schema::dropIfExists('user_org_trades');
        Schema::dropIfExists('user_notifications');
        Schema::dropIfExists('user_notification_reads');
        Schema::dropIfExists('user_payments');
        Schema::dropIfExists('trade_lot_favorites');
        Schema::table('trade_lots', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}

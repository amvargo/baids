<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersAndPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('service_id')->nullable()->index();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->unsignedInteger('staff_id')->nullable()->index()->nullable();
            $table->boolean('payment')->default(false);
            $table->string('stage', 64)->nullable();
            $table->string('status', 16)->default('new');
        });

        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('user_id')->nullable()->index();
            $table->unsignedInteger('staff_id')->nullable()->index()->nullable();
            $table->unsignedInteger('service_id')->nullable()->index();
            $table->string('type', 64)->nullable();
            $table->float('amount', 15, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
        Schema::dropIfExists('payments');
    }
}

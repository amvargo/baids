<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradeDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_documents', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('trade_id');
            $table->foreign('trade_id')
                ->references('id')
                ->on('trades')
                ->onDelete('cascade');

            $table->string('name');
            $table->string('url');
            $table->string('hash');
            $table->string('size');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_documents');
    }
}

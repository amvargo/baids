<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAmList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(
"ALTER TABLE `fr_arbitr_manager`
        ADD COLUMN `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
        DROP PRIMARY KEY,
        ADD PRIMARY KEY (`id`) USING BTREE;"
        );
        DB::statement(
"ALTER TABLE `fr_arbitr_manager`
	    CHANGE COLUMN `ArbitrManagerID` `ArbitrManagerID` BIGINT(20) UNSIGNED NULL AFTER `id`;"
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement(
    "ALTER TABLE `fr_arbitr_manager`
            CHANGE COLUMN `ArbitrManagerID` `ArbitrManagerID` BIGINT(20) UNSIGNED NULL FIRST,
            DROP COLUMN `id`,
            DROP PRIMARY KEY,
            ADD PRIMARY KEY (`ArbitrManagerID`) USING BTREE;"
        );
    }
}

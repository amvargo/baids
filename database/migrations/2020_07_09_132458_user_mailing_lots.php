<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserMailingLots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_mailing_lots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('category_id')->index();
            $table->double('price_from', 16, 2)->nullable();
            $table->double('price_to', 16, 2)->nullable();
            $table->timestamp('last_report')->nullable();
            $table->boolean('enabled')->default(true)->index();

            $table->foreign('user_id', 'user_mailing_lots_fk1')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('category_id', 'user_mailing_lots_fk2')
                ->references('id')->on('trade_lot_categories')
                ->onDelete('cascade');
        });

        Schema::create('user_mailing_lot_filters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_mailing_lot_id')->index();
            $table->unsignedBigInteger('trade_lot_param_id')->index();
            $table->string('value')->index();
            $table->bigInteger('value_from')->nullable();
            $table->bigInteger('value_to')->nullable();
            //$table->unique(['user_mailing_lot_id', 'trade_lot_param_id', 'value'], 'ukey1');

            $table->foreign('user_mailing_lot_id', 'user_mailing_lot_filters_fk1')
                ->references('id')->on('user_mailing_lots')
                ->onDelete('cascade');

            $table->foreign('trade_lot_param_id', 'user_mailing_lot_filters_fk2')
                ->references('id')->on('trade_lot_params')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('user_mailing_lot_filters');
        Schema::dropIfExists('user_mailing_lots');

        Schema::enableForeignKeyConstraints();
    }
}

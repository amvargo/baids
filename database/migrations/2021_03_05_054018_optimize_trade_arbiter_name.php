<?php

use App\Models\Trade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OptimizeTradeArbiterName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $output = new \Symfony\Component\Console\Output\ConsoleOutput();

        Schema::table('trades', function (Blueprint $table) {
            $table->string('arbitr_name')->nullable();
        });

        $output->writeln('Updating trade names... Please wait');
        //быстрое заполнение ФИО организаторов:
        DB::statement("UPDATE `trades` SET arbitr_name = concat(trim(JSON_VALUE(trades.ArbitrManager, '$.LastName')), ' ', trim(JSON_VALUE(trades.ArbitrManager, '$.FirstName')), ' ', trim(JSON_VALUE(trades.ArbitrManager, '$.MiddleName'))) WHERE arbitr_name IS NULL");
        
        //у кого нет отчества:
        $output->writeln('Updating trade first names... Please wait');
        DB::statement("UPDATE `trades` SET arbitr_name = concat(trim(JSON_VALUE(trades.ArbitrManager, '$.LastName')), ' ', trim(JSON_VALUE(trades.ArbitrManager, '$.FirstName'))) WHERE arbitr_name IS NULL");
        
        //краткие названия компаний
        $output->writeln('Updating arbiters short names... Please wait');
        DB::statement("UPDATE `trades` SET arbitr_name = trim(JSON_VALUE(trades.ArbitrManager, '$.ShortName')) WHERE arbitr_name IS NULL");
        
        //полные названия компаний
        $output->writeln('Updating arbiters full names... Please wait');
        DB::statement("UPDATE `trades` SET arbitr_name = trim(JSON_VALUE(trades.ArbitrManager, '$.FullName')) WHERE arbitr_name IS NULL");

        DB::statement("ALTER TABLE `trade_lots` ADD INDEX `trade_lots_for_trades_index` (`published`, `deleted_at`);");
        DB::statement("ALTER TABLE `trade_lots` ADD INDEX `trade_lots_for_trades_inn_index` (`TradeId`, `TradePlaceINN`);");
        
        //вариант с помощью Eloquent, занимает около часа вместо 2-3 минут, но, возможно, пригодится в будущем
        //Trade::whereNull('arbitr_name')->orderBy('id', 'DESC')->chunk(100, function ($trades) {
        //    foreach ($trades as $trade) {
        //        $trade->fill([
        //            'arbitr_name' => sprintf(
        //                    "%s %s %s",
        //                    trim(data_get($trade->ArbitrManager, 'LastName', '')),
        //                    trim(data_get($trade->ArbitrManager, 'FirstName', '')),
        //                    trim(data_get($trade->ArbitrManager, 'MiddleName', '')),
        //                ) 
        //                ?? sprintf(
        //                    "%s %s",
        //                    trim(data_get($trade->ArbitrManager, 'LastName', '')),
        //                    trim(data_get($trade->ArbitrManager, 'FirstName', ''))
        //                )
        //                ?? $trade->ArbitrManager['ShortName']
        //                ?? $trade->ArbitrManager['FullName']
        //                ?? null
        //        ])->save();
        //    }
        //});

        Schema::table('trades', function (Blueprint $table) {
            $table->index('arbitr_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trades', function (Blueprint $table) {
            $table->dropIndex('trades_arbitr_name_index');
            $table->dropColumn('arbitr_name');
        });
        Schema::table('trade_lots', function (Blueprint $table) {
            $table->dropIndex('trade_lots_for_trades_index');
            $table->dropIndex('trade_lots_for_trades_inn_index');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserOrgTrades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_org_trades', function (Blueprint $table) {
            $table->integer('org_profile_type');
            $table->string('org_profile_first_name')->nullable();
            $table->string('org_profile_last_name')->nullable();
            $table->string('org_profile_middle_name')->nullable();
            $table->string('org_profile_full_name')->nullable();

            $table->dropColumn(['reg_number', 'sro']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_org_trades', function (Blueprint $table) {
            $table->dropColumn([
                'org_profile_type', 'org_profile_first_name', 'org_profile_last_name',
                'org_profile_middle_name', 'org_profile_full_name']);

            $table->string('sro', 15);
            $table->string('reg_number', 30);
        });
    }
}

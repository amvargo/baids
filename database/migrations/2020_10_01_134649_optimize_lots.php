<?php

use App\Models\TradeLot;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OptimizeLots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_lot_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('trade_lot_id')->unique();
            $table->text('PaymentInfo')->nullable();
            $table->text('SaleAgreement')->nullable();
            $table->text('PriceReduction')->nullable();
            $table->text('Concours')->nullable();
        });

        $output = new \Symfony\Component\Console\Output\ConsoleOutput();
        $output->writeln('Updating trade_lot_info table... Please wait');

        DB::table('trade_lots')->orderBy('id')->chunk(100, function ($lots) {
            foreach ($lots as $lot) {
                DB::table('trade_lot_info')->insert([
                    'trade_lot_id' => $lot->id,
                    'PaymentInfo' => $lot->PaymentInfo,
                    'SaleAgreement' => $lot->SaleAgreement,
                    'PriceReduction' => $lot->PriceReduction,
                    'Concours' => $lot->Concours,
                ]);
            }
        });

        Schema::table('trade_lots', function (Blueprint $table) {
            $table->dropColumn('PaymentInfo');
            $table->dropColumn('SaleAgreement');
            $table->dropColumn('PriceReduction');
            $table->dropColumn('Concours');
            $table->string('place_name')->nullable();
            $table->string('debtor_name')->nullable();
            $table->string('organizer_name')->nullable();
        });

        $output->writeln('Updating trade_lots... Please wait');
        TradeLot::orderBy('id')->chunk(100, function ($lots) {
            foreach ($lots as $lot) {
                $lot->fill([
                    'place_name' => $lot->trade->place['ShortName'] ?? null,
                    'debtor_name' => $lot->trade->DebtorType === 'DebtorPerson'
                        ? sprintf(
                            "%s %s %s",
                            $lot->trade->DebtorData['FirstName'],
                            $lot->trade->DebtorData['LastName'],
                            $lot->trade->DebtorData['MiddleName'] ?? null
                        )
                        : $lot->trade->DebtorData['ShortName'] ?? null,
                    'organizer_name' => $lot->trade->TradeOrganizer['ShortName']
                        ?? sprintf(
                            "%s %s %s",
                            data_get($lot->trade->TradeOrganizer, 'FirstName', ''),
                            data_get($lot->trade->TradeOrganizer, 'LastName', ''),
                            data_get($lot->trade->TradeOrganizer, 'MiddleName', ''),
                        )
                        ?? null
                ])->save();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trade_lots', function (Blueprint $table) {
            $table->dropColumn('place_name');
            $table->dropColumn('debtor_name');
            $table->dropColumn('organizer_name');
            $table->text('PaymentInfo')->nullable();
            $table->text('SaleAgreement')->nullable();
            $table->text('PriceReduction')->nullable();
            $table->text('Concours')->nullable();
        });

        $output = new \Symfony\Component\Console\Output\ConsoleOutput();
        $output->writeln('Updating trade_lots table... Please wait');

        DB::table('trade_lot_info')->orderBy('id')->chunk(100, function ($items) {
            foreach ($items as $item) {
                DB::table('trade_lots')->update([
                    'PaymentInfo' => $item->PaymentInfo,
                    'SaleAgreement' => $item->SaleAgreement,
                    'PriceReduction' => $item->PriceReduction,
                ]);
            }
        });

        Schema::drop('trade_lot_info');
    }
}

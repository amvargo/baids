<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalog extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('fr_messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('MessageGUID', 32);
            $table->bigInteger('BankruptId', false, true)->index();
            $table->string('Number', 30)->index();
            $table->string('CaseNumber', 60)->index()->nullable();
            $table->dateTime('PublishDate');

            // PublisherInfo
            $table->string('PublisherType', 30)->nullable();
            $table->unsignedBigInteger('PublisherID')->nullable()->index();
            $table->text('PublisherInfo')->nullable();

            // MessageInfo
            $table->string('MessageType', 64);
            $table->longText('MessageInfo')->nullable();
            //$table->integer('MeetingWorker', 15);

            // BankruptInfo
            $table->string('BankruptType', 30)->nullable();
            $table->string('BankruptCategory', 50)->nullable();
            $table->unsignedBigInteger('BankruptInfoID')->nullable()->index();
            $table->text('BankruptInfo')->nullable();
            $table->text('FileInfoList')->nullable();
            $table->text('MessageURLList')->nullable();
            $table->timestamps();
        });
        Schema::create('fr_arbitr_manager', function (Blueprint $table) {
            $table->bigInteger('ArbitrManagerID', false, true)->unique()->primary();
            $table->string('RegistryNumber', 30)->nullable();
            $table->string('FirstName', 50);
            $table->string('MiddleName', 50)->nullable();
            $table->string('LastName', 50);
            $table->string('RegNum', 30)->nullable();
            $table->string('Region', 100)->nullable();
            $table->string('INN', 12)->nullable();
            $table->string('OGRN', 15)->nullable();
            $table->string('OGRNIP', 15)->nullable();
            $table->string('SNILS', 11)->nullable();
            $table->string('SRORegNum', 30)->nullable();
            $table->timestamp('DateReg')->nullable();
            $table->timestamp('SRORegDate')->nullable();
            $table->timestamp('DateLastModif')->nullable();
            $table->timestamp('DateDelete')->nullable();
            $table->string('CorrespondenceAddress', 300)->nullable();
            $table->text('Sro')->nullable();
            $table->timestamps();
        });
        Schema::create('fr_debtor_person', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('BankruptId', false, true)->index()->nullable();
            $table->string('Category', 100)->nullable();
            $table->string('CategoryCode', 50)->nullable();
            $table->string('Region', 100)->nullable();
            $table->string('INN', 12)->index()->nullable();
            $table->date('Birthdate')->nullable();
            $table->string('Birthplace', 300)->nullable();
            $table->timestamp('DateLastModif')->nullable();
            $table->string('SNILS', 11)->nullable();
            $table->string('LastName', 50);
            $table->string('FirstName', 50);
            $table->string('MiddleName', 50)->nullable();
            $table->string('Address', 300)->nullable();
            $table->string('OGRNIP', 15)->nullable();
            // NameHistory
            $table->timestamp('LastMessageDate')->nullable();
            $table->timestamp('LastReportDate')->nullable();
            // LegalCaseList
            $table->timestamps();
        });
        Schema::create('fr_debtor_company', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('BankruptId', false, true)->index()->nullable();
            $table->string('Category', 100)->nullable();
            $table->string('CategoryCode', 50)->nullable();
            $table->string('Region', 100)->nullable();
            $table->string('INN', 10)->index();
            $table->string('FullName', 1024);
            $table->string('ShortName', 512);
            $table->string('OGRN', 13)->nullable();
            $table->string('LegalAddress', 300)->nullable();
            $table->timestamp('LastReportDate')->nullable();
            $table->timestamp('LastMessageDate')->nullable();
            // LegalCaseList
            $table->timestamp('DateLastModif')->nullable();
            $table->timestamps();
        });
        Schema::create('fr_foreign_system', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('Name', 256);
            $table->timestamps();
        });
        Schema::create('fr_organizations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('InsolventCategoryName', 256);
            $table->string('INN', 100)->nullable();
            $table->string('FullName', 1024);
            $table->string('ShortName', 512);
            $table->string('PostAddress', 300);
            $table->string('LegalAddress', 300);
            $table->string('OGRN', 13);
            $table->string('OKPO', 8)->nullable();
            $table->timestamps();
        });
        Schema::create('fr_persons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('InsolventCategoryName', 100);
            $table->string('FirstName', 50);
            $table->string('MiddleName', 50);
            $table->string('LastName', 50);
            $table->string('OGRNIP', 15)->nullable();
            $table->string('Address', 300);
            $table->string('INN', 12)->nullable();
            $table->string('SNILS', 11)->nullable();
            $table->date('Birthdate')->nullable();
            $table->string('Birthplace', 300)->nullable();
            // NameHistory
            $table->timestamps();
        });
        Schema::create('fr_company_trade_organizer', function (Blueprint $table) {
            $table->string('INN', 10)->primary()->unique();
            $table->string('FullName', 1024);
            $table->string('ShortName', 512);
            $table->string('OGRN', 13);
            $table->timestamp('DateLastModif');
            $table->timestamps();
        });

        Schema::create('trades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('TradePlaceINN', 16)->index()->nullable();
            $table->string('TradeId', 100)->index();
            $table->string('status', 32)->index()->nullable();
            $table->dateTime('EventTime')->index()->nullable();
            $table->string('IDEFRSB', 30)->index()->nullable();
            $table->string('DebtorType', 30)->index()->nullable();
            $table->string('DebtorINN', 12)->index()->nullable();
            $table->text('DebtorData')->nullable();
            $table->string('CaseNumber', 60)->index()->nullable();
            $table->text('ArbitrManager')->nullable();
            $table->text('CompanyBankrCommis')->nullable();
            $table->string('TradeOrganizerType', 30)->index()->nullable();
            $table->text('TradeOrganizer')->nullable();
            $table->string('AuctionType', 30)->nullable();
            $table->string('FormPrice', 30)->nullable();
            $table->boolean('ISRepeat')->default(false);
            $table->date('DatePublishSMI')->nullable();
            $table->date('DatePublishEFIR')->nullable();
            $table->dateTime('TimeBegin')->nullable();
            $table->dateTime('TimeEnd')->nullable();
            $table->dateTime('TimeResult')->nullable();
            $table->dateTime('ApplicationTimeBegin')->nullable();
            $table->dateTime('ApplicationTimeEnd')->nullable();
            $table->date('DateBegin')->nullable()->comment('Дата начала представления заявок');
            $table->date('DateRenewal')->nullable();
            $table->text('ApplicationRules')->nullable();
            $table->text('FailureInformation')->nullable();
            $table->timestamps();

            //$table->unique(['TradePlaceINN', 'TradeId']);
        });
        Schema::create('trade_legal_cases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('CaseNumber', 60)->index();
            $table->string('TradeId', 100)->index();
            $table->string('CourtName', 300)->nullable();
            $table->string('Base', 512)->nullable();
            $table->timestamps();
        });
        Schema::create('trade_lots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('TradePlaceINN', 16)->index()->nullable();
            $table->string('TradeId', 100)->index();
            $table->string('LotNumber', 100)->index();
            $table->string('status', 32)->index()->nullable();
            $table->string('status_log', 512)->nullable();
            //$table->integer('LotNumber')->index();
            $table->double('StartPrice', 15, 2)->nullable();
            $table->double('StepPrice', 16, 2)->nullable();
            $table->double('NewPrice', 15, 2)->nullable();
            $table->double('StepPricePercent', 15, 2)->nullable();
            $table->double('Advance', 15, 2)->nullable();
            $table->double('AdvancePercent', 15, 2)->nullable();
            $table->text('SuccessTradeResult')->nullable();
            $table->text('FailureTradeResult')->nullable();
            $table->mediumText('TradeObjectHtml')->nullable();
            $table->text('PriceReduction')->nullable();
            $table->text('Concours')->nullable();
            $table->text('PaymentInfo')->nullable();
            $table->text('SaleAgreement')->nullable();
            $table->string('Classification', 2000)->nullable();
            $table->text('Participants')->nullable();
            $table->longText('ParticipantsArr')->nullable();
            $table->boolean('BiddingInProcess')->default(false);
            $table->boolean('BiddingPaused')->default(false);
            $table->smallInteger('EntryCount')->nullable();
            $table->smallInteger('AcceptCount')->nullable();
            $table->text('ApplicationList')->nullable();
            $table->text('BiddingFailReason')->nullable();
            $table->text('BiddingCancelReason')->nullable();
            $table->timestamps();

            $table->unique(['TradePlaceINN', 'TradeId', 'LotNumber']);
        });
        Schema::create('trade_lot_sales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('trade_lot_id')->index();
            $table->string('TradePlaceINN', 16)->index();
            $table->string('TradeId', 100)->index();
            $table->string('LotNumber', 100)->index();
            $table->date('DateContract')->nullable();
            $table->double('Price', 15, 2);
            $table->string('ContractNumber', 50)->nullable();
            $table->text('AdditionalInfo')->nullable();
            $table->text('ContractParticipantList');

            $table->timestamps();

            $table->unique(['TradePlaceINN', 'TradeId', 'LotNumber']);
        });
        Schema::create('trade_lot_classificators', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 32)->index()->unique();
            $table->string('text', 1024);
            $table->timestamps();
        });
        Schema::create('trade_lot_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parent_id')->index()->nullable();
            $table->smallInteger('index')->default(50);
            $table->string('slug', 64)->index()->unique();
            $table->string('title', 512);
            //$table->string('classificators', 1024)->nullable();
            $table->boolean('published')->default(true)->index();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('trade_lot_category_trade_lot_classificator', function (Blueprint $table) {
            $table->bigInteger('trade_lot_category_id');
            $table->bigInteger('trade_lot_classificator_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fr_messages');
        Schema::dropIfExists('fr_arbitr_manager');
        Schema::dropIfExists('fr_debtor_person');
        Schema::dropIfExists('fr_debtor_company');
        Schema::dropIfExists('fr_foreign_system');
        Schema::dropIfExists('fr_organizations');
        Schema::dropIfExists('fr_persons');
        Schema::dropIfExists('fr_company_trade_organizer');

        // todo: temporary
        /*Schema::dropIfExists('fr_trade_message');
        Schema::dropIfExists('fr_trade_message_lot_list');
        Schema::dropIfExists('fr_trade_message_lot_list_attach');*/

        Schema::dropIfExists('trades');
        Schema::dropIfExists('trade_legal_cases');
        Schema::dropIfExists('trade_lots');
        Schema::dropIfExists('trade_lot_sales');
        Schema::dropIfExists('trade_lot_classificators');
        Schema::dropIfExists('trade_lot_category_trade_lot_classificator');
        Schema::dropIfExists('trade_lot_categories');
    }
}

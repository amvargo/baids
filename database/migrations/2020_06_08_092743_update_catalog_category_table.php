<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCatalogCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trade_lots', function (Blueprint $table) {
            $table->bigInteger('updated_user_id')->nullable();
        });

        Schema::table('trade_lot_categories', function (Blueprint $table) {
            $table->string('lot_image', 128)->nullable();
            $table->string('image', 128)->nullable();
            $table->string('options', 2000)->nullable();
        });

        Schema::create('trade_lot_category_regulars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('trade_lot_category_id')->index();
            $table->smallInteger('sort')->default(100);
            $table->string('regex', 1024);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trade_lots', function (Blueprint $table) {
            $table->dropColumn(['updated_user_id']);
        });
        Schema::table('trade_lot_categories', function (Blueprint $table) {
            $table->dropColumn(['lot_image', 'image', 'options']);
        });
        Schema::dropIfExists('trade_lot_category_regulars');
    }
}

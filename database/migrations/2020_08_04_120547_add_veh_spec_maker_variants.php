<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVehSpecMakerVariants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehicle_special_makers', function (Blueprint $table) {
            $table->timestamps();
        });
        Schema::table('vehicle_special_models', function (Blueprint $table) {
            $table->timestamps();
        });
        Schema::create('vehicle_special_maker_variants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('vehicle_special_maker_id')->index('vehspecmakvar1');
            $table->string('text', 72)->unique();
        });
        Schema::create('vehicle_special_model_variants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('vehicle_special_model_id')->index('vehspecmodvar1');
            $table->string('text', 100)->index('vehspecmodvar2');
            $table->unique(['vehicle_special_model_id', 'text'], 'vehspecmodvar3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicle_special_makers', function (Blueprint $table) {
            $table->dropColumn(['created_at', 'updated_at']);
        });
        Schema::table('vehicle_special_models', function (Blueprint $table) {
            $table->dropColumn(['created_at', 'updated_at']);
        });
        Schema::dropIfExists('vehicle_special_maker_variants');
        Schema::dropIfExists('vehicle_special_model_variants');
    }
}

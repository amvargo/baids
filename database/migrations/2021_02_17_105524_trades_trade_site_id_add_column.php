<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TradesTradeSiteIdAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trades', function (Blueprint $table) {
            $table->unsignedBigInteger('trade_site_id')->nullable();
            $table->foreign('trade_site_id')
                ->references('id')
                ->on('trade_sites')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trades', function (Blueprint $table) {
            $table->dropForeign(['trade_site_id']);
            $table->dropColumn('trade_site_id');
        });
    }
}

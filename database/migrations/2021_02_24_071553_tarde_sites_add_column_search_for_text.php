<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TardeSitesAddColumnSearchForText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trade_sites', function (Blueprint $table) {
            $table->boolean('search_for_text')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trade_sites', function (Blueprint $table) {
            $table->dropColumn('search_for_text');
        });
    }
}

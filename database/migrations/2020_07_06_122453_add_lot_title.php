<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLotTitle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trade_lots', function (Blueprint $table) {
            $table->string('LotTitle', 250)->nullable();
        });
        Schema::table('trades', function (Blueprint $table) {
            $table->bigInteger('user_id')->index();
        });
        Schema::table('trade_lot_params', function (Blueprint $table) {
            $table->string('validator', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('trade_lots', function (Blueprint $table) {
            $table->dropColumn('LotTitle');
        });
        Schema::create('trades', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
        Schema::create('trade_lot_params', function (Blueprint $table) {
            $table->dropColumn('validator');
        });
    }
}

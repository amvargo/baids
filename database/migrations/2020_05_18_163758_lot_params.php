<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LotParams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trade_lot_params', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 64)->index();
            $table->string('type', 32);
            $table->string('title', 128);
            $table->boolean('published')->default(true);
            $table->smallInteger('sort')->default(100);
            $table->string('options', 2000)->nullable();
        });

        Schema::create('trade_lot_param_values', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('trade_lot_id')->index();
            $table->bigInteger('trade_lot_param_id')->index();
            $table->string('value', 1200)->nullable();
            $table->unsignedSmallInteger('mark')->nullable();
        });

        Schema::create('trade_lot_trade_lot_category', function (Blueprint $table) {
            $table->bigInteger('trade_lot_id')->index();
            $table->bigInteger('trade_lot_category_id')->index();
            // todo: develop
            //$table->unique(['trade_lot_id', 'trade_lot_category_id']);
        });

        Schema::create('trade_lot_category_trade_lot_param', function (Blueprint $table) {
            $table->bigInteger('trade_lot_category_id')->index();
            $table->bigInteger('trade_lot_param_id')->index();
            // todo: develop
            //$table->unique(['trade_lot_category_id', 'trade_lot_param_id']);
        });

        Schema::create('trade_lot_param_regulars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('trade_lot_param_id')->index();
            $table->smallInteger('sort')->default(100);
            $table->string('regex', 1024);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trade_lot_params');
        Schema::dropIfExists('trade_lot_param_values');
        Schema::dropIfExists('trade_lot_trade_lot_category');
        Schema::dropIfExists('trade_lot_category_trade_lot_param');
        Schema::dropIfExists('trade_lot_param_regulars');
    }
}

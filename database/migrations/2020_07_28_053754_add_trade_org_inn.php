<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTradeOrgInn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('trades', function (Blueprint $table) {
            $table->string('TradeOrganizerINN', 20)->after('TradeOrganizerType')->nullable()->index();
        });

        $output = new \Symfony\Component\Console\Output\ConsoleOutput();
        $output->writeln('Updating tables... Please wait');

        DB::table('trades')
            ->select(['id', 'TradeId', 'TradeOrganizer'])
            ->whereNotNull('TradeOrganizer')
            ->chunkById(100, function ($trades) {
                foreach ($trades as $trade) {
                    $obj = @json_decode($trade->TradeOrganizer, true);
                    if ($obj && isset($obj['INN'])) {
                        DB::table('trades')
                            ->where('id', $trade->id)
                            ->update(['TradeOrganizerINN' => $obj['INN']]);
                    }
                }
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trades', function (Blueprint $table) {
            $table->dropColumn('TradeOrganizerINN');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConsultingMessagesUserAnswer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consulting_messages', function (Blueprint $table) {
            $table->text('answer')->nullable()->default(null);
            $table->unsignedBigInteger('user_id')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consulting_messages', function (Blueprint $table) {
            $table->dropColumn(['answer', 'user_id']);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_models', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 64);
            $table->string('code', 64);
            $table->unsignedBigInteger('vehicle_maker_id')->index();
            $table->timestamps();
            //$table->foreign('make_id')->references('id')->on('vehicle_makes');
        });
        Schema::create('vehicle_model_variants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('vehicle_model_id')->index();
            $table->string('text', 100)->index();
            $table->unique(['vehicle_model_id', 'text']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::table('vehicle_models', function (Blueprint $table) {
            $table->dropForeign('vehicle_models_make_id_foreign');
        });*/
        Schema::dropIfExists('vehicle_models');
        Schema::dropIfExists('vehicle_model_variants');
    }
}

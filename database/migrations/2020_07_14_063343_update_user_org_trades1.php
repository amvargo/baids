<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserOrgTrades1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_org_trades', function (Blueprint $table) {
            $table->dropColumn('confirmed');
            $table->tinyInteger('status')->default(1);
            $table->boolean('exist')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_org_trades', function (Blueprint $table) {
            $table->tinyInteger('confirmed')->default(0);
            $table->dropColumn('status');
            $table->dropColumn('exist');
        });
    }
}

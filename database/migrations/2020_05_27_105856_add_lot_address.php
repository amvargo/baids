<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLotAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trade_lots', function (Blueprint $table) {
            $table->string('postal_code', 6)->nullable();
            $table->string('federal_district', 64)->nullable()->index();
            $table->string('region_with_type', 102)->nullable();
            $table->string('area_with_type', 124)->nullable();
            $table->string('city_with_type', 72)->nullable();
            $table->text('dadata_suggest')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trade_lots', function (Blueprint $table) {
            $table->dropColumn([
                'postal_code', 'federal_district', 'region_with_type', 'area_with_type', 'city_with_type', 'dadata_suggest'
            ]);
        });
    }
}

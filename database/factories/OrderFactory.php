<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    $workers = \App\Models\User::where('roles', 'like', '%' . \App\Role\UserRole::ROLE_STAFF . '%')->get('id');
    return [
        'created_at' => $faker->dateTimeInInterval('-30 days'),
        'service_id' => rand(1, 10),
        'user_id' => rand(20, 30),
        'staff_id' => $workers[rand(0, count($workers) - 1)],
        'payment' => false,
    ];
});

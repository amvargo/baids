<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    $user_id = rand(0, 100) < 10 ? rand(1, 20) : null;
    return ([
        'created_at' => $faker->dateTimeInInterval('-120 days'),
        'author' => $faker->name,
        'email' => $faker->freeEmail,
        //'question' => $faker->sentences(3, true),
        'question' => $faker->realText(512),
        //'answer' => $faker->sentences(5, true),
        'answer' => $faker->realText(2015),
        'priority' => $faker->numberBetween(10, 100000),
        'published' => rand(0, 100) > 10,
        'user_id' => $user_id,
    ]);
});

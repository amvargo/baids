<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Service;
use Faker\Generator as Faker;

$factory->define(Service::class, function (Faker $faker) {
    return [
        'created_at' => $faker->dateTimeInInterval('-120 days'),
        'title' => $faker->realText(60),
        'annotation' => $faker->realText(512),
        'description' => $faker->realText(1500),
        'image' => null,
        'priority' => $faker->numberBetween(10, 100000),
        'published' => rand(0, 100) > 10,
    ];
});

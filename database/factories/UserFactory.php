<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Panel\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'created_at' => $faker->dateTimeInInterval('-4 month'),
        'name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'middle_name' => $faker->middleName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'avatar' => $faker->imageUrl(340, 340, 'cats'),
        //'avatar' => $faker->image('public/storage/', 340, 340),
        /*'phone' => $faker->phoneNumber,
        'appointment' => $faker->randomElement([
            'Должность 1', 'Должность 2', 'Должность 3'
        ]),
        'department' => $faker->randomElement([
            'Отдел 1', 'Отдел 2', 'Отдел 3'
        ]),*/
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ConsultingCategory;
use Faker\Generator as Faker;

$factory->define(ConsultingCategory::class, function (Faker $faker) {
    return [
        'created_at' => $faker->dateTimeInInterval('-120 days'),
        'title' => $faker->realText(52),
    ];
});

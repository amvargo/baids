<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ConsultingMessage;
use Faker\Generator as Faker;

$factory->define(ConsultingMessage::class, function (Faker $faker) {
    $category_id = rand(0, 12);
    return [
        'created_at' => $faker->dateTimeInInterval('-120 days'),
        'category_id' => $category_id === 0 ? null : $category_id,
        'full_name' => $faker->name,
        'email' => $faker->freeEmail,
        'phone' => $faker->phoneNumber,
        'message' => $faker->realText(700),
    ];
});

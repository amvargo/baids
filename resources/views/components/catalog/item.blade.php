<div class="lot-preview rounded shadow p-3">
    <div class="media align-items-stretch m-1">
        
        @if(auth()->user() && auth()->user()->isTariffActive())
        <images-preview
            class="mr-4 lot-preview-img"
            :width="200"
            :height="200"
            href="{{ route('site.catalog.lot.show', $lot->id) }}"
            :images='@json($lot->firstImagesArray(5))'
            @if($lot->getOptionsForMap())
            :map='@json($lot->getOptionsForMap())'
            @endif
            no-img-url="{{ $lot->last_category_lot_img }}"
        ></images-preview>
        @else
        <images-preview
            class="mr-4 lot-preview-img"
            :width="200"
            :height="200"
            href="{{ route('site.catalog.lot.show', $lot->id) }}"
            no-img-url="{{ $lot->last_category_lot_img }}"
        ></images-preview>
        @endif

        <div class="lot-preview-body">

            <div class="mb-3">
                <!--@if($lot->updated_user_id)
                    <span class="float-right text-info">[Edited!]</span>
                @endif-->
                <span class="mr-3">
                <span>№{{ $lot->id }}</span>
                    @auth @if(auth()->user()->isAdmin())
                        <a
                            class="text-primary"
                            target="_blank"
                            href="{{ url("panel/catalog-lot-{$lot->id}") }}">️
                                <svg class="bi bi-box-arrow-up-right" width="1em" height="1em" viewBox="0 0 16 16"
                                     fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd"
                                          d="M1.5 13A1.5 1.5 0 0 0 3 14.5h8a1.5 1.5 0 0 0 1.5-1.5V9a.5.5 0 0 0-1 0v4a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 0 0-1H3A1.5 1.5 0 0 0 1.5 5v8zm7-11a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 .5.5v5a.5.5 0 0 1-1 0V2.5H9a.5.5 0 0 1-.5-.5z"/>
                                    <path fill-rule="evenodd"
                                          d="M14.354 1.646a.5.5 0 0 1 0 .708l-8 8a.5.5 0 0 1-.708-.708l8-8a.5.5 0 0 1 .708 0z"/>
                                </svg>
                        </a>
                    @endif @endif
                </span>

                {{--<a href="#" class="mr-3 text-dark">Тип</a>--}}

                @if($lot->first_category)
                    @php($count = $lot->categories()->count())
                    @if($count === 1)
                        <a href="{{ $lot->first_category->url }}"
                           class="mr-3 text-dark d-none d-sm-inline">{{ $lot->first_category->title }}</a>
                    @else
                        @php($html = '<div>')
                        @foreach($lot->categories as $category)
                            @php($html .= "<a href='{$category->url}'>• {$category->title}</a></br>")
                        @endforeach
                        @php($html .= '</div>')
                        <span class="d-inline-block mr-3 d-none d-sm-inline"
                              data-toggle="tooltip"
                              data-html="true"
                              data-delay='{ "show": 100, "hide": 800 }'
                              title="{{$html}}">
                            <span>{{ trans_choice('messages.in_categories_count', $count) }}</span>
                        </span>
                    @endif
                @endif

                <a href="#" class="lot-item-place mr-3 text-dark">{{ $lot->region_with_type ?: '(не определено)' }}</a>
            </div>

            <a href="{{ route('site.catalog.lot.show', $lot->id) }}">
                <h3 class="h3 text-dark lot-text-nowrap lot-title-nowrap text-break-all">{{ strip_tags($lot->getTitleAttribute(100)) }}</h3>
            </a>
            <p class="m-0 lot-text-nowrap lot-desc-nowrap text-break-all">{{ \Illuminate\Support\Str::limit($lot->description, 200) }}</p>

            @if(auth()->user() && auth()->user()->id === 1)
                <div class="dev-section mb-4" style="font-size: .7em;">
                    TradeId: {{ $lot->trade->TradeId }} <br>
                    Status: {{ $lot->status }}<br>
                    CaseNumber: {{ $lot->trade->CaseNumber }} <br>
                    --<br>
                    @foreach($lot->params as $param)
                        {{ $param->title }}: {{ $param->value }}<br>
                    @endforeach
                </div>
            @endif

            <div class="float-right">
                <small class="text-muted">{{ $lot->publisher }}</small>
            </div>
            <small class="text-muted"></small>

            <div class="lot-preview-bottom-row d-none d-sm-block position-absolute">
                <div class="lot-preview-price">
                    <b>@money($lot->current_price) ₽</b>

                    @switch($lot->trade->AuctionType)
                        @case('OpenAuction')
                        <img
                            data-toggle="tooltip" data-placement="top" title="Аукцион"
                            class="lot-preview-price__type"
                            src="{{ asset('img/icons/16px/circle-arr-up.svg') }}"
                            alt="Аукцион">
                        @break
                        @case('PublicOffer')
                        <img
                            data-toggle="tooltip" data-placement="top" title="Публичное предложение"
                            class="lot-preview-price__type"
                            src="{{ asset('img/icons/16px/circle-arr-down.svg') }}"
                            alt="Публичное предложение">
                        @break
                    @endswitch
                </div>

                <div class="float-right">
                    
                @if(auth()->user() && auth()->user()->isTariffActive())
                    <a href="javascript:" data-id="{{ $lot->id }}"
                       class="j-favorite-link lot-preview-link @if($lot->in_favorite) in-favorite @endif">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M12.0003 5.1285C10.266 3.80563 8.37235 3.30728 6.65968 3.56553C4.71813 3.8583 3.10627 5.11707 2.39279 7.01392C0.962262 10.8171 3.24623 16.4602 11.4522 21.8365C11.7851 22.0545 12.2155 22.0545 12.5483 21.8365C20.7543 16.4602 23.0383 10.8171 21.6077 7.01392C20.8943 5.11707 19.2824 3.8583 17.3408 3.56553C15.6282 3.30728 13.7345 3.80563 12.0003 5.1285ZM12.0003 19.798C19.5266 14.6868 20.617 10.0608 19.7358 7.71804C19.2829 6.51404 18.2832 5.73023 17.0426 5.54318C15.7982 5.35553 14.215 5.7622 12.6747 7.16931C12.2928 7.51825 11.7077 7.51825 11.3258 7.16931C9.78553 5.7622 8.20231 5.35553 6.95789 5.54318C5.71736 5.73023 4.71761 6.51404 4.26474 7.71804C3.38352 10.0608 4.4739 14.6868 12.0003 19.798Z"/>
                        </svg>
                        <span class="d-none d-lg-inline">В избранное</span>
                    </a>
                @endif
                    <small style="margin-top:15px;" class="align-items-center d-flex mr-3 lot-social-share">
                        <div class="ya-share2" 
                            data-curtain 
                            data-size="s" 
                            data-shape="round" 
                            data-limit="0" 
                            data-copy="first"
                            data-more-button-type="short" 
                            data-title="{{ $lot->title }}"
                            data-description="{{ mb_substr($lot->description, 0, 100) }}"
                            data-image="{{ $lot->last_category_lot_img }}"
                            data-url="{{ Request::url() }}"
                            data-services="vkontakte,facebook,odnoklassniki,viber,whatsapp">
                        </div>
                        <span>Поделиться</span>
                    </small>
                </div>
            </div>

        </div>
    </div>
    <div class="lot-preview-bottom-row d-sm-none m-1 mt-3">
        <div class="lot-preview-price">
            <b>@money($lot->current_price) ₽</b>

            @switch($lot->trade->AuctionType)
                @case('OpenAuction')
                <img
                    data-toggle="tooltip" data-placement="top" title="Аукцион"
                    class="lot-preview-price__type"
                    src="{{ asset('img/icons/16px/circle-arr-up.svg') }}"
                    alt="Аукцион">
                @break
                @case('PublicOffer')
                <img
                    data-toggle="tooltip" data-placement="top" title="Публичное предложение"
                    class="lot-preview-price__type"
                    src="{{ asset('img/icons/16px/circle-arr-down.svg') }}"
                    alt="Публичное предложение">
                @break
                @default
                {{-- текст мешает корректному отображению содержимого --}}
                <code>{{--{{ $lot->trade->AuctionType }}--}}</code> 
                @break
            @endswitch
        </div>

        <div class="float-right">

            @if(auth()->user() && auth()->user()->isTariffActive())
              <a href="javascript:" data-id="{{ $lot->id }}"
                class="j-favorite-link lot-preview-link @if($lot->in_favorite) in-favorite @endif">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M12.0003 5.1285C10.266 3.80563 8.37235 3.30728 6.65968 3.56553C4.71813 3.8583 3.10627 5.11707 2.39279 7.01392C0.962262 10.8171 3.24623 16.4602 11.4522 21.8365C11.7851 22.0545 12.2155 22.0545 12.5483 21.8365C20.7543 16.4602 23.0383 10.8171 21.6077 7.01392C20.8943 5.11707 19.2824 3.8583 17.3408 3.56553C15.6282 3.30728 13.7345 3.80563 12.0003 5.1285ZM12.0003 19.798C19.5266 14.6868 20.617 10.0608 19.7358 7.71804C19.2829 6.51404 18.2832 5.73023 17.0426 5.54318C15.7982 5.35553 14.215 5.7622 12.6747 7.16931C12.2928 7.51825 11.7077 7.51825 11.3258 7.16931C9.78553 5.7622 8.20231 5.35553 6.95789 5.54318C5.71736 5.73023 4.71761 6.51404 4.26474 7.71804C3.38352 10.0608 4.4739 14.6868 12.0003 19.798Z"/>
                    </svg>
                    <span class="d-none d-lg-inline">В избранное</span>
                </a>
            @endif
            <small style="margin-top:15px;" class="align-items-center d-flex mr-3 lot-social-share">
                <div class="ya-share2" 
                    data-curtain 
                    data-size="s" 
                    data-shape="round" 
                    data-limit="0" 
                    data-copy="first"
                    data-more-button-type="short" 
                    data-title="{{ $lot->title }}"
                    data-description="{{ mb_substr($lot->description, 0, 100) }}"
                    data-image="{{ $lot->last_category_lot_img }}"
                    data-url="{{ Request::url() }}"
                    data-services="vkontakte,facebook,odnoklassniki,viber,whatsapp">
                </div>
                <span>Поделиться</span>
            </small>
        </div>
    </div>


</div>

<div class="lot-preview lot-preview-item-small rounded shadow p-3" style="min-width: 500px; max-width: 554px">
    <div class="media align-items-stretch m-1">

        @if(auth()->user() && auth()->user()->isTariffActive())
        <images-preview
            class="mr-4 lot-preview-img"
            :width="200"
            :height="200"
            href="{{ route('site.catalog.lot.show', $lot->id) }}"
            :images='@json($lot->firstImagesArray(5))'
            @if($lot->getOptionsForMap())
            :map='@json($lot->getOptionsForMap())'
            @endif
            no-img-url="{{ $lot->last_category_lot_img }}"
        ></images-preview>
        @else
        <images-preview
            class="mr-4 lot-preview-img"
            :width="200"
            :height="200"
            href="{{ route('site.catalog.lot.show', $lot->id) }}"
            @if($lot->getOptionsForMap())
            :map='@json($lot->getOptionsForMap())'
            @endif
            no-img-url="{{ $lot->last_category_lot_img }}"
        ></images-preview>
        @endif
        
        <div class="lot-preview-body">

            <div class="mb-3">
                @if($lot->updated_user_id)
                    <span class="float-right text-info">[Edited!]</span>
                @endif
                <span class="mr-3">№{{ $lot->id }}</span>

                <a href="#" class="mr-3 text-dark">{{ $lot->region_with_type ?: '(не определено)' }}</a>
            </div>

            <a href="{{ route('site.catalog.lot.show', $lot->id) }}">
                <h3 class="h3 text-dark lot-text-nowrap lot-title-nowrap text-break-all">{{ strip_tags($lot->getTitleAttribute(70)) }}</h3>
            </a>
            <p class="m-0 lot-text-nowrap lot-desc-nowrap text-break-all">{{ \Illuminate\Support\Str::limit($lot->description, 70) }}</p>

            <div class="float-right">
                <small class="text-muted">{{ $lot->publisher }}</small>
            </div>
            <small class="text-muted"></small>

            <div class="lot-preview-bottom-row d-none d-sm-block position-absolute">
                <div class="lot-preview-price">
                    <b>@money($lot->current_price) ₽</b>

                    @switch($lot->trade->AuctionType)
                        @case('OpenAuction')
                        <img
                            data-toggle="tooltip" data-placement="top" title="Аукцион"
                            class="lot-preview-price__type d-inline-block"
                            src="{{ asset('img/icons/16px/circle-arr-up.svg') }}"
                            alt="Аукцион">
                        @break
                        @case('PublicOffer')
                        <img
                            data-toggle="tooltip" data-placement="top" title="Публичное предложение"
                            class="lot-preview-price__type d-inline-block"
                            src="{{ asset('img/icons/16px/circle-arr-down.svg') }}"
                            alt="Публичное предложение">
                        @break
                        @default
                        {{-- поле мешает корректному отображению содержимого --}}
                        {{-- <code>{{ $lot->trade->AuctionType }}</code> --}}
                        @break
                    @endswitch
                </div>

                <div class="float-right">

                    <a href="javascript:" data-id="{{ $lot->id }}"
                       class="j-favorite-link lot-preview-link @if($lot->in_favorite) in-favorite @endif">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M12.0003 5.1285C10.266 3.80563 8.37235 3.30728 6.65968 3.56553C4.71813 3.8583 3.10627 5.11707 2.39279 7.01392C0.962262 10.8171 3.24623 16.4602 11.4522 21.8365C11.7851 22.0545 12.2155 22.0545 12.5483 21.8365C20.7543 16.4602 23.0383 10.8171 21.6077 7.01392C20.8943 5.11707 19.2824 3.8583 17.3408 3.56553C15.6282 3.30728 13.7345 3.80563 12.0003 5.1285ZM12.0003 19.798C19.5266 14.6868 20.617 10.0608 19.7358 7.71804C19.2829 6.51404 18.2832 5.73023 17.0426 5.54318C15.7982 5.35553 14.215 5.7622 12.6747 7.16931C12.2928 7.51825 11.7077 7.51825 11.3258 7.16931C9.78553 5.7622 8.20231 5.35553 6.95789 5.54318C5.71736 5.73023 4.71761 6.51404 4.26474 7.71804C3.38352 10.0608 4.4739 14.6868 12.0003 19.798Z"/>
                        </svg>
                    </a>
{{--                    <a href="javascript:" class="lot-preview-link">--}}
{{--                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                            <path fill-rule="evenodd" clip-rule="evenodd"--}}
{{--                                  d="M17.4546 6.5C18.3325 6.5 19 5.80418 19 5C19 4.19582 18.3325 3.5 17.4546 3.5C16.5766 3.5 15.9091 4.19582 15.9091 5C15.9091 5.80418 16.5766 6.5 17.4546 6.5ZM17.4546 8.5C19.4127 8.5 21 6.933 21 5C21 3.067 19.4127 1.5 17.4546 1.5C15.4965 1.5 13.9091 3.067 13.9091 5C13.9091 5.22237 13.9301 5.4399 13.9703 5.65075L7.39418 9.37811C6.76873 8.83169 5.94633 8.5 5.04545 8.5C3.08735 8.5 1.5 10.067 1.5 12C1.5 13.933 3.08735 15.5 5.04545 15.5C6.12344 15.5 7.08906 15.0251 7.73932 14.2756L14.1198 17.8086C13.9835 18.1805 13.9091 18.5817 13.9091 19C13.9091 20.933 15.4965 22.5 17.4546 22.5C19.4127 22.5 21 20.933 21 19C21 17.067 19.4127 15.5 17.4546 15.5C16.6594 15.5 15.9254 15.7584 15.334 16.1948L8.56249 12.4453C8.58124 12.2995 8.59091 12.1508 8.59091 12C8.59091 11.6781 8.5469 11.3664 8.46449 11.0704L14.8973 7.42425C15.5425 8.08731 16.4497 8.5 17.4546 8.5ZM6.59091 12C6.59091 12.8042 5.92339 13.5 5.04545 13.5C4.16752 13.5 3.5 12.8042 3.5 12C3.5 11.1958 4.16752 10.5 5.04545 10.5C5.92339 10.5 6.59091 11.1958 6.59091 12ZM19 19C19 19.8042 18.3325 20.5 17.4546 20.5C16.5766 20.5 15.9091 19.8042 15.9091 19C15.9091 18.1958 16.5766 17.5 17.4546 17.5C18.3325 17.5 19 18.1958 19 19Z"--}}
{{--                                  fill="#D6DEEB"/>--}}
{{--                        </svg>--}}
{{--                    </a>--}}
                </div>
            </div>

        </div>
    </div>
    <div class="lot-preview-bottom-row d-sm-none m-1 mt-3">
        <div class="lot-preview-price">
            <b>@money($lot->current_price) ₽</b>

            @switch($lot->trade->AuctionType)
                @case('OpenAuction')
                <img
                    data-toggle="tooltip" data-placement="top" title="Аукцион"
                    class="lot-preview-price__type d-inline-block"
                    src="{{ asset('img/icons/16px/circle-arr-up.svg') }}"
                    alt="Аукцион">
                @break
                @case('PublicOffer')
                <img
                    data-toggle="tooltip" data-placement="top" title="Публичное предложение"
                    class="lot-preview-price__type d-inline-block"
                    src="{{ asset('img/icons/16px/circle-arr-down.svg') }}"
                    alt="Публичное предложение">
                @break
                @default
                {{-- текст мешает корректному отображению содержимого --}}
                <code>{{--{{ $lot->trade->AuctionType }}--}}</code> 
                @break
            @endswitch
        </div>

        <div class="float-right">

            <a href="javascript:" data-id="{{ $lot->id }}"
               class="j-favorite-link lot-preview-link @if($lot->in_favorite) in-favorite @endif">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M12.0003 5.1285C10.266 3.80563 8.37235 3.30728 6.65968 3.56553C4.71813 3.8583 3.10627 5.11707 2.39279 7.01392C0.962262 10.8171 3.24623 16.4602 11.4522 21.8365C11.7851 22.0545 12.2155 22.0545 12.5483 21.8365C20.7543 16.4602 23.0383 10.8171 21.6077 7.01392C20.8943 5.11707 19.2824 3.8583 17.3408 3.56553C15.6282 3.30728 13.7345 3.80563 12.0003 5.1285ZM12.0003 19.798C19.5266 14.6868 20.617 10.0608 19.7358 7.71804C19.2829 6.51404 18.2832 5.73023 17.0426 5.54318C15.7982 5.35553 14.215 5.7622 12.6747 7.16931C12.2928 7.51825 11.7077 7.51825 11.3258 7.16931C9.78553 5.7622 8.20231 5.35553 6.95789 5.54318C5.71736 5.73023 4.71761 6.51404 4.26474 7.71804C3.38352 10.0608 4.4739 14.6868 12.0003 19.798Z"/>
                </svg>
            </a>
{{--            <a href="javascript:" class="lot-preview-link">--}}
{{--                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                    <path fill-rule="evenodd" clip-rule="evenodd"--}}
{{--                          d="M17.4546 6.5C18.3325 6.5 19 5.80418 19 5C19 4.19582 18.3325 3.5 17.4546 3.5C16.5766 3.5 15.9091 4.19582 15.9091 5C15.9091 5.80418 16.5766 6.5 17.4546 6.5ZM17.4546 8.5C19.4127 8.5 21 6.933 21 5C21 3.067 19.4127 1.5 17.4546 1.5C15.4965 1.5 13.9091 3.067 13.9091 5C13.9091 5.22237 13.9301 5.4399 13.9703 5.65075L7.39418 9.37811C6.76873 8.83169 5.94633 8.5 5.04545 8.5C3.08735 8.5 1.5 10.067 1.5 12C1.5 13.933 3.08735 15.5 5.04545 15.5C6.12344 15.5 7.08906 15.0251 7.73932 14.2756L14.1198 17.8086C13.9835 18.1805 13.9091 18.5817 13.9091 19C13.9091 20.933 15.4965 22.5 17.4546 22.5C19.4127 22.5 21 20.933 21 19C21 17.067 19.4127 15.5 17.4546 15.5C16.6594 15.5 15.9254 15.7584 15.334 16.1948L8.56249 12.4453C8.58124 12.2995 8.59091 12.1508 8.59091 12C8.59091 11.6781 8.5469 11.3664 8.46449 11.0704L14.8973 7.42425C15.5425 8.08731 16.4497 8.5 17.4546 8.5ZM6.59091 12C6.59091 12.8042 5.92339 13.5 5.04545 13.5C4.16752 13.5 3.5 12.8042 3.5 12C3.5 11.1958 4.16752 10.5 5.04545 10.5C5.92339 10.5 6.59091 11.1958 6.59091 12ZM19 19C19 19.8042 18.3325 20.5 17.4546 20.5C16.5766 20.5 15.9091 19.8042 15.9091 19C15.9091 18.1958 16.5766 17.5 17.4546 17.5C18.3325 17.5 19 18.1958 19 19Z"--}}
{{--                          fill="#D6DEEB"/>--}}
{{--                </svg>--}}
{{--            </a>--}}
        </div>
    </div>

</div>

<div class="catalog-filter mb-3">
    <catalog-filter  @isset($titles) v-bind:titles="{{ $titles }}" @endisset @isset($category) v-bind:category="{{ $category }}"  @endisset></catalog-filter>
    {{--<form class="row" action="{{ url()->current() }}">

        --}}{{-- price range --}}{{--
        <div class="mb-4 col-6 col-md-4 col-lg-12">
            <div class="h3 mb-2">Цена лота</div>
            <div class="row">
                <div class="col-6 pr-1">
                    <input
                        type="text"
                        name="filter[price][from]"
                        value="{{ request("filter.price.from") }}"
                        placeholder="От"
                        class="form-control input-price">
                </div>
                <div class="col-6 pl-1">
                    <input
                        type="text"
                        name="filter[price][to]"
                        value="{{ request("filter.price.to") }}"
                        placeholder="До"
                        class="form-control input-price">
                </div>
            </div>
        </div>

        --}}{{-- bidding type --}}{{--
        <div class="mb-4 col-6 col-md-4 col-lg-12">
            <div class="h3 mb-2">Тип торгов</div>
            <div class="form-group">
                @php($type_path = "filter.auction_type")
                @foreach(\App\Models\Trade::$auction_types_for_filter as $type => $name)
                    <div class="custom-control custom-checkbox">
                        <input
                            class="custom-control-input"
                            type="checkbox"
                            name="filter[auction_type][]"
                            value="{{ $type }}"
                            {{ in_array($type, request($type_path, [])) ? ' checked' : '' }}
                            id="type{{ $type }}">
                        <label class="custom-control-label" for="type{{ $type }}">
                            {{ $name }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>

        @if(isset($catalog_filter_params))
            @forelse($catalog_filter_params as $param)
                @component('components.catalog.filter-item', ['param'=>$param]) @endcomponent
            @empty
            @endforelse
        @endif

        --}}{{-- organization trader --}}{{--
        <div class="mb-4 col-6 col-md-4 col-lg-12">
            <div class="h3 mb-2">Огранизатор торгов</div>
            <form-select-ajax-search
                class="d-block"
                placeholder="Огранизатор торгов"
                name="filter[trade_organizer]"
                value="{{ request("filter.trade_organizer") }}"
                search-url="{{ url('/api/catalog/trade_organizer_search_list') }}"
            ></form-select-ajax-search>
        </div>

        --}}{{-- Фильтр по должнику в дебиторской задолжности (ID: 6) >>>> isset($catalog_category) && $catalog_category->id === 6 --}}{{--
        <div class="mb-4 col-6 col-md-4 col-lg-12">
            <div class="h3 mb-2">Должник</div>
            <form-select-ajax-search
                class="d-block"
                placeholder="Должник"
                name="filter[debtor]"
                value="{{ request("filter.debtor") }}"
                value-text="{{ request("filter.debtor_text") }}"
                search-url="{{ url('/api/catalog/debtor_search_list') }}"
            ></form-select-ajax-search>
        </div>

        <div class="mt-0 mb-2 mt-md-4 mt-lg-0 col-12 col-md-4 col-lg-12">
            <button type="submit" class="btn btn-primary btn-block">Применить</button>
        </div>
    </form>--}}
</div>

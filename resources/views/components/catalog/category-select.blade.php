@php
    $categories = \App\Models\TradeLotCategory::getGeneralCategoriesWithActive(99, true, false);
    //dump($categories->toArray());
    $categories->push([
        "parent_id" => null,
        "url" => route('site.catalog.without'),
        "title" => "Прочее",
        "published" => true,
        "created_at" => null,
        "updated_at" => "2020-07-08 07:06:27",
        "deleted_at" => null,
    ]);
@endphp
<div class="mb-4">
    <form-multi-select
        title="Категории лотов"
        show-search
        class="d-block"
        text-key="title"
        :items='{!! $categories->toJson() !!}'
        :on-change="goToCategoryPage"
    ></form-multi-select>
</div>

@php($disabled = !$param->values()->whereHas('lots', function ($query){ return $query->catalogLocationFilter(); })->count())

@if($items = $catalog_filter_param_values->get($param->id))
    @if(!$items->count())
        @php($disabled = true)
    @endif
@endif

<div class="mb-4 col-6 col-md-4 col-lg-12">
    <div class="h3 mb-2">
        {{ $param->title }}
        @auth @if(auth()->user()->isAdmin())
            <a
                class="text-primary float-right"
                target="_blank"
                title="Редактировать"
                href="{{ url("panel/catalog-param-{$param->id}") }}">
                <svg class="bi bi-box-arrow-up-right" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                          d="M1.5 13A1.5 1.5 0 0 0 3 14.5h8a1.5 1.5 0 0 0 1.5-1.5V9a.5.5 0 0 0-1 0v4a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 0 0-1H3A1.5 1.5 0 0 0 1.5 5v8zm7-11a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 .5.5v5a.5.5 0 0 1-1 0V2.5H9a.5.5 0 0 1-.5-.5z"/>
                    <path fill-rule="evenodd"
                          d="M14.354 1.646a.5.5 0 0 1 0 .708l-8 8a.5.5 0 0 1-.708-.708l8-8a.5.5 0 0 1 .708 0z"/>
                </svg>
            </a>
        @endif @endif
    </div>
    <div>
        @switch($param->type)
            @case('checkbox')
            @case('checkbox-vehicle-brands')
            @case('checkbox-vehicle-models')
            @php($selected = request("filter.{$param->code}"))

            <form-multi-select
                class="d-block"
                title="{{ $param->title }}"
                show-btn show-search
                multicheck auto-submit
                @if($disabled) disabled @endif
                input-name="filter[{{ $param->code }}][]"
                :input-value='@json(data_get(request()->get("filter"), $param->code, []))'
                :items="{{$catalog_filter_param_values->get($param->id)}}"
            ></form-multi-select>
            @break

            @case('numeric-range')
            @php($maxlength = data_get($param->options, 'maxlength', 99))
            @php($input_mask = data_get($param->options, 'input_mask', null))
            <div class="row">
                <div class="col-6 pr-1">
                    <input
                        type="text"
                        name="filter[{{ $param->code }}][from]"
                        onblur="onChangeFilterElement(event)"
                        value="{{ request("filter.{$param->code}.from") }}"
                        placeholder="От"
                        data-inputmask="{{ $input_mask }}"
                        maxlength="{{ $maxlength }}"
                        @if($disabled) disabled @endif
                        class="form-control">
                </div>
                <div class="col-6 pl-1">
                    <input
                        type="text"
                        name="filter[{{ $param->code }}][to]"
                        onblur="onChangeFilterElement(event)"
                        value="{{ request("filter.{$param->code}.to") }}"
                        placeholder="До"
                        data-inputmask="{{ $input_mask }}"
                        @if($disabled) disabled @endif
                        maxlength="{{ $maxlength }}"
                        class="form-control">
                </div>
            </div>
            @break

            @default
            <s>TYPE: {{ $param->type }}</s>
        @endswitch
    </div>
</div>

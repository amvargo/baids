<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', config('app.name', 'Laravel'))</title>

    <!-- Styles -->
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
@stack('styles')

<!-- Scripts -->
    <script src="{{ asset('js/site.js') }}" defer></script>
    @stack('scripts')

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
</head>
<body>
<div id="site">
    @include('includes.navbar')
    @section('header')
        @include('includes.header')
    @show
    <div class="container i-min-height-container">
        <nav aria-label="breadcrumb" class="d-none d-sm-block">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Личный кабинет</li>
            </ol>
        </nav>
        <div class="content">
            <div class="row navbar-expand-sm">
                <div class="col-12 d-sm-none d-flex mb-3">
                    <a href="" class="mr-auto cabinet-menu-btn" data-toggle="collapse" data-target="#navbarCabinet"
                       aria-controls="navbarCabinet" aria-expanded="false" aria-label="Toggle navigation">
                        <img class="m-1" src="{{ asset('img/back.svg') }}" alt="info" style="height: 16px;">
                    </a>
                    @php $name = request()->get("name", "Личный кабинет"); @endphp
                    <span class="h1 m-0 mr-auto cabinet-header">{{ $name }}</span>
                </div>
                <div class="collapse navbar-collapse col-12 col-sm-3 pr-0" id="navbarCabinet">
                    <div class="list-group list-group-cabinet w-100">
                        <a
                            href="{{ route('cabinet.profile', ["name" => "Профиль"]) }}"
                            class="list-group-item list-group-item-action @if($current_route === 'cabinet.profile') active disabled @endif">
                            <img class="mb-1 mr-1" src="{{ asset('img/icons/16px/user.svg') }}">
                            <span class="d-sm-none d-md-inline">Профиль</span>
                        </a>
                        <a
                            href="{{ route('cabinet.notifications.index', ["name" => "Оповещения"]) }}"
                            class="list-group-item list-group-item-action @if($current_route === 'cabinet.notifications.index') active disabled @endif">
                            <img class="mb-1 mr-1" src="{{ asset('img/icons/16px/mail.svg') }}" alt="icon">
                            <span class="d-sm-none d-md-inline">
                                Оповещения
                                @if($user_notifications) ({{ $user_notifications }}) @endif
                            </span>
                        </a>
                        <a
                            href="{{ route('cabinet.favorites', ["name" => "Избранное"]) }}"
                            class="list-group-item list-group-item-action @if($current_route === 'cabinet.favorites') active disabled @endif">
                            <img class="mb-1 mr-1" src="{{ asset('img/icons/16px/heart.svg') }}">
                            <span class="d-sm-none d-md-inline">
                                Избранное
                                @if($user_favorites) ({{ $user_favorites }}) @endif
                            </span>
                        </a>
                        <a
                            href="{{ route('cabinet.payments', ["name" => "Платежи"]) }}"
                            class="list-group-item list-group-item-action @if($current_route === 'cabinet.payments') active disabled @endif">
                            <img class="mb-1 mr-1" src="{{ asset('img/icons/16px/wallet.svg') }}">
                            <span class="d-sm-none d-md-inline">Платежи</span>
                        </a>
                        <a
                            href="{{ route('cabinet.services', ["name" => "Мои услуги"]) }}"
                            class="list-group-item list-group-item-action @if($current_route === 'cabinet.services') active disabled @endif">
                            <img class="mb-1 mr-2" src="{{ asset('img/icons/16px/portfolio.svg') }}">
                            <span class="d-sm-none d-md-inline">Мои услуги</span>
                        </a>

                        <a
                            href="{{ route('cabinet.banners', ["name" => "Баннеры"]) }}"
                            class="list-group-item list-group-item-action @if($current_route === 'cabinet.banners') active disabled @endif">
                            <img class="mb-1 mr-1" src="{{ asset('img/icons/16px/hummer.svg') }}">
                            <span class="d-sm-none d-md-inline">Баннеры</span>
                        </a>

                        @can('create', \App\Models\Trade::class)
                        <a
                            href="{{ route('cabinet.bidding', ["name" => "Торги"]) }}"
                            class="list-group-item list-group-item-action @if($current_route === 'cabinet.bidding') active disabled @endif">
                            <img class="mb-1 mr-1" src="{{ asset('img/icons/16px/hummer.svg') }}">
                            <span class="d-sm-none d-md-inline">Торги</span>
                        </a>
                        @endcan

                        <a
                            href="{{ route('logout', ["name" => "Выход"]) }}"
                            class="list-group-item list-group-item-action ">
                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                      d="M3 1.5C2.17157 1.5 1.5 2.17157 1.5 3V13C1.5 13.8284 2.17157 14.5 3 14.5H9.49918C10.3276 14.5 10.9992 13.8284 10.9992 13V11.3913C10.9992 11.1152 10.7753 10.8913 10.4992 10.8913C10.223 10.8913 9.99918 11.1152 9.99918 11.3913V13C9.99918 13.2761 9.77532 13.5 9.49918 13.5H3C2.72386 13.5 2.5 13.2761 2.5 13L2.5 3C2.5 2.72386 2.72386 2.5 3 2.5H9.49918C9.77532 2.5 9.99918 2.72386 9.99918 3V4.6087C9.99918 4.88484 10.223 5.1087 10.4992 5.1087C10.7753 5.1087 10.9992 4.88484 10.9992 4.6087V3C10.9992 2.17157 10.3276 1.5 9.49918 1.5H3ZM12.2741 5.5085C12.0816 5.31055 11.7651 5.30616 11.5671 5.4987C11.3692 5.69124 11.3648 6.00779 11.5573 6.20574L12.8162 7.49997H6.2496C5.97346 7.49997 5.7496 7.72383 5.7496 7.99997C5.7496 8.27612 5.97346 8.49997 6.2496 8.49997H12.8162L11.5573 9.79421C11.3648 9.99216 11.3692 10.3087 11.5671 10.5012C11.7651 10.6938 12.0816 10.6894 12.2741 10.4915L14.3584 8.34859C14.5472 8.15451 14.5472 7.84543 14.3584 7.65135L12.2741 5.5085Z"
                                      fill="#39475F"/>
                            </svg>
                            <span class="d-sm-none d-md-inline">@lang('Exit')</span>
                        </a>
                    </div>
                </div>
                <div class="col-12 col-sm-9 border-left pl-sm-4">
                    @yield('content')
                </div>
            </div>
        </div>
        <div class="row d-none d-sm-block">
            <div class="col">
                <get-banners :limit="1" variant="horizontal" position="15"></get-banners>
            </div>
        </div>
    </div>
    @include('includes.footer')
</div>
</body>
</html>

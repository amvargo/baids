@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="justify-content-center">
            <div>
                @include('includes.auth.login')
            </div>
        </div>
    </div>
@endsection

@extends('layouts.site')
@section('content')
<div class="content">
    <div class="row">
        <div class="col">
            <div class="h1">Разместим Вашу рекламу</div>
            <p>Уважаемые гости сайта. ООО БАИДС готов предложить Вам услуги, в виде размещения Вашей информации на платформе сайта baids.ru на правах рекламы.</p>
            <p>Заказать услугу Вы можете в верхней панельной строке сайта, во вкладке 
                <a href="{{ url('/advertising')  }}"
                    @auth

                    @else
                    data-redirect="/advertising"
                    data-toggle="modal"
                    data-target="#modalLogin"
                    @endauth>
                    Реклама.
                </a>
            </p>
            <p>Итоговая стоимость услуг определяется после указания параметров заказа.</p>
            <p>Получить более подробную информацию по услуге Вы можете:</p>
            <p>E-mail: <a href="mailto:info@baids.ru" class="text-dark">info@baids.ru</a></p>
            <p>Телефон: <a href="tel:+79612011338" class="text-dark">8 (961) 201 13 38</a></p>
        </div>
    </div>
</div>
@endsection
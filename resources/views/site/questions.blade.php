@extends('layouts.site')

@section('title', __('Question answer'))

@section('content')
    @php($show_form = false)
    @error('author') @php($show_form = true) @enderror
    @error('email') @php($show_form = true) @enderror
    @error('question') @php($show_form = true) @enderror
    <div class="content">
        <nav aria-label="breadcrumb" class="d-none d-sm-block">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">@lang('Question answer')</li>
            </ol>
        </nav>
        <h1>@lang('Question answer')</h1>

        <nav>
            <div class="nav nav-pills nav-pills-cabinet mb-3" id="nav-tab" role="tablist">
                <a class="nav-item nav-link @if(!$show_form) active @endif" id="nav-list-tab" data-toggle="tab"
                   href="#list" role="tab"
                   aria-controls="nav-home" aria-selected="true">Вопросы пользователей</a>
                <a class="nav-item nav-link @if($show_form) active @endif" id="nav-form-tab" data-toggle="tab"
                   href="#service" role="tab"
                   aria-controls="nav-profile" aria-selected="false">Задать свой вопрос</a>
            </div>
        </nav>

        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show  @if(!$show_form) active @endif" id="list" role="tabpanel"
                 aria-labelledby="nav-list-tab">

                <div class="flash-message mt-3">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('question-alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">
                                @lang(Session::get('question-alert-' . $msg))
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                        @endif
                    @endforeach
                </div>

                <div class="my-3">
                    @if(blank($questions))
                        @component('components.alert', ['type' => 'info'])
                            @lang('No questions')
                        @endcomponent
                    @endif
                    @each('includes.questions.item', $questions, 'question')
                </div>

                {{ $questions->links() }}

            </div>
            <div class="tab-pane fade show @if($show_form) active @endif" id="service" role="tabpanel"
                 aria-labelledby="nav-form-tab">
                <div class="mb-4">
                    <h2 class="my-4">Задайте нам интересующий Вас вопрос. В ближайшее время в Ваш личный кабинет придет
                        ответ, а вопрос опубликуется в разделе “Вопросы пользователей”</h2>
                    <div class="row pb-5">
                        <div class="col-lg-7">
                            @include('includes.questions.create')
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

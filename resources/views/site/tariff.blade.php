@extends('layouts.site')
@section('content')
<div class="content">
    <div class="row">
        <tariff :tariff-active='{{ (auth()->user() && auth()->user()->isTariffActive()) ? 1 : 0 }}'></tariff>
    </div>
    
    <div class="modal fade" id="modalTariff" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close close-rt" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('img/icons/16px/close.svg') }}" alt="close">
                </button>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@extends('layouts.site')

@section('title', __('Services'))

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Услуги</li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="content">

        <h1>Воспользуйтесь нашими услугами </h1>

        @if(blank($services))
            @component('components.alert', ['type' => 'info'])
                @lang('No services')
            @endcomponent
        @endif

        <div class="row">
            @foreach($services as $service)
                <div class="col-12 col-md-6 col-lg-4">
                    @include('includes.services.item', ['services' => $services])
                </div>
            @endforeach
        </div>

        {{ $services->links() }}

    </div>
@endsection

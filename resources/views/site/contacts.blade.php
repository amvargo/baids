@extends('layouts.site')

@section('content')
    <div class="content">

        <div class="row">
            <div class="col-12">
                <nav>
                    <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if(!isset($tab) || $tab === "contacts") active @endif" id="contacts-tab" data-toggle="tab" href="#contacts" role="tab"
                               aria-controls="contacts-tab" aria-selected="true">Контакты</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if(isset($tab) && $tab === "requisites") active @endif" id="requisites-tab" data-toggle="tab" href="#requisites" role="tab"
                               aria-controls="requisites-tab" aria-selected="true">Реквизиты</a>
                        </li>
                    </ul>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade @if(!isset($tab) || $tab === "contacts")show active @endif" id="contacts" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="h1">Контакты</div>
                        <div class="contacts-items">
                            <p><strong>E-mail:</strong> <a href="mailto:info@baids.ru">info@baids.ru</a></p>
                            <p><strong>Телефон:</strong> <a href="tel:8(961)2011338">8 (961) 201 13 38</a>
                            <p><strong>Адрес:</strong> 625035, Тюменская область, г. Тюмень, ул. Республики, д. 164, стр. 2, оф 409</p>
                            <p><strong>График работы:</strong> 7:00-17:00 МСК</p>
                        </div>
                        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A6a1799d7016b5839b1af00bb4db9c2496f8c838c83b702e1795435b626f66743&amp;source=constructor" width="100%" height="400" frameborder="0"></iframe>
                    </div>
                    <div class="tab-pane fade @if(isset($tab) && $tab === "requisites")show active @endif" id="requisites" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="h1">Реквизиты</div>

                        <div class="requisites-items">
                            <p><strong>Полное наименование:</strong> Общество с ограниченной ответственностью "БАИДС"</p>
                            <p><strong>Генеральный директор:</strong> Емельянова Елена Владиславовна</p>
                            <p><strong>Наименование банка:</strong> БАНК ВТБ (ПАО)</p>
                            <p><strong>Корреспондентский счет:</strong> 30101810145250000411</p>
                            <p><strong>БИК:</strong> 044525411</p>
                            <p><strong>Расчетный счет:</strong> 40702810000000022725</p>
                            <p><strong>ОГРН:</strong> 1207200004000</p>
                            <p><strong>ИНН:</strong> 7203499243</p>
                            <p><strong>КПП:</strong> 720301001</p>
                            <p><strong>Юридический адреc:</strong> 625035, Тюменская область, г. Тюмень, ул. Республики, д. 164, стр. 2, оф 409</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection


@extends('layouts.site')

@section('title', __('Service').': '.$service->title)

@section('content')
    <div class="content">
        <h1>{{ $service->title }}</h1>
        <p>
            {{ $service->description }}
        </p>
        @if(filled($service->url_image))
            <img src="{{ $service->url_image }}" class="img" alt="{{ $service->title }}">
        @endif

    </div>
@endsection

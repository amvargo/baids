@extends('layouts.site')

@section('content')
    <div class="content">
        <div class="row mb-4">
            <div class="col-xl-8 col-lg-7">
                <div class="rounded shadow mb-4 overflow-hidden">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators mb-0 mb-sm-2">
                            @foreach($sliders as $slider)
                                <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" {{ $loop->first ? 'class=active' : ''  }}></li>
                            @endforeach
                        </ol>
                        <div class="carousel-inner">
                            @foreach($sliders as $slider)
                                <div class="carousel-item {{ $loop->first ? 'active' : ''  }}" >
                                    <div class="slider-item d-flex" style="{{ $slider->color ? "background-color: $slider->color;" : ''  }}">
                                        <div class="slider-content m-4">
                                            {!! $slider->content !!}
                                        </div>
                                        @if($slider->image)
                                            <img src="{{ $slider->image->url }}" class="d-block slider-img" alt="...">
                                        @endif
                                    </div>

                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-5 d-none d-lg-block">
                @auth
                    <div class="rounded shadow p-4 mb-4" style="height: 250px;">
                        <div class="h1 m-0">Привет, {{ auth()->user()->name }}!</div>

                        <ul class="list-unstyled my-4">
                            <li class="mb-1">
                                <img class="mb-1 mr-1" src="{{ asset('img/icons/message.svg') }}">
                                <a href="{{ route('cabinet.notifications.index') }}" class="text-body">
                                    <span>@lang('Messages') ({{ $user_notifications }})</span>
                                </a>
                            </li>
                            <li class="mb-1">
                                <img class="mb-1 mr-1" src="{{ asset('img/icons/16px/portfolio.svg') }}">
                                <a href="{{ route('cabinet.services') }}" class="text-body">
                                    <span>@lang('Services')</span>
                                </a>
                            </li>
                            <li class="mb-1">
                                <img class="mb-1 mr-1" src="{{ asset('img/icons/16px/heart.svg') }}">
                                <a href="{{ route('cabinet.favorites') }}" class="text-body">
                                    <span>@lang('Favorites') ({{ $user_favorites }})</span>
                                </a>
                            </li>
                            @can('create', \App\Models\Trade::class)
                            <li class="mb-1">
                                <img class="mb-1 mr-1" src="{{ asset('img/icons/16px/hummer.svg') }}">
                                <a href="{{ route('cabinet.bidding') }}" class="text-body">
                                    <span>@lang('Bidding')</span>
                                </a>
                            </li>
                            @endcan
                        </ul>

                        <div class="row">
                            <div class="col flex-grow-1 pr-0">
                                <a class="btn btn-sm btn-light bg-white shadow btn-login" href="{{ route('cabinet.profile') }}">
                                    @lang('Go to cabinet')
                                </a>
                            </div>
                            <div class="col flex-grow-0 text-right py-1" style="min-width: auto">
                                <a href="{{ url('/logout') }}"
                                   class="text-nowrap align-items-center">
                                    <span>@lang('Exit')</span>
                                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                              d="M3 1.5C2.17157 1.5 1.5 2.17157 1.5 3V13C1.5 13.8284 2.17157 14.5 3 14.5H9.49918C10.3276 14.5 10.9992 13.8284 10.9992 13V11.3913C10.9992 11.1152 10.7753 10.8913 10.4992 10.8913C10.223 10.8913 9.99918 11.1152 9.99918 11.3913V13C9.99918 13.2761 9.77532 13.5 9.49918 13.5H3C2.72386 13.5 2.5 13.2761 2.5 13L2.5 3C2.5 2.72386 2.72386 2.5 3 2.5H9.49918C9.77532 2.5 9.99918 2.72386 9.99918 3V4.6087C9.99918 4.88484 10.223 5.1087 10.4992 5.1087C10.7753 5.1087 10.9992 4.88484 10.9992 4.6087V3C10.9992 2.17157 10.3276 1.5 9.49918 1.5H3ZM12.2741 5.5085C12.0816 5.31055 11.7651 5.30616 11.5671 5.4987C11.3692 5.69124 11.3648 6.00779 11.5573 6.20574L12.8162 7.49997H6.2496C5.97346 7.49997 5.7496 7.72383 5.7496 7.99997C5.7496 8.27612 5.97346 8.49997 6.2496 8.49997H12.8162L11.5573 9.79421C11.3648 9.99216 11.3692 10.3087 11.5671 10.5012C11.7651 10.6938 12.0816 10.6894 12.2741 10.4915L14.3584 8.34859C14.5472 8.15451 14.5472 7.84543 14.3584 7.65135L12.2741 5.5085Z"
                                              fill="#39475F"/>
                                    </svg>
                                </a>
                            </div>
                        </div>

                    </div>
                @else
                    @include('includes.auth.login')
                @endauth
            </div>
        </div>

        @includeWhen(isset($service_banners), 'includes.services.banners')

        @includeWhen(isset($general_categories), 'site.catalog.blocks.categories')

        <h2 class="mb-4">Новые лоты</h2>
        <div class="row">
            <div class="col-lg-9 order-2 order-md-0">
                @if(filled($lots))
                    @each('components.catalog.item', $lots, 'lot')
                    {{--{{ $lots->links() }}--}}
                    <div class="mb-5 mt-n2 mt-lg-0">
                        <a href="{{ route('site.catalog.index') }}" class="btn btn-primary btn-seemore">Смотреть больше</a>
                    </div>
                @else
                    <div class="mb-4" style="min-height: 220px;">
                        По выбранному Вами местоположению нет лотов
                    </div>
                @endif
            </div>
            <div class="d-none d-lg-block  col-lg-3 order-1 order-md-1">
                <get-banners :limit="2" variant="vertical" position="13" classes="mb-4" styles="height: 460px;"></get-banners>
            </div>
        </div>

        @if(isset($hot_lots))
            <h2  class="mb-4">Успей купить</h2>
            <div class="row">
                <div class="col-lg-9 order-2 order-md-0">
                    @if(filled($hot_lots))
                        @each('components.catalog.item', $hot_lots, 'lot')
                        {{--{{ $completed_lots->links() }}--}}
                        <div class="mb-5 mt-n2 mt-lg-0">
                            <a href="{{ route('site.hot-lots.index') }}" class="btn btn-primary btn-seemore">Смотреть больше</a>
                        </div>
                    @endif
                </div>
            </div>
        @endif

        @if(isset($completed_lots))
            <h2  class="mb-4">Завершенные лоты</h2>
            <div class="row">
                <div class="col-lg-9 order-2 order-md-0">
                    @if(filled($completed_lots))
                        @each('components.catalog.item', $completed_lots, 'lot')
                        {{--{{ $completed_lots->links() }}--}}
                    @endif
                </div>
            </div>
        @endif
        <div class="row order-1 order-md-1">
            <get-banners :limit="1" variant="horizontal" position="13" classes="mb-4 col-12"></get-banners>
        </div>
    </div>
@endsection

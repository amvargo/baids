@extends('layouts.site')
@section('content')
<div class="content">
    <div class="row">
        <about-project :sections="{{ json_encode($sections) }}"></about-project>
    </div>
</div>
@endsection
@extends('layouts.site')

@section('content')
    <div class="content">

        <div class="row">
            <div class="col-12 pb-4">
                <nav>
                    <ul class="nav nav-tabs mb-3" id="payTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if(!isset($tab) || $tab === "payment-method") active @endif" id="payment-method-tab" data-toggle="tab" href="#payment-method" role="tab"
                               aria-controls="payment-method" aria-selected="true">Способы оплаты</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if(isset($tab) && $tab === "payment-security") active @endif" id="payment-security-tab" data-toggle="tab" href="#payment-security" role="tab"
                               aria-controls="payment-security" aria-selected="true">Безопасность платежей</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if(isset($tab) && $tab === "refund") active @endif" id="refund-tab" data-toggle="tab" href="#refund" role="tab"
                               aria-controls="refund" aria-selected="true">Возврат</a>
                        </li>
                       <!-- <li class="nav-item" role="presentation">
                            <a class="nav-link @if(isset($tab) && $tab === "user-agreement") active @endif" id="user-agreement-tab" data-toggle="tab" href="#user-agreement" role="tab"
                               aria-controls="user-agreement" aria-selected="true">Пользовательское соглашение</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if(isset($tab) && $tab === "policy") active @endif" id="policy-tab" data-toggle="tab" href="#policy" role="tab"
                               aria-controls="policy" aria-selected="true">Политика конфиденциальности</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link @if(isset($tab) && $tab === "oferta") active @endif" id="oferta-tab" data-toggle="tab" href="#oferta" role="tab"
                               aria-controls="oferta" aria-selected="true">Публичный договор оферта</a>
                        </li>-->
                    </ul>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade @if(!isset($tab) || $tab === "payment-method") show active @endif" id="payment-method" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="h1">Способы оплаты</div>
                        <div class="h2">Банковская карта</div>
                        <p><strong>Оплата банковской картой, через Интернет, возможна через системы электронных платежей.</strong></p>
                        <div>
                            <p>Номер карты (PAN) должен иметь не менее 15 и не более 19 символов.<br>
                                Мы принимаем платежи с сайта по следующим банковским картам:</p>
                            <div class="payment-icons mt-2">
                                <img src="{{ asset('/img/visa.svg') }}" alt="">
                                <img src="{{ asset('/img/mastercard.svg') }}" alt="">
                                <img src="{{ asset('/img/mir.svg') }}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade @if(isset($tab) && $tab === "payment-security") show active @endif" id="payment-security" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="h1">Безопасность платежей</div>
                        <div class="h3">Уважаемый клиент!</div>
                        <p>Вы можете оплатить свой заказ онлайн с помощью банковской карты через платежный сервис компании
                        Uniteller. После подтверждения заказа Вы будете перенаправлены на защищенную платежную страницу
                        Uniteller, где необходимо будет ввести данные для оплаты заказа. После успешной оплаты на указанную
                        в форме оплаты электронную почту будет направлен электронный чек с информацией о заказе и
                        данными по произведенной оплате.</p>
                        <div class="h3">Гарантии безопасности</div>
                        <p>Безопасность процессинга Uniteller подтверждена сертификатом стандарта безопасности данных
                        индустрии платежных карт PCI DSS. Надежность сервиса обеспечивается интеллектуальной системой
                        мониторинга мошеннических операций, а также применением 3D Secure - современной технологией
                        безопасности интернет-платежей.</p>
                        <p>Данные Вашей карты вводятся на специальной защищенной платежной странице. Передача информации
                        в процессинговую компанию Uniteller происходит с применением технологии шифрования TLS.
                        Дальнейшая передача информации осуществляется по закрытым банковским каналам, имеющим
                        наивысший уровень надежности.</p>
                        <p>Uniteller не передает данные Вашей карты магазину и иным третьим лицам!
                        Если Ваша карта поддерживает технологию 3D Secure, для осуществления платежа, Вам необходимо
                        будет пройти дополнительную проверку пользователя в банке-эмитенте (банк, который выпустил Вашу
                        карту). Для этого Вы будете направлены на страницу банка, выдавшего карту. Вид проверки зависит от
                        банка. Как правило, это дополнительный пароль, который отправляется в SMS, карта переменных кодов,
                        либо другие способы.</p>
                    </div>
                    <div class="tab-pane fade @if(isset($tab) && $tab === "refund") show active @endif" id="refund" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="h1">Возврат</div>
                        <p>Для возврата денежных средств на банковскую карту необходимо заполнить «Заявление о возврате денежных
                        средств», которое высылается по требованию компанией на электронный адрес и оправить его вместе с
                        приложением копии паспорта по адресу <a href="mailto:info@baids.ru">info@baids.ru</a></p>

                        <p>Возврат денежных средств будет осуществлен на банковскую карту в течение <strong>10 (десяти)</strong> дней с момента получения «Заявление о возврате денежных средств» Компанией.</p>

                        <p>Для возврата денежных средств по операциям проведенными с ошибками необходимо обратиться с письменным
                        заявлением и приложением копии паспорта и чеков/квитанций, подтверждающих ошибочное списание.
                        Данное заявление необходимо направить по адресу <a href="mailto:info@baids.ru">info@baids.ru</a></p>

                        <p>Сумма возврата будет равняться сумме покупки. Срок рассмотрения Заявления и возврата денежных
                        средств начинает исчисляться с момента получения Компанией Заявления и рассчитывается в рабочих
                        днях без учета праздников/выходных дней.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection


@extends('layouts.site', ['synonyms' => $synonyms ?? ''])

@php($seo_title = isset($page_title) ?: '')

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>

            @if(strpos(request()->route()->action['as'], 'site.hot-lots') !== false)
                <li class="breadcrumb-item"><a href="{{ route('site.hot-lots.index') }}">Успей купить</a></li>
            @else
                <li class="breadcrumb-item"><a href="{{ route('site.catalog.index') }}">Каталог лотов</a></li>
            @endif

            @if(isset($catalog_category) && filled($catalog_category))
                @foreach($catalog_category->getBreadcrumbArray() as $item)
                    <li class="breadcrumb-item @if($item->active) active @endif" aria-current="page">
                        @if($item->url)
                            <a href="{{ $item->url }}">{{ $item->title }}</a>
                            @auth @if(auth()->user()->isAdmin())
                                <a
                                    class="text-primary"
                                    target="_blank"
                                    title="Редактировать"
                                    href="{{ url("panel/catalog-category-{$item->id}") }}">
                                    <svg class="bi bi-box-arrow-up-right" width="1em" height="1em" viewBox="0 0 16 16"
                                         fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                              d="M1.5 13A1.5 1.5 0 0 0 3 14.5h8a1.5 1.5 0 0 0 1.5-1.5V9a.5.5 0 0 0-1 0v4a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 0 0-1H3A1.5 1.5 0 0 0 1.5 5v8zm7-11a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 .5.5v5a.5.5 0 0 1-1 0V2.5H9a.5.5 0 0 1-.5-.5z"/>
                                        <path fill-rule="evenodd"
                                              d="M14.354 1.646a.5.5 0 0 1 0 .708l-8 8a.5.5 0 0 1-.708-.708l8-8a.5.5 0 0 1 .708 0z"/>
                                    </svg>
                                </a>
                            @endif @endif
                        @else
                            <span>{{ $item->title }}</span>
                        @endif
                    </li>
                @endforeach
            @endif

        </ol>
    </nav>
@endsection

@section('content')
    <div class="content">

        @includeWhen(isset($general_categories), 'site.catalog.blocks.categories')

        @if (isset($page_title))
            <h1>{{$page_title}}</h1>
        @endif
{{--        @if (isset($titles))--}}
{{--            <div class="row">--}}

{{--                @foreach($titles as $key_status => $title)--}}
{{--                    <div class="col-12 col-md-4 text-center">--}}
{{--                        <h1>--}}
{{--                            @if ($statusOfCategory == $key_status)--}}
{{--                                {{ $title }}--}}
{{--                            @else--}}
{{--                                <a--}}
{{--                                    href="{{ request()->fullUrlWithQuery(['status' => $key_status]) }}">--}}
{{--                                    {{ $title }}--}}
{{--                                </a>--}}
{{--                            @endif--}}
{{--                        </h1>--}}
{{--                    </div>--}}
{{--                @endforeach--}}

{{--            </div>--}}
{{--        @endif--}}

        <div class="row">
            <div class="col-12 col-lg-3">
                @component('components.catalog.category-select', ['category'=> $catalog_category ?? null, 'titles' => $titles ?? null]) @endcomponent
                @component('components.catalog.filter', ['category'=> $catalog_category ?? null, 'titles' => $titles ?? null]) @endcomponent
                <get-banners
                    class="mt-4 d-none d-lg-block"
                    :limit="1"
                    variant="vertical"
                    @if(isset($catalog_category))
                    category="{{$catalog_category->id."|".$catalog_category->parent_id}}"
                    @endif
                    position="14"></get-banners>

            </div>

            <div class="col-12 col-lg-9">

                <section id="catalog-content" style="min-height: 210px">
                    @if(blank($lots))
                        @component('components.alert', ['type' => 'info'])
                            <i class="text-black-50">Нет подходящих лотов</i>
                        @endcomponent
                    @endif

                    @each('components.catalog.item', $lots, 'lot')

                    {{ $lots->appends(request()->query())->links() }}
                </section>
            </div>
        </div>
        <get-banners
            :limit="1"
            variant="horizontal"
            @if(isset($catalog_category))
            category="{{$catalog_category->id."|".$catalog_category->parent_id}}"
            @endif
            position="14"></get-banners>
    </div>
@endsection

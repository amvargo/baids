<section class="mb-4 general-categories-section">
    <div class="h2">Выберите категорию</div>
    <div class="row">
        @foreach($general_categories as $item)

            @php($route_name = 'site.catalog.category.show')
            @if(strpos(request()->route()->action['as'], 'site.hot-lots') !== false)
                @php($route_name = str_replace('.index', '.category.show', request()->route()->action['as']))
            @endif

            <div class="col-4 col-md-2 mb-4">
                <a class="text-dark"
                   href="{{ route($route_name, $item->slug) }}">
                    <div class="shadow rounded">
                        <svg class="w-100"
                             width="100%"
                             viewBox="0 0 160 160"
                             fill="none" xmlns="http://www.w3.org/2000/svg">
                        </svg>
                        <div class="position-absolute fixed-bottom mx-3 pt-4 overflow-hidden pr-0 pl-0 category-item">
                            <img src="{{ $item->image_url ?: '/img/general-cat-noimg.svg' }}" alt="{{ $item->title }}"
                                 width="100%">
                        </div>
                        <div class="position-absolute fixed-top mx-3 p-3">
                            <div class="h4">{{ $item->title }}</div>
                        </div>
                    </div>
                </a>
            </div>

        @endforeach
    </div>
</section>

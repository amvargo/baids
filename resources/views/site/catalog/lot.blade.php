@extends('layouts.site')

@php($seo_title = __('Lot').': '.$lot->id)

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('site.catalog.index') }}">Каталог лотов</a></li>

            @php($category = $lot->firstCategory)
            @if(isset($category) && filled($category))
                @foreach($category->getBreadcrumbArray() as $item)
                    <li class="breadcrumb-item @if($item->active) active @endif" aria-current="page">
                        @if($item->url)
                            <a href="{{ $item->url }}">{{ $item->title }}</a>
                            @auth @if(auth()->user()->isAdmin())
                                <a
                                    class="text-primary"
                                    target="_blank"
                                    title="Редактировать"
                                    href="{{ url("panel/catalog-category-{$item->id}") }}">
                                    <svg class="bi bi-box-arrow-up-right" width="1em" height="1em" viewBox="0 0 16 16"
                                         fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                              d="M1.5 13A1.5 1.5 0 0 0 3 14.5h8a1.5 1.5 0 0 0 1.5-1.5V9a.5.5 0 0 0-1 0v4a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 0 0-1H3A1.5 1.5 0 0 0 1.5 5v8zm7-11a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 .5.5v5a.5.5 0 0 1-1 0V2.5H9a.5.5 0 0 1-.5-.5z"/>
                                        <path fill-rule="evenodd"
                                              d="M14.354 1.646a.5.5 0 0 1 0 .708l-8 8a.5.5 0 0 1-.708-.708l8-8a.5.5 0 0 1 .708 0z"/>
                                    </svg>
                                </a>
                            @endif @endif
                        @else
                            <span>{{ $item->title }}</span>
                        @endif
                    </li>
                @endforeach
            @endif

            <li class="breadcrumb-item active">
                <span>Лот №{{ $lot->id }}</span>
                @auth @if(auth()->user()->isAdmin())
                    <a
                        class="text-primary"
                        target="_blank"
                        title="Редактировать"
                        href="{{ url("panel/catalog-lot-{$lot->id}") }}">
                        <svg class="bi bi-box-arrow-up-right" width="1em" height="1em" viewBox="0 0 16 16"
                             fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                  d="M1.5 13A1.5 1.5 0 0 0 3 14.5h8a1.5 1.5 0 0 0 1.5-1.5V9a.5.5 0 0 0-1 0v4a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 0 0-1H3A1.5 1.5 0 0 0 1.5 5v8zm7-11a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 .5.5v5a.5.5 0 0 1-1 0V2.5H9a.5.5 0 0 1-.5-.5z"/>
                            <path fill-rule="evenodd"
                                  d="M14.354 1.646a.5.5 0 0 1 0 .708l-8 8a.5.5 0 0 1-.708-.708l8-8a.5.5 0 0 1 .708 0z"/>
                        </svg>
                    </a>
                @endif @endif
            </li>
        </ol>
    </nav>
@endsection

@section('content')
    <div class="content">
        <div class="h3 mb-2 pt-3">Лот №{{ $lot->id }}</div>
        <h1 class="h1 mb-4 pb-3">{{ $lot->title }}</h1>

        <div class="d-flex d-md-none mb-5">
            <div class="px-3 py-3 rounded bg-light">
                <div class="mb-1">
                    @if($lot->current_price !== $lot->StartPrice)
                        <span>Текущая стоимость</span>
                    @else
                        <span>Начальная стоимость</span>
                    @endif
                    @switch($lot->trade->AuctionType)
                        @case('OpenAuction')
                        <img
                            data-toggle="tooltip" data-placement="left" title="Аукцион"
                            class="lot-preview-price__type"
                            src="{{ asset('img/icons/16px/circle-arr-up.svg') }}"
                            alt="Аукцион">
                        @break
                        @case('PublicOffer')
                        <img
                            data-toggle="tooltip" data-placement="left" title="Публичное предложение"
                            class="lot-preview-price__type"
                            src="{{ asset('img/icons/16px/circle-arr-down.svg') }}"
                            alt="Публичное предложение">
                        @break
                    @endswitch
                </div>
                @if($lot->StartPrice && $lot->current_price !== $lot->StartPrice)
                    <div class="mt-1 font-weight-bolder"><s>@money($lot->StartPrice) ₽</s></div>
                @endif
                <div class="h1 m-0">@money($lot->current_price) ₽</div>
            </div>
            
            
            <div class="ml-3">
                @if(auth()->user() && auth()->user()->isTariffActive())
                <a href="javascript:" data-id="{{ $lot->id }}"
                class="j-favorite-link lot-preview-link mt-1 d-block  @if($lot->in_favorite) in-favorite @endif">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M12.0003 5.1285C10.266 3.80563 8.37235 3.30728 6.65968 3.56553C4.71813 3.8583 3.10627 5.11707 2.39279 7.01392C0.962262 10.8171 3.24623 16.4602 11.4522 21.8365C11.7851 22.0545 12.2155 22.0545 12.5483 21.8365C20.7543 16.4602 23.0383 10.8171 21.6077 7.01392C20.8943 5.11707 19.2824 3.8583 17.3408 3.56553C15.6282 3.30728 13.7345 3.80563 12.0003 5.1285ZM12.0003 19.798C19.5266 14.6868 20.617 10.0608 19.7358 7.71804C19.2829 6.51404 18.2832 5.73023 17.0426 5.54318C15.7982 5.35553 14.215 5.7622 12.6747 7.16931C12.2928 7.51825 11.7077 7.51825 11.3258 7.16931C9.78553 5.7622 8.20231 5.35553 6.95789 5.54318C5.71736 5.73023 4.71761 6.51404 4.26474 7.71804C3.38352 10.0608 4.4739 14.6868 12.0003 19.798Z"></path>
                    </svg>
                    <span>В избранное</span>
                </a>
                @endif
                <small style="margin-top:15px;" class="align-items-center d-flex mr-3 lot-social-share">
                    <div class="ya-share2" 
                        data-curtain 
                        data-size="s" 
                        data-shape="round" 
                        data-limit="0" 
                        data-copy="first"
                        data-more-button-type="short" 
                        data-title="{{ $lot->title }}"
                        data-description="{{ mb_substr($lot->description, 0, 100) }}"
                        data-image="{{ $lot->last_category_lot_img }}"
                        data-url="{{ Request::url() }}"
                        data-services="vkontakte,facebook,odnoklassniki,viber,whatsapp">
                    </div>
                    <span>Поделиться</span>
                </small>

                <small style="margin-top:15px;" class="align-items-center d-flex mr-3 lot-report">
                    <a  data-lot-id="{{ $lot->id }}"
                        data-toggle="modal"
                        data-target="#modalReport"
                        href="javascript:">
                        <svg style="width:24px;height:24px;" version="1.0" xmlns="http://www.w3.org/2000/svg"
                            width="1280.000000pt" height="1126.000000pt" viewBox="0 0 1280.000000 1126.000000"
                            preserveAspectRatio="xMidYMid meet">
                            <g transform="translate(0.000000,1126.000000) scale(0.100000,-0.100000)"
                            fill="#000000" stroke="none">
                            <path d="M6201 11240 c-41 -10 -113 -37 -160 -61 -70 -35 -105 -62 -187 -144 -61 -60 -124 -134 -157 -185 -85 -132 -681 -1182 -2962 -5215 -793 -1402 -1714 -3032 -2047 -3620 -333 -589 -617 -1098 -631 -1131 -79 -187 -72 -394 19 -559 15 -28 64 -86 108 -130 91 -90 177 -139 306 -175 l76 -20 5879 2 5880 3 81 27 c363 124 494 499 304 878 -21 43 -899 1580 -1951 3417 -1052 1836 -2308 4029 -2791 4873 -484 844 -909 1580 -946 1635 -118 177 -268 311 -419 373 -125 52 -272 64 -402 32z m1607 -3410 c793 -1383 2019 -3523 2724 -4755 l1283 -2240 -2712 -3 c-1492 -1 -3934 -1 -5427 0 l-2715 3 1666 2945 c3188 5637 3725 6583 3734 6572 4 -4 655 -1139 1447 -2522z"/>
                            <path d="M6290 7874 c-14 -3 -61 -14 -104 -25 -390 -98 -706 -474 -706 -837 0 -46 22 -254 50 -461 27 -207 113 -857 190 -1446 201 -1535 199 -1517 216 -1581 42 -165 141 -297 271 -361 67 -33 86 -38 168 -41 152 -7 246 30 348 136 99 105 144 224 176 464 11 84 61 462 111 841 49 378 131 996 180 1375 50 378 100 756 111 840 24 182 25 305 4 387 -82 323 -360 599 -693 686 -75 20 -266 33 -322 23z"/>
                            <path d="M6322 2739 c-345 -44 -594 -371 -552 -726 20 -166 86 -301 204 -410 114 -107 237 -160 391 -170 187 -11 336 47 475 187 134 134 192 273 193 465 1 116 -13 183 -58 280 -120 261 -379 409 -653 374z"/>
                            </g>
                        </svg>
                        <span>Сообщить об ошибках</span>
                    </a>
                </small>
            </div>
        </div>

        <div class="row">
            @if(auth()->user() && auth()->user()->isTariffActive())
            <div class="col-12 col-sm-3 d-flex mb-5 mb-sm-0">
                    <images-preview
                        class="flex-1 w-100"
                        :images='@json($lot->firstImagesArray(5))'
                        @if($lot->getOptionsForMap())
                        :map='@json($lot->getOptionsForMap())'
                        @endif
                        no-img-url="{{ $lot->last_category_lot_img }}"
                    ></images-preview>
            </div>
            @else
            <div class="col-12 col-sm-3 d-flex mb-5 mb-sm-0">
                    <images-preview
                        class="flex-1 w-100"
                        @if($lot->getOptionsForMap())
                        :map='@json($lot->getOptionsForMap())'
                        @endif
                        no-img-url="{{ $lot->last_category_lot_img }}"
                    ></images-preview>
            </div>
            @endif
            <div class="col-12 col-sm-9">
                <div class="float-right d-none d-md-block">
                    <div class="px-4 py-3 rounded bg-light ">
                        <div class="mb-1">
                            @if($lot->current_price !== $lot->StartPrice)
                                <span>Текущая стоимость</span>
                            @else
                                <span>Начальная стоимость</span>
                            @endif
                            @switch($lot->trade->AuctionType)
                                @case('OpenAuction')
                                <img
                                    data-toggle="tooltip" data-placement="left" title="Аукцион"
                                    class="lot-preview-price__type"
                                    src="{{ asset('img/icons/16px/circle-arr-up.svg') }}"
                                    alt="Аукцион">
                                @break
                                @case('PublicOffer')
                                <img
                                    data-toggle="tooltip" data-placement="left" title="Публичное предложение"
                                    class="lot-preview-price__type"
                                    src="{{ asset('img/icons/16px/circle-arr-down.svg') }}"
                                    alt="Публичное предложение">
                                @break
                            @endswitch
                        </div>
                        @if($lot->StartPrice && $lot->current_price !== $lot->StartPrice)
                            <div class="mt-1 font-weight-bolder"><s>@money($lot->StartPrice) ₽</s></div>
                        @endif
                        <div class="h1 m-0">@money($lot->current_price) ₽</div>
                    </div>
                    
                    <div class="mt-4 d-xl-none">
                        @if(auth()->user() && auth()->user()->isTariffActive())
                        <a href="javascript:" data-id="{{ $lot->id }}"
                           class="j-favorite-link lot-preview-link mt-1 d-block  @if($lot->in_favorite) in-favorite @endif">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M12.0003 5.1285C10.266 3.80563 8.37235 3.30728 6.65968 3.56553C4.71813 3.8583 3.10627 5.11707 2.39279 7.01392C0.962262 10.8171 3.24623 16.4602 11.4522 21.8365C11.7851 22.0545 12.2155 22.0545 12.5483 21.8365C20.7543 16.4602 23.0383 10.8171 21.6077 7.01392C20.8943 5.11707 19.2824 3.8583 17.3408 3.56553C15.6282 3.30728 13.7345 3.80563 12.0003 5.1285ZM12.0003 19.798C19.5266 14.6868 20.617 10.0608 19.7358 7.71804C19.2829 6.51404 18.2832 5.73023 17.0426 5.54318C15.7982 5.35553 14.215 5.7622 12.6747 7.16931C12.2928 7.51825 11.7077 7.51825 11.3258 7.16931C9.78553 5.7622 8.20231 5.35553 6.95789 5.54318C5.71736 5.73023 4.71761 6.51404 4.26474 7.71804C3.38352 10.0608 4.4739 14.6868 12.0003 19.798Z"></path>
                            </svg>
                            <span>В избранное</span>
                        </a>
                        @endif
                        <small style="margin-top:15px;" class="align-items-center d-flex mr-3 lot-social-share">
                            <div class="ya-share2" 
                                data-curtain 
                                data-size="s" 
                                data-shape="round" 
                                data-limit="0" 
                                data-copy="first"
                                data-more-button-type="short" 
                                data-title="{{ $lot->title }}"
                                data-description="{{ mb_substr($lot->description, 0, 100) }}"
                                data-image="{{ $lot->last_category_lot_img }}"
                                data-url="{{ Request::url() }}"
                                data-services="vkontakte,facebook,odnoklassniki,viber,whatsapp">
                            </div>
                            <span>Поделиться</span>
                        </small>

                        <small style="margin-top:15px;" class="align-items-center d-flex mr-3 lot-report">
                                <a  data-lot-id="{{ $lot->id }}"
                                    data-toggle="modal"
                                    data-target="#modalReport"
                                    href="javascript:">
                                    <svg style="width:24px;height:24px;" version="1.0" xmlns="http://www.w3.org/2000/svg"
                                        width="1280.000000pt" height="1126.000000pt" viewBox="0 0 1280.000000 1126.000000"
                                        preserveAspectRatio="xMidYMid meet">
                                        <g transform="translate(0.000000,1126.000000) scale(0.100000,-0.100000)"
                                        fill="#000000" stroke="none">
                                        <path d="M6201 11240 c-41 -10 -113 -37 -160 -61 -70 -35 -105 -62 -187 -144 -61 -60 -124 -134 -157 -185 -85 -132 -681 -1182 -2962 -5215 -793 -1402 -1714 -3032 -2047 -3620 -333 -589 -617 -1098 -631 -1131 -79 -187 -72 -394 19 -559 15 -28 64 -86 108 -130 91 -90 177 -139 306 -175 l76 -20 5879 2 5880 3 81 27 c363 124 494 499 304 878 -21 43 -899 1580 -1951 3417 -1052 1836 -2308 4029 -2791 4873 -484 844 -909 1580 -946 1635 -118 177 -268 311 -419 373 -125 52 -272 64 -402 32z m1607 -3410 c793 -1383 2019 -3523 2724 -4755 l1283 -2240 -2712 -3 c-1492 -1 -3934 -1 -5427 0 l-2715 3 1666 2945 c3188 5637 3725 6583 3734 6572 4 -4 655 -1139 1447 -2522z"/>
                                        <path d="M6290 7874 c-14 -3 -61 -14 -104 -25 -390 -98 -706 -474 -706 -837 0 -46 22 -254 50 -461 27 -207 113 -857 190 -1446 201 -1535 199 -1517 216 -1581 42 -165 141 -297 271 -361 67 -33 86 -38 168 -41 152 -7 246 30 348 136 99 105 144 224 176 464 11 84 61 462 111 841 49 378 131 996 180 1375 50 378 100 756 111 840 24 182 25 305 4 387 -82 323 -360 599 -693 686 -75 20 -266 33 -322 23z"/>
                                        <path d="M6322 2739 c-345 -44 -594 -371 -552 -726 20 -166 86 -301 204 -410 114 -107 237 -160 391 -170 187 -11 336 47 475 187 134 134 192 273 193 465 1 116 -13 183 -58 280 -120 261 -379 409 -653 374z"/>
                                        </g>
                                    </svg>
                                    <span>Сообщить об ошибках</span>
                                </a>
                        </small>
                    </div>
                </div>

                <ul class="list-unstyled">
                    <li class="mb-2">
                        <b class="mr-2">Статус лота:</b>
                        <span class="text-{{ $lot->status_info->color_variant }}">{{ $lot->status_info->text }}</span>
                    </li>
                    @if($lot->trade->debtor)
                        <li class="mb-2">
                            <b class="mr-2">Регион:</b> {{  $lot->region_with_type == $lot->city_with_type ? $lot->city_with_type : implode(', ', [$lot->region_with_type, $lot->city_with_type]) }}
                        </li>
                    @endif
                    <li class="mb-2">
                        <b class="mr-2">Вид торгов:</b> <span>{{ $lot->trade->AuctionTypeText }}</span>
                    </li>
                    <li class="mb-2">
                        <b class="mr-2">Форма:</b> <span>{{ $lot->trade->FormPriceText }}</span>
                    </li>
                    @isset($lot->trade->TimeBegin)
                        <li class="mb-2">
                            <b class="mr-2">Дата начала торгов:</b>
                            <span>{{ $lot->trade->TimeBegin->format('d.m.Y H:i') }}</span>
                        </li>
                    @endunless
                    <li class="mb-2">
                        <b class="mr-2">Дата окончания торгов:</b>
                        @isset($lot->trade->TimeEnd)
                            <span>{{ $lot->trade->TimeEnd->format('d.m.Y H:i') }}</span>
                        @else
                            <span>Не указано</span>
                        @endunless
                    </li>
                    <li class="mb-2">
                        <b class="mr-2">Дата начала приема заявок:</b>
                        <span>{{ $lot->trade->ApplicationTimeBegin->format('d.m.Y H:i') }}</span>
                    </li>
                    <li class="mb-2">
                        <b class="mr-2">Дата окончания приема заявок:</b>
                        <span>{{ $lot->trade->ApplicationTimeEnd->format('d.m.Y H:i') }}</span>
                    </li>

                </ul>
                
                <div class="d-xl-flex d-none">
                    @if(auth()->user() && auth()->user()->isTariffActive())
                        <a href="javascript:" data-id="{{ $lot->id }}"
                        class="j-favorite-link lot-preview-link mr-3 @if($lot->in_favorite) in-favorite @endif">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M12.0003 5.1285C10.266 3.80563 8.37235 3.30728 6.65968 3.56553C4.71813 3.8583 3.10627 5.11707 2.39279 7.01392C0.962262 10.8171 3.24623 16.4602 11.4522 21.8365C11.7851 22.0545 12.2155 22.0545 12.5483 21.8365C20.7543 16.4602 23.0383 10.8171 21.6077 7.01392C20.8943 5.11707 19.2824 3.8583 17.3408 3.56553C15.6282 3.30728 13.7345 3.80563 12.0003 5.1285ZM12.0003 19.798C19.5266 14.6868 20.617 10.0608 19.7358 7.71804C19.2829 6.51404 18.2832 5.73023 17.0426 5.54318C15.7982 5.35553 14.215 5.7622 12.6747 7.16931C12.2928 7.51825 11.7077 7.51825 11.3258 7.16931C9.78553 5.7622 8.20231 5.35553 6.95789 5.54318C5.71736 5.73023 4.71761 6.51404 4.26474 7.71804C3.38352 10.0608 4.4739 14.6868 12.0003 19.798Z"/>
                            </svg>
                            <span>В избранное</span>
                        </a>
                    @endif
                    <small class="align-items-center d-flex mr-3 lot-social-share">
                        <div class="ya-share2" 
                            data-curtain 
                            data-size="s" 
                            data-shape="round" 
                            data-limit="0" 
                            data-copy="first"
                            data-more-button-type="short" 
                            data-title="{{ $lot->title }}"
                            data-description="{{ mb_substr($lot->description, 0, 100) }}"
                            data-image="{{ $lot->last_category_lot_img }}"
                            data-url="{{ Request::url() }}"
                            data-services="vkontakte,facebook,odnoklassniki,viber,whatsapp">
                        </div>
                        <span>Поделиться</span>
                    </small>

                    <small class="align-items-center d-flex mr-3 lot-report">
                            <a  data-lot-id="{{ $lot->id }}"
                                data-toggle="modal"
                                data-target="#modalReport"
                                href="javascript:">
                                <svg style="width:24px;height:24px;" version="1.0" xmlns="http://www.w3.org/2000/svg"
                                    width="1280.000000pt" height="1126.000000pt" viewBox="0 0 1280.000000 1126.000000"
                                    preserveAspectRatio="xMidYMid meet">
                                    <g transform="translate(0.000000,1126.000000) scale(0.100000,-0.100000)"
                                    fill="#000000" stroke="none">
                                    <path d="M6201 11240 c-41 -10 -113 -37 -160 -61 -70 -35 -105 -62 -187 -144 -61 -60 -124 -134 -157 -185 -85 -132 -681 -1182 -2962 -5215 -793 -1402 -1714 -3032 -2047 -3620 -333 -589 -617 -1098 -631 -1131 -79 -187 -72 -394 19 -559 15 -28 64 -86 108 -130 91 -90 177 -139 306 -175 l76 -20 5879 2 5880 3 81 27 c363 124 494 499 304 878 -21 43 -899 1580 -1951 3417 -1052 1836 -2308 4029 -2791 4873 -484 844 -909 1580 -946 1635 -118 177 -268 311 -419 373 -125 52 -272 64 -402 32z m1607 -3410 c793 -1383 2019 -3523 2724 -4755 l1283 -2240 -2712 -3 c-1492 -1 -3934 -1 -5427 0 l-2715 3 1666 2945 c3188 5637 3725 6583 3734 6572 4 -4 655 -1139 1447 -2522z"/>
                                    <path d="M6290 7874 c-14 -3 -61 -14 -104 -25 -390 -98 -706 -474 -706 -837 0 -46 22 -254 50 -461 27 -207 113 -857 190 -1446 201 -1535 199 -1517 216 -1581 42 -165 141 -297 271 -361 67 -33 86 -38 168 -41 152 -7 246 30 348 136 99 105 144 224 176 464 11 84 61 462 111 841 49 378 131 996 180 1375 50 378 100 756 111 840 24 182 25 305 4 387 -82 323 -360 599 -693 686 -75 20 -266 33 -322 23z"/>
                                    <path d="M6322 2739 c-345 -44 -594 -371 -552 -726 20 -166 86 -301 204 -410 114 -107 237 -160 391 -170 187 -11 336 47 475 187 134 134 192 273 193 465 1 116 -13 183 -58 280 -120 261 -379 409 -653 374z"/>
                                    </g>
                                </svg>
                                <span>Сообщить об ошибках</span>
                            </a>
                    </small>
                </div>

            </div>
        </div>

        @includeWhen(isset($service_banners), 'includes.services.banners-for-lot')

        <section class="mt-4 pt-3">
            <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="tab1-tab" data-toggle="tab" href="#tab1" role="tab"
                       aria-controls="tab1" aria-selected="true">Информация о лоте</a>
                </li>
                @if(auth()->user() && auth()->user()->isTariffActive())
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="tab2-tab" data-toggle="tab" href="#tab2" role="tab"
                        aria-controls="tab2" aria-selected="true">Информация о торгах</a>
                    </li>
                    @if($lot->trade->attaches || $lot->trade->documents->count())
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="tab3-tab" data-toggle="tab" href="#tab3" role="tab"
                            aria-controls="tab3" aria-selected="true">Документы</a>
                        </li>
                    @endif
                    @if(($lot->PriceReduction || $lot->info->PriceReductionAuction) && !in_array($lot->trade->AuctionType, ['CloseAuction', 'OpenAuction']))
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="tab4-tab" data-toggle="tab" href="#tab4" role="tab"
                            aria-controls="tab4" aria-selected="true">Этапы изменения цены</a>
                        </li>
                    @endif
                @endif
            </ul>

            <div class="tab-content" id="nav-tabContent">

                <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="nav-home-tab">

                    <div class="h2 mt-5">Основные характеристики</div>
                    <ul class="list-unstyled">
                        @foreach($lot->params as $param)
                            <li class="mb-2 row"><b class="col-6 property-name">{{ $param->title }}:</b>
                                <span class="col-6 pl-0">{{ \Illuminate\Support\Str::ucfirst($param->value) }}</span></li>
                        @endforeach
                    </ul>

                    <div class="h2 mt-5">Информация о торгах</div>
                    <ul class="list-unstyled">
                        <li class="mb-2 row">
                            <b class="col-6 property-name">Начальная стоимость лота:</b>
                            @if($lot->StartPrice)
                                <span class="col-6 pl-0">@money($lot->StartPrice) ₽</span>
                            @endif
                        </li>
                        <li class="mb-2 row">
                            <b class="col-6 property-name">Шаг аукциона:</b>
                            @if($lot->StepPricePercent)
                                <span class="col-6 pl-0">{{ $lot->StepPricePercent }} %</span>
                            @elseif($lot->StepPrice)
                                <span class="col-6 pl-0">@money($lot->StepPrice) ₽</span>
                            @else
                                <span class="col-6 pl-0">-</span>
                            @endif
                        </li>
                        <li class="mb-2 row">
                            <b class="col-6 property-name">Размер задатка для участия:</b>
                            @if($lot->AdvancePercent)
                                <span class="col-6 pl-0">{{ $lot->AdvancePercent }} %</span>
                            @elseif($lot->Advance)
                                <span class="col-6 pl-0">@money($lot->Advance) ₽</span>
                            @else
                                <span class="col-6 pl-0">-</span>
                            @endif
                        </li>
                        <li class="mb-2 row">
                            <b class="col-6 property-name">Тип торгов:</b>
                            <span class="col-6 pl-0">{{ $lot->trade->AuctionTypeText }}</span>
                        </li>
                        @if($lot->trade->place && empty($lot->trade->trade_site))
                            <li class="mb-2 row">
                                <b class="col-6 property-name">Площадка:</b>
                                <a target="_blank"
                                   href="{{ $lot->trade->place->full_site_url }}">{{ $lot->trade->place->ShortName }}</a>
                            </li>
                        @endif
                        @if($lot->trade->trade_site && auth()->user() && auth()->user()->isTariffActive())
                            <li class="mb-2 row">
                                <b class="col-6 property-name">ЭТП:</b>
                                <a target="_blank"
                                   href="{{ $lot->trade->trade_site->url }}">{{ $lot->trade->trade_site->TradeSite }}</a>
                            </li>
                        @endif
                        @if($lot->trade->DebtorData && auth()->user() && auth()->user()->isTariffActive())
                            <li class="mb-2 row">
                                <b class="col-6 property-name">Должник:</b>
                                @if($lot->trade->DebtorType == 'DebtorCompany')
                                    <span class="col-6 pl-0">{{ $lot->trade->DebtorData['FullName'] }}</span>
                                @else
                                    <span class="col-6 pl-0">
                                        {{ $lot->trade->DebtorData['LastName'] }}
                                        {{ $lot->trade->DebtorData['FirstName'] }}
                                        @if(isset($lot->trade->DebtorData['MiddleName'])) {{ $lot->trade->DebtorData['MiddleName'] }} @endif
                                    </span>
                                @endif
                            </li>
                            <li class="mb-2 row">
                                <b class="col-6 property-name">ИНН должника:</b>
                                <span class="col-6 pl-0">{{ $lot->trade->DebtorData['INN'] ?? '' }}</span>
                            </li>
                        @endif
                        @if($lot->trade->TradeOrganizer && auth()->user() && auth()->user()->isTariffActive())
                            <li class="mb-2 row">
                                <b class="col-6 property-name">Огранизатор торгов:</b>
                                @if($lot->trade->TradeOrganizerType == 'TradeOrganizerCompany')
                                    <span class="col-6 pl-0">{{ $lot->trade->TradeOrganizer['FullName'] }}
                                        (
                                            ИНН: {{ $lot->trade->TradeOrganizer['INN'] }}
                                            {{ !empty($lot->trade->TradeOrganizer['OGRN']) ? ', ОГРН: ' . $lot->trade->TradeOrganizer['OGRN'] : '' }}
                                        )
                                    </span>
                                @else
                                    <span class="col-6 pl-0">
                                        {{ $lot->trade->TradeOrganizer['LastName'] }}
                                        {{ $lot->trade->TradeOrganizer['FirstName'] }}
                                        {{ @$lot->trade->TradeOrganizer['MiddleName'] }}
                                        (
                                            ИНН: {{  $lot->trade->TradeOrganizer['INN'] }}
                                        {{ !empty($lot->trade->TradeOrganizer['SNILS']) ? ', СНИЛС: ' . $lot->trade->TradeOrganizer['SNILS'] : '' }}
                                        )
                                    </span>
                                @endif
                            </li>
                        @endif
                        @if($lot->trade->ArbitrManager && auth()->user() && auth()->user()->isTariffActive())
                            <li class="mb-2 row">
                                <b class="col-6 property-name">Арбитражный управляющий:</b>
                                    <span class="col-6 pl-0">
                                        {{ $lot->trade->ArbitrManager['LastName'] }}
                                        {{ $lot->trade->ArbitrManager['FirstName'] }}
                                        {{ @$lot->trade->ArbitrManager['MiddleName'] }}
                                        ( ИНН: {{ $lot->trade->ArbitrManager['INN'] }} )
                                    </span>
                            </li>
                        @endif
                    </ul>

                    @includeWhen(!empty($map), 'includes.map')

                    <lot-info html = "{{ $lot->TradeObjectHtml }}"></lot-info>

                </div>

                <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="nav-profile-tab">

                    @if($lot->trade->Text)

                        {!! $lot->trade->Text !!}

                    @else

                        <div class="h1 mt-5">
                            Порядок оформления участия в торгах, перечень документов участника и требования к оформлению:
                        </div>
                        {!! $lot->SaleAgreement !!}

                        <div class="h1 mt-5">
                            Сроки уплаты покупной цены по итогам проведения торгов:
                        </div>
                        {!! $lot->PaymentInfo !!}

                    @endif

                </div>

                <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div class="h1 mt-5">Файлы прикрепленные к торгам</div>
                    @if($lot->trade)
                        <ul class="list-unstyled">
                            @foreach($lot->trade->attaches as $path)
                                <li class="mb-2">
                                    <file-link url="{{ Storage::disk('trade')->url($path) }}"></file-link>
                                </li>
                            @endforeach

                                @foreach($lot->trade->documents as $document)
                                    <li class="mb-2">
                                        <file-link
                                            url="{{ $document->url }}"
                                            file-name="{{ $document->name }}"
                                        ></file-link>
                                    </li>
                                @endforeach

                        </ul>
                    @endif
                </div>

                @if( !in_array($lot->trade->AuctionType, ['CloseAuction', 'OpenAuction']))
                    <div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="h1 mt-5">Информация о снижении цены.</div>
                        {!! $lot->PriceReduction !!}


                        @if($lot->info->PriceReductionAuction)

                            <div class="mt-5">
                                @foreach($lot->info->PriceReductionAuctionRows as $row)
                                    <li class="mb-2">
                                        {{ $row }}
                                    </li>
                                @endforeach
                            </div>

                        @endif

                    </div>
                @endif
            </div>
        </section>

        <div class="row mt-4 pt-3">
            <div class="col-12">
                <div class="h2">Похожие лоты</div>
                <div class="slick-variable-width-lot row">
                    @foreach($lot->similar()->limit(5)->get() as $similar)
                        <div class="p-2" style="display: none">
                            @component('components.catalog.item-small', ['lot' => $similar]) @endcomponent
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row my-5">
            <div class="col">
                @if($category)
                <get-banners :limit="1" variant="horizontal" category="{{$category->id."|".$category->parent_id}}" position="16"></get-banners>
                @endif
            </div>
        </div>
    </div>
@endsection

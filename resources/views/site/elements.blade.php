@extends('layouts.site')

@php
    $colors = [
        'primary' => 'Основной цвет',
        'secondary' => 'Дополнительный',
        'success' => null,
        'info' => null,
        'warning' => null,
        'danger' => null,
        'light' => null,
        'dark' => null,
    ];
    $shadows = [
        'shadow-sm',
        'shadow',
        'shadow-lg',
    ];
@endphp

@section('content')
    <div class="content">

        {{--@if(false)--}}

        <section class="my-5">
            <h2>1. Заголовки с параграфами</h2>
            <div class="row">
                <div class="col-md-6">

                    <h1>Header H1</h1>
                    <p>Regular text - Текстовый массив. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Adipisci aliquam illo impedit inventore optio? At
                        possimus quisquam ratione. Beatae exercitationem maxime necessitatibus optio quae quod,
                        repudiandae sequi
                        soluta ut voluptatibus?</p>

                    <h2>Title H2</h2>
                    <p>Regular text - Текстовый массив. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Adipisci aliquam illo impedit inventore optio? At
                        possimus quisquam ratione. Beatae exercitationem maxime necessitatibus optio quae quod,
                        repudiandae sequi
                        soluta ut voluptatibus?</p>

                    <h3>Title H3/H4/H5/H6</h3>
                    <p>Regular text - Текстовый массив. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Adipisci aliquam illo impedit inventore optio? At
                        possimus quisquam ratione. Beatae exercitationem maxime necessitatibus optio quae quod,
                        repudiandae sequi
                        soluta ut voluptatibus?</p>

                </div>
                <div class="col">

                    <p>Regular text - Текстовый массив. (p)</p>

                    <p>
                        <strong>Chips text - Хлебные крошки (strong)</strong>
                    </p>

                    <p>
                        <small>Private text - Уточнения (small)</small>
                    </p>

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">Главная</a></li>
                            <li class="breadcrumb-item"><a href="#">Раздел</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Текущая страница</li>
                        </ol>
                    </nav>

                </div>
            </div>
        </section>

        <section class="my-5">
            <h2>2. Отступы и размеры</h2>
            <div class="row">
                <div class="col-md-6">
                    <p>
                        Размеры элементов дизайна кратны <br>
                        8(4) пикселям, исключение - текст, так как высота строки задается шрифтом. <br>
                        <br>
                        Ширина объектов также может задаваться шириной колонки сетки и не быть кратной 8 пикс.
                    </p>
                </div>
                <div class="col">
                    <div class="bg-success">
                        <div class="p-2 bg-light ml-1">margin left: 1</div>
                        <div class="p-2 bg-light ml-2">margin left: 2</div>
                        <div class="p-2 bg-light ml-3">margin left: 3</div>
                        <div class="p-2 bg-light ml-4">margin left: 4</div>
                        <div class="p-2 bg-light ml-5">margin left: 5</div>
                    </div>
                </div>
            </div>
        </section>

        <section class="my-5">
            <h2>3. Иконки</h2>
            <div class="row">
                <div class="col-md-6">
                    <h3>Icons 16 pixels</h3>
                    <p>
                        @php
                            $icons = [
                                'circle-arr-up', 'circle-arr-down','heart','share','user','question','portfolio',
                                'mail', 'loupe', 'doc','chain', 'file', 'config','map-point','menu','like','close',
                                'circle-ok','hummer','wallet','photo','calendar','clock','exit','pen','trash',
                                'megaphone', 'star'
                            ];
                        @endphp
                        @foreach($icons as $icon)
                            <img src="{{ asset('img/icons/16px/'.$icon.'.svg') }}" alt="{{ $icon }}"
                                 title="{{ $icon }}">
                        @endforeach
                    </p>
                </div>
                <div class="col">
                    <h3>Icons 24 pixels</h3>
                    <p>
                        @php
                            $icons = [
                                'heart','share','like','dislike','loupe','doc','file','close','photo','exit','no-heart',
                                'pen','trash','star'
                            ];
                        @endphp
                        @foreach($icons as $icon)
                            <img src="{{ asset('img/icons/24px/'.$icon.'.svg') }}" alt="{{ $icon }}"
                                 title="{{ $icon }}">
                        @endforeach
                    </p>
                </div>
            </div>
        </section>

        <section class="my-5">
            <h2>4. Цвета</h2>
            <ul class="list-unstyled">
                @foreach($colors as $color => $text)
                    <li class="d-flex align-items-center my-1">
                        <div class="d-inline-block img-thumbnail rounded-sm bg-active bg-{{ $color }}"
                             style="width: 30px;height: 30px;"></div>
                        <span class="ml-2"><b class="text-{{ $color }}">{{$color}}: </b>{{ $text }}</span>
                    </li>
                @endforeach
            </ul>
        </section>

        <section class="my-5">
            <h2>5. Тени</h2>

            <div class="row">
                @foreach($shadows as $shadow)
                    <div class="col">
                        <div class="d-flex align-items-center flex-column">
                            <div class="rounded-sm {{ $shadow }}" style="width: 50px; height: 50px;"></div>
                            <span class="mt-2">{{ $shadow }}</span>
                        </div>
                    </div>
                @endforeach
                @foreach($shadows as $shadow)
                    <div class="col">
                        <div class="d-flex align-items-center flex-column">
                            <div class="rounded-sm bg-primary {{ $shadow }}" style="width: 50px; height: 50px;"></div>
                            <span class="mt-2">{{ $shadow }}</span>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>

        <section class="my-5">
            <h2>6. Кнопки</h2>
            <p>Высота кнопки 40 пикс, отступ слева и справа по 24 писк. В некоторых случаях ширина кнопки равняется по
                колонкам (отступ должен быть больше 24 пикс, но не меньше). Если в кнопке есть иконка, ее отступ от
                текста 8 пикс, отступы до края кнопки считаются совместно у иконки и текста</p>
            <p>Если рядом находятся 2 кнопки - расстояние между ними 12 пикс. <code>.mr-2</code></p>
            <div class="row">
                <div class="col">
                    <h3>Small</h3>
                    <div class="mb-2">
                        <button class="btn btn-sm">Default</button>
                        <button class="btn btn-sm disabled">Default</button>
                    </div>
                    @foreach($colors as $color => $info)
                        <div class="mb-2">
                            <button class="btn btn-sm btn-{{ $color }}">{{ $color }}</button>
                            <button class="btn btn-sm btn-{{ $color }} disabled">{{ $color }}</button>
                        </div>
                    @endforeach
                </div>
                <div class="col">
                    <h3>Default</h3>
                    <div class="mb-2">
                        <button class="btn ">Default</button>
                        <button class="btn  disabled">Default</button>
                    </div>
                    @foreach($colors as $color => $info)
                        <div class="mb-2">
                            <button class="btn btn-{{ $color }}">{{ $color }}</button>
                            <button class="btn btn-{{ $color }} disabled">{{ $color }}</button>
                        </div>
                    @endforeach
                </div>
                <div class="col">
                    <h3>Big</h3>
                    <div class="mb-2">
                        <button class="btn btn-lg">Default</button>
                        <button class="btn btn-lg disabled">Default</button>
                    </div>
                    @foreach($colors as $color => $info)
                        <div class="mb-2">
                            <button class="btn btn-lg btn-{{ $color }}">{{ $color }}</button>
                            <button class="btn btn-lg btn-{{ $color }} disabled">{{ $color }}</button>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

        <section class="my-5">
            <h2>7.1 Загруженные файлы</h2>
            <p>Высота элемента 32 пикс, отступ слева и справа по 12 пикс. Отступ текста от иконки 8 пикс, отступ до
                крестика 24 пикс</p>

            <div>
                <file-link url="/storage/docs/ДКП.doc" can-delete></file-link>
                <file-link url="/storage/docs/Документ 1.doc" can-delete></file-link>
                <file-link url="/storage/docs/Документ 2.doc" can-delete></file-link>
                <file-link url="/storage/photos/s7a5d454a.jpg"></file-link>
            </div>

        </section>

        <section class="my-5">
            <h2>7.2 Поле загрузки файла</h2>

            <h3>Фотографии</h3>
            <file-uploader
                class="mb-4"
            ></file-uploader>

            <h3>Добавление изображения</h3>
            <file-uploader
                class="mb-4"
                hint="Размер изображения 255х510 пикселей, форматы .jpg .png .gif"
            ></file-uploader>

            <h3>Фотографии</h3>
            <file-uploader
                class="mb-4"
                :files="['/img/img2.jpg', '/img/img2.jpg', '/img/img2.jpg', '/img/img2.jpg', '/img/img2.jpg', '/img/img2.jpg', '/img/img2.jpg']"
            ></file-uploader>

        </section>

        <section class="my-5">
            <h2>8 Поля ввода текста</h2>
            <p>Высота элемента 40 пикс, отступ слева 12 пикс. </p>
            <div class="row">
                <div class="col-md-6">
                    <form>
                        <div class="form-group">
                            <label for="inputName">Отчество</label>
                            <input type="text" class="form-control" id="inputName"
                                   aria-describedby="nameHelp" placeholder="Пустое поле">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">E-mail</label>
                            <input type="email" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp" value="email@test.ru">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail2">E-mail</label>
                            <input type="email" class="form-control" id="exampleInputEmail2"
                                   aria-describedby="emailHelp2" placeholder="E-mail"
                                   value="email@test.ru">
                            <small id="emailHelp2" class="form-text text-muted">Подсказка для данного поля ввода текста.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail3">E-mail</label>
                            <input type="email" class="form-control is-invalid" id="exampleInputEmail3"
                                   value="Не верная информация" aria-describedby="emailHelp3">
                            <div class="invalid-feedback">Сообщение об ошибке.</div>
                        </div>
                    </form>
                </div>
            </div>

        </section>

        <section class="my-5">
            <h2>9 Элементы форм</h2>
            <div class="row">
                <div class="col-md-6">
                    <form>
                        <div class="form-group">
                            <label>Checkbox</label>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Check this custom checkbox</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck2" checked>
                                <label class="custom-control-label" for="customCheck2">Check this custom checkbox</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Radio buttons</label>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                <label class="custom-control-label" for="customRadio1">Toggle this custom radio</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" checked>
                                <label class="custom-control-label" for="customRadio2">Or toggle this other custom radio</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Raiting start</label>
                            <div>
                                <rating :stars="5" :value="2"></rating>
                            </div>
                            <div>
                                <rating :stars="5" :value="3" editable></rating>
                            </div>
                            <div>
                                <rating :stars="10" :value="4" editable></rating>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">
                    <form>
                        <div class="form-group">
                            <label>Default select</label>
                            <select class="custom-select">
                                <option selected>Open this select menu</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Self vue select (Select item)</label>
                            <div>
                                <form-multi-select
                                    title="Checked items"
                                    show-btn show-search
                                    multicheck
                                    :items="['Item 1', 'Item 2', 'Item 3', 'Item 4']"></form-multi-select>
                                <form-multi-select
                                    title="Search and btn"
                                    show-btn show-search
                                    :items="['Item 1', 'Item 2', 'Item 3', 'Item 4']"></form-multi-select>
                                <form-multi-select
                                    title="Has button"
                                    show-btn
                                    :items="['Item 1', 'Item 2', 'Item 3', 'Item 4']"></form-multi-select>
                                <form-multi-select
                                    title="Select value"
                                    :items="['Item 1', 'Item 2', 'Item 3', 'Item 4']"></form-multi-select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Small custom select</label>
                            <div>
                                <form-multi-select
                                    title="Select value"
                                    size="sm"
                                    show-search
                                    :items="['Item 1', 'Item 2', 'Item 3', 'Item 4']"></form-multi-select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Small wide custom select</label>
                            <div>
                                <form-multi-select
                                    title="Select value"
                                    size="sm" wide
                                    :items="['Item 1', 'Item 2', 'Item 3', 'Item 4']"></form-multi-select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </section>

        <section class="my-5">
            <h2>10 Dropdown buttons</h2>
            <div class="row">
                <div class="col-md-6">

                    <div class="dropdown d-inline-block">
                        <button class="btn btn-outline-secondary btn-sm dropdown-toggle" type="button"
                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            Dropdown button
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>

                    <div class="dropdown d-inline-block">
                        <button class="btn btn-outline-dark btn-sm dropdown-toggle" type="button"
                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            Dropdown button
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="my-5">
            <h2>11 Tables</h2>
            <table class="table table-borderless table-striped table-hover">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">First</th>
                    <th scope="col">Last</th>
                    <th scope="col">Handle</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td colspan="2">Larry the Bird</td>
                    <td>@twitter</td>
                </tr>
                </tbody>
            </table>
        </section>
        {{--@endif--}}

        {{--<section class="my-5">
            <h2>12 Drug images/files</h2>
            <div class="row">
                <div class="col">

                    <file-uploader
                        class="mb-4"
                    ></file-uploader>

                </div>
            </div>
        </section>--}}


        <div style="margin-bottom: 400px;"></div>
    </div>
@endsection


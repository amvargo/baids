@extends('layouts.site')

@section('title', __('Размещение рекламы'))

@section('breadcrumb')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active"><a href="{{ route('site.advertising.index') }}">Реклама</a></li>
        </ol>
    </nav>
@endsection



    @section('content')

        @if($services)

            @auth
                <advertising
                    :select_categories="{{ $categories->toJson() }}"
                    :services="{{ json_encode($services) }}"
                    :edit_element="{{ json_encode($edit_element) }}"
                ></advertising>
            @else
                @include('includes.auth.login')
            @endauth

        @else

            <h2 class="m-5 text-center">К сожалению данная услуга отключена</h2>

        @endif



    @endsection



@extends('layouts.site')

@section('title', __('Online consulting'))

@section('content')
    <div class="content">

        <div class="mb-4">
            <h2>@lang('Online consulting')</h2>
            @include('includes.consulting.create')
        </div>

    </div>
@endsection

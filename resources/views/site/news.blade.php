@extends('layouts.site')
@section('content')
<div class="content">
    <div class="row">
        <div class="col">
            <div class="h1">Актуальные новости</div>
            @if($sorted->count())
                @foreach ($sorted as $section)
                <section class="news row rounded shadow p-3 mb-5">
                    <div class="col">
                        <div class="mb-5"><b>Дата публикации: {{ $section->created_at->format('d.m.Y') }}</b></div>
                        <img src="{{ $section->url_image }}" class="img-fluid">
                    </div>
                    <div class="col-9">
                        <p class="h2">{{ $section->title }}</p>
                        <div class="content_block hide">
                            <p class="news-text">{!! nl2br($section->description) !!}</p>
                        </div>
                        <button class="btn btn-primary float-right content_toggle">Читать полностью</button>
                    </div>
                </section>
                @endforeach
            @else
                <h2 class="mt-3">Скоро здесь будут новости</h2>
            @endif
        </div>
    </div>
</div>
@endsection
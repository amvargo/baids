@extends('layouts.cabinet')

@section('content')
    <nav class="d-none">
        <div class="nav nav-pills nav-pills-cabinet mb-3" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#favorites" role="tab"
               aria-controls="nav-home" aria-selected="true">Избранные лоты</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">

        <div class="tab-pane fade show active" id="favorites" role="tabpanel" aria-labelledby="nav-home-tab">
            <section>
                @if(blank($lots))
                    @component('components.alert', ['type' => 'info'])
                        <span>Лотов пока нет.</span>
                    @endcomponent
                @endif

                <div class="section-container py-2">
                    @each('components.catalog.item', $lots, 'lot')
                </div>

                {{ $lots->links('pagination.default') }}
            </section>
        </div>
    </div>
@endsection

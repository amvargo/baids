@extends('layouts.cabinet')

@section('content')
    <nav>
        <div class="nav nav-pills nav-pills-cabinet mb-3" id="nav-tab" role="tablist">
            <a class="nav-item nav-link @if($lots->count()) active @endif" id="nav-home-tab" data-toggle="tab"
               href="#my-lots" role="tab"
               aria-controls="nav-home" aria-selected="true">Мои лоты</a>
            <a class="nav-item nav-link @if(!$lots->count()) active @endif" id="nav-profile-tab" data-toggle="tab"
               href="#addition-lots" role="tab"
               aria-controls="nav-profile" aria-selected="false">Добавление лота</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">

        @if(session('success'))
            @component('components.alert', ['type' => 'success'])
                <span>{{session('success')}}</span>
            @endcomponent
            @php(\Illuminate\Support\Facades\Session::remove('success'))
        @endif

        <div class="tab-pane show @if($lots->count()) active @endif" id="my-lots" role="tabpanel"
             aria-labelledby="nav-home-tab">
            <section>
                @if(blank($lots))
                    @component('components.alert', ['type' => 'info'])
                        <span>У вас нет лотов.</span>
                    @endcomponent
                @endif

                <div class="section-container py-2">
                    @each('components.catalog.item', $lots, 'lot')
                </div>

                {{ $lots->links('pagination.default') }}
            </section>
        </div>
        <div class="tab-pane @if(!$lots->count()) active @endif" id="addition-lots" role="tabpanel"
             aria-labelledby="nav-profile-tab">

            @php($user = auth()->user())

            @if($user->can('create', \App\Models\Trade::class))
                <form-self-lot-creator
                    :arbitr='@json($user->getArbitrDataArray())'
                ></form-self-lot-creator>
            @else
                <span>Для добавления лота необходимо отправить анкету на организатора торгов</span>
            @endif

        </div>
    </div>
@endsection

@extends('layouts.cabinet')

@section('content')
    <nav>
        <div class="nav nav-pills nav-pills-cabinet mb-3" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#notification" role="tab"
               aria-controls="nav-home" aria-selected="true">Уведомления системы</a>
            <a class="nav-item nav-link" id="nav-profile-tab" href="{{ route('cabinet.mailings') }}">Рассылка лотов</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">

        <div class="tab-pane fade show active" id="notification" role="tabpanel" aria-labelledby="nav-home-tab">
            <section>
                @if(blank($notifications))
                    @component('components.alert', ['type' => 'info'])
                        <span>Оповещений пока нет.</span>
                    @endcomponent
                @endif

                <div class="section-container py-2">
                    @foreach($notifications->items() as $notification)
                        <div class="shadow rounded p-4 mb-3">
                            <div class="mb-3" style="line-height: 1;">
                                <small class="float-right">
                                    @if($notification->is_read)
                                        <b class="text-grey">Прочитано</b>
                                    @else
                                        <b class="text-primary">• Новое сообщение</b>
                                    @endif
                                </small>
                                <small>
                                    {{ $notification->created_at->format('d.m.Y') }}
                                    <span class="ml-2">{{ $notification->created_at->format('H:i') }}</span>
                                </small>
                            </div>
                            <div>
                                <div class="h3 mb-1">{{ $notification->title }}</div>
                                <div>{!! nl2br($notification->body) !!}</div>
                                @if($notification->order_id !== null)
                                    <div>
                                        @if(empty($notification->order->assessment))
                                            <a
                                                data-toggle="modal" data-target="#modalEvalPopup"
                                                href="#" class="btn btn-sm btn-primary mt-3 evalOpen"
                                                data-order-id="{{ $notification->order_id }}"
                                                data-order-title="{{ $notification->order->service->title }}"
                                            >Оценить услугу</a>
                                        @else
                                            <button disabled class="btn btn-sm btn-primary mt-3">Вы оценили эту услугу</button>
                                        @endif
                                    </div>
                                @endif
                            </div>
                            @can('delete', $notification->notification_read)
                                <form
                                    class="float-right"
                                    method="post"
                                    action="{{ route('cabinet.notifications.destroy', $notification) }}">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-sm btn-link shadow-none p-0 mb-4" type="submit">Удалить
                                    </button>
                                </form>
                            @endcan
                        </div>
                    @endforeach
                </div>

                {{--@each('includes.catalog.item', $notifications, 'notification')--}}
                {{ $notifications->links('pagination.default') }}
            </section>
        </div>
    </div>

    {{-- Modal - order evaluation popup --}}
    <div class="modal fade" id="modalEvalPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <p class="h2 header-text-popup">Оцените качество оказанной услуги <span></span></p>

                    <button type="button" class="close close-rt" data-dismiss="modal" aria-label="Close">
                        <img src="{{ asset('img/icons/16px/close.svg') }}" alt="close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="fade show active" id="home" >
                        <form method="POST" action="{{ route('cabinet.order-evaluation') }}" id="modalLoginForm">
                            @method("PUT")
                            @csrf

                            <div class="form-group">
                                <label for="inp_assessment">Оценка</label>
                                <div class="rating-area">
                                    <input type="radio" id="star-5" name="assessment" value="5">
                                    <label for="star-5" title="Оценка «5»"></label>
                                    <input type="radio" id="star-4" name="assessment" value="4">
                                    <label for="star-4" title="Оценка «4»"></label>
                                    <input type="radio" id="star-3" name="assessment" value="3">
                                    <label for="star-3" title="Оценка «3»"></label>
                                    <input type="radio" id="star-2" name="assessment" value="2">
                                    <label for="star-2" title="Оценка «2»"></label>
                                    <input type="radio" id="star-1" name="assessment" value="1">
                                    <label for="star-1" title="Оценка «1»"></label>
                                </div>
{{--                                <input--}}
{{--                                    type="number" min="1" max="5" value="5" class="form-control @error('assessment') is-invalid @enderror"--}}
{{--                                    id="inp_assessment" name="assessment" required--}}
{{--                                    autocomplete="assessment" placeholder="Введите оценку">--}}
                                <input type="hidden" name="order_id" class="order-id">
                                @error('assessment')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="inp_comment">Отзыв</label>
                                <textarea name="comment" id="inp_comment" class="comment form-control" rows=5 placeholder="Оставьте здесь свой отзыв"></textarea>
                                @error('comment')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="pt-3">
                                <button type="submit" class="btn btn-block btn-primary">
                                    <span class="spinner-border spinner-border-sm d-none" role="status"
                                          aria-hidden="true"></span>
                                    Отправить оценку
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

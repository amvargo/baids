@extends('layouts.cabinet')

@section('content')
    <nav class="d-none">
        <div class="nav nav-pills nav-pills-cabinet mb-3" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#payments" role="tab"
               aria-controls="nav-home" aria-selected="true">История платежей</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">

        <div class="tab-pane fade show active" id="payments" role="tabpanel" aria-labelledby="nav-home-tab">
            <section>

                @if(session('success'))
                    @component('components.alert', ['type' => 'success'])
                        <span>{{session('success')}}</span>
                    @endcomponent
                @endif

                @if(session('warning'))
                    @component('components.alert', ['type' => 'warning'])
                        <span>{{session('warning')}}</span>
                    @endcomponent
                @endif

                @if(blank($payments))
                    @component('components.alert', ['type' => 'info'])
                        <span>Платежей пока нет.</span>
                    @endcomponent
                @endif

                <div class="section-container py-2 table-responsive-md">
                    <table class="table table-borderless table-striped table-hover">
                        <thead>
                        <tr>
                            <th scope="col">Дата платежа</th>
                            <th scope="col">Оплаченная услуга</th>
                            <th scope="col">Сумма</th>
                            <th scope="col">Способ оплаты</th>
                            <th scope="col">Статус платежа</th>
                        </tr>
                        </thead>
                        <tbody>

                        @forelse($payments as $payment)
                            <tr>
                                <td>{{ $payment->created_at->setTimezone($user_timezone)->toDateTimeString() }}</td>
                                <td>{{ $payment->title }}</td>
                                <td class="text-right text-nowrap">@money($payment->amount) ₽</td>
                                <td class="text-right">{{ $payment->info['type'] ?? '' }}</td>
                                <td class="text-right">
                                    <span class="text-{{ $payment->status_color }}">
                                        {{ $payment->status_name }}
                                    </span>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td>Платежей не найдёно</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>

                {{ $payments->links('pagination.default') }}
            </section>
        </div>
    </div>
@endsection

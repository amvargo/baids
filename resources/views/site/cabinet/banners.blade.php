@extends('layouts.cabinet')

@section('content')
    <section>
        @if(empty($banners))
            @component('components.alert', ['type' => 'info'])
                <span>Реламных баннеров пока нет.</span>
            @endcomponent

        @else

            <div class="section-container">
                <div class="row">

                    <div class="table-responsive-lg">

                        <banners-table

                            :banners="{{ json_encode($banners->toArray()) }}"

                        ></banners-table>

                    </div>

                </div>
                {{ $banners->links('pagination.banners') }}
            </div>


        @endif

    </section>
@endsection


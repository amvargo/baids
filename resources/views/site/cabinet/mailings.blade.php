@extends('layouts.cabinet')

@section('content')
    <nav>
        <div class="nav nav-pills nav-pills-cabinet mb-3" id="nav-tab" role="tablist">
            <a class="nav-item nav-link" id="nav-home-tab" href="{{ route('cabinet.notifications.index') }}">Уведомления
                системы</a>
            <a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#messages" role="tab"
               aria-controls="nav-profile" aria-selected="false">Рассылка лотов</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">

        <div class="tab-pane fade show active pb-5 mb-5" id="messages" role="tabpanel" aria-labelledby="nav-profile-tab">

            @cannot('create', \App\Models\UserMailingLot::class)
                <div class="text-center my-4">
                    <div class="h2">Вы еще не подписались на нашу рассылку?</div>
                    <p>Подписавшиеся пользователи видят на этой странице персональную
                        подборку лотов по заданным ими фильтрам</p>
                    <div class="d-flex justify-content-between link-wrapper">
                        
                    @if(auth()->user() && auth()->user()->isTrialMailingUnused())
                        <div>
                            <p class="h2">Пробная подписка</p>
                            <a
                                href="#"
                                class="btn btn-primary"
                                data-duration = "trial"
                                data-toggle="modal"
                                data-target="#modalServiceTrial"
                                data-service="{{ $service_mailing->title }}"
                                data-id="{{ $service_mailing->id }}"
                            >
                                Подписаться
                            </a>
                        </div>
                        @endif

                       <div>
                            <p class="h2">Подписка на 1 месяц</p>
                            <a
                                href="#"
                                class="btn btn-primary"
                                data-toggle="modal"
                                data-target="#modalService"
                                data-service="{{ $service_mailing->title }}"
                                data-id="{{ $service_mailing->id }}"
                            >
                                Подписаться
                                <b>({{ $service_mailing->price }} ₽)</b>
                            </a>
                       </div>

                        <div>
                            <p class="h2">Подписка на 3 месяца</p>
                            <a
                                href="#"
                                class="btn btn-primary"
                                data-duration = "3"
                                data-toggle="modal"
                                data-target="#modalService"
                                data-service="{{ $service_mailing->title }}"
                                data-id="{{ $service_mailing->id }}"
                            >
                                Подписаться
                                <b>({{ $service_mailing->price * 1.5 }} ₽)</b>
                            </a>
                        </div>

                        <div>
                            <p class="h2">Подписка на 1 год</p>
                            <a
                                href="#"
                                class="btn btn-primary"
                                data-duration = "12"
                                data-toggle="modal"
                                data-target="#modalService"
                                data-service="{{ $service_mailing->title }}"
                                data-id="{{ $service_mailing->id }}"
                            >
                                Подписаться
                                <b>({{ $service_mailing->price * 4 }} ₽)</b>
                            </a>
                        </div>
                    </div>


                </div>


            @endcannot

            @can('create', \App\Models\UserMailingLot::class)
                <service-mailing-config
                    :categories='{!! $categories->toJson() !!}'
                ></service-mailing-config>
            @endcan

        </div>
    </div>
@endsection

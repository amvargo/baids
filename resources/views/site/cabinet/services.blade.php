@extends('layouts.cabinet')

@section('content')
    <nav>
        <div class="nav nav-pills nav-pills-cabinet mb-3" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#processOrders" role="tab"
               aria-controls="nav-home" aria-selected="true">Текущие услуги</a>
            <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#doneOrders" role="tab"
               aria-controls="nav-home" aria-selected="true">Полученные услуги</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">

        <div class="tab-pane fade show active" id="processOrders" role="tabpanel" aria-labelledby="nav-home-tab">
            <section>
                @if(blank($processOrders))
                    @component('components.alert', ['type' => 'info'])
                        <span>Вы ещё не пользовались нашими услугами.</span>
                    @endcomponent
                @else
                    <services-table

                        :services="{{ json_encode($processOrders->toArray()) }}"

                    ></services-table>
{{--                    <div class="section-container py-2 table-responsive-md">--}}
{{--                        <table class="table table-borderless table-striped table-hover">--}}
{{--                            <thead>--}}
{{--                            <tr>--}}
{{--                                <th scope="col">Дата</th>--}}
{{--                                <th scope="col">Услуга</th>--}}
{{--                                <th scope="col">Лот</th>--}}
{{--                                <th scope="col">Оплата</th>--}}
{{--                                <th scope="col">Статус</th>--}}
{{--                            </tr>--}}
{{--                            </thead>--}}
{{--                            <tbody>--}}

{{--                            @foreach($processOrders as $order)--}}
{{--                                <tr>--}}
{{--                                    <td>{{ $order->created_at->setTimezone($user_timezone)->toDateTimeString() }}</td>--}}
{{--                                    <td>{{ $order->service_title }}</td>--}}
{{--                                    <td>--}}
{{--                                        <a {{ $order->trade_lot_id ? 'href='.route('site.catalog.lot.show', ['lot' => $order->trade_lot_id]) : '' }}--}}
{{--                                           target="_blank"--}}
{{--                                        >{{ $order->trade_lot_id ? 'Лот #' . $order->trade_lot_id : '-' }}--}}
{{--                                        </a>--}}
{{--                                    </td>--}}
{{--                                    <td>--}}
{{--                                        <span class="text-{{ $order->payment_id ? 'success' : 'danger' }}">--}}
{{--                                            @if($order->payment_id)--}}
{{--                                            Оплачено--}}
{{--                                            @else--}}
{{--                                            <b-button @click="getPayment($order)" variant="outline-primary">Оплатить</b-button>--}}
{{--                                            @endif--}}
{{--                                        </span></td>--}}
{{--                                    <td>--}}
{{--                                        <span class="text-{{ $order->status_color }}">--}}
{{--                                            {{ $order->status_name }}--}}
{{--                                        </span>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
{{--                            </tbody>--}}
{{--                        </table>--}}
{{--                    </div>--}}
                @endif
            </section>
        </div>

        <div class="tab-pane fade show" id="doneOrders" role="tabpanel" aria-labelledby="nav-home-tab">
            <section>
                @if(blank($doneOrders))
                    @component('components.alert', ['type' => 'info'])
                        <span>Ещё нет ни одной завершённой услуги.</span>
                    @endcomponent
                @else
                    <div class="section-container py-2 table-responsive-md">
                        <table class="table table-borderless table-striped table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Дата начала</th>
                                <th scope="col">Дата завершения</th>
                                <th scope="col">Услуга</th>
                                <th scope="col">Лот</th>
                                <th scope="col">Оплата</th>
                                <th scope="col">Статус</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($doneOrders as $order)
                                <tr>
                                    <td>{{ $order->created_at->setTimezone($user_timezone)->toDateTimeString() }}</td>
                                    <td>{{ $order->service_end }}</td>
                                    <td>{{ $order->service_title }}</td>
                                    <td>
                                        <a {{ $order->trade_lot_id ? 'href='.route('site.catalog.lot.show', ['lot' => $order->trade_lot_id]) : '' }}
                                           target="_blank"
                                        >{{ $order->trade_lot_id ? 'Лот #' . $order->trade_lot_id : '-' }}
                                        </a>
                                    </td>
                                    <td>
                                        <span class="text-{{ $order->payment_id ? 'success' : 'danger' }}">
                                            {{ $order->payment_id ? 'Оплачено' : 'Не оплачено'}}
                                        </span>
                                    </td>
                                    <td>
                                        <span class="text-{{ $order->status_color }}">
                                            {{ $order->status_name }}
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </section>
        </div>

    </div>
@endsection

@extends('layouts.cabinet')

@section('content')
    <nav>
        <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
            <li class="nav-item active" role="presentation">
                <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                   aria-controls="profile" aria-selected="true">Личные данные</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="password-tab" data-toggle="tab" href="#password" role="tab"
                   aria-controls="password" aria-selected="true">Смена пароля</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="org-profile-tab" data-toggle="tab" href="#org-profile" role="tab"
                   aria-controls="org-profile" aria-selected="true">Настройки организатора торгов</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="requisites-tab" data-toggle="tab" href="#requisites" role="tab"
                   aria-controls="requisites" aria-selected="true">Реквизиты</a>
            </li>
        </ul>
    </nav>
    @if(session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="tab-content" id="nav-tabContent">

        <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="nav-home-tab">
            <form method="post" action="{{ route('cabinet.change-profile') }}">
                @csrf @method('put')

                <div class="row">
                    <div class="col-lg-6 mb-lg-4 order-lg-2">
                        <div class="rounded description-item px-4 py-3 mb-4"
                             style="line-height: 1.2;margin-top: 1.3rem;">
                            <small>
                                <p>Для удобной работы с порталом при покупке лота, пожалуйста, укажите Ваши корректные
                                    данные.</p>
                                <p>Мы гарантируем конфиденциальность данных, не передаем их третьим лицам и не рассылаем
                                    спам.</p>
                                <p class="mb-0">Подробнее о <a href="{{ route('site.documents', ['tab' => 'policy']) }}" target="_blank">положении по обработке персональных данных</a>
                                </p>
                            </small>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-6">

                        <div class="form-group">
                            <label for="inp_last_name">@lang('Last name')</label>
                            <input
                                placeholder="Введите фамилию"
                                type="text" class="form-control @error('last_name') is-invalid @enderror"
                                id="inp_last_name" name="last_name" value="{{ old('last_name', $user->last_name) }}"
                                required>
                            @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inp_name">@lang('Name')</label>
                            <input
                                placeholder="Введите имя"
                                type="text" class="form-control @error('name') is-invalid @enderror"
                                id="inp_name" name="name" value="{{ old('name', $user->name) }}" required>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inp_middle_name">@lang('Middle name')</label>
                            <input
                                placeholder="Введите отчество"
                                type="text" class="form-control @error('middle_name') is-invalid @enderror"
                                id="inp_middle_name" name="middle_name"
                                value="{{ old('middle_name', $user->middle_name) }}" required>
                            @error('middle_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inp_email">@lang('E-mail')</label>
                            <input
                                placeholder="Введите e-mail"
                                type="email" class="form-control @error('email') is-invalid @enderror"
                                id="inp_email" name="email" value="{{ old('email', $user->email) }}" required
                                autocomplete="email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inp_phone">@lang('Phone')</label>
                            <input
                                placeholder="Введите телефон для связи"
                                type="text" class="form-control @error('phone') is-invalid @enderror"
                                id="inp_phone" name="phone" value="{{ substr(old('phone', $user->phone), 1) }}" v-input-mask mask="+7(999)999-99-99"  required>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="mt-4 pt-2">
                            <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                        </div>

                    </div>
                </div>

            </form>
        </div>
        <div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="nav-profile-tab">
            <form method="post" action="{{ route('cabinet.change-password') }}">
                @csrf @method('put')

                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="inp_password">@lang('Old password')</label>
                            <input
                                placeholder="Введите текущий пароль"
                                type="password" class="form-control @error('password') is-invalid @enderror"
                                id="inp_password" name="password" value="{{ old('password') }}" required>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inp_new_password">@lang('New password')</label>
                            <input
                                placeholder="Введите новый пароль"
                                type="password" class="form-control @error('new_password') is-invalid @enderror"
                                id="inp_new_password" name="new_password" value="{{ old('new_password') }}" required>
                            @error('new_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inp_repeat_password">@lang('Repeat password')</label>
                            <input
                                placeholder="Введите новый пароль ещё раз"
                                type="password"
                                class="form-control @error('new_password_confirmation') is-invalid @enderror"
                                id="inp_repeat_password" name="new_password_confirmation"
                                value="{{ old('new_password_confirmation') }}" required>
                            @error('new_password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="mt-4 pt-2">
                            <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="border-left hide-border-left pl-1 pl-md-4 mt-4"
                             style="line-height: 1.1;min-height: calc(1.6em + 1rem + 2px);">
                            <small class="text-grey">
                                Пароль должен быть длиной 8-20 символов,
                                содержать цифры и заглавные буквы и НЕ содержать
                                пробелов
                            </small>
                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div class="tab-pane fade" id="org-profile" role="tabpanel" aria-labelledby="nav-contact-tab">
            <form method="post" class="profile-form {{ $org_profile->id && $org_profile->status == 1 ? 'disabled' : '' }}" action="{{ route('cabinet.change-org-profile') }}">
                @csrf @method('put')

                <div class="row">

                    <div class="col-lg-6 mb-lg-4 order-lg-2 my-4">
                        <div class="@if(empty($org_profile->id)) rounded description-item px-4 py-3 mb-4 @else border-lg-left h-100 pl-2 @endif  d-flex align-items-center justify-content-center">
                            @if(empty($org_profile->id))
                                <small class="text-grey">
                                    Чтобы иметь возможность публиковать лоты на портале, укажите корректные данные и
                                    дождитесь окончания модерации.
                                    <br><br>
                                    Уведомление об успешном прохождении модерации придет на указанный вами адрес электронной
                                    почты.
                                </small>
                            @elseif($org_profile->status == 1)
                                <div class="alert alert-warning wrapper-info text-center" role="alert">Ваши данные на модерации</div>
                            @elseif($org_profile->status == 2)
                                <div class="alert alert-success wrapper-info text-center"  role="alert">Ваши данные подтверждены</div>
                            @elseif($org_profile->status == 3)
                                <div class="alert alert-danger wrapper-info text-center"  role="alert">Ваши данные отклонены, пожалуйста поправьте</div>
                            @endif

                        </div>
                    </div>
                    <div class="col-12 col-md-6">

                        <div class="form-group">
                            <label for="org_profile_type">Выберите тип представителя</label>
                            <select
                                class="form-control @error('org_profile_type') is-invalid @enderror"
                                id="org_profile_type" name="org_profile_type" required>
                                <option style="display:none"></option>
                                @foreach($org_profile->allowed_types as $key => $type)
                                    <option value="{{ $key }}"
                                        {{ (old('org_profile_type', $org_profile->org_profile_type) == $key) ? 'selected' : '' }}>
                                        {{ $type }}
                                    </option>
                                @endforeach

                            </select>

                            @error('org_profile_type')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="inp_inn">ИНН</label>
                            <input
                                placeholder="Введите ИНН"
                                type="text" class="form-control @error('inn') is-invalid @enderror"
                                id="inp_inn" name="inn" value="{{ old('inn', $org_profile->inn) }}" required>
                            @error('inn')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>


                        <div class="form-group org_profile-type__1">
                            <label for="inp_org_profile_last_name">Фамилия</label>
                            <input
                                placeholder="Введите Фамилию"
                                type="text" class="form-control @error('org_profile_last_name') is-invalid @enderror"
                                id="inp_org_profile_last_name" name="org_profile_last_name"
                                value="{{ old('org_profile_last_name', $org_profile->org_profile_last_name) }}">
                            @error('org_profile_last_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group org_profile-type__1" >
                            <label for="inp_org_profile_first_name">Имя</label>
                            <input
                                placeholder="Введите Имя"
                                type="text" class="form-control @error('org_profile_first_name') is-invalid @enderror"
                                id="inp_org_profile_first_name" name="org_profile_first_name"
                                value="{{ old('org_profile_first_name', $org_profile->org_profile_first_name) }}">
                            @error('org_profile_first_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group org_profile-type__1">
                            <label for="inp_org_profile_middle_name">Отчество</label>
                            <input
                                placeholder="Введите Отчество"
                                type="text" class="form-control @error('org_profile_middle_name') is-invalid @enderror"
                                id="inp_org_profile_middle_name" name="org_profile_middle_name"
                                value="{{ old('org_profile_middle_name', $org_profile->org_profile_middle_name) }}">
                            @error('org_profile_middle_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group org_profile-type__2">
                            <label for="inp_org_profile_full_name">Наименование</label>
                            <input
                                placeholder="Введите Наименование"
                                type="text" class="form-control @error('org_profile_full_name') is-invalid @enderror"
                                id="inp_org_profile_full_name" name="org_profile_full_name"
                                value="{{ old('org_profile_full_name', $org_profile->org_profile_full_name) }}">
                            @error('org_profile_full_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="mt-4 pt-2">
                            @if($org_profile->status != 1 || empty($org_profile->id))

                                    <button type="submit" class="btn btn-primary">
                                        @if(empty($org_profile->id))
                                            Отправить на модерацию
                                        @else
                                            Изменить
                                        @endif
                                    </button>

                            @else

                                Данные на модерации

                            @endif

                        </div>

                    </div>
                </div>
            </form>
        </div>
        <div class="tab-pane fade" id="requisites" role="tabpanel" aria-labelledby="nav-contact-tab">
            <p class="col">Для участия в торгах необходимо загрузить документы (для физ. лица)</p>
            <form method="post" class="profile-form form-inline mb-3 {{ $org_profile->id && $org_profile->status == 1 ? 'disabled' : '' }}" action="">
                @csrf @method('put')
                <div class="col">
                    <div class="form-group mb-2">
                        <label for="passport" class="flex-1 d-block"><sup>*</sup>Паспорт:</label>
                        <input type="text" class="form-control mx-sm-3 flex-2" name="passport">
                        <label for="file-upload" class="btn btn-primary btn-sm">
                            загрузить
                        </label>
                        <input id="file-upload" type="file"/>
                    </div>
                    <div class="form-group mb-2">
                        <label for="inn" class="flex-1 d-block"><sup>*</sup>ИНН:</label>
                        <input type="text" class="form-control mx-sm-3 flex-2" name="inn">
                        <label for="file-upload" class="btn btn-primary btn-sm">
                            загрузить
                        </label>
                        <input id="file-upload" type="file"/>
                    </div>
                    <div class="form-group">
                        <label for="snils" class="flex-1 d-block"><sup>*</sup>СНИЛС:</label>
                        <input type="text" class="form-control mx-sm-3 flex-2" name="snils">
                        <label for="file-upload" class="btn btn-primary btn-sm">
                            загрузить
                        </label>
                        <input id="file-upload" type="file"/>
                    </div>
                </div>
            </form>
            <p class="col">Для участия в торгах необходимо загрузить документы (для юр. лица)</p>
            <form method="post" class="profile-form form-inline {{ $org_profile->id && $org_profile->status == 1 ? 'disabled' : '' }} mb-3" action="">
                @csrf @method('put')
                <div class="col">
                    <div class="form-group mb-2">
                        <label for="inn" class="flex-1 d-block"><sup>*</sup>ИНН:</label>
                        <input type="text" class="form-control mx-sm-3 flex-2" name="inn">
                        <label for="file-upload" class="btn btn-primary btn-sm">
                            загрузить
                        </label>
                        <input id="file-upload" type="file"/>
                    </div>
                    <div class="form-group mb-2">
                        <label for="charter" class="flex-1 d-block"><sup>*</sup>Устав:</label>
                        <input type="text" class="form-control mx-sm-3 flex-2" name="charter">
                        <label for="file-upload" class="btn btn-primary btn-sm">
                            загрузить
                        </label>
                        <input id="file-upload" type="file"/>
                    </div>
                    <div class="form-group mb-2">
                        <label for="order" class="flex-1 d-block"><sup>*</sup>Приказ о назначении директором:</label>
                        <input type="text" class="form-control mx-sm-3 flex-2" name="order">
                        <label for="file-upload" class="btn btn-primary btn-sm">
                            загрузить
                        </label>
                        <input id="file-upload" type="file"/>
                    </div>
                    <div class="form-group">
                        <label for="documents" class="flex-1 d-block"><sup>*</sup>Учредительные документы:</label>
                        <input type="text" class="form-control mx-sm-3 flex-2" name="documents">
                        <label for="file-upload" class="btn btn-primary btn-sm">
                            загрузить
                        </label>
                        <input id="file-upload" type="file"/>
                    </div>
                </div>
            </form>
            <small class="text-danger col"><sup>*</sup>Обязательные документы для участия в торгах</small>
            <button class="btn btn-primary d-block mt-3" type="submit">Сохранить изменения</button>
        </div>
    </div>
@endsection

<h3>На сайте появились новые лоты по вашей рассылке из категории: {{ $mailing->category->title }}.</h3>

@foreach($lots as $lot)
    <div style="padding: 15px 15px 0;margin-bottom: 15px;border: 1px solid grey">
        <a target="_blank" href="{{ route('site.catalog.lot.show', $lot->id) }}">Открыть лот №{{ $lot->id }} на
            сайте</a>
        @if($lot->region_with_type)
            <span> - {{ $lot->region_with_type }}</span>
        @endif
        <p>{{ $lot->title }}</p>
        <p><b>@money($lot->current_price) ₽</b></p>
    </div>
@endforeach

<p>Посмотреть весь список можно в <a href="{{ url('/cabinet/mailings') }}">в личном кабинете</a></p>

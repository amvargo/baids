<div class="row mt-4 mb-4">
    <div class="col-12">
        <div class="h2 mb-2">Воспользуйтесь нашими услугами</div>
        <div class="slick-variable-width row">
            @foreach($service_banners as $service)
                <a
                    class="service-slide"
                    href="#"
                    data-toggle="modal"
                    @auth
                    data-target="#modalServiceInfo"
                    data-service="{{ $service->title }}"
                    data-description="{{ $service->description }}"
                    data-id="{{ $service->id }}"
                    @else
                    data-target="#modalLogin"
                    @endauth
                    style="display: none">
                    <img
                        class="img-fluid"
                        src="{{ $service->url_image2 }}"
                        alt="{{ $service->title }}">
                </a>
            @endforeach
        </div>
    </div>
</div>

<div class="row mt-4 mb-4">
    <div class="col-12">
        <div class="h2 mb-0">Воспользуйтесь нашими услугами</div>
        <div class="slick-variable-width-banners-for-lot row">
            @foreach($service_banners as $service)
                <div class="p-3">
                    <div class="rounded shadow position-relative banner-for-lot"
                         style="width: 350px;min-height: 350px; padding-bottom: 90px;">
                        <div class="clearfix p-3">
                            <img class="float-left mr-3" src="{{ $service->url_image }}" alt="...">
                            <div class="h3 pt-3">{{ $service->title }}</div>
                        </div>
                        <p class="m-3">{!! nl2br(\Illuminate\Support\Str::limit($service->description, 130)) !!}</p>
                        <div class="position-absolute fixed-bottom p-3">
                            <a
                                href="#"
                                class="btn btn-primary float-right mt-md-2"
                                data-toggle="modal"
                                @auth
                                data-target="#modalService"
                                data-service="{{ $service->title }}"
                                data-id="{{ $service->id }}"
                                data-lot-id="{{ $lot->id }}"
                                @else
                                data-target="#modalLogin"
                                @endauth
                            >
                                Заказать
                                <svg width="14" height="8" viewBox="0 0 14 8" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 4L13 4M13 4L9.41658 1M13 4L9.41658 7" stroke="white"
                                          stroke-linecap="round"
                                          stroke-linejoin="round"/>
                                </svg>
                            </a>
                            <div class="display-5 banner-for-lot-price">@money_round($service->price) ₽</div>
                        </div>

                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

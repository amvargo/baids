<div class="shadow rounded mb-4 mt-1 p-4">
    <div class="position-relative" style="min-height: 450px; padding-bottom: 100px;">
        <div class="mb-3">
            @if(filled($service->url_image))
                <img src="{{ $service->url_image }}" alt="{{ $service->title }}" height="73">
            @else
                <img src="{{ asset('img/icons/service.svg') }}" alt="{{ $service->title }}">
            @endif
        </div>

        <h5 class="h1">{{ $service->title }}</h5>
        <p class="card-text">{{ \Illuminate\Support\Str::limit($service->description, 500) }}</p>

        <div class="position-absolute fixed-bottom">
            <div class="display-5 mb-2">от @money_round($service->price) ₽</div>
            <a
                href="#"
                class="btn btn-primary"
                data-toggle="modal"
                id="service-btn"
                @auth
                data-target="#modalService"
                data-service="{{ $service->title }}"
                data-id="{{ $service->id }}"
                @else
                data-target="#modalLogin"
                @endauth
            >
                Заказать
                <svg width="14" height="8" viewBox="0 0 14 8" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path d="M1 4L13 4M13 4L9.41658 1M13 4L9.41658 7" stroke="white" stroke-linecap="round"
                          stroke-linejoin="round"/>
                </svg>
            </a>
        </div>

    </div>
</div>

<section class="row mt-4 pt-3 section-map">
    <g-map :coordinates=@json($map['coordinates'])></g-map>
</section>

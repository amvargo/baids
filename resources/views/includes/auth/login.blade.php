<div class="shadow px-3 pt-2 pb-3 rounded bg-white" style="min-width: 350px; min-height: 250px;">
    <div class="h1 m-0">Авторизуйтесь,</div>
    <small class="font-weight-bolder">чтобы иметь доступ ко всем возможностям сайта</small>
    <div class="mt-2"></div>

    <form method="POST" action="{{ route('login') }}" id="authLoginForm">
        @csrf
        <div class="form-group mb-2">
            <label for="inp_email">@lang('Your e-mail')</label>
            <input
                type="email" class="form-control form-control-sm @error('email') is-invalid @enderror"
                id="inp_email" name="email" value="{{ old('email') }}" required autocomplete="email">
            @error('email')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="form-group">
            @if (Route::has('password.request'))
                <div class="float-right">
                    <a
                        data-toggle="modal"
                        data-target="#modalResetPass"
                        data-dismiss="modal"
                        href="{{ route('password.request') }}">@lang('Forgot Your Password?')</a>
                </div>
            @endif
            <label for="inp_pass">@lang('Password')</label>
            <input
                type="password" class="form-control form-control-sm @error('password') is-invalid @enderror"
                id="inp_pass" name="password" required autocomplete="current-password">
            @error('password')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="row align-items-center">
            <div class="col flex-grow-1">
                <div class="d-none">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" checked>
                </div>
                <button type="submit" class="btn btn-sm shadow bg-white btn-login">Войти в аккаунт</button>
            </div>
            <div class="col text-right">
                <a href="{{ route('register') }}"
                   data-toggle="modal" data-target="#modalLogin" onclick="$('#register-tab').tab('show')"
                   class="text-body">Регистрация</a>
            </div>
        </div>
    </form>

</div>

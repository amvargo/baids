<nav class="navbar navbar-expand-md navbar-light bg-light bg-light-nav py-md-0 pt-2 px-md-0 px-3" style="background-color: #F4F6FA;" id="main-navbar">
    <div class="container flex-row justify-content-center">
        <a href="" class="d-md-none d-flex mr-auto" data-toggle="collapse" data-target="#navbarSupportedContent"
           aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <img class="m-1 mt-0 d-flex mr-auto" src="{{ asset('img/icons/menu.svg') }}" alt="info" >
        </a>

        <div class="pr-md-0 pr-lg-4 d-flex d-md-none">
            <a href="/" class="">
                <img src="{{ asset('/img/logo-sm.svg') }}" alt="БАИДС" class="logo logo-sm"/>
            </a>
        </div>

        <a href="{{ route('cabinet.profile') }}" class="d-md-none d-flex ml-auto">
            <img class="m-1 mt-0" src="{{ asset('img/icons/user.svg') }}" alt="info">
        </a>

        <div class="collapse navbar-collapse py-2" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <span>
                        <svg
                            class="mb-1"
                            width="16" height="17" viewBox="0 0 16 17"
                            fill="none" xmlns="http://www.w3.org/2000/svg">
                        <mask id="path-2-inside-1" fill="white">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M13 6.24922C13 7.37699 12.6275 8.41765 11.9988 9.25459L8.86847 14.7388C8.48469 15.4112 7.51529 15.4112 7.13151 14.7388L4.00116 9.25459C3.37255 8.41765 3 7.37699 3 6.24922C3 3.48472 5.23858 1.24365 8 1.24365C10.7614 1.24365 13 3.48472 13 6.24922ZM8 8.25144C9.10457 8.25144 10 7.35502 10 6.24922C10 5.14342 9.10457 4.24699 8 4.24699C6.89543 4.24699 6 5.14342 6 6.24922C6 7.35502 6.89543 8.25144 8 8.25144Z"/>
                        </mask>
                        <path
                            d="M11.9988 9.25459L11.1993 8.65404C11.1741 8.6875 11.1511 8.72252 11.1304 8.75887L11.9988 9.25459ZM4.00116 9.25459L4.86964 8.75887C4.84889 8.72252 4.82588 8.6875 4.80074 8.65404L4.00116 9.25459ZM7.13151 14.7388L6.26303 15.2345L7.13151 14.7388ZM12.7984 9.85515C13.5527 8.85091 14 7.60086 14 6.24922H12C12 7.15312 11.7022 7.98439 11.1993 8.65404L12.7984 9.85515ZM9.73695 15.2345L12.8673 9.75032L11.1304 8.75887L7.99999 14.2431L9.73695 15.2345ZM3.13268 9.75031L6.26303 15.2345L7.99999 14.2431L4.86964 8.75887L3.13268 9.75031ZM2 6.24922C2 7.60086 2.44731 8.85091 3.20158 9.85515L4.80074 8.65404C4.29778 7.98439 4 7.15312 4 6.24922H2ZM8 0.243652C4.68524 0.243652 2 2.93348 2 6.24922H4C4 4.03596 5.79191 2.24365 8 2.24365V0.243652ZM14 6.24922C14 2.93348 11.3148 0.243652 8 0.243652V2.24365C10.2081 2.24365 12 4.03596 12 6.24922H14ZM9 6.24922C9 6.80378 8.55124 7.25144 8 7.25144V9.25144C9.6579 9.25144 11 7.90625 11 6.24922H9ZM8 5.24699C8.55124 5.24699 9 5.69465 9 6.24922H11C11 4.59218 9.6579 3.24699 8 3.24699V5.24699ZM7 6.24922C7 5.69465 7.44876 5.24699 8 5.24699V3.24699C6.3421 3.24699 5 4.59218 5 6.24922H7ZM8 7.25144C7.44876 7.25144 7 6.80378 7 6.24922H5C5 7.90625 6.3421 9.25144 8 9.25144V7.25144ZM7.99999 14.2431L7.99999 14.2431L6.26303 15.2345C7.0306 16.5793 8.96938 16.5793 9.73695 15.2345L7.99999 14.2431Z"
                            fill="#39475F" mask="url(#path-2-inside-1)"/>
                    </svg>
                    </span>
                    <span>Регион поиска:</span>
                    <a href="javascript:"
                       v-b-modal.modal-location>{{ Session::get('location.filter2.text') ?: __('location.all_russia') }}</a>
                </li>
            </ul>
            <ul class="navbar-nav top-right links d-lg-flex d-md-none">
            <li class="nav-item">
                    <a class="nav-link" href="{{ route('site.tariff') }}">
                        <span>Тарифы</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('site.about-project') }}">
                        <span>О проекте</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('site.news') }}">
                        <span>Новости</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('site.hot-lots.index') }}">
                        <span>Успей купить</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/advertising')  }}"


                       @auth

                       @else
                        data-redirect="/advertising"
                       data-toggle="modal"
                       data-target="#modalLogin"
                        @endauth

                    >
                        <img class="m-1 mt-0" src="{{ asset('img/icons/16px/megaphone.svg') }}" alt="info">
                        <span>Реклама</span>

                    </a>
                </li>
                {{--<li class="nav-item">
                    <a class="nav-link" href="{{ route('site.service.page') }}">
                        <img class="m-1 mt-0" src="{{ asset('img/icons/16px/portfolio.svg') }}" alt="info">
                        <span>@lang('Services')</span>
                    </a>
                </li>--}}
                {{--<li class="nav-item">
                    <a class="nav-link" href="{{ route('site.about') }}">
                        <img class="m-1 mt-0" src="{{ asset('img/icons/info.svg') }}" alt="info">
                        <span>@lang('About service')</span>
                    </a>
                </li>--}}
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('site.question.page') }}">
                        <img class="m-1 mt-0" src="{{ asset('img/icons/16px/question.svg') }}" alt="info">
                        <span>@lang('Question answer')</span>
                    </a>
                </li>
                {{--<li class="nav-item">
                    <a class="nav-link" href="{{ route('site.consulting.page') }}">
                        <span>@lang('Online consulting')</span>
                    </a>
                </li>--}}
            </ul>
            <ul class="navbar-nav navbar-login">
                @if (Route::has('login'))
                    @auth
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('cabinet.profile') }}">
                                <img class="m-1 mt-0" src="{{ asset('img/icons/16px/user.svg') }}" alt="info">
                                <span>@lang('Personal account')</span>
                            </a>
                        </li>
                        @if(auth()->user()->hasRole(\App\Role\UserRole::ROLE_SUPPORT))
                            <li class="nav-item ml-2">
                                <a class="btn btn-sm btn-dark" href="{{ route('panel') }}">@lang('Panel')</a>
                            </li>
                        @endif
                    @else

                        <li class="nav-item">
                            <a
                                class="nav-link"
                                href="#"
                                data-toggle="modal"
                                data-target="#modalLogin"
                                onclick="$('#login-tab').tab('show')"
                            >
                                <img class="m-1 mt-0" src="{{ asset('img/icons/16px/user.svg') }}" alt="info">
                                @lang('Login')</a>
                        </li>

                        {{--<li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">@lang('Login')</a>
                        </li>--}}

                        {{--@if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">@lang('Register')</a>
                            </li>
                        @endif--}}
                    @endauth
                @endif
            </ul>
        </div>
        <div class="d-lg-none d-none d-md-flex nav-item dropdown ml-auto">
            <a href="#" class="nav-link d-lg-none d-none d-sm-flex ml-auto dropdown-toggle"
               id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Ещё
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('site.tariff') }}">
                    <span>Тарифы</span>
                </a>
                <a class="dropdown-item" href="{{ route('site.hot-lots.index') }}">
                    <span>Успей купить</span>
                </a>
                <a class="dropdown-item" href="{{ url('/advertising')  }}"


                   @auth

                   @else
                   data-redirect="/advertising"
                   data-toggle="modal"
                   data-target="#modalLogin"
                    @endauth

                >
                    <img class="m-1 mt-0" src="{{ asset('img/icons/16px/megaphone.svg') }}" alt="info">
                    <span>Реклама</span>

                </a>
                <a class="dropdown-item" href="{{ route('site.service.page') }}">
                    <img class="m-1 mt-0" src="{{ asset('img/icons/16px/portfolio.svg') }}" alt="info">
                    <span>@lang('Services')</span>
                </a>
                {{--<li class="nav-item">
                    <a class="nav-link" href="{{ route('site.about') }}">
                        <img class="m-1 mt-0" src="{{ asset('img/icons/info.svg') }}" alt="info">
                        <span>@lang('About service')</span>
                    </a>
                </li>--}}
                <a class="dropdown-item" href="{{ route('site.question.page') }}">
                    <img class="m-1 mt-0" src="{{ asset('img/icons/16px/question.svg') }}" alt="info">
                    <span>@lang('Question answer')</span>
                </a>
                {{--<li class="nav-item">
                    <a class="nav-link" href="{{ route('site.consulting.page') }}">
                        <span>@lang('Online consulting')</span>
                    </a>
                </li>--}}
            </div>
        </div>
    </div>
</nav>

{{-- Modal - location --}}
<modal-location></modal-location>

{{-- Modal - report --}}
<div class="modal fade" id="modalReport" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-rt" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('img/icons/16px/close.svg') }}" alt="close">
                </button>
            </div>
            <div class="modal-body">
                <div class="tab-content" id="myTabContent">
                    <div class="fade show active" role="tabpanel" aria-labelledby="home-tab">
                        @php($user = auth()->user())
                        <form method="POST" action="{{ route('site.report') }}" id="modalReportError">
                            @csrf
                            <input type="hidden" name="trade_lot_id" id="modalReport_tradeLotId" value="">
                            <div class="service-text h2">Сообщить об ошибках</div>
                            <div class="form-group">
                                <label for="inp_tradelink">Ссылка на торги</label>
                                <input
                                    placeholder="Ссылка на торги"
                                    type="text" class="form-control @error('tradelink') is-invalid @enderror"
                                    id="inp_tradelink" name="tradelink" value="{{ Request::url() }}"
                                    readonly>
                                @error('tradelink')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <label for="inp_errortype">Тип ошибки:</label>
                                <select
                                    value=""
                                    name="errortype"
                                    class="form-control @error('category_id') is-invalid @enderror"
                                    id="inp_category_id"
                                    required>
                                    <option value=""></option>
                                    <option
                                        value="Некорректный статус лота"> Некорректный статус лота
                                    </option>
                                    <option
                                        value="Несоответствие дат торгов"> Несоответствие дат торгов
                                    </option>
                                    <option
                                        value="Несоответствие цены лота"> Несоответствие цены лота
                                    </option>
                                    <option
                                        value="Нет ссылки на ЭТП"> Нет ссылки на ЭТП
                                    </option>
                                    <option
                                        value="Неверные контакты ОТ (организатора торгов)"> Неверные контакты ОТ (организатора торгов)
                                    </option>
                                    <option
                                        value="Предложение / Пожелание"> Предложение / Пожелание
                                    </option>
                                    <option
                                        value="Другая"> Другая
                                    </option>
                                </select>
                                @error('errortype')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <input
                                    placeholder="Описание ошибки"
                                    type="text" class="form-control @error('description') is-invalid @enderror"
                                    id="inp_description" name="description"
                                    >
                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="pt-3">
                                <button
                                    type="submit"
                                    class="btn btn-block btn-primary btn-order"
                                >Отправить
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal - service --}}
<div class="modal fade" id="modalService" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <ul class="nav nav-tabs w-100" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab"
                           href="#service" role="tab" aria-controls="home" aria-selected="true">
                            Заказать услугу
                        </a>
                    </li>
                </ul>

                <button type="button" class="close close-rt" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('img/icons/16px/close.svg') }}" alt="close">
                </button>
            </div>
            <div class="modal-body">
                <div class="tab-content" id="myTabContent">
                    <div class="fade show active serviceBody" role="tabpanel" aria-labelledby="home-tab">
                        @php($user = auth()->user())
                        <form method="POST" action="{{ route('site.service.order') }}" id="modalServiceForm">
                            @csrf
                            <input type="hidden" name="service_id" class="modalServiceForm_serviceId" value="">
                            <input type="hidden" name="trade_lot_id" class="modalServiceForm_tradeLotId" value="">
                            <input type="hidden" class="duration" name="duration">
                            <input type="hidden" class="price" name="price">
                            <div class="service-text h2"></div>
                            <div class="form-group">
                                <label for="inp_email">Имя</label>
                                <input
                                    placeholder="Введите Ваше имя"
                                    type="text" class="form-control @error('name') is-invalid @enderror"
                                    id="inp_name" name="name" value="{{ old('name', $user ? $user->name : null) }}"
                                    required>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inp_pass">E-mail</label>
                                <input
                                    type="text" class="form-control @error('email') is-invalid @enderror"
                                    id="inp_email" name="email" required
                                    value="{{ old('email', $user ? $user->email : null) }}"
                                    placeholder="Введите адрес электронной почты">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inp_pass">Телефон</label>
                                <input
                                    type="text" class="form-control @error('phone') is-invalid @enderror"
                                    id="inp_phone" name="phone" required
                                    value="{{ substr(old('phone', $user ? $user->phone : null), 1) }}"
                                    placeholder="Телефон"
                                    v-input-mask mask="+7(999)999-99-99">
                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mt-4 mb-2">
                                <small>
                                    <span class="text-danger">*</span> Нажимая на кнопку “Заказать”, Вы даете свое
                                    <a href="{{ url('/documents?tab=policy') }}" target="_blank">согласие на обработку персональных данных</a>
                                    <label><input type="checkbox" class="agreement" >Ознакомлен и согласен с <a href="{{ route('site.documents', ['tab' => 'oferta']) }}" target="_blank">договором оферты</a></label>
                                    <label class="agreement-regulations"><input type="checkbox" class="agreement">Ознакомлен и согласен с <a class="regulations-link" href="{{ route('site.documents') }}" target="_blank">регламентом</a></label>
                                    <label class="eula-regulations"><input type="checkbox" class="agreement">Ознакомлен и согласен с <a class="regulations-link" href="https://baids.ru/documents?tab=user-agreement" target="_blank">пользовательским соглашением</a></label>
                                </small>
                            </div>
                            <div class="pt-3">
                                <button
                                    type="submit"
                                    class="btn btn-block btn-primary btn-order"
                                >Заказать
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal - service - INFO --}}
<div class="modal fade" id="modalServiceInfo" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close close-rt" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('img/icons/16px/close.svg') }}" alt="close">
                </button>
            </div>
            <div class="modal-body">
                <div class="tab-content" id="myTabContent">
                    <div class="fade show active serviceBody" role="tabpanel" aria-labelledby="home-tab">
                        @php($user = auth()->user())
                        <div id="modalServiceForm">
                            <input type="hidden" name="service_id" class="modalServiceForm_serviceId" value="">
                            <input type="hidden" name="trade_lot_id" class="modalServiceForm_tradeLotId" value="">
                            <div class="service-text h2"></div>
                            <p style="white-space: break-spaces;"class="service-description"></p>
                            <div class="pt-3">
                                <button data-dismiss="modal" aria-label="Close"
                                        class="btn btn-block btn-success"
                                >Понятно
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@auth
@else
    {{-- Modal - login/register --}}
    <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <ul class="nav nav-tabs w-100" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="login-tab" data-toggle="tab"
                               href="#home" role="tab" aria-controls="home" aria-selected="true">
                                Вход
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="register-tab" data-toggle="tab"
                               href="#register" role="tab" aria-controls="register" aria-selected="false">
                                Регистрация
                            </a>
                        </li>
                    </ul>

                    <button type="button" class="close close-rt" data-dismiss="modal" aria-label="Close">
                        <img src="{{ asset('img/icons/16px/close.svg') }}" alt="close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="login-tab">
                            <form method="POST" action="{{ route('login') }}" id="modalLoginForm">
                                @csrf
                                <input type="hidden" id="login_form_redirect" name="current_page" value="">
                                <div class="form-group">
                                    <label for="inp_email">@lang('Your e-mail')</label>
                                    <input
                                        type="email" class="form-control @error('email') is-invalid @enderror"
                                        id="inp_email" name="email" value="{{ old('email') }}" required
                                        autocomplete="email" placeholder="Введите адрес электронной почты">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    @if (Route::has('password.request'))
                                        <div class="float-right">
                                            <a
                                                data-toggle="modal"
                                                data-target="#modalResetPass"
                                                data-dismiss="modal"
                                                href="{{ route('password.request') }}">@lang('Forgot Your Password?')</a>
                                        </div>
                                    @endif
                                    <label for="inp_pass">@lang('Password')</label>
                                    <input
                                        type="password" class="form-control @error('password') is-invalid @enderror"
                                        id="inp_pass" name="password" required autocomplete="current-password"
                                        placeholder="Введите пароль">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="pt-3">
                                    <button type="submit" class="btn btn-block btn-primary">
                                        <span class="spinner-border spinner-border-sm d-none" role="status"
                                              aria-hidden="true"></span>
                                        Войти
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
                            <form method="POST" action="{{ route('register') }}" id="modalRegisterForm">
                                @csrf

                                <div class="form-group">
                                    <label for="name" class="text-md-right">{{ __('Name') }}</label>

                                    <div>
                                        <input id="name" type="text"
                                               class="form-control @error('name') is-invalid @enderror"
                                               name="name" value="{{ old('name') }}"
                                               placeholder="Введите Ваше имя"
                                               required autocomplete="name">

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div>
                                        <input id="email" type="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               name="email" value="{{ old('email') }}"
                                               placeholder="Введите адрес электронной почты"
                                               required autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="text-md-right">{{ __('Password') }}</label>

                                    <div>
                                        <input id="password" type="password"
                                               class="form-control @error('password') is-invalid @enderror"
                                               name="password" required
                                               autocomplete="new-password"
                                               placeholder="Введите пароль">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm"
                                           class="text-md-right">{{ __('Confirm Password') }}</label>

                                    <div>
                                        <input id="password-confirm" type="password" class="form-control"
                                               placeholder="Введите пароль повторно"
                                               name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>

                                <div class="pt-3">
                                    <div>
                                        <div class="mb-3" style="line-height: 1.2">
                                            <small>
                                                <span class="text-danger">* </span>
                                                <span>Нажимая на кнопку "@lang('Register')", Вы даете свое <br>
                                            <a target="_blank"
                                               href="/">согласие на обработку персональных данных</a></span>
                                            </small>
                                        </div>
                                        <button type="submit" class="btn btn-block btn-primary">
                                            <span class="spinner-border spinner-border-sm d-none" role="status"
                                                  aria-hidden="true"></span>
                                            @lang('Register')
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal - reset password --}}
    <div class="modal fade" id="modalResetPass" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <ul class="nav nav-tabs w-100" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab"
                               href="#reset" role="tab" aria-controls="home" aria-selected="true">
                                Восстановление пароля
                            </a>
                        </li>
                    </ul>

                    <button type="button" class="close close-rt" data-dismiss="modal" aria-label="Close">
                        <img src="{{ asset('img/icons/16px/close.svg') }}" alt="close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="reset" role="tabpanel" aria-labelledby="home-tab">
                            <form method="POST" action="{{ route('password.email') }}" id="formResetPass">
                                @csrf

                                <div class="form-group">
                                    <label for="email" class="text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div>
                                        <input id="email" type="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               name="email" value="{{ old('email') }}"
                                               placeholder="Введите адрес электронной почты"
                                               required autocomplete="email">
                                        <small class="form-text text-muted">На указанный Вами адрес придёт временный
                                            пароль для входа в систему</small>

                                        @error('email')

                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        <a
                                            data-toggle="modal"
                                            data-target="#modalLogin"
                                            data-dismiss="modal"
                                            href="#"
                                            onclick="$('#myTab li:last-child a').tab('show')"
                                        >{{ __('Register') }}</a>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group mb-0 mt-4">
                                    <div>
                                        @if(session('status'))
                                            <div class="alert alert-success show" role="alert">
                                                {{session('status')}}
                                            </div>
                                        @else
                                            <button type="submit" class="btn btn-primary">
                                                <span class="spinner-border spinner-border-sm d-none" role="status"
                                                      aria-hidden="true"></span>
                                                {{ __('Send Password Reset Link') }}
                                            </button>
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endauth

{{-- Modal - service - free trial--}}
<div class="modal fade" id="modalServiceTrial" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-rt" data-dismiss="modal" aria-label="Close">
                    <img src="{{ asset('img/icons/16px/close.svg') }}" alt="close">
                </button>
            </div>
            <div class="modal-body">
                <div class="tab-content" id="myTabContent">
                    <div class="fade show active serviceBody" role="tabpanel" aria-labelledby="home-tab">
                        @php($user = auth()->user())
                        <form method="POST" action="{{ route('site.service.order') }}" id="modalServiceFormTrial">
                            @csrf
                            <input type="hidden" name="service_id" class="modalServiceForm_serviceId" value="">
                            <input type="hidden" class="duration" name="duration" value="trial">
                            <input type="hidden" name="name"  value="{{ old('name', ($user && $user->name) ? $user->name : 'trial') }}">
                            <input type="hidden" name="email"  value="{{ old('email', ($user && $user->email) ? $user->email : 'trial@baids.ru') }}">
                            <input type="hidden" name="phone"  value="{{ old('phone', ($user && $user->phone) ? $user->phone : '0000') }}">
                            <div class="service-text h2">Подбор ликвидных предложений (рассылка лотов).</div>                            
                            <div class="h2">Вы подключаете бесплатный период 7 дней</div>                            
                            <div class="pt-3">
                                <button
                                    type="submit"
                                    class="btn btn-block btn-primary btn-order"
                                >Заказать
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

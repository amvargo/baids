<div class="card mb-2">
    <div class="card-body">

        <form action="{{ route('site.consulting.store') }}" method="POST">
            {{ method_field('POST') }}
            @csrf

            @if(filled($categories = \App\Models\ConsultingCategory::get()))
            <div class="form-group">
                <label for="inp_category_id">@lang('Category'):</label>
                <select
                    value="{{ old('category_id') }}"
                    name="category_id"
                    class="form-control @error('category_id') is-invalid @enderror"
                    id="inp_category_id">
                    <option value="">--</option>
                    @foreach($categories as $category)
                        <option
                            value="{{ $category->id }}"
                            @if($category->id === (int) old('category_id')) selected @endif>
                            {{ $category->title }}
                        </option>
                    @endforeach
                </select>

                @error('category_id')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            @endif

            <div class="form-group">
                <label for="inp_full_name">@lang('Full name'):</label>
                <input type="text" name="full_name" value="{{ old('full_name') }}"
                       class="form-control @error('full_name') is-invalid @enderror" id="inp_full_name">
                @error('full_name')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="inp_phone">@lang('Phone'):</label>
                <input type="text" name="phone" value="{{ old('phone') }}"
                       class="form-control @error('phone') is-invalid @enderror" id="inp_phone"
                       v-input-mask mask="+7(999)999-99-99">
                @error('phone')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="inp_email">@lang('E-mail'):</label>
                <input type="email" name="email" value="{{ old('email') }}"
                       class="form-control @error('email') is-invalid @enderror" id="inp_email"
                       aria-describedby="inp_email_hint">
                @error('email')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="inp_message">Сообщение:</label>
                <textarea type="text" name="message"
                          class="form-control @error('message') is-invalid @enderror"
                          id="inp_message"
                          rows="5">{{ old('message') }}</textarea>
                @error('message')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">@lang('Submit')</button>
        </form>

        <div class="flash-message mt-3">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('consulting-alert-' . $msg))
                    <p class="alert alert-{{ $msg }}">
                        @lang(Session::get('consulting-alert-' . $msg))
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    </p>
                @endif
            @endforeach
        </div>

    </div>
</div>

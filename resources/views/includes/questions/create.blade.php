<div>

    <form action="{{ route('site.question.store') }}" method="POST" id="questionForm">
        {{ method_field('POST') }}
        @csrf
        @php($user = auth()->user())

        <div class="form-group">
            <label for="inp_author">Имя:</label>
            <input type="text" name="author" value="{{ old('author', $user ? $user->full_name : null) }}"
                   class="form-control @error('author') is-invalid @enderror" id="inp_author"
                   aria-describedby="inp_author_hint">
            @error('author')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="inp_email">@lang('E-mail'):</label>
            <input type="text" name="email" value="{{ old('email', $user ? $user->email : null) }}"
                   class="form-control @error('email') is-invalid @enderror" id="inp_email"
                   aria-describedby="inp_email_hint">
            @error('email')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="inp_question">Ваш вопрос:</label>
            <textarea type="text" name="question"
                      class="form-control @error('question') is-invalid @enderror"
                      id="inp_question"
                      rows="5"
                      aria-describedby="inp_question_hint">{{ old('question') }}</textarea>
            @error('question')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>

        <div class="mt-4 mb-2">
            <small>
                <span class="text-danger">*</span> Нажимая на кнопку “Отправить вопрос”, Вы соглашаетесь 
                <a href="{{ route('site.documents', ['tab' => 'policy']) }}" target="_blank"> с положением по обработке персональных данных</a>
            </small>
        </div>
        <button type="submit" class="btn btn-primary">Отправить вопрос</button>
    </form>
</div>

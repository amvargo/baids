<div class="question-item mb-4">

    <div class="rounded shadow p-4">
        <div class="float-right">
            <div class="question-item-btn">
                <svg width="31" height="32" viewBox="0 0 31 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="15.5" cy="15.7727" r="14.75" stroke-width="1.5"/>
                    <g>
                        <line x1="15.75" y1="9.02271" x2="15.75" y2="22.5227" stroke-width="1.5"
                              stroke-linecap="round"/>
                        <line x1="8.75" y1="15.5227" x2="22.25" y2="15.5227" stroke-width="1.5" stroke-linecap="round"/>
                    </g>
                </svg>
            </div>
        </div>
        <div class="px-3">
            <div class="h3">{{ $question->question }}</div>
            <small class="text-grey">{{ $question->created_at }} от {{ $question->author }}</small>
        </div>
    </div>
    <div class="question-item-answer-container">
        <div class="rounded p-4 mt-3 question-item-answer">
            <div class="px-3">
                <h5 class="card-title">@lang('Answer')</h5>
                {{ $question->answer }}
            </div>
        </div>
    </div>

</div>

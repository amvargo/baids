<div class="container">
    <div class="mt-4 mb-2 mb-sm-4">
        <div class="row">
            <div class="col-md-4 col-lg-4 col-12 pr-md-0 pr-lg-4 d-none d-md-block">
                <a href="/" class="">
                    <img src="{{ asset('/img/logo.svg') }}" alt="БАИДС" class="logo w-100 d-none d-lg-inline"/>
                    <img src="{{ asset('/img/logo-sm.svg') }}" alt="БАИДС" class="logo d-lg-none logo-sm"/>
                </a>
            </div>
            <div class="flex-grow-0">
                <button
                    type="button"
                    class="btn btn-lg btn-light shadow rounded bg-white ml-3"
                    onclick="javascript:$('.header-categories').toggleClass('open')"
                >
                    <svg class="category-svg" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M0.5 3.5H9.5" stroke="#39475F" stroke-linecap="round"/>
                        <path d="M0.5 8.5H13" stroke="#39475F" stroke-linecap="round"/>
                        <path d="M0.5 13.5H15.5" stroke="#39475F" stroke-linecap="round"/>
                    </svg>
                    <span class="d-none d-lg-inline">Категории лотов</span>
                </button>
            </div>
            <search-form
                placeholder = "@lang('Search for lots')"
                action = "{{ $search_url ?? route('site.catalog.index') }}"
                input_str = "{{ request()->get('q') }}"
                synonyms_str = "{{ $synonyms ?? '' }}"
            >
            </search-form>
        </div>
        <div class="row d-none d-sm-block">
            <div class="offset-md-4 col-md-8 col-12 clearfix">

                <div class="float-right">
                    <a href="{{ route('site.catalog.index') }}">
                        <span class="pr-2 p-2">Расширенный поиск</span>
                        <img src="{{ asset('img/icons/16px/config_blue.svg') }}" alt="filter">
                    </a>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="header-categories">
    @php
        $categories = \App\Models\TradeLotCategory::getAllCategories()->sortBy('parent_id');
    @endphp
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-3 pr-0">
                @php($categories->push((object)['id'=>null, "url" => route('site.catalog.without'), "title" => "Прочее", 'image_url'=>null, 'children'=>[]]))
                @foreach($categories as $category)
                    <div class="list-group list-group-cabinet">
                        <a
                            href="{{ $category->url }}"
                            onmouseover="$('#subcategory-{{ $category->id }}').removeClass('d-none').siblings().addClass('d-none')"
                            class="list-group-item list-group-item-action @if($category->url == url()->current()) text-black-50 @endif"
                        >{{ $category->title }}</a>
                    </div>
                @endforeach
            </div>
            <div class="col-sm-9 border-left pl-4 d-none d-sm-block">
                @foreach($categories as $category)
                    <div id="subcategory-{{ $category->id }}" class="subcategory @if(isset($n)) d-none @endif">

                        <img
                            class="header-categories-image-bg"
                            src="{{ $category->image_url ?: '/img/general-cat-noimg.svg' }}"
                            alt="{{ $category->title }}">

                        <div class="row">
                            <div class="col">
                                @php($n = 0)
                                @forelse($category->children as $sub_category)
                                    <div class="list-group list-group-cabinet">
                                        <a
                                            href="{{ $sub_category->url }}"
                                            class="list-group-item list-group-item-action @if($sub_category->url == url()->current()) disabled text-black-50 @endif"
                                        >{{ $sub_category->title }}</a>
                                    </div>
                                    @if(++$n % 6 === 0) </div>
                            <div class="col"> @endif
                                @empty
                                    <div class="p-4">
                                        <span class="text-black-50 font-italic">Нет подкатегорий</span>
                                    </div>
                                @endforelse
                            </div>
                            {{--<div class="col d-flex flex-grow-0" style="min-height: 300px">
                                <div class="d-flex flex-fill align-content-end">
                                    <img
                                        class="d-flex "
                                        src="{{ $category->image_url ?: '/img/general-cat-noimg.svg' }}"
                                        alt="{{ $category->title }}">
                                </div>
                            </div>--}}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

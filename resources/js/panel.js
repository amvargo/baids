/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import {ROLE} from "./helper";

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'

window.Vue.use(VueRouter);

import {BootstrapVue, BootstrapVueIcons} from "bootstrap-vue"

window.Vue.use(BootstrapVue);
window.Vue.use(BootstrapVueIcons);

import Multiselect from 'vue-multiselect'

window.Vue.component('multiselect', Multiselect);


import moment from 'moment-timezone'

moment.defaultFormat = 'L LT';
moment.tz.setDefault('UTC');
moment.tz.zone("UTC");
window.Vue.use(require('vue-moment'), {moment});
moment.locale('ru');

require('./filters');


import axios from 'axios'
import VueAxios from 'vue-axios'

window.Vue.use(VueAxios, axios);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./components/panel/', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
Vue.component('main-layout', require('./layouts/Main.vue').default);

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('file-uploader', require('./components/site/FileUploader.vue').default);
Vue.component('images-preview', require('./components/site/ImagesPreview.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import routes from './routes'

const router = new VueRouter({mode: 'history', routes});

import api from "./api";

const app = new Vue({
    el: '#app',
    router,
    template: '<router-view></router-view>',
    data() {
        return {
            profile: window.profile,
            members: [],
            questions_categories: [],
            parserToast: null,
            ip: null,
        }
    },
    created() {
        api.link(this);

        // open create pages
        this.$root.$on('catalog-categorie-open', id => this.$router.push(`/panel/catalog-category-${id}`));
        this.$root.$on('catalog-param-open', id => this.$router.push(`/panel/catalog-param-${id}`));
        this.$root.$on('catalog-lot-open', id => this.$router.push(`/panel/catalog-lot-${id}`));
        this.$root.$on('catalog-lot-open-site', id => location.replace(`/catalog/lot/${id}`));
        this.$root.$on('model-VehicleMaker-open', id => this.$router.push(`/panel/model/VehicleMaker-${id}`));
        this.$root.$on('model-VehicleSpecialMaker-open', id => this.$router.push(`/panel/model/VehicleSpecialMaker-${id}`));
        this.$root.$on('model-Trade-open', id => this.$router.push(`/panel/model/Trade-${id}`));
        this.$root.$on('model-Order-open', id => this.$router.push(`/panel/model/Order-${id}`));

        Echo.join('panel')
            .here(members => this.members = members)
            .joining(member => {
                this.members.push(member);
            })
            .leaving(member => {
                this.members = this.members.filter(item => item.id !== member.id);
            })
            .listen('PanelEvent', ({model, event, obj}) => {
                if (event.indexOf('parsing') !== -1) {
                    this.showParserToast();
                }
                ;
                this.$emit('panel-event-' + model, {model, event, obj})
            });

    },
    mounted() {
        this.$on('toast', (title, msg, variant) => {
            //return alert(msg);
            // todo: fix me
            this.$bvToast.toast(msg, {
                title,
                variant,
                autoHideDelay: 5e3,
                noAutoHide: ['warning', 'error'].indexOf(variant) !== -1,
                appendToast: true,
                solid: true
            });
        });
        this.loadQuestionCategories();
        this.createParserToast();
        setTimeout(() => this.$bvToast.hide('parserToast'), 60)
        setTimeout(() => this.$bvToast.hide('parserToast'), 160)

    },
    methods: {
        iHaveRole(role) {
            return this.profile.roles.indexOf(role) !== -1
        },
        iAmAdmin() {
            return this.profile.roles.indexOf(ROLE.ADMIN) !== -1
        },
        iAmStaff() {
            return this.iAmAdmin() || this.profile.roles.indexOf(ROLE.STAFF) !== -1
        },
        iAmContentMen() {
            return this.iAmAdmin() || this.profile.roles.indexOf(ROLE.CONTENT) !== -1
        },
        loadQuestionCategories() {
            api.getResources('consulting_categories')
                .then(res => {
                    this.questions_categories = res.data;
                })
                .catch(err => console.log({err}))
        },

        showParserToast() {
            if (!$('#parserToast').length) {
                this.createParserToast();
            }
            this.$bvToast.show('parserToast')
        },
        createParserToast() {
            // Use a shorter name for this.$createElement
            const h = this.$createElement;
            // Create the message
            const vNodesMsg = h(
                'div',
                {ref: 'parserToast'},
                [
                    h('progress-parsing-params', {
                        props: {
                            eventModel: "TradeLot",
                            title: "Parsing lots",
                            hideIfEmpty: true
                        }
                    }),
                    h('progress-parsing-params', {
                        props: {
                            eventModel: "TradeLotParam",
                            title: "Обновление категорий и параметров",
                        }
                    }),
                ]
            )
            // Pass the VNodes as an array for message and title
            this.$bvToast.toast([vNodesMsg], {
                title: 'Обработка лотов',
                id: 'parserToast',
                solid: true,
                variant: 'warning',
                noAutoHide: true
            });

        }
    }
});

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('bootstrap/dist/js/bootstrap.min');
window.$ = require('jquery/dist/jquery');

window.Vue = require('vue');
window.inputmask = require('inputmask');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

import {BootstrapVue, BootstrapVueIcons} from "bootstrap-vue"

import Multiselect from 'vue-multiselect';
window.Vue.component('multiselect', Multiselect);
import 'vue-multiselect/dist/vue-multiselect.min.css'

window.Vue.use(BootstrapVue);
window.Vue.use(BootstrapVueIcons);

import axios from 'axios'
import VueAxios from 'vue-axios'

window.Vue.use(VueAxios, axios);

import 'slick-carousel'
import 'slick-carousel/slick/slick.scss'

const files = require.context('./components/site/', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

// Google maps

import * as VueGoogleMaps from 'vue2-google-maps';

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyDpO0h21ribJIYrlmvrnClu1GrUfhqy_kA',
        libraries: 'places'
    },

    installComponents: true
})
Vue.directive('input-mask', {
    bind: function(el) {
        new Inputmask({
            mask: $(el).attr('mask'),
        }).mask(el);
    },
});



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const app = new Vue({
    el: '#site',
    http: {
        headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
        }
    },
    methods: {
        goToCategoryPage(item) {
            location = item.url
        }
    }
});

/*let url = document.location.toString();
if (url.match('#')) {
    $('.nav-pills-cabinet a[href="#' + url.split('#')[1] + '"]').tab('show');
}*/

// Change hash for page-reload
/*$('.nav-pills-cabinet a').on('shown.bs.tab', function (e) {
    window.location.hash = e.target.hash;
});*/

let tab_invalid = $('.tab-pane .form-control.is-invalid').first().closest('.tab-pane').attr('id');
if (tab_invalid) {
    console.log({tab_invalid})
    $('nav a[href="#' + tab_invalid + '"]').tab('show');
}

// вализация формы
$.fn.ajaxForm = function (options) {
    return this.each(function (index, form) {
        let $form = $(this);
        $form.submit(function (e) {
            e.preventDefault();
            $form.find('.btn').prop('disabled', true)
                .find('.spinner-border').removeClass('d-none');
            let form_data = $(e.target).serialize();
            $.ajax({
                url: this.action,
                method: this.method,
                data: form_data,
            }).done(res => {
                if(res.status == 302) {
                    window.location.href = res.url;
                } else {
                    window.location.reload();
                }
            }).fail((jqXHR, textStatus, errorThrown) => {
                let res = jqXHR.responseJSON;
                //console.log(res.message);
                $form.find('.form-control').removeClass('is-invalid');
                $form.find('.invalid-feedback').remove();
                if (res.errors) {
                    for (let field in res.errors) {
                        let msg_ar = res.errors[field];
                        let group = $form.find(`[name="${field}"]`).closest('.form-group');
                        group.find('.form-control').addClass('is-invalid')
                            .after($('<span class="invalid-feedback" role="alert">').text(msg_ar.join(', ')));
                    }

                } else if (res.message) {
                    alert(res.message)
                }
                $form.find('.btn').prop('disabled', false)
                    .find('.spinner-border').addClass('d-none');
            });
        });
        return this;
    });
};

$(document).on('submit', '#modalService form, #modalServiceTrial form', function(e){
    let formId = $(this);
    let formArr = formId.serializeArray();
    let formDuration;
    $.each(formArr, function(i, fd) {
        if(fd.name === "duration") formDuration = fd.value;
    });  
    if (formDuration !== 'trial') {
        e.preventDefault();
    }
    let action = formId.attr('action');
    let method = formId.attr('method');
    $.ajax({
        'url': action,
        'type': method,
        'dataType': "json",
        'data': formId.serialize(),
        'beforeSend': function() {

        },
        'success': function(response) {
            let form = document.createElement("form");

            form.action = response.action
            form.method = response.method

            for (let key in response.fields) {
                let elem = document.createElement("input");
                elem.type = "hidden"
                elem.name = key
                elem.value = response.fields[key]

                form.appendChild(elem);
            }

            document.body.appendChild(form);
            form.submit();
        },
    });
});
function getPayment(banner) {
    axios.get('/advertising/new-payment/' + banner.order.payment.id, {
        params: {
            back_url_ok: window.location.href,
            back_url_no: window.location.href,
        }
    })
        .then(resp => {
            let data = resp.data
            let form = document.createElement("form");

            form.action = data.action
            form.method = data.method

            console.log(data)

            for(let key in data.fields) {
                let elem = document.createElement("input");
                elem.type = "hidden"
                elem.name = key
                elem.value = data.fields[key]

                form.appendChild(elem);
            }

            document.body.appendChild(form);
            form.submit();
        })
}
$('form#modalLoginForm, form#modalRegisterForm, form#authLoginForm/*, form#questionForm*/').ajaxForm();

// form reset password
$('form#formResetPass').submit(function (e) {
    e.preventDefault();
    var $form = $(e.target);
    $form.find('.btn').prop('disabled', true);
    $form.find('.spinner-border').removeClass('d-none');
    $.post($form.attr('action'), $form.serialize())
        .then(function (res) {
            $form.html($(res).find('#' + $form.attr('id')).children())
        });
    return false;
});
Inputmask("currency").mask('input.input-price');

// Inputmask("phone").mask('input#inp_phone');
$('[data-toggle="tooltip"]').tooltip();
$('.header-categories').on('mouseleave', function () {
    $('.header-categories.open').removeClass('open')
});

// question-item
$('.question-item').each(function (index, el) {
    var $container = $(this);
    var $btn = $(this).find('.question-item-btn');
    var $answer = $(this).find('.question-item-answer-container');
    $btn.click((e) => {
        if ($container.hasClass('opened')) {
            $answer.slideUp();
        } else {
            $answer.slideDown()
        }
        $container.toggleClass('opened')
    })
});

// slick sliders
$('.slick-variable-width').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    variableWidth: false,
    responsive: [
        {
            breakpoint: 996,
            settings: {
                autoplay: true,
                autoplaySpeed: 3000,
            }
        },
        {
            breakpoint: 520,
            settings: {
                slidesToShow: 1,
            }
        }
    ]
});

$('.slick-variable-width-banners-for-lot').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    variableWidth: true,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                arrows: false,
                infinite: true,
                autoplay: true,
                autoplaySpeed: 3000,
            }
        },
        {
            breakpoint: 520,
            settings: {
                slidesToShow: 1,
            }
        }
    ]
});

$('.slick-variable-width-lot').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    variableWidth: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 10000,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                arrows: false,
            }
        },
        {
            breakpoint: 520,
            settings: "unslick"
        }
    ]
});

$('#modalService, #modalServiceInfo, #modalServiceTrial').on('show.bs.modal', function (event) {
    var $button = $(event.relatedTarget);
    var btnOrder = $(this).find('.modal-body .btn-order');
    var service = $button.data('service');
    var description = $button.data('description');
    var regulations = $button.attr('data-regulations');
    var service_id = $button.data('id');
    var duration = $button.data('duration');
    var serv_id = $button.attr('data-id');
    var targetLink = $(this).find('.modal-body .regulations-link');
    var inputDuration = $(this).find('.modal-body .duration');
    var href = 'https://baids.ru/documents';

    inputDuration.val(duration);

    var checkBoxes = $(this).find('.modal-body .agreement');
    if (checkBoxes.length > 0) {
        btnOrder.prop('disabled', true);
    }
    checkBoxes.prop('checked', false);
    $(this).find('.modal-body .agreement-regulations').show();
    $(this).find('.modal-body .eula-regulations').hide();
    $(this).find('.modal-body .eula-regulations input').prop('checked', true);

    switch (serv_id) {
        case "7":
            href += '?tab=mailing';
            break;
        case "8":
            href += '?tab=inspection';
            break;
        case "9":
            href += '?tab=application';
            break;
        case "10":
            href += '?tab=escort';
            break;
        case "12":
            href += '?tab=familiarization';
            break;
        default:
           $(this).find('.modal-body .agreement-regulations').hide();
           $(this).find('.modal-body .agreement-regulations input').prop('checked', true);
    }

    if (service.indexOf("Тарифный план") !== -1) {
        $(this).find('.modal-body .eula-regulations').show();
        $(this).find('.modal-body .eula-regulations input').prop('checked', false);
        $(this).find('.modal-body .agreement-regulations').hide();
        $(this).find('.modal-body .agreement-regulations input').prop('checked', true);
    }

    targetLink.attr('href', href);

    checkBoxes.on('click', function () {
        if (checkBoxes.filter(':checked').length === checkBoxes.length) {
            btnOrder.prop('disabled', false);
        } else {
            btnOrder.prop('disabled', true);
        }
    });

    var trade_lot_id = $button.data('lot-id');
    $(this).find('.modal-body .service-text').text(service);
    $(this).find('.modal-body .service-description').text(description);
    $(this).find('.modal-body .service-regulations').text(regulations);
    $(this).find('.modal-body .modalServiceForm_serviceId').val(service_id);
    $(this).find('.modal-body .modalServiceForm_tradeLotId').val(trade_lot_id);
});

$('#modalLogin').on('show.bs.modal', function(event) {
    var $button = $(event.relatedTarget);
    var redirect = $button.data('redirect') || '';
    if(redirect) {
        $(this).find('#login_form_redirect').val(redirect);
    }
});

// lot components
$('body').on('click', '.j-favorite-link', function (e) {
    let id = $(this).data('id');
    let flag = !$(this).hasClass('in-favorite');
    $(this).prop('disabled', true);
    axios.post('/api/cabinet/favorite', {id, flag})
        .then(res => {
            if (res.data) {
                $(this).addClass('in-favorite')
            } else {
                $(this).removeClass('in-favorite')
            }
        })
        .catch(error => {
            console.log(error);
            if (error.response.status === 401) {
                $('#modalLogin').modal('show')
            }
        })
        .finally(() => {
            $(this).prop('disabled', false);
        });
});

/* Скрытие и раскрытие элементов в зависимости от селекта */

function formDisabled() {

    let form = $('.profile-form');
    let disabled = form.hasClass('disabled');

    console.log(disabled);

    if(disabled){
        form.find('select').prop('disabled', true);
        form.find('input').prop('disabled', true);
    }

}


$('#org_profile_type').on('change', function(e){

    let select = $(this);
    let value = select.val();

    let show_el = '.org_profile-type__';
    let hide_el = '.org_profile-type__';

    let show_hide = true;

    if(value == 1) {
        show_el += 1;
        hide_el += 2;
    }
    else if(value == 2) {
        show_el += 2;
        hide_el += 1;
    }
    else {
        show_hide = false;
    }

    if(show_hide) {
        $(show_el).show();
        $(hide_el).hide();
    }


});

// lk menu for sm
$(".cabinet-menu-btn").click(function () {
    $("#navbarCabinet + div").toggle("slow");
})

function setMinHeightContainer(){
    $('.i-min-height-container').css('min-height', window.innerHeight - 467)
}
$(document).ready(setMinHeightContainer);
$(window).resize(setMinHeightContainer);

/* При загрузке страницы вызываем собитие */

$(document).ready(function(){
    $('#org_profile_type').change();
    formDisabled();

    /*$('#filterForm').submit(function (event) {
        let $content = $('#catalog-content');
        event.preventDefault();
        $content.addClass('loading').load('?'+ $(event.target).serialize() + " #catalog-content", _ => {
            $content.removeClass('loading');
            // add history url
        })
    })*/

    let toggleTextBtn = document.querySelectorAll('.content_toggle');
    let newsText = document.querySelectorAll('.news-text');
    let news = document.querySelectorAll('.content_block');
    toggleTextBtn.forEach((btn, index) => {
        
        if(newsText[index].offsetHeight < 220) {
            toggleTextBtn[index].style.display = 'none';
            return;
        }

        btn.addEventListener('click', () => {
            news[index].classList.toggle('hide');
            if(news[index].classList.contains('hide')) {
                btn.textContent = 'Читать полностью';
            } else {
                btn.textContent = 'Скрыть';
            }
        });
    });
});			



/* Fill up order evaluation modal dialog */

$('.evalOpen').on('click', function() {
    let orderTitle = $('.evalOpen').attr("data-order-title"),
        orderId = $('.evalOpen').attr("data-order-id");

    $('.order-id').val(orderId);
    $('.header-text-popup span')[0].textContent = `"${orderTitle}"`;
});		

$('#modalReport').on('show.bs.modal', function (event) {
    let trade_lot_id = $(event.relatedTarget).data('lot-id');
    $(this).find('#modalReport_tradeLotId').val(trade_lot_id);
});

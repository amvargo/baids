import moment from 'moment-timezone'

Vue.filter('localDateTime', (date) => {
    if (!date) {
        return '-';
    }
    return moment.utc(date).local().format('L LT');
});
Vue.filter('localDate', (date) => {
    if (!date) {
        return '-';
    }
    return moment.utc(date).local().format('L');
});
Vue.filter('localTime', (time) => {
    if (!time) {
        return '-';
    }
    return moment(time, "HH:mm:ss").format('hh:mm A');
});
Vue.filter('localCalendar', (date) => {
    if (!date) {
        return '-';
    }
    return moment.utc(date).local().calendar();
});
Vue.filter('localLastTime', (date) => {
    if (!date) return '-';
    return moment.utc(date).local().fromNow();
});

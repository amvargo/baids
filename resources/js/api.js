const url = '/';
const url_api = url + 'panel/api/';

let app_link = null;

const alertService = {
    alert: (msg, type, translate) => {
        console.log('alertService.alert:', msg, {type, translate});
        app_link.$emit('toast', 'Error', msg, 'warning')
    },
};

function middlewareErrors(res) {
    return new Promise((resolve, reject) => {

        const contentType = res.headers.get("content-type");
        //console.log({contentType})
        if (contentType && contentType.indexOf("application/json") !== -1) {
            let status = res.status + "";
            if (status === '401') {
                return location.href = '/login';
            }
            return res.json().then(data => {
                // process your JSON data further
                //console.log('process your JSON data further', {data})
                if (!!data.error) {
                    console.log("Error:", data.error.code, "->", data.error.message);
                    alertService.alert(data.error.message, (data.error.type || "warning"), (data.error.translate || false));
                    return !!data.continue ? resolve(data) : reject(data.error.message);
                } else if (status !== "200") {
                    if (!!data.message) {
                        alertService.alert(data.message, "danger", false);
                    }
                    return reject(data);
                } else {
                    return resolve(data);
                }

            });
        } else {
            return res.text().then(text => {
                // this is text, do something with it
                //console.log('this is text, do something with it', {status:res.status, text})
                if (res.status >= 200 && res.status < 300) {
                    return resolve(text);
                } else {
                    return reject(text);
                }
            });
        }

    });
}

const headers = {
    "Accept": "application/json",
    "Cache-Control": "no-cache",
    "Content-Type": "application/json",
};

function fetchGet(url) {
    return fetchRequest(url, 'get');
}

function fetchPost(url, data = {}) {
    return fetchRequest(url, 'post', data);
}

function fetchPut(url, data = {}) {
    return fetchRequest(url, 'put', data);
}

function fetchDelete(url) {
    return fetchRequest(url, 'delete');
}

function fetchRequest(url, method = 'post', data = {}) {
    //console.log({url, data})
    data._token = window.token;
    let options = {
        method: method,
        credentials: "same-origin",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-TOKEN": window.token
        },
    };
    if (['post', 'put'].indexOf(method) !== -1) {
        options.body = JSON.stringify(data);
    }
    return fetch(url, options);
}

function fetchPostForm(url, formData) {
    //formData.append('_token', window.token);
    let options = {
        method: 'post',
        credentials: "same-origin",
        headers: {
            "Accept": "application/json",
            //"Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            "X-CSRF-TOKEN": window.token
        },
        body: formData,
    };
    return fetch(url, options);
}

export default {
    fetchGet,
    fetchPost,
    fetchPut,
    fetchDelete,
    fetchRequest,
    fetchPostForm,
    middlewareErrors,
    link(app) {
        app_link = app
    },
    login: (data) => {
        return fetchPost(url + 'login', data)
            .then(middlewareErrors)
            .then(token => window.token = token)
    },
    register: (data) => {
        return fetchPost(url + 'register', data)
            .then(middlewareErrors)
            .then(token => window.token = token)
    },
    logout: () => {
        return fetchPost(url + 'logout', {})
    },
    loadDashboard: () => {
        return fetch(url_api + 'dashboard', {headers})
            .then(middlewareErrors)
            .then(res => {
                if (res.counts !== undefined) {
                    app_link.$emit('dashboard_counts', res.counts)
                }
                return res
            })
    },

    // resources
    getResources: (table, params) => {
        let url = url_api + 'resource/' + table;
        if (params) {
            let query = $.param(params);
            url += '?' + query;
        }
        return fetch(url, {headers})
            .then(middlewareErrors)
    },
    getResource: (table, id) => {
        return fetch(url_api + 'resource/' + table + '/' + id, {headers})
            .then(middlewareErrors)
    },
    updateResource: (table, form, params) => {
        let url = url_api + 'resource/' + table + '/' + form.id;
        if (params) {
            let query = $.param(params);
            url += '?' + query;
        }
        return fetchPut(url, form)
            .then(middlewareErrors)
    },
    updateResourceByFormData: (table, form_data) => {
        return fetchPostForm(url_api + 'resource/' + table + '/' + form_data.get('id'), form_data)
            .then(middlewareErrors)
    },
    createResource: (table, form, params) => {
        let url = url_api + 'resource/' + table;
        if (params) {
            let query = $.param(params);
            url += '?' + query;
        }
        return fetchPost(url, form)
            .then(middlewareErrors)
    },
    deleteResource: (table, id, params) => {
        let url = url_api + 'resource/' + table + '/' + id;
        if (params) {
            let query = $.param(params);
            url += '?' + query;
        }
        return fetchDelete(url)
            .then(middlewareErrors)
    },

}

import DashboardScreen from "./screens/DashboardScreen";
import NotFoundScreen from "./screens/NotFoundScreen";
import UsersScreen from "./screens/UsersScreen";
import QuestionsScreen from "./screens/QuestionsScreen";
import ConsultingScreen from "./screens/ConsultingScreen";
import ServicesScreen from "./screens/ServicesScreen";
import OrdersScreen from "./screens/OrdersScreen";
import OrderScreen from "./screens/OrderScreen";
import PaymentsScreen from "./screens/PaymentsScreen";
import StaffScreen from "./screens/StaffScreen";
import CatalogScreen from "./screens/CatalogScreen";
import CatalogLotsScreen from "./screens/CatalogLotsScreen";
import CatalogLotScreen from "./screens/CatalogLotScreen";
import CatalogCategoriesScreen from "./screens/CatalogCategoriesScreen";
import CatalogCategoryScreen from "./screens/CatalogCategoryScreen";
import CatalogParamsScreen from "./screens/CatalogParamsScreen";
import CatalogParamScreen from "./screens/CatalogParamScreen";
import VehiclesScreen from "./screens/VehiclesScreen";
import SpecialVehiclesScreen from "./screens/SpecialVehiclesScreen";
import VehicleScreen from "./screens/VehicleScreen";
import SpecialVehicleScreen from "./screens/SpecialVehicleScreen";
import TradesScreen from "./screens/TradesScreen";
import TradeScreen from "./screens/TradeScreen";
import MessagesScreen from "./screens/MessagesScreen";
import SlidersScreen from "./screens/SlidersScreen";
import ModerationUserOrgTradeScreen from "./screens/ModerationUserOrgTradeScreen";
import ModerationAdvertisingScreen from "./screens/ModerationAdvertisingScreen";
import NewsScreen from "./screens/NewsScreen";
import AboutProjectScreen from "./screens/AboutProjectScreen";

import SeoConfigurationsScreen from "./screens/SeoConfigurationsScreen";
import OptionsScreen from "./screens/OptionsScreen";

export default [
    { path: '/panel', component: DashboardScreen },
    { path: '/panel/users', component: UsersScreen },
    { path: '/panel/users-page-:page', component: UsersScreen },
    { path: '/panel/questions', component: QuestionsScreen },
    { path: '/panel/questions-page-:page', component: QuestionsScreen },
    { path: '/panel/consulting', component: ConsultingScreen },
    { path: '/panel/consulting_messages-page-:page', component: ConsultingScreen },
    { path: '/panel/services', component: ServicesScreen },
    { path: '/panel/services-page-:page', component: ServicesScreen },
    { path: '/panel/orders', component: OrdersScreen },
    { path: '/panel/orders-page-:page', component: OrdersScreen },
    { path: '/panel/order/:id', component: OrderScreen },
    { path: '/panel/payments', component: PaymentsScreen },
    { path: '/panel/payments-page-:page', component: PaymentsScreen },
    { path: '/panel/staff', component: StaffScreen },
    { path: '/panel/staff-page-:page', component: StaffScreen },
    { path: '/panel/news', component: NewsScreen },
    { path: '/panel/about-project', component: AboutProjectScreen },

    { path: '/panel/catalog', component: CatalogScreen },

    { path: '/panel/catalog-categories', component: CatalogCategoriesScreen },
    { path: '/panel/catalog-categories-page-:page', component: CatalogCategoriesScreen },
    { path: '/panel/catalog-category-create', component: CatalogCategoryScreen },
    { path: '/panel/catalog-category-:id', component: CatalogCategoryScreen },

    { path: '/panel/catalog-params', component: CatalogParamsScreen },
    { path: '/panel/catalog-params-page-:page', component: CatalogParamsScreen },
    { path: '/panel/catalog-param-create', component: CatalogParamScreen },
    { path: '/panel/catalog-param-:id', component: CatalogParamScreen },

    { path: '/panel/catalog-lots', component: CatalogLotsScreen },
    { path: '/panel/catalog-lots-page-:page', component: CatalogLotsScreen },
    { path: '/panel/catalog-lot-create', component: CatalogLotScreen },
    { path: '/panel/catalog-lot-:id', component: CatalogLotScreen },

    { path: '/panel/model/VehicleMaker', component: VehiclesScreen },
    { path: '/panel/model/VehicleMaker-page-:page', component: VehiclesScreen },
    { path: '/panel/model/VehicleMaker-create', component: VehicleScreen },
    { path: '/panel/model/VehicleMaker-:id', component: VehicleScreen },

    {path: '/panel/model/VehicleSpecialMaker', component: SpecialVehiclesScreen},
    {path: '/panel/model/VehicleSpecialMaker-page-:page', component: SpecialVehiclesScreen},
    {path: '/panel/model/VehicleSpecialMaker-create', component: SpecialVehicleScreen},
    {path: '/panel/model/VehicleSpecialMaker-:id', component: SpecialVehicleScreen},

    { path: '/panel/model/Trade', component: TradesScreen },
    { path: '/panel/model/Trade-page-:page', component: TradesScreen },
    //{path: '/panel/model/Trade-create', component: VehicleScreen},
    { path: '/panel/model/Trade-:id', component: TradeScreen },

    { path: '/panel/model/Order', component: OrdersScreen },
    { path: '/panel/model/Order-page-:page', component: OrdersScreen },
    { path: '/panel/model/Order-:id', component: OrderScreen },

    { path: '/panel/model/UserPayment', component: PaymentsScreen },
    { path: '/panel/model/UserPayment-page-:page', component: PaymentsScreen },

    { path: '/panel/messages', component: MessagesScreen },

    { path: '/panel/sliders', component: SlidersScreen },

    { path: '/panel/moderation/user-org-trade', component: ModerationUserOrgTradeScreen },
    { path: '/panel/moderation/advertising', component: ModerationAdvertisingScreen },

    { path: '/panel/seo-configurations', component: SeoConfigurationsScreen },

    { path: '/panel/options', component: OptionsScreen },

    { path: '/panel/*', component: NotFoundScreen },
];

<?php

return [
    'welcome' => 'Welcome to our application',
    'lots' => ':count лот|:count лота|:count лотов',
    'in_categories_count' => 'в :count категории|в :count категориях|в :count категориях',
];

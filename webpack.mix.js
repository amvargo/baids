const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/panel.js', 'public/js')
    .sass('resources/sass/panel.scss', 'public/css');

mix.js('resources/js/site.js', 'public/js')
    .sass('resources/sass/site.scss', 'public/css');

mix.copyDirectory('resources/img', 'public/img');

if (mix.inProduction()) {
    mix.version();
} else {
    // https://browsersync.io/docs/options
    // mix.browserSync({
    //     proxy: {
    //         target: "http://bankrotstvo.loc",
    //         ws: true
    //     }
    // });
}


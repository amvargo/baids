<?php

namespace App\Http\Requests;

use App\Models\TradeLotCategory;
use App\Rules\Phone;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserLotCreating extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'category' => 'required|exists:trade_lot_categories,id',
            'address' => 'required|max:255',
            'ApplicationTimeBegin' => 'required|date|after:now',
            'ApplicationTimeEnd' => 'required|date|after:ApplicationTimeBegin',
            'arbitr.fio' => 'required',
            'arbitr.email' => 'required|email',
            'arbitr.inn' => 'required|string|min:10|max:12',
            'arbitr.phone' => ['required', new Phone()],
            'arbitr.sro' => 'required|string|min:3|max:15',
            'auction_type' => 'required',
            'debtor.inn' => 'required|string|min:10|max:12',
            'debtor.name' => 'required',
            'debtor.ogrn' => 'required',
            'debtor.type' => 'required',
            'title' => 'required',
            'description' => 'required',
            'ApplicationRules' => 'required',
            'SaleAgreement' => 'required',
            'PaymentInfo' => 'required',
            'start_price' => 'required|numeric',
            'advance_price' => [
                'required',
                'regex:/^(\d+)(%)?$/'
            ],
            'step_price' => [
                'required',
                'regex:/^(\d+)(%)?$/'
            ],
            'trade_place_url' => 'url',
            'trade_section' => 'required',
            'images.*' => 'image|mimes:jpeg,png,jpg|max:2000',
            'docs.*' => 'file|mimes:pdf,zip,doc,docx,txt|max:2000',
        ];
        $category = TradeLotCategory::find(request()->get('category'));
        if ($category) {
            foreach ($category->params as $param) {
                if (!empty($param->validator)) {
                    $rules["params.{$param->code}"] = $param->validator;
                }
            }
        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'address' => 'Адрес',
            'title' => 'Название лота',
            'description' => 'Общая информация',
            'ApplicationRules' => 'Порядок, место, срок и время представления заявок',
            'SaleAgreement' => 'Порядок и срок заключения договора..',
            'PaymentInfo' => 'Сроки платежей, реквизиты счетов, на которые вносятся платежи',
            'images' => 'Изображения лота',
            'images.*' => 'Изображения лота',
            'start_price' => 'Начальная цена лота',
            'advance_price' => 'Размер задатка для участия',
            'step_price' => 'Шаг повышения цены',
            'ApplicationTimeBegin' => 'Начало приема заявок',
            'ApplicationTimeEnd' => 'Окончание приема заявок',
            'trade_place_url' => 'Страница лота на ЭТП',
            'debtor.name' => 'ФИО / организация',
            'debtor.inn' => 'ИНН',
            'debtor.ogrn' => 'ОГРН(ИП)',
            'arbitr.fio' => 'ФИО',
            'arbitr.email' => 'E-mail',
            'arbitr.phone' => 'Телефон',
            'arbitr.inn' => 'ИНН',
            'arbitr.sro' => 'Наименование СРО',
            'docs' => 'Документы',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Поле обязательно для заполнения',
            'exists' => 'Неправильное значение поля',
            'date' => 'Неправильный формат даты',
            'date_format' => 'Неправильный формат даты',
            'email' => 'Неправильный формат электронной почты',
            'phone' => 'Неправильный формат телефона',
            'url' => 'Неправильный формат ссылки',
            'numeric' => 'Должно быть числом',

            'category.exists' => 'Выберите категорию',
            'ApplicationTimeBegin.after' => 'Дата должна быть больше текущей',
            'ApplicationTimeEnd.after' => 'Дата должна быть после даты начала',
            'images.*.mimes' => 'Формат должен быть: :values',
            'images.*.image' => 'Файл должен быть изображением',
            'images.*.uploaded' => 'Ошибка загрузки',

            'params.*.min' => 'Неправильное значение поля',
            'params.*.max' => 'Неправильное значение поля',
            'params.*.digits' => 'Неправильное значение поля',
            'params.*.integer' => 'Должно быть числом',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

class CatalogLocation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'locations' => [
                'array'
            ],
            'locations.*.region_with_type' => [
                'string', 'required'
            ],
            'locations.*.city_with_type' => [
                'string', 'nullable'
            ]
        ];
    }

    public function saveToSession()
    {
        if ($filter = $this->validated()) {
            Session::put('location.filter2', $filter);

            // $locations = [ { region_with_type: '...', city_with_type: '...' }, ... ];
            $locations = data_get($filter, 'locations', []);
            $locations_count = count($locations);
            if($locations_count > 2){
                $location_str = trans_choice('location.region_count', $locations_count);
            } else {
                $location_arr = [];
                foreach ($locations as $location) {
                    $location_arr[] = $location['city_with_type'] ?: $location['region_with_type'];
                }
                $location_str = implode(', ', $location_arr);
            }

            Session::put('location.filter2.text', $location_str);
        };
        return $filter;
    }

    public static function getFromSession()
    {
        return Session::get('location.filter2', [
            'locations' => []
        ]);
    }

    public static function getRegionString()
    {
        $data = self::getFromSession();
        if (!$data || !$data['all_russia']) {
            return 'Вся Россия';
        };
        return implode(data_get($data, 'regions', []), ', ');
    }
}

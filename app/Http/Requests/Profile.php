<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class Profile extends FormRequest
{

    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:64',
            'last_name' => 'required|string|min:3|max:64',
            'middle_name' => 'required|string|min:3|max:64',
            'email' => 'required|string|email|max:127',
            'phone' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'phone' => 'телефон',
            'email' => 'E-mail',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->checkEmail()) {
                $validator->errors()->add('email', 'Указанный Вами E-mail занят');
            }
            if ($this->checkPhone()) {
                $validator->errors()->add('phone', 'Указанный Вами телефон занят');
            }
        });
    }

    private function checkEmail()
    {
        $users_with_email = User::where('id', '<>', $this->user()->id)
            ->where('email', '=', $this->input('email'))
            ->take(1)
            ->count();
        return $users_with_email > 0;
    }

    private function checkPhone()
    {
        $users_with_email = User::where('id', '<>', $this->user()->id)
            ->where('phone', '=', $this->input('phone'))
            ->take(1)
            ->count();
        return $users_with_email > 0;
    }
}

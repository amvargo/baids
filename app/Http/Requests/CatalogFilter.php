<?php

namespace App\Http\Requests;

use App\Models\Trade;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class CatalogFilter extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filter.*' => [
                'nullable',
            ],
            'filter.*.from' => [
                'nullable',
            ],
            'filter.*.to' => [
                'nullable',
            ],
            'filter.auction_type.*' => [
                'nullable',
                Rule::in(array_keys(Trade::$auction_types_for_filter))
            ],
            'filter.trade_organizer' => [
                'nullable',
            ],
            'filter.debtor' => [
                'nullable',
            ],
        ];
    }

    public function getUsedKeys()
    {
        $result = [];
        if ($this->validated()) foreach ($this->get('filter') as $key => $item) {
            if (is_array($item) && key_exists('from', $item) && key_exists('to', $item)) {
                if ($item['from'] !== null || $item['to'] !== null) {
                    $result[] = $key;
                }
            } elseif (is_array($item) && !empty($item)) {
                $result[] = $key;
            }
        }
        return $result;
    }
}

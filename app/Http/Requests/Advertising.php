<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Advertising extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'link' => 'required|active_url',
            'image' => 'required|file|mimes:jpeg,gif,png',
            'variant' => 'required|in:vertical,horizontal',
            'date_start' => 'required|date_format:d.m.Y',
            //'date_end' => 'required|date_format:d.m.Y',
            'service_id' => 'required',
            'category_id' => 'nullable'

        ];
    }

    public function attributes()
    {
        return [
            'link' => 'Ссылка',
            'image' =>'Баннер',
            'variant' => 'Рекламное место',
            'date_start' => 'Дата',
            //'date_start' => 'Дата начала',
            //'date_end' => 'Дата окончания',
            'place' => 'Страница размещения',
            'category_id' => 'Категория',
            'service_id' => 'Услуга',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Поле :attribute обязательно для заполнения',
            'active_url' => 'Введите корректую ссылку',
            'file' => 'Не правильный формат',
            'mimes' => 'Поле :attribute может иметь расширения: jpeg, gif, png',
            'in' => 'Не правильное значение поля :attribute',
            'date_format' => 'Поле :attribute имеет не правильный формат'
        ];
    }

}

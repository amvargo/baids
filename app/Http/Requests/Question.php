<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Question extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'author' => 'required|string|min:3|max:64',
            'email' => 'required|string|email|max:255',
            'question' => 'required|string|min:30',
        ];
    }

    public function attributes()
    {
        return [
            'author' => 'Имя',
            'email' => 'E-mail',
            'question' => 'Вопрос',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Поле :attribute обязательно для заполнения',
            'max' => 'Превышена длина :max символов',
            'min' => 'Должно содержать не меньше :min символов',
            'email' => 'Не верный формат электронной почты',
        ];
    }

}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Consulting extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required|numeric',
            'full_name' => 'required|string|min:3|max:64',
            'email' => 'required|string|email|max:255',
            'phone' => 'nullable',
            'message' => 'required|string|min:30',
            //'answer' => 'nullable|string|min:10',
        ];
    }

    public function messages()
    {
        return [
            'email.email' => 'Введён неверный Email',
        ];
    }

    public function attributes()
    {
        return [
            'category_id' => 'Категория вопроса',
            'full_name' => 'ФИО',
            'phone' => 'Телефон',
            'message' => 'Сообщение',
            //'answer' => 'Ответ',
        ];
    }

}

<?php

namespace App\Http\Requests;

use App\Models\TradeLot;
use Illuminate\Foundation\Http\FormRequest;

class CatalogSort extends FormRequest
{

    public $field = 'id';
    public $dir = 'desc';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $sort_fields = implode('|', array_keys(TradeLot::$sort_list));
        return [
            'sort' => [
                'nullable',
                "regex:/^([-])?({$sort_fields})$/",
            ]
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $sort = $this->getSorting();
            $this->field = $sort->field;
            $this->dir = $sort->dir;
        });
    }

    public function getSorting()
    {
        // sort
        $sorting = $this->get('sort', null);
        $sort_by = 'id';
        $sort_dir = 'desc';
        if ($sorting && preg_match("/^(?P<dir>[-])?(?P<field>\w+)$/", $sorting, $m)) {
            $sort_by = $m['field'];
            $sort_dir = $m['dir'] === '-' ? 'desc' : 'asc';
        }
        return (object)[
            'field' => $sort_by,
            'dir' => $sort_dir,
        ];
    }
}

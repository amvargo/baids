<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\RequiredIf;

class OrgProfile extends FormRequest
{

    public function rules()
    {
        return [
            'inn' => 'required|string|min:10|max:12',
            'org_profile_type' => 'required|integer|digits_between:1,2',
            'org_profile_first_name' => 'required_if:org_profile_type,==,1',
            'org_profile_last_name' => 'required_if:org_profile_type,==,1',
            'org_profile_middle_name' => 'required_if:org_profile_type,==,1',
            'org_profile_full_name' => 'required_if:org_profile_type,==,2'
        ];
    }

    public function attributes()
    {
        return [
            'inn' => 'ИНН',
            'org_profile_type' => 'Тип организатора',
            'org_profile_first_name' => 'Имя организатора',
            'org_profile_last_name' => 'Фамилия организатора',
            'org_profile_middle_name' => 'Отчество организатора',
            'org_profile_full_name' => 'Наименование организации'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Поле :attribute обязательно для заполнения',
            'required_if' => 'Поле :attribute обязательно для заполнения',
            'min' => [
                'numeric' => ':attribute должен содержать не менее :min символов',
                'string' => ':attribute должен содержать не менее :min символов',
            ],
            'max' => [
                'numeric' => ':attribute должен содержать не более :max символов',
                'string' => ':attribute должен содержать не более :max символов',
            ],
        ];
    }


}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Message extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
//    public function authorize()
//    {
//        return false;
//    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'body' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Тема',
            'body' => 'Сообщение',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Поле :attribute обязательно для заполнения',
            'max' => [
                'numeric' => ':attribute должен содержать не более :max символов',
                'string' => ':attribute должен содержать не более :max символов',
            ],
        ];
    }
}

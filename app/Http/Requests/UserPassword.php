<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class UserPassword extends FormRequest
{

    public function rules()
    {
        return [
            'password' => 'required|string',
            'new_password' => [
                'required', 'min:8', 'max:16',
                'regex:/^.*(?=.{6,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d]).*$/', // (?=.*[!$#%])
            ],
            'new_password_confirmation' => 'same:new_password',
        ];
    }

    public function attributes()
    {
        return [
            'password' => 'Пароль',
            'new_password' => 'Новый пароль',
            'new_password_confirmation' => 'Повторите пароль',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Поле :attribute обязательно для заполнения',
            'max' => 'Превышена длина :max символов',

            'new_password.regex' => 'Пароль не достаточно сложный',
            'new_password.min' => 'Пароль не достаточно сложный',
            'new_password_confirmation.same' => 'Поле не прошло поверку'
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->checkOldPassword()) {
                $validator->errors()->add('password', 'Введен неверный пароль');
            }
        });
    }

    private function checkOldPassword()
    {
        if (!Hash::check($this->input('password'), auth()->user()->password)) {
            return true;
        }
        return false;
    }
}

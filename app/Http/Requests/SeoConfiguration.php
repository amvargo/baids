<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SeoConfiguration extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'object_type' => 'nullable',
            'object_id' => 'nullable',
            'name' => 'required_without_all:object_id,object_type',
            'title' => 'required',
            'description' => 'required',
            'keywords' => 'nullable'
        ];
    }

    public function attributes()
    {
        return [
            'url' => 'Ссылка',
            'name' =>'Название',
            'title' => 'SEO тайтл',
            'description' => 'SEO описание',
            'keywords' => 'Ключевые слова',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Поле :attribute обязательно для заполнения',
            'required_without_all' => 'Поле :attribute обязательно для заполнения',
            'active_url' => 'Введите корректую ссылку',
        ];
    }

}

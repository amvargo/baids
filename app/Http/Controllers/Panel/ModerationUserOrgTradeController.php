<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Models\UserOrgTrade;
use Illuminate\Support\Arr;


class ModerationUserOrgTradeController extends Controller
{

    public function index(Request $request) {

        $order = $request->input('order');
        $filter = json_decode($request->input('filter'), true);
        $filter_status =  $this->getParam($filter, 'filter_status');
        $type = $this->getParam($filter, 'type');

        $items = UserOrgTrade::orderBy('id', $order)
            ->with(['user' => function($query){
                return $query->select('id', 'name', 'last_name', 'middle_name');
            }])
            ->when($filter_status, function($query) use ($filter_status){
                return $query->byStatus($filter_status);
            })
            ->when($type, function($query) use($type){
                return $query->byType($type);
            })
            ->paginate(10);
        return $items;
    }

    public function changeStatus(Request $request){

        $id = $request->post('id');
        $status = $request->post('status');
        if($id) {

            $model = UserOrgTrade::find($id);
            if($model) {
                $model->update(['status' => $status]);
            }
        }

    }

    public function getStatuses(){

        return ['statuses' => UserOrgTrade::STATUSES, 'types' => (new UserOrgTrade)->allowed_types];
    }

    public function getParam($arr = [], $param = '') {

        $result = Arr::get($arr, $param, null);

        return $result;
    }
}

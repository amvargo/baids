<?php

namespace App\Http\Controllers\Panel;

use App\Models\News;
use App\Role\UserRole;
use App\Traits\PageTableController;

class NewsController extends Controller
{
    use PageTableController;

    function tableModel()
    {
        return News::class;
    }

    public function resourceAccess()
    {
        return [
            'list' => UserRole::ROLE_CONTENT,
            'item' => UserRole::ROLE_CONTENT,
            'create' => UserRole::ROLE_CONTENT,
            'update' => UserRole::ROLE_CONTENT,
            'delete' => UserRole::ROLE_CONTENT,
        ];
    }

    public function geCreateValidation()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
        ];
    }

    public function getUpdateValidation()
    {
        return $this->geCreateValidation();
    }

    public function imageUpload2($id)
    {
        $request = request();
        $item = News::findOrFail($id);
        $item->uploadImage($request->file('image'), true);
        return $item;
    }

    public function imageDelete2($id)
    {
        $item = News::findOrFail($id);
        $item->deleteImage(true);
        return $item;
    }
}

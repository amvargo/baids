<?php

namespace App\Http\Controllers\Panel\Catalog;

use App\Http\Controllers\Controller;
use App\Models\TradeLot;
use App\Models\TradeLotCategory;
use App\Models\TradeLotClassificator;
use App\Role\UserRole;
use App\Traits\PageTableController;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CategoryController extends Controller
{
    use PageTableController;

    function tableModel()
    {
        return TradeLotCategory::class;
    }

    public function tableModelWith()
    {
        return ['parent'];
    }

    public function tableModelDetailWith()
    {
        return ['classifications', 'regulars'];
    }

    public function resourceAccess()
    {
        return [
            'list' => UserRole::ROLE_ADMIN,
            'item' => UserRole::ROLE_ADMIN,
            'create' => UserRole::ROLE_ADMIN,
            'update' => UserRole::ROLE_ADMIN,
            'delete' => UserRole::ROLE_ADMIN,
        ];
    }

    public function geCreateValidation()
    {
        return [

        ];
    }

    public function getUpdateValidation()
    {
        return $this->geCreateValidation();
    }

    public function lists()
    {
        $classifications = TradeLotClassificator::all();
        $categories = TradeLotCategory::all();
        return compact('classifications', 'categories');
    }

    public function update(request $request)
    {
        $this->checkResourceAccess('update');
        $this->checkUpdatingValidation();

        $id = $request->get('id');
        if ($with = $this->tableModelDetailWith()) {
            $item = TradeLotCategory::withTrashed()->with($with)->findOrFail($id);
        } else {
            $item = TradeLotCategory::withTrashed()->findOrFail($id);
        };

        if ($request->post('restore')) {
            $item->restore();
        }

        $item->fill($request->all());

        if (!$item->save()) {
            throw new HttpException(400, 'Error updating model');
        }

        $this->afterUpdate($item);
        //Artisan::queue("parser:params", ['--category' => $item->id]);

        return response()->json($item->refresh());
    }

    public function store()
    {
        $this->checkResourceAccess('create');
        if (!empty($rules = $this->geCreateValidation())) {
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                throw new HttpException(400, $validator->errors()->first());
            }
        }

        // store
        $model = new TradeLotCategory();
        $model->fill(Input::all());
        $model->save();

        $this->afterUpdate($model);

        return response()->json($model->refresh());
    }

    private function afterUpdate($item, $request = null)
    {
        if (!$request) {
            $request = request();
        }

        $item->classifications()->sync(Arr::pluck($request->post('classifications'), 'id'));

        $regulars = Collection::make($request->get('regulars', []));
        $sended_ids = $regulars->filter(function ($item) {
            return !empty($item['id']);
        })->pluck('id');
        foreach ($regulars as $regular) {
            if (empty($regular['regex'])) {
                continue;
            }
            if (isset($regular['id'])) {
                $req_model = $item->regulars()->find($regular['id']);
                $req_model->update($regular);
            } else {
                $id = $item->regulars()->create($regular);
                $sended_ids->push($id->id);
            }
        }
        $item->regulars()->whereNotIn('id', $sended_ids)->delete();
    }

    public function imagesLoad(Request $request)
    {
        $category_id = $request->get('id');
        $category = TradeLotCategory::findOrFail($category_id);
        $storage = Storage::disk('public');

        if ($lot_image_file = $request->file('lot_image')) {
            $path = $storage->put("category-images/{$category->id}/lot_noimg", $lot_image_file);
            if (!empty($category->lot_image)) {
                $storage->delete($category->lot_image);
            }
            $category->lot_image = $path;
            $category->save();
        }

        if ($image_file = $request->file('image')) {
            $path = $storage->put("category-images/{$category->id}", $image_file);
            if (!empty($category->image)) {
                $storage->delete($category->image);
            }
            $category->image = $path;
            $category->save();
        }

        return response()->json('ok');
    }

    public function imagesDelete(Request $request)
    {
        $category_id = $request->get('id');
        $category = TradeLotCategory::findOrFail($category_id);
        $storage = Storage::disk('public');

        if ($request->post('name') === 'lot_image') {
            if (!empty($category->lot_image)) {
                $storage->delete($category->lot_image);
            }
            $category->lot_image = null;
            $category->save();
        }

        if ($request->post('name') === 'image') {
            if (!empty($category->image)) {
                $storage->delete($category->image);
            }
            $category->image = null;
            $category->save();
        }

        return response()->json('ok');
    }
}

<?php

namespace App\Http\Controllers\Panel\Catalog;

use App\Http\Controllers\Controller;
use App\Models\TradeLot;
use App\Models\TradeLotCategory;
use App\Models\TradeLotImage;
use App\Role\UserRole;
use App\Traits\PageTableController;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LotController extends Controller
{
    use PageTableController;

    function tableModel()
    {
        return TradeLot::class;
    }

    public function tableModelWith()
    {
        return ['categories'];
    }

    public function tableModelDetailWith()
    {
        return ['images', 'params.field', 'classificators', 'categories', 'info'];
    }

    public function resourceAccess()
    {
        return [
            'list' => UserRole::ROLE_ADMIN,
            'item' => UserRole::ROLE_ADMIN,
            'create' => UserRole::ROLE_ADMIN,
            'update' => UserRole::ROLE_ADMIN,
            'delete' => UserRole::ROLE_ADMIN,
        ];
    }

    public function geCreateValidation()
    {
        return [

        ];
    }

    public function getUpdateValidation()
    {
        return $this->geCreateValidation();
    }

    public function index()
    {
        $this->checkResourceAccess('list');

        $query = $this->getCollectionQuery();

        # filter
        $filter = \request()->get('filter');
        if ($filter) {
            $query = $this->filterTable($query, $filter);
        }

        if ($table_with = $this->tableModelWith()) {
            $query->with($table_with);
        }

        $pagination = $query->paginate($this->getPaginate());

        $pagination->transform(function (TradeLot $lot) {
            return $lot->append('params_filling');
        });

        return JsonResource::collection($pagination);
    }

    /**
     * Загрузка фотографий
     * @return \Illuminate\Http\JsonResponse
     */
    public function imagesLoad()
    {
        $request = request();
        $lot = TradeLot::findOrFail($request->get('id'));
        $image = $request->file('files');
        $path = Storage::disk('public')->put("lot-images/{$lot->id}", $image);
        $lot->images()->save(new TradeLotImage(['file' => $path]));
        $lot->refresh();
        return response()->json($lot->images);
    }

    /**
     * Удаление фотографий
     * @return \Illuminate\Http\JsonResponse
     */
    public function imagesDelete()
    {
        $request = request();
        $lot = TradeLot::findOrFail($request->get('id'));
        $image_id = $request->get('image_id');
        $lot->images()->findOrFail($image_id)->delete();
        $lot->refresh();
        return response()->json($lot->images);
    }

    public function update(request $request)
    {

        $this->checkResourceAccess('update');
        $this->checkUpdatingValidation();

        $id = $request->get('id');
        if ($with = $this->tableModelDetailWith()) {
            $item = TradeLot::with($with)->find($id);
        } else {
            $item = TradeLot::find($id);
        };

        $item->fill($request->all());
        if (!$item->save()) {
            throw new HttpException(400, 'Error updating model');
        }

        if ($info = $request->get('info')) {
            $item->info()->updateOrCreate(['trade_lot_id' => $item->id], $info);
        }

        $updates_ids = $item->categories()->sync(Arr::pluck($request->post('categories'), 'id'));
        if (!empty($updates_ids["attached"]) || !empty($updates_ids["detached"]) || !empty($updates_ids["updated"])) {
            $item->updated_user_id = auth()->user()->id;
        }
        $item->save();
        Artisan::queue("parser:params", ['--id' => $item->id]);

        $item->refresh();
        if ($with = $this->tableModelDetailWith()) {
            $item->load($with);
        }

        return response()->json($item);
    }

}

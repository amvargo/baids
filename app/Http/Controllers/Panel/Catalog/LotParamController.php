<?php

namespace App\Http\Controllers\Panel\Catalog;

use App\Events\PanelEvent;
use App\Http\Controllers\Controller;
use App\Models\TradeLot;
use App\Models\TradeLotParam;
use App\Role\UserRole;
use App\Traits\PageTableController;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LotParamController extends Controller
{
    use PageTableController;

    function tableModel()
    {
        return TradeLotParam::class;
    }

    function tableModelDetailWith()
    {
        return ['categories', 'regulars'];
    }

    public function resourceAccess()
    {
        return [
            'list' => UserRole::ROLE_ADMIN,
            'item' => UserRole::ROLE_ADMIN,
            'create' => UserRole::ROLE_ADMIN,
            'update' => UserRole::ROLE_ADMIN,
            'delete' => UserRole::ROLE_ADMIN,
        ];
    }

    public function getCollectionQuery()
    {
        return $this->sortedTableModel();
    }

    public function getShowQuery()
    {
        return TradeLotParam::query();
    }

    public function update(request $request)
    {
        $this->checkResourceAccess('update');
        $this->checkUpdatingValidation();

        $id = $request->get('id');
        if ($with = $this->tableModelDetailWith()) {
            $item = TradeLotParam::with($with)->find($id);
        } else {
            $item = TradeLotParam::find($id);
        };

        $item->fill($request->all());

        if (!$item->save()) {
            throw new HttpException(400, 'Error updating model');
        }

        $this->afterUpdate($item, $request);

        return response()->json($item->refresh());
    }


    public function store()
    {
        $this->checkResourceAccess('create');
        if (!empty($rules = $this->geCreateValidation())) {
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                throw new HttpException(400, $validator->errors()->first());
            }
        }

        // store
        $model = new TradeLotParam();
        $model->fill(Input::all());
        if ($pass = Input::get('password')) {
            $model->password = Hash::make($pass);
        }
        $model->save();

        $this->afterUpdate($model);
        $model->load($this->tableModelDetailWith());

        return response()->json($model->refresh());
    }

    private function afterUpdate($item, $request = null)
    {
        if (!$request) {
            $request = request();
        }
        $item->categories()->sync(Arr::pluck($request->post('categories'), 'id'));

        $regulars = Collection::make($request->get('regulars', []));
        $sended_ids = $regulars->filter(function ($item) {
            return !empty($item['id']);
        })->pluck('id');
        foreach ($regulars as $regular) {
            if (empty($regular['regex'])) {
                continue;
            }
            if (isset($regular['id'])) {
                $req_model = $item->regulars()->find($regular['id']);
                $req_model->update($regular);
            } else {
                $id = $item->regulars()->create($regular);
                $sended_ids->push($id->id);
            }
        }
        $item->regulars()->whereNotIn('id', $sended_ids)->delete();
    }

}

<?php

namespace App\Http\Controllers\Panel\Consulting;

use App\Http\Controllers\Panel\Controller;
use App\Models\Panel\ConsultingMessage;
use App\Role\UserRole;
use App\Traits\PageTableController;

class MessageController extends Controller
{
    use PageTableController;

    function tableModel()
    {
        return ConsultingMessage::class;
    }

    function tableModelWith()
    {
        return ['category'];
    }

    public function resourceAccess()
    {
        return [
            'list' => UserRole::ROLE_CONTENT,
            'item' => UserRole::ROLE_CONTENT,
            'create' => UserRole::ROLE_CONTENT,
            'update' => UserRole::ROLE_CONTENT,
            'delete' => UserRole::ROLE_CONTENT,
        ];
    }

    public function geCreateValidation()
    {
        return [
            'full_name' => 'required|string|min:3|max:64',
            'email' => 'required|string|email|max:255',
            'message' => 'required|string|min:30',
            'answer' => 'nullable|string|min:10',
            'category_id' => 'nullable|numeric',
        ];
    }

    public function getUpdateValidation()
    {
        return $this->geCreateValidation();
    }

}

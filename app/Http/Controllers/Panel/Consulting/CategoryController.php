<?php

namespace App\Http\Controllers\Panel\Consulting;

use App\Http\Controllers\Panel\Controller;
use App\Http\Resources\PanelResource;
use App\Models\Panel\ConsultingCategory;
use App\Role\UserRole;
use App\Traits\PageTableController;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryController extends Controller
{
    use PageTableController;

    public function getPaginate()
    {
        return 200;
    }

    function tableModel()
    {
        return ConsultingCategory::class;
    }

    public function index()
    {
        $this->checkResourceAccess('list');

        $query = $this->sortedTableModel();

        if ($table_with = $this->tableModelWith()) {
            $query->with($table_with);
        }

        return PanelResource::collection($query->paginate($this->getPaginate()));
    }

    public function resourceAccess()
    {
        return [
            'item' => UserRole::ROLE_CONTENT,
            'create' => UserRole::ROLE_CONTENT,
            'update' => UserRole::ROLE_CONTENT,
            'delete' => UserRole::ROLE_CONTENT,
        ];
    }

    public function geCreateValidation()
    {
        return [
            'title' => 'required|string|min:3|max:127',
        ];
    }

    public function getUpdateValidation()
    {
        return $this->geCreateValidation();
    }

}

<?php

namespace App\Http\Controllers\Panel;

use App\Models\Banner;
use App\Traits\PageTableController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Option;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

class OptionsController extends Controller
{

    public function index()
    {
        $default_options = (new Collection(Option::DEFAULT_OPTIONS))->keyBy('key');
        $exist_options = Option::whereNotNull('name')->get(['key', 'name', 'value'])->keyBy('key');
        $result = $default_options->merge($exist_options)->values()->toArray();
        return $result;
    }

    public function save() {
        $post = request()->post();
        Option::updateOrCreate(['key' => $post['key']], Arr::except($post, 'key'));
    }
}

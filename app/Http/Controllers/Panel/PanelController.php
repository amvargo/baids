<?php

namespace App\Http\Controllers\Panel;


use Illuminate\Http\Request;


class PanelController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('panel');
    }

}

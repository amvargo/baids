<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\Image;
use Intervention\Image\Facades\Image as Resize;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::active()->with('image')->paginate(3);

        return $sliders;
    }

    public function editOrCreate(Request $request)
    {
        $image = $request->file('image');
        $id = $request->post('id');
        $post = $request->post();
        if ($id) {
            $slider = Slider::find($id);
            if($slider) {
                $slider->update($post);
            }
        } else {
            $slider = Slider::create($post);
        }

        if($image) {
            $name = $image->store( null, 'sliders');
            $path = "/storage/sliders/{$name}";
            $slider->deleteImage();
            $slider->image()->create(['name' => $name, 'url' => $path]);
        }

    }

    public function delete($id)
    {
        $slider = Slider::find($id);
        if (!empty($slider)) {
            $slider->delete();
        }
    }
}

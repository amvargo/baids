<?php

namespace App\Http\Controllers\Panel;

use App\Events\ParserCommandEvent;
use App\Jobs\PanelCommand;
use Illuminate\Http\Request;

class CommandController extends Controller
{
    public function parserParams(Request $request)
    {
        event(new ParserCommandEvent('params', $request->post('command')));
        return response()->json('ok');
    }
}

<?php

namespace App\Http\Controllers\Panel;

use App\Models\TradeLot;
use App\Models\TradeLotCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SeoConfiguration as SeoConfigurationModel;
use App\Http\Requests\SeoConfiguration;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\NamespacedItemResolver;


class SeoConfigurationController extends Controller
{
	public function index(){
	    $paths = [];
        $routeCollection = Route::getRoutes()->getRoutesByMethod('GET');
        $routeCollection = isset($routeCollection['GET']) ? $routeCollection['GET'] : [];
        $pattern = '/^(panel|cabinet|api|logout|password|register|broadcasting|login).*/i';

	    foreach($routeCollection as $route) {
            preg_match($pattern, $route->uri(), $matches);
            if(empty($matches))
            {
                $paths[] = $route->uri();
            }
            
        }

	    $exists_paths = SeoConfigurationModel::whereIn('url', $paths)->get();
        $not_exists_paths = array_unique(array_diff($paths, $exists_paths->pluck('url')->all()));


		return compact('exists_paths', 'not_exists_paths');
	}

    public function save(SeoConfiguration $request) {
    	$post = $request->post();

    	if($id = $request->post('id')) {
            $seo = SeoConfigurationModel::findOrFail($id);
    		$seo->update($post);
    	}

    	elseif($class_name = $request->post('class_name')) {
    	    $class = SeoConfigurationModel::CLASSES[$class_name];
            SeoConfigurationModel::updateOrCreate(['object_type' => $class, 'object_id' => $request->post('object_id')], $post);
        }
    	else {
    		SeoConfigurationModel::create($post);
    	}
    }

    public function delete($id) {

    	SeoConfigurationModel::find($id)->delete();

    }

    public function getElement(Request $request) {

        $class = SeoConfigurationModel::CLASSES[$request->post('class_name')];
        $id = $request->post('id');
        $model = SeoConfigurationModel::where('object_type', $class)->where('object_id', $id)->first();
        return $model;
    }
}

<?php

namespace  App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Models\AboutProjectSection;
use App\Role\UserRole;
use App\Traits\PageTableController;

class AboutProjectController extends Controller
{
    use PageTableController;

    function tableModel()
    {
        return AboutProjectSection::class;
    }

    public function resourceAccess()
    {
        return [
            'list' => UserRole::ROLE_CONTENT,
            'item' => UserRole::ROLE_CONTENT,
            'create' => UserRole::ROLE_CONTENT,
            'update' => UserRole::ROLE_CONTENT,
            'delete' => UserRole::ROLE_CONTENT,
        ];
    }

    public function geCreateValidation()
    {
        return [
            'title' => 'required|string',
            'link'=> 'required|string'
        ];
    }

    public function getUpdateValidation()
    {
        return $this->geCreateValidation();
    }
}

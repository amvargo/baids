<?php

namespace App\Http\Controllers\Panel;

use App\Models\UserPayment;
use App\Role\UserRole;
use App\Traits\PageTableController;

class PaymentController extends Controller
{
    use PageTableController;

    function tableModel()
    {
        return UserPayment::class;
    }

    /**
     * @return mixed
     */
    function tableModelWith()
    {
        return ['service', 'user'];
    }

    public function getCollectionQuery()
    {
        return $this->sortedTableModel();
    }

    /**
     * @return mixed
     */
    function tableModelDetailWith()
    {
        return ['service', 'user', 'staff'];
    }

    public function resourceAccess()
    {
        return [
            'list' => UserRole::ROLE_STAFF,
            'item' => UserRole::ROLE_STAFF,
            'create' => UserRole::ROLE_STAFF,
            'update' => UserRole::ROLE_STAFF,
            'delete' => UserRole::ROLE_ADMIN,
        ];
    }

    public function geCreateValidation()
    {
        return [

        ];
    }

    public function getUpdateValidation()
    {
        return $this->geCreateValidation();
    }

}

<?php


namespace App\Http\Controllers\Panel;


use App\Http\Resources\PanelResource;
use App\Models\User;
use App\Models\UserNotification;
use App\Models\VehicleMaker;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TableItemsController extends Controller
{

    protected $model_name = null;
    protected $resource = null;
    protected $model = null;
    protected $with = null;
    protected $page_size = 20;

    public function __construct()
    {
        // todo: develop
        if ($model = request()->get('model')) {
            $this->model_name = 'App\Models\\' . $model;
        }

        $this->resource = $this->model_name;
        $this->model = app($this->model_name);

        if ($with = request()->get('with')) {
            $this->with = is_array($with) ? $with : explode(',', $with);
        }

        parent::__construct();
    }

    public function index()
    {
        //todo: add - checking access

        $query = $this->getCollectionQuery();

        # filter
        if ($filter = request()->get('filter')) {
            $query = $this->filterTable($query, $filter);
        }

        if ($this->with) {
            $query->with($this->with);
        }

        return PanelResource::collection($query->paginate($this->page_size));
    }

    public function show($id)
    {
        //todo: add - checking access
        $item = $this->getShowQuery();
        if ($this->with) {
            $item->with($this->with);
        }
        return response()->json($item->find($id));
    }


    public function update(request $request)
    {
        //todo: add - checking access
        $id = $request->get('id');
        if ($this->with) {
            $model = $this->model::with($this->with)->find($id);
        } else {
            $model = $this->model::find($id);
        };

        $model->fill($request->all());

        if ($model->save()) {
            $this->afterSave($model);
        } else {
            throw new HttpException(400, 'Error updating model');
        }

        $model->refresh();
        $model->load($this->with);

        return response()->json($model);
    }

    public function store()
    {
        //todo: add - checking access

        // store
        $model = $this->model;
        $model->fill(Input::all());
        if ($model->save()) {
            $this->afterSave($model);
        };

        return response()->json($model->refresh());
    }

    public function destroy($id, request $request)
    {
        //todo: add - checking access
        $this->model::find($id)->delete();
        if (method_exists($this, 'deleteEvent')) {
            $this->deleteEvent($id);
        }
        return response()->json('ok');
    }

    public function afterSave($model)
    {
        $request = request();
        if ($this->with) {
            foreach ($this->with as $relation) {

                if (strpos($relation, '.') !== false) {
                    // has dots: models.variants >> models.[idx].variants
                    $relation_path = explode('.', $relation);
                    $relation_list = $request->get($relation_path[0]);
                    if (method_exists($model, $relation_path[0]) && isset($relation_list)) {
                        self::updateRelationModels($model, $relation_path[0], $relation_list, $relation_path[1]);
                        // todo: develop
                    }
                } else {
                    // does't have dots: variants
                    $relation_list = $request->get($relation);
                    if (method_exists($model, $relation) && isset($relation_list)) {
                        self::updateRelationModels($model, $relation, $relation_list);
                    }
                }
            }
        }
    }

    public static function updateRelationModels($model, $relation_method, $relation_data_list, $relation_method_children = null)
    {
        $ids = [];
        foreach ($relation_data_list as $relation_item) {
            if (isset($relation_item['id'])) {
                $tmp_model = $model->$relation_method()->find($relation_item['id']);
                if ($tmp_model) {
                    $tmp_model->update($relation_item);
                }
                $ids[] = $relation_item['id'];
            } else {
                $tmp_model = $model->$relation_method()->create($relation_item);
                $ids[] = $tmp_model->id;
            }
            if ($relation_method_children && $tmp_model && isset($relation_item[$relation_method_children])) {
                self::updateRelationModels($tmp_model, $relation_method_children, $relation_item[$relation_method_children]);
            }
        }
        $model->$relation_method()->whereNotIn('id', $ids)->delete();
    }

    protected function getShowQuery()
    {
        return $this->model::orderBy('id');//->withTrashed();
    }

    protected function getCollectionQuery()
    {
        return $this->sortedTableModel();//->withTrashed();
    }

    protected function sortedTableModel()
    {
        $sort_by = 'id';
        $sort_dir = 'asc';

        if ($sort = \request()->query('sort')) {
            if (strpos($sort, '-') === 0) {
                $sort_dir = 'desc';
                $sort_by = substr($sort, 1);
            } else {
                $sort_by = $sort;
            }
        }

        return $this->model::orderBy($sort_by, $sort_dir);
    }

    protected function filterTable($query, $search)
    {
        foreach ($search as $key => $value) {
            if (empty($value)) {
                continue;
            }
            if ($key === 'role') {
                if ($value === 'ROLE_USER') {
                    $query = $query->where('roles', '=', null);
                } else {
                    $query = $query->where('roles', 'like', '%' . $value . '%');
                }
            } elseif ($key === 'name') {
                $query = $query->where('name', 'like', '%' . $value . '%');
            } elseif ($key === 'TradeLotId') {
                $query = $query->whereHas('lots', function ($query) use ($value) {
                    return $query->where('id', $value);
                });
            } else {
                $query = $query->where($key, '=', $value);
            }
        }
        return $query;
    }

}

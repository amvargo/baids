<?php

namespace App\Http\Controllers\Panel;

use App\Models\UserNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Support\Arr;

class ModerationAdvertisingController extends Controller
{
    public function index(Request $request) {

        $order = $request->input('order');
        $filter = json_decode($request->input('filter'), true);
        $filter_status =  $this->getParam($filter, 'filter_status');
        $filter_variant =  $this->getParam($filter, 'filter_variant');

        $items = Banner::orderBy('id', $order)
            ->whereHas('order', function($query) {
                $query->whereHas('payment', function($query){
                   $query->successPayment();
                });
            })
            ->with(['user' => function($query){
                return $query->select('id', 'name', 'last_name', 'middle_name');
            }, 'service' => function($query){
                return $query->select('id', 'title', 'price');
            }, 'category' => function($query){
                return $query->select('id', 'title', 'slug');
            }, 'order' => function($query){
                return $query->select('id', 'service_begin', 'service_end', 'price');
            }])
            ->with('image')
            ->when($filter_status, function($query) use ($filter_status){
                return $query->byStatus($filter_status);
            })
            ->when($filter_variant, function($query) use ($filter_variant){
                return $query->variant($filter_variant);
            })
            ->paginate(10);


        return response()->json([
            'items' => $items
        ], 200);
    }

    public function changeStatus(Request $request){

        $id = $request->post('id');
        $status = $request->post('status');
        if($id) {
            $model = Banner::find($id);
            if($model) {
                $model->status = $status;
                $model->save();
            }
        }

    }

    public function getStatuses(){
        return ['statuses' => Banner::STATUSES, 'variants' => Banner::VARIANTS];
    }

    public function getParam($arr = [], $param = '') {

        $result = Arr::get($arr, $param, null);

        return $result;
    }
}

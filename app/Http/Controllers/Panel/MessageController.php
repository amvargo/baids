<?php

namespace App\Http\Controllers\Panel;


use App\Models\UserNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Message;


class MessageController extends Controller
{
    public function getMessages()
    {
        $messages = UserNotification::notUsers()->with(['author' => function($q){
            $q->select('id', 'name', 'last_name', 'middle_name');
        }])->orderBy('created_at', 'desc')->paginate(10);
        return $messages;
    }

    public function createMessage(Message $request) {
        $author = Auth::user();
        $mess = UserNotification::create($request->post() + ['author_id'=> $author->id] );
    }

    public function deleteMessage($id){
        if(!empty($id)) {
            $mess = UserNotification::find($id);
            if(!empty($mess)) {
                $mess->delete();
            }
        }
    }
}

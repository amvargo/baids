<?php

namespace App\Http\Controllers\Panel;

use App\Models\Panel\User;
use App\Role\UserRole;
use App\Traits\PageTableController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserController extends Controller
{
    use PageTableController;

    function tableModel()
    {
        return User::class;
    }

    public function getCollectionQuery()
    {
        return $this->sortedTableModel();
    }

    public function resourceAccess()
    {
        return [
            'list' => UserRole::ROLE_SUPPORT,
            'item' => UserRole::ROLE_SUPPORT,
            'create' => UserRole::ROLE_ACCOUNT_MANAGER,
            'update' => UserRole::ROLE_ACCOUNT_MANAGER,
            'delete' => UserRole::ROLE_ACCOUNT_MANAGER,
        ];
    }

    public function geCreateValidation()
    {
        return [
            'name' => 'required|string|min:3|max:64',
            'email' => 'required|string|email|max:255|unique:users',
        ];
    }

    public function getUpdateValidation()
    {
        return [
            'name' => 'required|string|min:3|max:64',
            'email' => 'required|string|email|max:255',
            'image' => 'nullable|image|max:2048|dimensions:min_width=100,max_width=512',
            'image_clear' => 'nullable|string',
        ];
    }

    public function getIp()
    {
        $IP = $_SERVER['REMOTE_ADDR'];
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $IP = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
        }
        return $IP;
    }

    public function update(Request $request)
    {
        $this->checkResourceAccess('update');

        $this->checkUpdatingValidation();

        $id = $request->get('id');
        /** @var User $user */
        $user = User::find($id);

        $user->fill($request->only(['name', 'last_name', 'middle_name', 'email']));

        if ($request->user()->hasRole(UserRole::ROLE_ADMIN)) {
            $roles = explode(',', $request->get('roles'));
            $user->roles = array_filter($roles, function ($item) {
                return !empty($item) && $item !== 'null';
            });
        }

        if ($password = $request->get('password')) {
            $user->password = Hash::make($password);
        }

        if ($image = $request->file('image')) {
            $user->deleteAvatarFile();
            $user->uploadAvatarFile($request->file('image'));
        } elseif ($request->has('image_clear')) {
            $user->deleteAvatarFile();
        }

        if (!$user->save()) {
            throw new HttpException(400, 'Error save user');
        }

        return response()->json($user);
    }

}

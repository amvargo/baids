<?php

namespace App\Http\Controllers\Panel;

use App\Role\UserRole;

class StaffController extends UserController
{

    public function sortedTableModel()
    {
        $sort_by = 'id';
        $sort_dir = 'asc';

        if ($sort = \request()->query('sort')) {
            if (strpos($sort, '-') === 0) {
                $sort_dir = 'desc';
                $sort_by = substr($sort, 1);
            } else {
                $sort_by = $sort;
            }
        }

        $query = $this->tableModel();

        $query = $query::where('roles', '<>', null)
            ->orderBy($sort_by, $sort_dir);

        return $query;
    }

}

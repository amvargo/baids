<?php

namespace App\Http\Controllers\Panel;

use App\Models\Panel\Question;
use App\Models\Panel\Service;
use App\Role\UserRole;
use App\Traits\PageTableController;
use App\Traits\PageTableImageController;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ServiceController extends Controller
{
    use PageTableController, PageTableImageController;

    function tableModel()
    {
        return Service::class;
    }

    public function resourceAccess()
    {
        return [
            'list' => UserRole::ROLE_SUPPORT,
            'item' => UserRole::ROLE_SUPPORT,
            'create' => UserRole::ROLE_CONTENT,
            'update' => UserRole::ROLE_CONTENT,
            'delete' => UserRole::ROLE_CONTENT,
        ];
    }

    public function geCreateValidation()
    {
        return [
            'title' => 'required|string|min:3|max:256',
            'annotation' => 'nullable',
            'image' => 'nullable|string|min:3|max:127',
            'description' => 'required',
            'priority' => 'nullable|numeric|max:1000000',
            'published' => 'boolean',
        ];
    }

    public function getUpdateValidation()
    {
        return $this->geCreateValidation();
    }

    public function imageUpload2($id)
    {
        $request = request();
        $item = Service::findOrFail($id);
        $item->uploadImage2($request->file('image2'), true);
        return $item;
    }

    public function imageDelete2($id)
    {
        $item = Service::findOrFail($id);
        $item->deleteImage2(true);
        return $item;
    }


}

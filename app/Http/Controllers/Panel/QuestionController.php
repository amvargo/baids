<?php

namespace App\Http\Controllers\Panel;

use App\Models\Panel\Question;
use App\Role\UserRole;
use App\Traits\PageTableController;

class QuestionController extends Controller
{
    use PageTableController;

    function tableModel()
    {
        return Question::class;
    }

    function tableModelWith()
    {
        return ['user'];
    }

    public function resourceAccess()
    {
        return [
            'list' => UserRole::ROLE_CONTENT,
            'item' => UserRole::ROLE_CONTENT,
            'create' => UserRole::ROLE_CONTENT,
            'update' => UserRole::ROLE_CONTENT,
            'delete' => UserRole::ROLE_CONTENT,
        ];
    }

    public function geCreateValidation()
    {
        return [
            'author' => 'required|string|min:3|max:64',
            'email' => 'required|string|email|max:255',
            'question' => 'required|string|min:30',
            'answer' => 'nullable|string|min:30',
            'priority' => 'nullable|numeric|max:1000000',
            'published' => 'boolean',
        ];
    }

    public function getUpdateValidation()
    {
        return $this->geCreateValidation();
    }

}

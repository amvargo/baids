<?php

namespace App\Http\Controllers;

use App\Events\OrderCreated;
use App\Models\UserPayment;

class UnitellerController extends Controller
{

    protected $user_payment = UserPayment::class;


    public function findOrder($id = null){
        $id = $id ?: request()->post('Order_ID');
        $this->user_payment = $this->user_payment::find($id);
    }

    public function checkHash(){
        $result = false;
        if ($this->user_payment) {   $result = $this->user_payment->checkHash(request()->post());   }
        if(empty($result)){   abort(404);   }
    }

    public function setResult() {
        $this->findOrder();
        $this->checkHash();
       //logger(['message'=> 'aaaaaaaaaaaaa' . request()->post('Status')]);
        $this->user_payment->setUnitellerStatus(request()->post('Status'));
        event(new OrderCreated($this->user_payment->order));
    }

}

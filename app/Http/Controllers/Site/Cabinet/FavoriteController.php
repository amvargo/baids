<?php

namespace App\Http\Controllers\Site\Cabinet;


use App\Models\Banner;

class FavoriteController extends \App\Http\Controllers\Controller
{

    public function index()
    {
        $user = auth()->user();
        $lots = $user->favorite_lots()
            ->orderBy('id', 'desc')
            ->with('favorite')->paginate(20);

        return view('site/cabinet/favorites', compact('lots'));
    }

}

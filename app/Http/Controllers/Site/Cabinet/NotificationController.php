<?php

namespace App\Http\Controllers\Site\Cabinet;


use App\Models\Order;
use App\Models\Service;
use App\Models\TradeLotCategory;
use App\Models\User;
use App\Models\UserMailingLot;
use App\Models\UserNotification;
use App\Models\UserNotificationRead;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class NotificationController extends \App\Http\Controllers\Controller
{

    public function index()
    {
        /** @var User $user */
        $user = auth()->user();
        $notifications = UserNotification::where(function ($query) use ($user) {
            return $query->where('user_id', $user->id)->orWhereNull('user_id');
        })
            ->whereDoesntHave('notification_read', function ($query) {
                return $query->onlyTrashed();
            })
            ->orderBy('id', 'desc')
            ->paginate(10);
        /** @var UserNotification $item */
        foreach ($notifications->items() as $item) {
            if (!$item->is_read) {
                $item->notification_read()->create(['user_id' => $user->id]);
            }
        }


        return view('site/cabinet/notifications', compact('notifications'));
    }

    public function destroy(Request $request, UserNotification $notification)
    {
        $notification_read = $notification->reads()->where('user_id', $request->user()->id)->firstOrFail();
        if ($request->user()->cannot('delete', $notification_read)) {
            throw new AccessDeniedHttpException();
        };
        UserNotificationRead::where('user_notification_id', $notification->id)
            ->where('user_id', $request->user()->id)
            ->delete();
        return redirect()->back();
    }

    public function mailings()
    {
        /** @var User $user */
        $user = auth()->user();


        if ($user->can('create', UserMailingLot::class)) {
            $categories = TradeLotCategory::getGeneralCategoriesWithActive(99, true, false);
            View::share('categories', $categories);
        } else {
            $service_mailing = Service::find(UserMailingLot::ACCESS_FOR_SERVICE_ID);
            View::share('service_mailing', $service_mailing);
        };

        return view('site/cabinet/mailings');
    }

    /**
     * Save the assessment and comment of the order.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function saveOrderEvaluation(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $order_id = $request->get('order_id');

        $order = $user->orders()->find($order_id);

        $order->fill($request->only(['comment', 'assessment']));
        $order->save();

        return $this->index();
    }
}

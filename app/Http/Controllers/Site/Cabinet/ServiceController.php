<?php

namespace App\Http\Controllers\Site\Cabinet;


use App\Models\Banner;
use App\Models\Order;
use App\Models\Service;
use App\Models\UserPayment;

class ServiceController extends \App\Http\Controllers\Controller
{

    public function index()
    {
        // TODO: develop

        $processOrders = auth()->user()->orders()
            ->orderBy('id', 'desc')
            ->where('service_begin', '<=', now())
            ->where('service_end', '>=', now())
            ->Where('status', '!=', Order::STATUS_DONE)
            ->whereHas('service', function($query){
                return $query
                    ->whereNull('system_key')
                    ->orWhereNotIn('system_key', ['advertising_general', 'advertising_catalog', 'advertising_lk']);
            })
            ->get();
        $doneOrders = auth()->user()->orders()
        ->where(function($query) {
            $query->orWhere('service_end', '<=', now());
            $query->orWhere('status', Order::STATUS_DONE);
        })->orderBy('id', 'desc')->get();
        foreach ($processOrders  as $order) {
            $this->processOrder($order);
        }
        foreach ($doneOrders as $order) {
            $this->processOrder($order);
        }

        return view('site/cabinet/services', compact('processOrders', 'doneOrders'));
    }

    public function getNewPayment(UserPayment $payment) {
        $url = config('app.url').$payment->service->redirect_to;
        $payment->url_return_no = $url;
        $payment->url_return_ok = $url;
        return $payment->createPaymentForm();
    }

    public function processOrder(Order $order)
    {
        $order['payment_status'] = $order->payment->status ?? null;
        $order['service_title'] = $order->service->title;
        $order['status_name'] = $order->getStatusNameAttribute();
        $order['status_color'] = $order->getStatusColorAttribute();
    }

}

<?php

namespace App\Http\Controllers\Site\Cabinet;


use App\Models\Banner;
use App\Models\User;

class PaymentController extends \App\Http\Controllers\Controller
{

    public function index()
    {
        /** @var User $user */
        $user = auth()->user();
        $payments = $user->payments()
            ->orderBy('id', 'desc')
            ->paginate(10);

        return view('site/cabinet/payments', compact('payments'));
    }

}

<?php

namespace App\Http\Controllers\Site\Cabinet;


use App\Http\Requests\OrgProfile;
use App\Http\Requests\Profile;
use App\Http\Requests\UserPassword;
use App\Models\User;
use App\Models\UserOrgTrade;
use App\Models\Banner;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class ProfileController extends \App\Http\Controllers\Controller
{

    public function index()
    {
        $user = auth()->user();
        $org_profile = $user->org_profile ?: new UserOrgTrade();

        return view('site/cabinet/profile')
            ->with(compact('user', 'org_profile'));
    }

    public function store(Profile $request)
    {
        if ($request->validated()) {
            $input = $request->only('name', 'last_name', 'middle_name', 'email', 'phone');
            auth()->user()->fill($input)->save();
            return redirect()->back()->withSuccess('Данные профиля успешно сохранены');
        }
        return redirect()->back();
    }

    public function store_password(UserPassword $request)
    {
        if ($request->validated()) {
            $user = auth()->user();
            $user->password = Hash::make($request->input('new_password'));
            $user->save();
            Auth::logoutOtherDevices($request->input('new_password'));
            return redirect()->back()->withSuccess('Пароль успешно изменен');
        }
        return redirect()->back();
    }

    public function store_org_profile(OrgProfile $request)
    {
        if ($request->validated()) {
            $input = $request->only('inn', 'org_profile_type', 'org_profile_first_name',
                'org_profile_last_name', 'org_profile_middle_name', 'org_profile_full_name');
            /** @var User $user */
            $user = auth()->user();
            $org_profile = $user->org_profile()->firstOrNew([]);
            $org_profile->fill($input)->save();
            return redirect()->back()->withSuccess('Данные организатора торгов успешно сохранены');
        }
        return redirect()->back();
    }

}

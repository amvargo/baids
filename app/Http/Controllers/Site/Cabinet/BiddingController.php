<?php

namespace App\Http\Controllers\Site\Cabinet;


use App\Models\Banner;
use App\Models\TradeLot;
use App\Models\TradeLotCategory;
use App\Models\TradeLotParam;
use App\Models\User;
use Illuminate\Http\Request;

class BiddingController extends \App\Http\Controllers\Controller
{

    public function index()
    {
        // TODO: develop
        /** @var User $user */
        $user = auth()->user();
        $lots = $user->lots()->orderBy('id', 'desc')->paginate();
        return view('site/cabinet/bidding', compact('lots'));
    }

    public function dataForLotAdding(request $request)
    {
        $result = [];
        switch ($request->get('step')) {
            case 2:
                $category_id = $request->get('category');
                $params = TradeLotParam::where('published', true)
                    ->whereHas('categories', function ($query) use ($category_id) {
                        return $query->where('id', $category_id);
                    })
                    ->get()
                    ->makeHidden(['replace_table', 'published'])
                    ->each(function ($param) {
                        $param->values = [];
                        if ($param->code === 'region_with_type') {
                            $param->values = TradeLot::whereNotNull('region_with_type')
                                ->select(['region_with_type'])->distinct()
                                ->orderBy('region_with_type')
                                ->get()->pluck('region_with_type');
                        } elseif ($param->type === 'checkbox' && $param->code !== 'car_model') { // car_model отдельно запрашиваем
                            $param->values = $param->values()
                                ->select(['value'])->distinct()
                                ->orderBy('value')
                                ->get()->pluck('value');
                        }
                    });

                $result['params'] = $params;
                $category = TradeLotCategory::find($category_id);
                // category props
                $result['category_name'] = $category->title;
                $result['lot_no_img'] = $category->lot_image_url;

                break;
            default:
                $result['categories'] = TradeLotCategory::getGeneralCategoriesWithActive(99, true, false);
                break;
        }
        return response()->json($result);
    }

}

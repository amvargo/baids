<?php

namespace App\Http\Controllers\Site;


use App\Models\ConsultingMessage;
use Illuminate\Http\Request;
use App\Http\Requests\Consulting as ConsultingRequest;
use Illuminate\Support\Facades\Validator;

class ConsultingController extends \App\Http\Controllers\Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('site/consulting');
    }

    public function store(ConsultingRequest $request)
    {
        if ($request->validated()) {
            $msg = new ConsultingMessage($request->only(['category_id', 'full_name', 'phone', 'email', 'message']));
            $msg->user_id = auth()->user()->id;
            $msg->saveOrFail();
            $request->session()->flash('consulting-alert-success', 'Message was successful added!');
        }
        return back();
    }

}

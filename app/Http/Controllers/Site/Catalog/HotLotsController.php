<?php

namespace App\Http\Controllers\Site\Catalog;

use App\Http\Requests\CatalogFilter;
use App\Http\Requests\CatalogSearch;
use App\Http\Requests\CatalogSort;
use App\Models\Trade;
use App\Models\TradeLot;
use App\Models\TradeLotCategory;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Контроллер каталога
 * Class CatalogController
 * @package App\Http\Controllers\Site
 */
class HotLotsController extends \App\Http\Controllers\Controller
{
    protected function getCatalogFilterParams(TradeLotCategory $category)
    {
        return $category->params()
            ->where('published', 1)
            ->whereHas('values', function ($query) {
                return $query->whereHas('lots', function ($query) {
                    return $query->onlyHotLots();
                });
            })
            ->orderBy('sort')
            ->get();
    }

    protected function getCatalogFilterParamValues($catalog_filter_params, TradeLotCategory $category, CatalogFilter $filter, CatalogSearch $search)
    {
        $catalog_filter_param_values = Collection::make();
        $filter_data = $filter->get('filter', []);
        $filter_used_keys = $filter->getUsedKeys();
        $filter_stack = [];

        foreach ($catalog_filter_params as $filter_param) {
            if (!in_array($filter_param->type, ['checkbox', 'checkbox-vehicle-brands', 'checkbox-vehicle-models'])) {
                continue;
            };
            $catalog_filter_param_values->put($filter_param->id, $filter_param->values()
                ->whereHas('lots', function ($query) use (&$filter_stack, &$debug, $search, $filter, $filter_used_keys, $filter_param, $category, $filter_data, &$data) {
                    $data = Arr::only($filter_data, $filter_stack);
                    $filter_stack[] = $filter_param->code;
                    if (empty($filter_data)) {
                        return $query;
                    }

                    // AuctionType
                    $query->whereHas('trade', function ($query) use ($filter) {
                        $auction_types = data_get($filter->get('filter'), 'auction_type', array_keys(Trade::$auction_types_for_filter));
                        return $query->whereIn('AuctionType', $auction_types);
                    });

                    if ($filter_param->code === 'car_model') {
                        // скрыть модели машин (car_model) если не выбрана марка
                        if (!in_array('car_brand', $filter_used_keys)) {
                            return $query->where('id', 0);
                        }
                    }

                    return $query
                        ->onlyPublic()
                        ->onlyHotLots()
                        ->hasCategory($category->id)
                        ->catalogLocationFilter()
                        ->catalogSearch($search->get('q'))
                        ->catalogFilter($data, $filter_used_keys);
                })
                ->select('value')
                ->distinct()
                ->orderBy('value')
                ->get()
                ->pluck('value')
            );
        }

        return $catalog_filter_param_values;
    }

    public function filterData(TradeLotCategory $category, CatalogFilter $filter, CatalogSearch $search)
    {
        // add filter component
        $catalog_filter_params = $this->getCatalogFilterParams($category);

        // catalog_filter_param_values
        $catalog_filter_param_values = $this->getCatalogFilterParamValues($catalog_filter_params, $category, $filter, $search);

        $filter_query = request('filter', new \stdClass());
        return compact('catalog_filter_params', 'catalog_filter_param_values', 'filter_query');
    }

    public function index(CatalogFilter $filter, CatalogSearch $search, CatalogSort $sort)
    {
        // category
        $page_title = 'Успей купить';

        $categories = TradeLotCategory::getGeneralCategories();
        View::share('general_categories', $categories);

        // lots
        $lotsQuery = TradeLot::onlyPublic()
            ->onlyHotLots()
            ->neatLots()
            ->catalogLocationFilter()
            ->catalogSearch($search->get('q'))
            ->catalogFilter($filter->get('filter'));

        $counts = $this->getCountsOfLots(clone $lotsQuery);

        $statusOfCategory = request('status', 'opened');

        $titles = [
            'all' => "{$counts['all']}",
            'opened' => "{$counts['opened']}",
            'finished' => "{$counts['finished']}",
            'now' => $statusOfCategory,
        ];

        $titles = json_encode($titles, TRUE);

        if ($statusOfCategory == 'opened') {
            $lotsQuery->onlyOpened();
        } else if ($statusOfCategory == 'finished') {
            $lotsQuery->onlyFinished();
        }
        $lots = $lotsQuery->with('favorite')->orderBy($sort->field, $sort->dir)->paginate(10);

        return view('site.catalog.catalog', compact('page_title', 'lots', 'titles'));
    }

    protected function getCountsOfLots($query)
    {
        $counts = $query->selectRaw('count(*) as count_status, status')
            ->groupBy('trade_lots.status')
            ->pluck('count_status', 'status');

        $result = [];
        $result['all'] = $counts->sum();
        $result['finished'] = $counts->get('Finished', 0)
            + $counts->get('BiddingCanceled', 0)
            + $counts->get('BiddingFail', 0)
            + $counts->get('ApplicationSessionEnd', 0)
            + $counts->get('BiddingPaused', 0);
        $result['opened'] = $result['all'] - $result['finished'];

        return $result;
    }

    /**
     * Страница категории
     * @param TradeLotCategory $category
     * @param CatalogFilter $filter
     * @param CatalogSearch $search
     * @param CatalogSort $sort
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(TradeLotCategory $category, CatalogFilter $filter, CatalogSearch $search, CatalogSort $sort)
    {
        if (!$category->published) {
            throw new NotFoundHttpException('Page not found');
        }
        // current category
        View::share('catalog_category', $category);

        // category
        View::share('page_title', $category->getPageTitle());

        // add filter component
        $catalog_filter_params = $category->params()
            ->where('published', 1)
            ->orderBy('sort')
            ->get();
        View::share('catalog_filter_params', $catalog_filter_params);

        // catalog_filter_param_values
        $catalog_filter_param_values = Collection::make();
        $filter_used_keys = $filter->getUsedKeys();
        foreach ($catalog_filter_params as $catalog_filter_param) {
            if (!in_array($catalog_filter_param->type, ['checkbox', 'checkbox-vehicle-brands', 'checkbox-vehicle-models'])) {
                continue;
            };
            $catalog_filter_param_values->put($catalog_filter_param->id, $catalog_filter_param->values()
                ->whereHas('lots', function ($query) use ($search, $filter, $filter_used_keys, $catalog_filter_param) {
                    $filter_data = $filter->get('filter');
                    if ($catalog_filter_param->code === 'car_model') {
                        if (!in_array('car_brand', $filter_used_keys)) {
                            // скрыть модели машин (car_model) если не выбрана марка
                            return $query->where('id', 0);
                        }
                        // не фильтровать значения фильтров по модели (car_model)
                        return $query
                            ->onlyHotLots()
                            ->neatLots()
                            ->catalogLocationFilter()
                            ->catalogSearch($search->get('q'))
                            ->catalogFilter(Arr::except($filter_data, 'car_model'), $filter_used_keys);
                    }
                    if (in_array($catalog_filter_param->code, $filter_used_keys) || empty($filter_data)) {
                        return $query;
                    }
                    return $query
                        ->onlyHotLots()
                        ->neatLots()
                        ->catalogLocationFilter()
                        ->catalogSearch($search->get('q'))
                        ->catalogFilter($filter->get('filter'), $filter_used_keys);
                })
                ->select('value')
                ->distinct()
                ->orderBy('value')
                ->get()
                ->pluck('value')
            );
        }
        View::share('catalog_filter_param_values', $catalog_filter_param_values);

        // lots
        $lots = TradeLot::onlyPublic()
            ->forCategory($category, true)
            ->onlyHotLots()
            ->catalogLocationFilter()
            ->catalogSearch($search->get('q'))
            ->catalogFilter($filter->get('filter'))
            ->orderBy($sort->field, $sort->dir)
            ->paginate(10);

        return view('site.catalog.catalog', [
            'lots' => $lots
        ]);
    }

}

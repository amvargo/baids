<?php

namespace App\Http\Controllers\Site\Catalog;

use App\Models\Service;
use App\Models\TradeLot;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Контроллер каталога
 * Class CatalogController
 * @package App\Http\Controllers\Site
 */
class LotController extends \App\Http\Controllers\Controller
{

    /**
     * Страница лота
     * @param TradeLot $lot
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(TradeLot $lot)
    {
        $service_banners = Service::published()
            ->orderBy('priority', 'desc')
            ->whereNotNull('image')
            ->where('title', '!=', 'Подбор ликвидных предложений')
            ->where('system_key', '=', null)->limit(10)->get();
        View::share('service_banners', $service_banners);
        $map = $lot->getOptionsForMap();
        return view('site.catalog.lot', compact('lot', 'map'));
    }


}

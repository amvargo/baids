<?php

namespace App\Http\Controllers\Site\Catalog;

use App\Http\Requests\CatalogFilter;
use App\Http\Requests\CatalogSearch;
use App\Http\Requests\CatalogSort;
use App\Models\Trade;
use App\Models\TradeLot;
use App\Models\TradeLotCategory;
use App\Models\VehicleMaker;
use Carbon\Carbon;
use Illuminate\Routing\Route;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Контроллер каталога
 * Class CatalogController
 * @package App\Http\Controllers\Site
 */
class CatalogController extends \App\Http\Controllers\Controller
{

    protected function getCatalogFilterParams(TradeLotCategory $category)
    {
        return $category->params()
            ->where('published', 1)
            ->orderBy('sort')
            ->get();
    }

    protected function getCatalogFilterParamValues($catalog_filter_params, $category, CatalogFilter $filter, CatalogSearch $search)
    {
        $catalog_filter_param_values = Collection::make();
        $filter_data = $filter->get('filter', []);
        $filter_used_keys = $filter->getUsedKeys();
        $filter_stack = [];

        foreach ($catalog_filter_params as $filter_param) {
            if (!in_array($filter_param->type, ['checkbox', 'checkbox-vehicle-brands', 'checkbox-vehicle-models'])) {
                continue;
            };
            $catalog_filter_param_values->put($filter_param->id, $filter_param->values()
                ->whereHas('lots', function ($query) use (&$filter_stack, &$debug, $search, $filter, $filter_used_keys, $filter_param, $category, $filter_data, &$data) {
                    $data = Arr::only($filter_data, $filter_stack);
                    $filter_stack[] = $filter_param->code;

                    if (isset($filter_data['price'])) {
                        $query->filterPrice(data_get($filter_data, 'price.from', null), data_get($filter_data, 'price.to', null));
                    }

                    if ($filter_param->code === 'car_model') {
                        // скрыть модели машин (car_model) если не выбрана марка
                        if (!in_array('car_brand', $filter_used_keys)) {
                            return $query->where('id', 0);
                        }
                    }

                    if (empty($filter_data)) {
                        $query->onlyPublic();
                        if ($category) {
                            $query->hasCategory($category->id);
                        }
                        $query->catalogLocationFilter()
                            ->catalogSearch($search->get('q'));
                        return $query;
                    }

                    // AuctionType
                    $query->whereHas('trade', function ($query) use ($filter) {
                        $auction_types = data_get($filter->get('filter'), 'auction_type', array_keys(Trade::$auction_types_for_filter));
                        $query->whereIn('AuctionType', $auction_types);
                    });

                    $query->onlyPublic();
                    if ($category) {
                        $query->hasCategory($category->id);
                    };
                    $query->catalogLocationFilter()
                        ->catalogSearch($search->get('q'))
                        ->catalogFilter($data, $filter_used_keys);
                    return $query;
                })
                ->select('value')
                ->distinct()
                ->orderBy('value')
                ->get()
                ->pluck('value')
            );
        }

        return $catalog_filter_param_values;
    }

    public function filterData(TradeLotCategory $category, CatalogFilter $filter, CatalogSearch $search)
    {
        // add filter component
        $catalog_filter_params = $this->getCatalogFilterParams($category);

        // catalog_filter_param_values
        $catalog_filter_param_values = $this->getCatalogFilterParamValues($catalog_filter_params, $category, $filter, $search);

        $filter_query = request('filter', new \stdClass());
        return compact('catalog_filter_params', 'catalog_filter_param_values', 'filter_query');
    }

    public function filterDataWithoutCategory(CatalogFilter $filter, CatalogSearch $search)
    {
        // add filter component
        $catalog_filter_params = [];

        // catalog_filter_param_values
        $catalog_filter_param_values = [];

        $filter_query = request('filter', new \stdClass());
        return compact('catalog_filter_params', 'catalog_filter_param_values', 'filter_query');
    }

    public function withoutCategory(CatalogFilter $filter, CatalogSearch $search, CatalogSort $sort)
    {
        $page_title = 'Без категории';
        // lots
        $lots = TradeLot::onlyPublic()
            ->whereDoesntHave('categories')
            ->catalogLocationFilter()
            ->catalogSearch($search->get('q'))
            ->catalogFilter($filter->get('filter'))
            ->with('favorite')
            ->orderBy($sort->field, $sort->dir)
            ->paginate(10);

        return view('site.catalog.catalog', compact('page_title', 'lots'));
    }

    public function index(CatalogFilter $filter, CatalogSearch $search, CatalogSort $sort)
    {
        $categories = TradeLotCategory::getGeneralCategories();

        View::share('general_categories', $categories);

        // lots

        $lotsQuery = TradeLot::onlyPublic()
            ->neatLots()
            ->catalogLocationFilter()
            ->catalogSearch($search->get('q'))
            ->catalogFilter($filter->get('filter'));

        $counts = $this->getCountsOfLots(clone $lotsQuery);

        $statusOfCategory = request('status', 'opened');

        $titles = [
            'all' => "{$counts['all']}",
            'opened' => "{$counts['opened']}",
            'finished' => "{$counts['finished']}",
            'now' => $statusOfCategory,
        ];

        $titles = json_encode($titles, TRUE);

        if ($statusOfCategory == 'opened') {
            $lotsQuery->onlyOpened();
        } else if ($statusOfCategory == 'finished') {
            $lotsQuery->onlyFinished();
        }
        $lots = $lotsQuery->orderByDesc('trade_lots.id')
            ->with(['categories', 'trade', 'images'])
            ->paginate(10);

        $synonyms = VehicleMaker::getSynonymsStr($search->get('q'));

        return view('site.catalog.catalog', compact('lots', 'statusOfCategory', 'titles','synonyms'));
    }

    /**
     * Страница категории
     * @param TradeLotCategory $category
     * @param CatalogFilter $filter
     * @param CatalogSearch $search
     * @param CatalogSort $sort
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(TradeLotCategory $category, CatalogFilter $filter, CatalogSearch $search, CatalogSort $sort)
    {
        if (!$category->published) {
            throw new NotFoundHttpException('Page not found');
        }

        // куда посулать форму поиска
        View::share('search_url', $category->url);

        // current category
        View::share('catalog_category', $category);

        // category
        View::share('page_title', $category->getPageTitle());

        // lots
        $lotsQuery = TradeLot::onlyPublic()
            ->forCategory($category, true)
            ->catalogLocationFilter()
            ->catalogSearch($search->get('q'))
            ->catalogFilter($filter->get('filter'))
            ->orderBy($sort->field, $sort->dir);

        // Titles
        $counts = $this->getCountsOfLots(clone $lotsQuery);

        $statusOfCategory = request('status', 'opened');

        $titles = [
            'all' => "{$counts['all']}",
            'opened' => "{$counts['opened']}",
            'finished' => "{$counts['finished']}",
            'now' => $statusOfCategory,
        ];

        $titles = json_encode($titles, TRUE);

        if ($statusOfCategory == 'opened') {
            $lotsQuery->onlyOpened();
        } else if ($statusOfCategory == 'finished') {
            $lotsQuery->onlyFinished();
        }

        $lots = $lotsQuery->paginate(10);

        return view('site.catalog.catalog', compact('lots', 'titles', 'statusOfCategory'));
    }



    protected function getCountsOfLots($query)
    {
        $counts = $query->selectRaw('count(*) as count_status, trade_lots.status')
            ->groupBy('trade_lots.status')
            ->pluck('count_status', 'trade_lots.status');

        $result = [];
        $result['all'] = $counts->sum();
        $result['finished'] = $counts->get('Finished', 0)
            + $counts->get('BiddingCanceled', 0)
            + $counts->get('BiddingFail', 0)
            + $counts->get('ApplicationSessionEnd', 0)
            + $counts->get('BiddingPaused', 0);
        $result['opened'] = $result['all'] - $result['finished'];

        return $result;
    }

}

<?php

namespace App\Http\Controllers\Site;


use App\Http\Requests\CatalogSearch;
use App\Models\Question;
use App\Models\Service;
use App\Models\News;
use App\Models\AboutProjectSection;
use App\Models\Slider;
use App\Models\TradeLot;
use App\Models\TradeLotCategory;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Request;

class SiteController extends \App\Http\Controllers\Controller
{

    public function index(CatalogSearch $search)
    {
        if ($search->validated()) {
            return redirect(route('site.catalog.index', $search->input()));
        }

        $categories = TradeLotCategory::getGeneralCategories();
        View::share('general_categories', $categories);

        $service_banners = Service::published()->withBanners()->orderBy('priority', 'desc')->limit(10)->get();
        View::share('service_banners', $service_banners);

        $sliders = Slider::active()->whereHas('image')
            ->with('image')->orderBy('order', 'asc')->get();

        $lots = TradeLot::orderBy('id', 'desc')
            ->neatLots()
            ->catalogLocationFilter()
            ->limit(5)->get();

        $hot_lots = TradeLot::onlyPublic()
            ->onlyOpened()
            ->catalogLocationFilter()
            ->onlyHotLots()
            ->neatLots()
            ->with('favorite')
            ->orderBy('id', 'desc')
            ->limit(5)->get();
        if ($hot_lots->count()) {
            View::share('hot_lots', $hot_lots);
        }

        return view('site/general', compact('lots', 'sliders'));
    }

    public function about()
    {
        return view('site/page');
    }

    public function advertisers() {
        return view('site/advertisers');
    }
    
    public function commerce()
    {
        return view('site/commerce');
    }

    public function contacts(Request $request)
    {
        $tab = $request->get('tab') ?? "contacts";
        return view('site/contacts', compact('tab'));
    }

    public function payment(Request $request)
    {
        $tab = $request->get('tab') ?? "payment-method";
        return view('site/payment', compact('tab'));
    }

    public function view(Request $request) 
    {
        $tab = $request->get('tab') ?? "user-agreement";
        return view('site/documents', compact('tab'));
    }

    public function news() {
        $sorted = News::getNews();
        return view('site/news', compact('sorted'));
    }

    public function aboutProject() {
        $sections = AboutProjectSection::getSections();
        return view('site/about-project', compact('sections'));
    }
    
    public function tariff()
    {
        return view('site/tariff');
    }
    
    public function tariffAll()
    {
        return view('site/tariff-all');
    }

    public function lotReport(Request $request)
    {
        $subject  = 'Жалоба на ошибки лота №' . $request->get('trade_lot_id');
        $messageText = $subject . "\n";
        $messageText .= 'Ссылка: ' . $request->get('tradelink') . "\n";
        $messageText .= 'Вид ошибки: ' . $request->get('errortype') . "\n";
        if ($request->get('description')) {
            $messageText .= 'Описание: ' . $request->get('description') . "\n";
        }

        Mail::raw($messageText, function($message) use ($request)
        {
            $message->from(config('mail.from.address'), config('mail.from.username'));

            $message->subject('Жалоба на ошибки лота №' . $request->get('trade_lot_id'));

            $message->to(config('mail.lot_report_address'));
        });

        return redirect()
        ->back()
        ->withWarning('Сообщение отправлено');
    }

    
}

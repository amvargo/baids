<?php

namespace App\Http\Controllers\Site;


use App\Events\OrderCreated;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Service;
use App\Models\TradeLot;
use App\Models\User;
use App\Models\UserPayment;
use App\Role\UserRole;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ServiceController extends \App\Http\Controllers\Controller
{

    public function index()
    {
        $services = Service::published()
            ->whereNotNull('image')
            ->orderBy('id', 'desc')->paginate(10);
        return view('site/services', compact('services'));
    }

    public function show(Service $service)
    {
        return view('site/service', compact('service'));
    }

    public function order(\App\Http\Requests\Order $form)
    {
        $trade_lot_id = $form->get('trade_lot_id');
        $service = Service::find($form->get('service_id'));
        $duration = $form->get('duration') ?? 1;
        $price = $service->price;
        
        //Подбор ликвидных предложений (рассылка лотов)
        if ($service->id == 7) {
            switch ($duration) {
                case 3:
                    $price = $service->price * 1.5;
                break;
                case 12:
                    $price = $service->price * 4;
                break;
                default:
                    $price = $service->price;
                break;
            }
        }

        $user = $form->user();
        if (!$user) {
            throw new AuthorizationException('You do not have permission to view this page');
        }

        $similar_order_query = $user->active_orders()
            ->where('service_id', $service->id);
        if ($trade_lot_id) {
            $similar_order_query->where('trade_lot_id', $trade_lot_id);
        }
        if ($similar_order_query->count()) {
            return redirect()
                ->route('cabinet.payments')
                ->withWarning('У вас уже подключена данная услуга');
        }
        $order = new Order();
        $order->user()->associate($user);
        $order->service()->associate($service);

        if ($trade_lot_id = $form->get('trade_lot_id')) {
            $lot = TradeLot::findOrFail($trade_lot_id);
            $order->lot()->associate($lot);
        }

        $staff = $this->getNextStaff();
        $order->staff()->associate($staff);

        if ($duration && $duration==='trial') {
            if (User::Find($order->user_id)->isTrialMailingUnused()) {
                $payment = new UserPayment();
                $payment->fill([
                    'amount' => 0,
                    'status' => UserPayment::STATUS_SUCCESS,
                    'title' => $order->service->title,
                    'service_id' => $order->service_id,
                    'user_id' => $order->user_id    
                ]);
                $payment->save();
    
                $order->fill([
                    'service_begin' => now(),
                    'service_end' => now()->addWeek(),
                    'price' => 0,
                    'payment_id' => $payment->id
                ])
                ->setStatus(Order::STATUS_DONE)
                ->save();
                
                $payment->fill(['order_id' => $order->id])->save();
                
                return redirect()
                    ->route('cabinet.mailings')
                    ->withWarning('Услуга подключена');
            }
        }
        elseif ($duration) {
            $order->fill([
                'service_begin' => now(),
                'service_end' => now()->addMonth($duration),
                'price' => $price ?? $service->price,
            ]);
        } else {
            $order->fill([
                'service_begin' => now(),
                'service_end' => now()->addMonth(),
                'price' => $price ?? $service->price,
            ]);
        }


        $order
            ->setStatus(Order::STATUS_IN_WORK)
            ->save();

        //event(new OrderCreated($order));

        $payment = $order->createUnitellerPayment();

        $url = config('app.url').$service->redirect_to;
        $payment->url_return_no = $url;
        $payment->url_return_ok = $url;
        return $payment->createPaymentForm();
    /*    return redirect()
            ->route('cabinet.payments')
            ->withSuccess('Ваш запрос принят');*/
    }

    /**
     * Метод возвращает ответственного сотрудника на новый заказ
     * @return mixed
     */
    private function getNextStaff()
    {

        // @todo: fix logic
        $user = User::where('roles', 'like', '%' . UserRole::ROLE_STAFF . '%')
            ->inRandomOrder()->first();

        return $user;
    }

}

<?php

namespace App\Http\Controllers\Site;


use App\Models\Question;
use Illuminate\Http\Request;
use App\Http\Requests\Question as QuestionRequest;
use Illuminate\Support\Facades\Validator;

class QuestionController extends \App\Http\Controllers\Controller
{

    public function index()
    {
        $questions = Question::published()
            ->orderBy('id', 'desc')->paginate(5);
        return view('site/questions', compact('questions'));
    }

    public function store(QuestionRequest $request)
    {
        if ($request->validated()) {
            $question = new Question($request->only(['author', 'email', 'question']));
            $question->saveOrFail();
            $request->session()->flash('question-alert-success', 'Ваш вопрос успешно отправлен');
        }
        return back();
    }

}

<?php

namespace App\Http\Controllers\Site\Api;

use App\Http\Requests\UserLotCreating;
use App\Models\Trade;
use App\Models\TradeLot;
use App\Models\TradeLotCategory;
use App\Models\TradeLotParam;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Контроллер каталога
 * Class CatalogController
 * @package App\Http\Controllers\Site
 */
class CabinetController extends \App\Http\Controllers\Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function setFavorite(Request $request)
    {
        $validator = Validator::make(Input::all(), [
            'id' => 'required|exists:trade_lots,id',
            'flag' => 'required|boolean',
        ]);
        if ($validator->fails()) {
            throw new HttpException(400, $validator->errors()->first());
        }
        $lot = TradeLot::findOrFail($request->get('id'));
        /** @var User $user */
        $user = Auth::user();
        if ($request->get('flag')) {
            $user->favorite_lots()->attach($lot);
        } else {
            $user->favorite_lots()->detach($lot);
        }

        return response()->json($lot->in_favorite);
    }

    public function publishLot(UserLotCreating $request)
    {
        /** @var User $user */
        $user = Auth::user();

        if (!$user->can('create', Trade::class)) {
            throw new AccessDeniedHttpException();
        }

        $post_debtor = $request->get('debtor');
        $post_arbitr = $request->get('arbitr');
        $trade = new Trade([
            'status' => 'new',
            'TradeId' => "BAIDS-temp-user-{$user->id}",
            'user_id' => $user->id,
            'FormPrice' => 'OpenForm',
            'ApplicationTimeBegin' => Carbon::parse($request->get('ApplicationTimeBegin')),
            'ApplicationTimeEnd' => Carbon::parse($request->get('ApplicationTimeEnd')),
            'AuctionType' => $request->get('auction_type'),
            'DebtorType' => Arr::get($post_debtor, 'type'),
            'DebtorINN' => Arr::get($post_debtor, 'inn'),
            'CaseNumber' => $request->get('CaseNumber'),
            'ApplicationRules' => $request->get('ApplicationRules'),
            'ArbitrManager' => [
                'FullName' => Arr::get($post_arbitr, 'fio'),
                'INN' => Arr::get($post_arbitr, 'inn'),
                'Phone' => Arr::get($post_arbitr, 'phone'),
                'SRO' => Arr::get($post_arbitr, 'sro'),
            ],
        ]);
        $trade->save();
        $trade->TradeId = "BAIDS-{$trade->id}";
        $trade->save();
        foreach ((array)$request->file('docs') as $doc) {
            $trade->saveFileDoc($doc);
        }

        $lot = new TradeLot([
            'status' => 'new',
            'user_id' => $user->id,
            'TradeId' => "BAIDS-{$trade->id}",
            'LotNumber' => 1,
            'StartPrice' => $request->get('start_price'),
            'LotTitle' => $request->get('title'),
            'TradeObjectHtml' => $request->get('description'),
            'SaleAgreement' => $request->get('SaleAgreement'),
            'PaymentInfo' => $request->get('PaymentInfo'),
            'trade_place_url' => $request->get('trade_place_url'),
            'trade_section' => $request->get('trade_section'),
        ]);


        $step_price = $request->get('step_price');
        if (strpos($step_price, '%') === false) {
            $lot->StepPrice = $step_price;
        } else {
            $lot->StepPricePercent = substr($step_price, 0, -1);
        }

        $advance_price = $request->get('advance_price');
        if (strpos($advance_price, '%') === false) {
            $lot->Advance = $advance_price;
        } else {
            $lot->AdvancePercent = substr($advance_price, 0, -1);
        }

        $lot->save();
        foreach ((array)$request->file('images') as $image) {
            $lot->saveFileImage($image);
        }

        $category = TradeLotCategory::find(request()->get('category'));
        $lot->categories()->attach($category);

        $post_params = $request->get("params");
        foreach ($category->params as $param) {
            $param_value = Arr::get($post_params, $param->code);
            if (!empty($param_value)) {
                $lot->params()->updateOrCreate([
                    'trade_lot_param_id' => $param->id
                ], [
                    'value' => $param_value
                ]);
            }
        }

        $address = $request->get('address');
        $lot->params()->updateOrCreate([
            'trade_lot_param_id' => 1
        ], [
            'value' => $address
        ]);
        $lot->updateAddressByDadata();
        $lot->load(['images']);
        // ...

        Session::put('success', 'Лот успешно опубликован');
        $redirect_url = route('cabinet.bidding');
        return response()->json(compact('lot', 'redirect_url'));
    }

    public function userMailings()
    {
        /** @var User $user */
        $user = Auth::user();
        $result = [
            'user_mailings' => $user->mailings()
                ->with('category.params', 'filters.param')
                ->get()
                ->map(function ($item) {
                    $item->params = Collection::make();
                    foreach ($item->category->params()->where('published', 1)->get() as $param) {
                        if ($param->code === 'region_with_type') {
                            $param->values = TradeLot::whereNotNull('region_with_type')
                                ->select(['region_with_type'])->distinct()
                                ->orderBy('region_with_type')
                                ->get()->pluck('region_with_type');
                        } elseif ($param->type === 'checkbox') {
                            $param->values = $param->values()
                                ->select(['value'])->distinct()
                                ->orderBy('value')
                                ->get()->pluck('value');
                        } else {
                            $param->values = [];
                        }
                        $item->params->push($param);
                    }
                    return $item;
                })
        ];
        return response()->json($result);
    }

    public function updateUserMailing(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'nullable|exists:user_mailing_lots,id',
            'category' => 'required|exists:trade_lot_categories,id',
            'price_from' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'price_to' => 'nullable|regex:/^\d+(\.\d{1,2})?$/',

            'filters' => 'nullable|array',
            //'filters.*' => 'required|array',
        ]);

        $validator->validate();

        // filters validation
        $rules = [];
        foreach ($request->get('filters') as $param_code => $value) {
            $param = TradeLotParam::where('code', $param_code)->first();
            if (!$param) {
                continue;
            }
            switch ($param->type) {
                case 'checkbox':
                case 'checkbox-vehicle-brands':
                case 'checkbox-vehicle-models':
                    $rules['filters.' . $param->code] = 'nullable|array';
                    break;
                case 'numeric-range':
                    $rules['filters.' . $param->code . '.from'] = 'nullable|regex:/^\d+(\.\d{1,2})?$/';
                    $rules['filters.' . $param->code . '.to'] = 'nullable|regex:/^\d+(\.\d{1,2})?$/';
                    break;
                default: // string
                    break;
            }
        }
        $validator = Validator::make($request->all(), $rules);
        $validator->validate();


        /** @var User $user */
        $user = Auth::user();

        $mailing = $user->mailings()->findOrNew($request->get('id'));
        if (empty($mailing->id)) {
            $mailing->fill([
                'last_report' => now()
            ]);
        }

        $category = TradeLotCategory::findOrFail($request->get('category'));

        $mailing->fill([
            'price_from' => $request->get('price_from'),
            'price_to' => $request->get('price_to'),
            'category_id' => $category->id,
            'user_id' => $user->id
        ]);

        if ($mailing->save()) {
            $mailing->filters()->delete();
            foreach ($request->get('filters') as $param_code => $value) {
                $param = TradeLotParam::where('code', $param_code)->first();
                if (!$param) {
                    continue;
                }
                switch ($param->type) {
                    case 'checkbox':
                    case 'checkbox-vehicle-brands':
                    case 'checkbox-vehicle-models':
                        foreach ($value as $item) {
                            $mailing->filters()->create([
                                'trade_lot_param_id' => $param->id,
                                'value' => $item,
                            ]);
                        }
                        break;
                    case 'numeric-range':
                        if (empty($value['from']) && empty($value['to'])) {
                            break;
                        }
                        $mailing->filters()->create([
                            'trade_lot_param_id' => $param->id,
                            'value' => $value['from'] . '-' . $value['to'],
                            'value_from' => $value['from'],
                            'value_to' => $value['to'],
                        ]);
                        break;
                    default: // string
                        break;
                }
            }
        }

        $mailing->refresh();
        $mailing->params = Collection::make();
        foreach ($mailing->category->params()->where('published', 1)->get() as $param) {
            if ($param->type === 'checkbox') {
                $param->values = $param->values()
                    ->select(['value'])->distinct()
                    ->orderBy('value')
                    ->get()->pluck('value');
            } else {
                $param->values = [];
            }
            $mailing->params->push($param);
        }

        return response()->json([
            'mailing' => $mailing
        ]);

    }

    public function deleteUserMailing(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'nullable|exists:user_mailing_lots,id',
        ]);

        $validator->validate();

        /** @var User $user */
        $user = Auth::user();

        $mailing = $user->mailings()->findOrFail($request->get('id'));

        $mailing->delete();
    }

    public function dataForMailings(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category' => 'required|exists:trade_lot_categories,id',
        ]);

        $validator->validate();

        $category = TradeLotCategory::onlyPublic()->with('params');

        $result = [
            'data_for_mailings' => $category->find($request->get('category'))
        ];
        return response()->json($result);
    }

    public function mailingLots()
    {
        /** @var User $user */
        $user = Auth::user();

        $query = TradeLot::byUserMailings($user->active_mailings)
            ->with('images', 'categories')
            ->select(['id','TradePlaceINN','TradeId','StartPrice','NewPrice','TradeObjectHtml','created_at',
                'region_with_type','federal_district','LotTitle'])
            ->orderBy('id', 'desc');

        $lots = $query->paginate(20);

        $lots->each(function (TradeLot $lot) {
            $lot->append('in_favorite');
        });

        return response()->json($lots);
    }
}

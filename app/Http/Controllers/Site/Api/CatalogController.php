<?php

namespace App\Http\Controllers\Site\Api;

use App\Http\Requests\CatalogFilter;
use App\Http\Requests\CatalogSearch;
use App\Models\FedResurs\CompanyTradeOrganizer;
use App\Models\FedResurs\DebtorCompany;
use App\Models\FedResurs\DebtorPerson;
use App\Models\Trade;
use App\Models\TradeLot;
use App\Models\TradeLotCategory;
use App\Models\User;
use App\Services\SphinxService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class CatalogController
 * @package App\Http\Controllers\Site
 */
class CatalogController extends \App\Http\Controllers\Controller
{


    public function trade_organizer_search_list(TradeLotCategory $category, CatalogFilter $filter, CatalogSearch $search)
    {
        $q = \request()->get('str');

        $query = SphinxService::getSphinxSearch('baids_trades_index', '*'.$q.'*', 'arbitr_name', 'arbitr_name');
        
        if ($query === false) {
            $query = Trade::whereIn('TradeId',  function ($query) use ($q) {
                $query->select('TradeId')->from('trade_lots')->where('published', 1)->whereNull('deleted_at');    
            })->where('arbitr_name', 'LIKE', '%' . $q . '%')->selectRaw('arbitr_name as organizer_name')    
                ->groupBy('organizer_name')->pluck('organizer_name');   
        } elseif (count($query) == 0) {
            $query = SphinxService::getSphinxSearch('baids_lot_index', '*'.$q.'*', 'organizer_name', 'organizer_name');
        }

        $response = [];

        foreach ($query as $name) {
            $key = trim(preg_replace('/\s\s+/', ' ', $name));
            $response[$key] = 0;
            if( count( $response ) == 10) {
                break;
            }
        }

        return response()->json( array_keys( $response ) );
    }

    public function trade_organizer_search_list_without_category(CatalogFilter $filter, CatalogSearch $search)
    {
        $query = $this->searchByField('trade_organizer', 'organizer_name', null, $filter, $search);
        return response()->json($query->get()->pluck('organizer_name'));
    }

    public function debtor_search_list(TradeLotCategory $category, CatalogFilter $filter, CatalogSearch $search)
    {
        $q = \request()->get('str');
        
        $query = SphinxService::getSphinxSearch('baids_lot_index', $q, 'debtor_name', 'debtor_name');

        if ($query === false) {
            $query = $this->searchByField('debtor', 'debtor_name', $category, $filter, $search);

            return response()->json($query->get()->pluck('debtor_name'));
        }

        $response = [];

        foreach ($query as $name) {
            $key = trim(preg_replace('/\s\s+/', ' ', $name));
            $response[$key] = 0;
            if( count( $response ) == 10) {
                break;
            }
        }
        return response()->json( array_keys( $response ) );
    }

    public function debtor_search_list_without_category(CatalogFilter $filter, CatalogSearch $search)
    {
        $query = $this->searchByField('debtor', 'debtor_name', null, $filter, $search);
        return response()->json($query->get()->pluck('debtor_name'));
    }

    private function searchByField($field, $key, $category, CatalogFilter $filter, CatalogSearch $search)
    {
        $q = \request()->get('str');
        $filter_data = Arr::except($filter->get('filter', []), [$field]);

        $query = TradeLot::onlyPublic();
        if($category){
            $query->neatLots();
        }

        /** @var $category TradeLotCategory */
        if ($category && !empty($category->id)) {
            $query->hasCategory($category);
        }

        $query->catalogLocationFilter()->catalogFilter($filter_data);

        $query->where($key, '<>', '')->where($key, 'like', '%' . $q . '%');//->dd()

        $query->orderBy($key)
        ->select($key)
        ->limit(10)
        ->groupBy($key);

        return $query;
    }
}

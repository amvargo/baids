<?php

namespace App\Http\Controllers\Site\Api;

use App\Http\Requests\CatalogLocation;
use App\Models\TradeLot;
use Fomvasss\Dadata\Facades\DadataSuggest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Контроллер каталога
 * Class CatalogController
 * @package App\Http\Controllers\Site
 */
class LocationController extends \App\Http\Controllers\Controller
{
    public function regions()
    {
        if (empty($region = request()->post('region'))) {
            throw new BadRequestHttpException('Bad request');
        }
        $result = TradeLot::select('city_with_type as text')
            ->where('region_with_type', $region)
            ->orderBy('text')
            ->distinct()
            ->get();
        return response()->json($result);
    }

    public function regionsGet()
    {
        if (empty($str = request()->post('str'))) {
            throw new BadRequestHttpException('Bad request');
        }
        try {
            $result = DadataSuggest::suggest("address", ["query" => $str, "count" => 5]);
        } catch (\Exception $err){
            $result = [];
        }
        return response()->json($result);
    }

    public function getSession()
    {
        return response()->json(CatalogLocation::getFromSession());
    }

    public function setSession(CatalogLocation $request)
    {
        $filter = $request->saveToSession();
        return response()->json($filter);
    }
}

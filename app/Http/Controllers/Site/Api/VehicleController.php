<?php


namespace App\Http\Controllers\Site\Api;


use App\Models\VehicleMaker;
use App\Models\VehicleModel;
use App\Models\VehicleSpecialMaker;
use App\Models\VehicleSpecialModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

class VehicleController extends \App\Http\Controllers\Controller
{
    // api/vehicle/models?brand=Ford
    public function models(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'brand' => 'required|exists:vehicle_makers,name'
        ]);
        $validator->validate();

        $vehicle_maker = VehicleMaker::where(['name' => $request->get('brand')])->firstOrFail();
        return response()->json([
            'models' => $vehicle_maker->models()->get()->pluck('name')
        ]);
    }

    public function specModels(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'brand' => 'required|exists:vehicle_special_makers,name'
        ]);
        $validator->validate();

        return response()->json([
            'models' => VehicleSpecialModel::select('name')
                ->distinct()
                ->whereHas('maker', function ($query) use ($request) {
                    return $query->where('name', $request->get('brand'));
                })
                ->get()
                ->pluck('name')
        ]);
    }

    // api/vehicle/brands
    public function brands()
    {
        return response()->json([
            'brands' => VehicleMaker::select('name')
                ->distinct()
                ->get()
                ->pluck('name')
        ]);
    }

    public function specBrands()
    {
        return response()->json([
            'brands' => VehicleSpecialMaker::select('name')
                ->distinct()
                ->get()
                ->pluck('name')
        ]);
    }
}

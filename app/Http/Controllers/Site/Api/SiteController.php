<?php

namespace App\Http\Controllers\Site\Api;

use App\Http\Requests\UserLotCreating;
use App\Models\Service;
use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Контроллер апи для сайта
 * Class SiteController
 * @package App\Http\Controllers\Site
 */
class SiteController extends \App\Http\Controllers\Controller
{
    public function tariffGet(Request $request){
        //тарифные планы на 1/3/12 месяцев
        $tariffs = Service::whereIn('id', Service::TARIFF_IDS)->get();
        return $tariffs;
    }
}

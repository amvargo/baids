<?php

namespace App\Http\Controllers\Site;

use App\Models\Banner;
use App\Models\Order;
use App\Models\Service;
use App\Models\TradeLotCategory;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use App\Http\Requests\Advertising;
use App\Models\UserPayment;
use DateTime;

class AdvertisingController extends Controller
{
    protected $edit_element;

    public function index() {
        $categories = TradeLotCategory::onlyPublic()->rootCategories()->get(['title', 'id', 'slug']);
        $services = Service::published()->advertising()->get(['title', 'id', 'system_key'])->toArray();
        $edit_element = $this->edit_element;

        return view('site/advertising', compact('services', 'categories', 'edit_element'));
    }


    public function getPrice(Request $request){
        $post = $request->post();
        $default_price = Service::published()->advertising()->where('id', $post['service_id'])->value('price');
        $price = Banner::getPrice($post['date_start'], $post['date_end'], $default_price);

        return $price;
    }

    public function dateCheck(Request $request){
        $arr = [];

        $banners = Banner::where('variant', $request['variant'])->where('category_id', $request['category'])->get();
        foreach($banners as $banner) {
            $orders = Order::where('id', $banner['order_id'])->get();
            foreach ($orders as $order) {
                $payments = UserPayment::where('id', $order['payment_id'])->get();
                if (!empty($order['service_begin'])) {
                    if ($payments[0]['status'] === 'success') {
                        array_push($arr, [$order['service_begin'], $order['service_end']]);
                    } elseif (date("Y-m-d H:i:m", time()) <= date("Y-m-d H:i:m", strtotime($order['created_at'] . "+1 day")) AND $payments[0]['status'] === "waiting") {
                        array_push($arr, [$order['service_begin'], $order['service_end']]);
                    }
                }
            }
        }

        return json_encode($arr);
    }


    public function create(Advertising $request) {
        $user = Auth::user();
        $post = request()->post();
        $default_price = Service::published()->advertising()->where('id', $post['service_id'])->value('price');
        $price = Banner::getPrice($post['date_start'], $post['date_end'], $default_price);

        $banner = Banner::create($post + ['user_id'=> $user->id]);

        $banner->setImage($request->file("image"));

        $order = Order::create([
            'service_begin' => $post['date_start'],
            'service_end' => $post['date_end'],
            'user_id' => $user->id,
            'service_id' => $post['service_id'],
            'price' => $price
        ]);

        $banner->update(['order_id' => $order->id]);

        $payment = $order->createUnitellerPayment();

        return $payment->createPaymentForm();
    }

    public function getBanners(Request $request){
        $variant = $request->post('variant', 'horizontal');
        $limit = $request->post('limit', 3);
        $service = $request->post('position', 3);
        $category = $request->post('category', 0);
        if($category) {
            $category = explode('|', $category);
            if ($category[1]) {
                $category = $category[1];
            } elseif ($category[0]) {
                $category = $category[0];
            }
        }
        $banners = Banner::allowed()->active()->variant($variant)->category($category)->service($service)
            ->inRandomOrder()->with('image')
            ->whereHas('order', function($query){
                return $query->where('service_begin', '<=', now())
                    ->where('service_end', '>=', now());
            })
            ->limit($limit)->get()
            ->makeHidden(['user_id', 'category_id', 'created_at', 'order_id', 'service_id', 'status', 'status_name', 'updated_at', 'variant_name']);
        return $banners;
    }

    public function getStringDays($count) {
        return $count . " " . Lang::choice('день|дня|дней', $count, [], 'ru');
    }

    public function getUserTable() {
        if($user = Auth::user()) {
            $banners = Banner::byUser($user->id)->with('order.payment', 'category', 'service')->orderByDesc('created_at')->paginate(10);

            return view('site.cabinet.banners', compact('banners'));
        }
    }

    public function getEditBanner(Banner $banner){
        $this->checkUser($banner);

        $this->edit_element = $banner->load('order', 'image')->toArray();
        return $this->index();
    }

    public function setEditBanner(Banner $banner) {
        $this->checkUser($banner);
        $result = [];
        if($banner->isDeny()) {
            $banner->setModerateFields();

            if(request()->post('add_file')) {
                $image = $banner->setImage(request()->file("image"));
                $banner->setModerateStatus();
                $result['image'] = [
                    'name' => $image->name,
                    'url' => $image->url,
                ];
            }

            $banner->save();
            $result['status'] = $banner->status;
        }
        return response()->json($result, 200);

    }

    public function getNewPayment(UserPayment $payment) {
        $payment->url_return_no = request()->post('back_url_no');
        $payment->url_return_ok = request()->post('back_url_ok');
        return $payment->createPaymentForm();
    }

    public function checkUser($banner) {
        $user = Auth::user();
        if($user && $user->id != $banner->user_id) {
            abort(404);
        }
    }
}

<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PanelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        /*if(isset($this->resource->created_at)){
            $this->resource->created_at = $this->resource->created_at->setTimezone('UTC');
        }*/

        return parent::toArray($request);
    }
}

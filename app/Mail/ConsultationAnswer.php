<?php

namespace App\Mail;

use App\Models\Panel\ConsultingMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConsultationAnswer extends Mailable
{
    use Queueable, SerializesModels;

    public $model;

    /**
     * Create a new message instance.
     *
     * @param ConsultingMessage $model
     */
    public function __construct(ConsultingMessage $model)
    {
        $this->model = $model;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            //->view('emails.consulting.answer')
            ->text('emails.consulting.answer_plain');
    }
}

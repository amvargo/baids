<?php

namespace App\Mail;

use App\Models\TradeLot;
use App\Models\UserMailingLot;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;

class UserMailingLots extends Mailable
{
    use Queueable, SerializesModels;

    public $lots = null;
    public $mailing = null;

    public $subject = 'Рассылка лотов';

    /**
     * Create a new message instance.
     *
     * @param UserMailingLot $mailing
     * @param TradeLot[]|Collection $lots
     */
    public function __construct(UserMailingLot $mailing, $lots)
    {
        $this->lots = $lots;
        $this->mailing = $mailing;
        $this->subject = "Рассылка лотов из категории: {$mailing->category->title}";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.mailing_lots.report');
    }
}

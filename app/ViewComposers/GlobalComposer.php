<?php


namespace App\ViewComposers;


use App\Models\UserNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

class GlobalComposer
{

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('user_favorites', 0);
        $view->with('user_notifications', 0);
        $view->with('current_route', Route::currentRouteName());
        $user_timezone = 'Asia/Yekaterinburg';
        if ($user = Auth::user()) {
            $view->with('user_favorites', $user->favorite_lots()->count());
            $user_timezone = $user->timezone;
            $view->with('user_timezone', $user_timezone);
            $user = auth()->user();
            $messages = 0;
            $notifications = UserNotification::where(function ($query) use ($user) {
                return $query->where('user_id', $user->id)->orWhereNull('user_id');
            })->orderBy('id', 'desc')->get();
            foreach ($notifications as $item) {
                if (!$item->is_read) {
                    $messages++;
                }
            }
            $view->with('user_notifications', $messages);
        };
    }
}

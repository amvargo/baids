<?php

namespace App\Jobs;

use App\Models\TradeLot;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ProcessSignAndSendOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $lot;

    private static $disk_signature = 'signature';

    /**
     * Create a new job instance.
     *
     * @param TradeLot $lot
     */
    public function __construct(TradeLot $lot)
    {
        $this->lot = $lot;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('-----');
        Log::info('Test process for Lot ID: ' . $this->lot->id);

        $this->signFileAndSendByMail();
    }

    private function signFileAndSendByMail()
    {
        Log::info('-----signFileAndSendByMail-----');
        $disk_signature = $key = Storage::disk(self::$disk_signature);
        $disk_docs = $key = Storage::disk(self::$disk_signature);

        $file_name = 'doc1.txt';
        $file_name_out = "{$file_name}.sig";

        //$key = $disk_signature->path('signer1/key.pfx');
        $signer = $disk_signature->path('signer1/p12.pem');
        $in = $disk_docs->path($file_name);
        $output = $disk_docs->path($file_name_out);

        $cmd = "openssl cms -sign -in {$in} -signer {$signer} -out {$output} -outform pem -nodetach"; // -inkey {$key}

        exec($cmd, $result, $return_var);
        Log::info('exec:', compact('cmd', 'result', 'return_var'));
        $size_in = $disk_docs->size($file_name);
        $size_out = $disk_docs->size($file_name_out);
        Log::info("Size {$in}: {$size_in}");
        Log::info("Size {$output}: {$size_out}");
    }
}

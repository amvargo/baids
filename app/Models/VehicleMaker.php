<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class VehicleMaker extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vehicle_makers';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code', 'created_at', 'updated_at'];

    protected $appends = ['models_count', 'variants_count'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->code = Str::slug($model->name);
        });
        static::deleted(function ($check) {
            foreach ($check->variants as $variant) {
                $variant->delete();
            }
            foreach ($check->models as $model) {
                $model->delete();
            }
        });
    }

    public function models()
    {
        return $this->hasMany(VehicleModel::class);
    }

    public function getModelsCountAttribute()
    {
        return $this->models()->count();
    }

    public function variants()
    {
        return $this->hasMany(VehicleMakerVariant::class);
    }

    public function getVariantsCountAttribute()
    {
        return $this->variants()->count();
    }

    public function scopeSearchText($query, $text)
    {
        return $query->leftJoin('vehicle_maker_variants', 'vehicle_maker_variants.vehicle_maker_id', '=', 'vehicle_makers.id')
            ->where('name', '=', $text)
            ->orWhere('text', '=', $text);
    }

    public static function getSynonymsStr($search)
    {
        if (empty($search)) {
           return '';
        }
        $maker = VehicleMaker::where('name', 'like', '%' . $search . '%')->get()->toArray();
        $maker2 = VehicleSpecialMaker::where('name', 'like', '%' . $search . '%')->get()->toArray();
        $maker = collect(array_merge($maker, $maker2));
        if (!$maker->count()) {
            $maker = VehicleMakerVariant::where('text', 'like', '%' . $search . '%')->get();
            $maker = VehicleMaker::whereIN('id', $maker->pluck('vehicle_maker_id'))->get();
            $synonyms = VehicleMakerVariant::whereIN('vehicle_maker_id', $maker->pluck('id'))->get()->toArray();

            $maker2 = VehicleSpecialMakerVariant::where('text', 'like', '%' . $search . '%')->get();
            $maker2 = VehicleSpecialMaker::whereIN('id', $maker2->pluck('vehicle_special_maker_id'))->get();
            $synonyms2 = VehicleSpecialMakerVariant::whereIN('vehicle_special_maker_id', $maker2->pluck('id'))->get()->toArray();

            $maker = collect(array_merge($maker->toArray(), $maker2->toArray()));
            $synonymsArr = collect(array_merge($synonyms, $synonyms2))->pluck('text')->merge($maker->pluck('name'));
        } else {
            $synonyms = VehicleMakerVariant::whereIN('vehicle_maker_id',  $maker->pluck('id'))->get()->toArray();
            $synonyms2 = VehicleSpecialMakerVariant::whereIN('vehicle_special_maker_id',  $maker->pluck('id'))->get()->toArray();
            $synonyms = collect(array_merge($synonyms,$synonyms2));
            $synonymsArr = $maker->pluck('name')->merge($synonyms->pluck('text'));
        }
        return implode(' ',$synonymsArr->all());
    }
}

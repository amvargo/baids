<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleSpecialModelVariant extends Model
{

    protected $table = 'vehicle_special_model_variants';

    protected $fillable = ['text', 'created_at', 'updated_at'];
    protected $hidden = ['vehicle_special_model_id'];

    public $timestamps = false;

    public function model()
    {
        return $this->belongsTo(VehicleSpecialModel::class, 'vehicle_special_model_id');
    }

    public function scopeSearchText($query, $text)
    {
        return $query->where('text', '=', $text)->with('model')
            ->orWhereHas('model', function ($query) use ($text) {
                return $query->where('name', '=', $text);
            });
    }
}

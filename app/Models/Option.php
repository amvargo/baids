<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string key
 * @property mixed value
 */
class Option extends Model
{
    public $incrementing = false;
    protected $primaryKey = 'key';
    protected $fillable = ['key', 'value', 'name'];

    const DEFAULT_OPTIONS = [
         ['key' => 'banner_price.general_day', 'value' => 1000, 'name' => 'Цена для главной страницы (день)' ],
         ['key' => 'banner_price.catalog_day', 'value' => 500, 'name' => 'Цена для стр. каталога (день)' ],
         ['key' => 'a', 'value' => 'c', 'name' => 'a']
    ];

    public function setValue($val)
    {
        $this->value = $val;
        return $this->save();
    }

    public static function get($key)
    {
        $res = self::findOrNew($key);
        if (empty($res->key)) {
            $res->key = $key;
        }
        return $res;
    }

    public static function getValue($key, $default = null)
    {
        $result = $default;
        $res = self::find($key);
        if ($res) {
            $result = $res->value;
        }
        if(!empty(self::DEFAULT_OPTIONS[$key])) {
            $result = self::DEFAULT_OPTIONS[$key];
        }

        return $result;
    }

    public static function put($key, $val)
    {
        $res = self::get($key);
        return $res->setValue($val);
    }

    public static function getBannerDefaultPrice() {
        $default_options = [];
        foreach(self::DEFAULT_OPTIONS as $option) {
            if( strpos($option['key'], 'banner_price') !== false) {
                $default_options[$option['key']] = $option['value'];
            }
        }

        $options = Option::where('key', 'like', "banner_price.%")->pluck('value', 'key')->all();

        return $options + $default_options;


    }

    public function scopeNotParser($query) {
        return $query->where('key', 'not like', 'parser%');
    }
}

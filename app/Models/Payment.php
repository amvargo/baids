<?php

namespace App\Models;

use App\Traits\firePanelModelEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

/**
 * @property integer id
 */
class Payment extends Model
{
    use SoftDeletes, Notifiable, firePanelModelEvent;

    const
        STATUS_WAITING = 'waiting';


    protected $fillable = [
        'service_id', 'user_id', 'staff_id', 'type', 'amount'
    ];

    protected $casts = [
        'amount' => 'float',
    ];

    public function service()
    {
        return $this->belongsTo(Service::class)->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function staff()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function setNotification(){

        if( ($this->wasChanged('type')) && ($this->type == 'success') ) {

            $service = $this->service;

            $title = 'Успешная оплата услуги ' .  $service->title;
            $body = $this->user->full_name . ' у вас успешна проплачена услуга - ' . $service->title;

            UserNotification::setNotification($title, $body, $this->user_id);
        }
    }


    protected static function boot()
    {
        parent::boot();
        static::updated(function ($payment) {

            $payment->setNotification();

        });
    }

}

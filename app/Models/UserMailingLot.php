<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class UserMailingLot
 * @property float price_from
 * @property float price_to
 * @property string|Carbon|null last_report
 * @property Collection|UserMailingLotFilter[] filters
 * @property TradeLotCategory category
 * @package App\Models
 */
class UserMailingLot extends Model
{
    const ACCESS_FOR_SERVICE_ID = 7;

    public $timestamps = null;

    protected $fillable = [
        'price_from', 'price_to', 'category_id', 'user_id', 'last_report'
    ];

    protected $dates = [
        'last_report'
    ];

    public function filters()
    {
        return $this->hasMany(UserMailingLotFilter::class);
    }

    public function category()
    {
        return $this->belongsTo(TradeLotCategory::class);
    }

    public function active_order()
    {
        return $this->belongsTo(Order::class, 'user_id', 'user_id')
            ->where('service_id', self::ACCESS_FOR_SERVICE_ID)
            ->where('service_begin', '<', now())
            ->where('service_end', '>', now())
            ->where('status', Order::STATUS_IN_WORK);
    }
}

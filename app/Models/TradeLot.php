<?php

namespace App\Models;

use App\Http\Requests\CatalogLocation;
use App\Jobs\ProcessSignAndSendOrder;
use App\Models\Trade;
use App\Models\TradeLotParam;
use App\Models\FedResurs\CompanyTradeOrganizer;
use App\Models\FedResurs\DebtorCompany;
use App\Models\FedResurs\DebtorPerson;
use App\Services\SphinxService;
use Awobaz\Compoships\Compoships;
use Carbon\Carbon;
use Fomvasss\Dadata\Facades\DadataSuggest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\QueriesRelationships;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use mysql_xdevapi\Collection;
use phpDocumentor\Reflection\Types\Integer;
use PhpParser\Node\Expr\Cast\Object_;
use function foo\func;
use function Psy\debug;

/**
 * @property mixed TradeObjectHtml Предмет торгов (Сведения об имуществе (предприятии) должника...)
 * @property Trade|null trade
 * @property float|null StartPrice Начальная цена, руб
 * @property float|null NewPrice Итоговая/текущая цена, руб.
 * @property float|null CurrentPrice Текущая цена, руб.
 * @property array Classification Классификация имущества
 * @property string Title
 * @property int|null EntryCount Количество поступивших заявок
 * @property int|null AcceptCount Количество принятых заявок
 * @property object|null SuccessTradeResult Информация о состоявшихся торгах
 * @property string WinnerText Победитель
 * @property string WinnerSubstantiationText Обоснование итогового решения
 * @property bool BiddingInProcess
 * @property float StepPrice Шаг аукциона. Заполняется, если задается в рублях. Шаг аукциона может быть
 * указан в процентах, для этого используется атрибут StepPricePercent.
 * @property float StepPricePercent Шаг аукциона. Заполняется, если задается в процентах
 * @property string PriceReduction Информация о снижении цены. Присутствие элемента обязательно для публичного
 * предложения. Присутствие элемента необязательно для всех конкурсов и аукционов
 * @property float Advance Задаток. Задается в рублях.
 * @property float AdvancePercent Задаток. Задается в процентах.
 * @property string Concours Условия проведения открытых и закрытых торгов в форме конкурса
 * @property string Participants Требования к участникам в случае проведения торгов, закрытых по составу участников.
 * Элемент обязателен для закрытого аукциона, закрытого конкурса и закрытого публичного предложения
 * @property string PaymentInfo Сроки платежей, реквизиты счетов, на которые вносятся платежи
 * @property string SaleAgreement Порядок и срок заключения договора купли-продажи имущества (предприятия) должника
 * @property string BiddingFailReason - Причина, по которой торги не состоялись.
 * @property string BiddingCancelReason - Причина отмены торгов.
 * @property string
 * @property mixed TradePlaceINN
 * @property mixed id
 * @property mixed TradeId
 * @property bool BiddingPaused
 * @property string|null status
 * @property array|null status_log
 * @property string|null category_name
 * @property \Illuminate\Support\Collection|TradeLotCategory[] categories
 * @property \Illuminate\Support\Collection|TradeLotClassificator[] classificators
 * @property string|null postal_code
 * @property string|null federal_district
 * @property string|null region_with_type
 * @property string|null area_with_type
 * @property string|null city_with_type
 * @property array|null dadata_suggest
 * @property int|null updated_user_id
 * @property int|null user_id
 * @property string|null LotTitle
 * @method static Builder|static byUserMailings(UserMailingLot|\Illuminate\Support\Collection $mailing, $only_new = false) собирает лоты по рассылкам
 * @method static Builder|static onlyPublic() только опубликованные лоты
 * @method static Builder|static onlyOpened() только активные лоты
 * @method static Builder|static onlyFinished() только закрытые лоты
 * @method static Builder|static orderByPrice($orderType = 'DESC')
 * @method static Builder|static catalogLocationFilter()
 * @method static Builder|static catalogSearch(Object $search_str)
 * @method static Builder|static filterPrice($from, $to = null)
 * @method static Builder|static catalogFilter(Object $filter)
 * @method static Builder|static hasCategory(Object|int|array $category)
 * @method static Builder|static onlyHotLots($last_days = 7)
 * @method static Builder|static neatLots() только со всеми данными
 * @method static Builder|static forCategory(TradeLotCategory|int $category, bool $with_children) по категории
 */
class TradeLot extends Model
{
    use SoftDeletes, Compoships;

    protected $casts = [
        'Classification' => 'array',
        'ParticipantsArr' => 'array',
        'SuccessTradeResult' => 'array',
        'FailureTradeResult' => 'array',
        'ApplicationList' => 'array',
        'status_log' => 'array',
        'TradeObjectHtml' => 'text',
        'BiddingInProcess' => 'boolean',
        'BiddingPaused' => 'boolean',
        'dadata_suggest' => 'array',
        'published' => 'boolean',
    ];

    protected $fillable = [
        'TradeId', 'TradePlaceINN', 'LotNumber', 'StartPrice', 'NewPrice', 'BiddingInProcess',
        'BiddingFailReason', 'BiddingPaused', 'status', 'status_log', 'updated_user_id',
        'postal_code', 'federal_district', 'region_with_type', 'area_with_type', 'city_with_type', 'published', 'dadata_suggest',

        'StepPrice', 'Advance', 'LotTitle', 'TradeObjectHtml', 'user_id',
        'TradeOrganizerINN', 'StepPricePercent', 'AdvancePercent',
        'place_name', 'debtor_name', 'organizer_name', 'PriceReductionAuction'
    ];

    protected $appends = [
        'title', 'current_price', 'category_name', 'short_description', 'last_category_lot_img'
    ];

    public static $sort_list = [
        'id' => 'By ID',
        'updated_at' => 'By time updated',
        'created_at' => 'By created',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->status_log = [$model->status];
        });

        self::updating(function ($model) {
            if ($model->status !== last($model->status_log)) {
                $status_log = (array)$model->status_log;
                array_push($status_log, (string)$model->status);
                $model->status_log = $status_log;
            }
        });

        self::saving(function ($model) {
            if ($model->trade->isDirty('TradeOrganizer')) {
                $model->organizer_name = $model->trade->TradeOrganizer['ShortName']
                ?? $model->trade->TradeOrganizer['FullName']
                ?? sprintf(
                    "%s %s %s",
                    trim(data_get($model->trade->TradeOrganizer, 'LastName', '')),
                    trim(data_get($model->trade->TradeOrganizer, 'FirstName', '')),
                    trim(data_get($model->trade->TradeOrganizer, 'MiddleName', '')),
                ) 
                ?? sprintf(
                    "%s %s",
                    trim(data_get($model->trade->TradeOrganizer, 'LastName', '')),
                    trim(data_get($model->trade->TradeOrganizer, 'FirstName', ''))
                )
                ?? null;
            };
        });
    }

    public function trade()
    {
        return $this->hasOne(Trade::class, ['TradeId', 'TradePlaceINN'], ['TradeId', 'TradePlaceINN']);
    }

    public function info()
    {
        return $this->hasOne(TradeLotInfo::class, 'trade_lot_id', 'id');
    }

    public function getPaymentInfoAttribute()
    {
        return $this->info->PaymentInfo ?? null;
    }

    public function setPaymentInfoAttribute($value)
    {
        if (empty($this->id)) {
            throw new \Exception('Before set PaymentInfo, save lot model');
        }
        $this->info()->updateOrCreate(['trade_lot_id' => $this->id], ['PaymentInfo' => $value]);
    }

    public function getSaleAgreementAttribute()
    {
        return $this->info->SaleAgreement ?? null;
    }

    public function setSaleAgreementAttribute($value)
    {
        if (empty($this->id)) {
            throw new \Exception('Before set SaleAgreement, save lot model');
        }
        $this->info()->updateOrCreate(['trade_lot_id' => $this->id], ['SaleAgreement' => $value]);
    }

    public function getPriceReductionAttribute()
    {
        return $this->info->PriceReduction ?? null;
    }

    public function setPriceReductionAttribute($value)
    {
        if (empty($this->id)) {
            throw new \Exception('Before set PriceReduction, save lot model');
        }
        $this->info()->updateOrCreate(['trade_lot_id' => $this->id], ['PriceReduction' => $value]);
    }

    public function getConcoursAttribute()
    {
        return $this->info->PriceReduction ?? null;
    }

    public function setConcoursAttribute($value)
    {
        if (empty($this->id)) {
            throw new \Exception('Before set Concours, save lot model');
        }
        $this->info()->updateOrCreate(['trade_lot_id' => $this->id], ['Concours' => $value]);
    }

    public function params()
    {
        return $this->hasMany(TradeLotParamValue::class);
    }

    public function images()
    {
        return $this->hasMany(TradeLotImage::class)
            ->orderBy('sort')
            ->orderBy('id');
    }

    public function categories()
    {
        return $this->belongsToMany(TradeLotCategory::class, 'trade_lot_trade_lot_category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|TradeLotClassificator[]
     */
    public function classificators()
    {
        return $this->belongsToMany(TradeLotClassificator::class);
    }

    public function favorite()
    {
        $user = Auth::user();
        return $this->hasMany(
            UserFavoriteLot::class,
            'trade_lot_id', 'id'
        )->where('user_id', $user ? $user->id : null);
    }

    public function getInFavoriteAttribute()
    {
        return !!$this->favorite->count();
    }

    public function scopeWhereLike($query, $column, $value)
    {
        return $query->where($column, 'like', '%' . $value . '%');
    }

    public function scopeOrWhereLike($query, $column, $value)
    {
        return $query->orWhere($column, 'like', '%' . $value . '%');
    }

    public function scopeCatalogSearch($query, $search_string)
    {
        if (empty($search_string)) {
            return $query;
        }

        $search_string_for_makers = '%' . $search_string . '%';

        $maikers = VehicleMaker::where('name', 'like', $search_string_for_makers)
            ->orWhereHas('variants', function($query) use($search_string_for_makers) {
            return $query->where('text', 'like', $search_string_for_makers);
        })->get();



        $special_maikers = VehicleSpecialMaker::where('name', 'like', $search_string_for_makers)
            ->orWhereHas('variants', function($query) use($search_string_for_makers) {
                return $query->where('text', 'like', $search_string_for_makers);
            })->get();


        $synonyms = VehicleMakerVariant::whereIn('vehicle_maker_id', $maikers->pluck('id'))->pluck('text');
        $special_synonyms = VehicleSpecialMakerVariant::whereIn('vehicle_special_maker_id',  $special_maikers->pluck('id'))->pluck('text');

        $models_trs = $maikers->pluck('name')->merge( $special_maikers->pluck('name') )->merge( $synonyms )->merge( $special_synonyms )->unique();

        if($models_trs->count()) {
            $sphinxIds = SphinxService::getSphinxSearchMulti('baids_lot_index', $models_trs->all(), 'id');
        } else {
            $sphinxIds = SphinxService::getSphinxSearch('baids_lot_index', '*'.$search_string.'*', 'id', '*');
        }
        if ($sphinxIds === false) {
            return $query
                ->whereLike('TradeObjectHtml', '%'.$search_string.'%')
                ->orWhereLike('debtor_name', '%'.$search_string.'%')
                ->orWhereLike('organizer_name', '%'.$search_string.'%');
        }

        //с внедрением поиска через Sphinx уже не требуется поиск по трем полям через whereLike
        return $query->whereIn('id', $sphinxIds);
    }

    public function scopeHasCategory($query, $category)
    {
        if ($category instanceof TradeLotCategory) {
            $ids = $category->id;
        } elseif ($category instanceof \Illuminate\Support\Collection) {
            $ids = $category->pluck('id')->toArray();
        } else {
            $ids = $category;
        }
        return $query->whereHas('categories', function ($query) use ($ids) {
            return $query->where('id', (is_array($ids) ? 'in' : '='), $ids);
        });
    }

    public function scopeFilterPrice($query, $from, $to = null)
    {
        if ($price_from = floatval(str_replace(',', null, $from))) {
            $query->where(function ($query) use ($price_from) {
                $query
                    ->whereNotNull('NewPrice')->where('NewPrice', '>=', $price_from)
                    ->orWhereNull('NewPrice')->where('StartPrice', '>=', $price_from);
            });
        }

        if ($price_to = floatval(str_replace(',', null, $to))) {
            $query->where(function ($query) use ($price_to) {
                $query
                    ->whereNotNull('NewPrice')->where('NewPrice', '<=', $price_to)
                    ->orWhereNull('NewPrice')->where('StartPrice', '<=', $price_to);
            });
        };

        return $query;
    }

    /**
     * Логика фильтрации лотов
     * @param $query
     * @param $filter_data
     * @return mixed
     */
    public function scopeCatalogFilter($query, $filter_data)
    {
        //dump($filter_data);
        if (!$filter_data) {
            return $query;
        }

        return $query->where(function (Builder $query) use ($filter_data) {            
            if (isset($filter_data['price'])) {
                $query->filterPrice(data_get($filter_data, 'price.from', null), data_get($filter_data, 'price.to', null));
            }

            if (isset($filter_data['debtor'])) {
                if ($debtor_name = data_get($filter_data, 'debtor', null)) {
                    $debtorIds = SphinxService::getSphinxSearchMulti('baids_lot_index', $debtor_name, 'id', 'debtor_name');

                    if ($debtorIds === false) {
                        $query->where('debtor_name', $debtor_name);
                    } else {
                        $query->whereIn('id', $debtorIds);
                    }
                };
            }

            if (isset($filter_data['auction_type']) || isset($filter_data['dates'])) {
                $query->whereHas('trade', function ($query) use ($filter_data) {
                    $query->timeBeginFilter(data_get($filter_data, 'dates.TimeBegin.from', null), data_get($filter_data, 'dates.TimeBegin.to', null));
                    $query->timeEndFilter(data_get($filter_data, 'dates.TimeEnd.from', null), data_get($filter_data, 'dates.TimeEnd.to', null));
                    $query->applicationTimeBeginFilter(data_get($filter_data, 'dates.ApplicationTimeBegin.from', null), data_get($filter_data, 'dates.ApplicationTimeBegin.to', null));
                    $query->applicationTimeEndFilter(data_get($filter_data, 'dates.ApplicationTimeEnd.from', null), data_get($filter_data, 'dates.ApplicationTimeEnd.to', null));

                    if (isset($filter_data['auction_type'])) {
                        $auction_types = data_get($filter_data, 'auction_type', array_keys(Trade::$auction_types_for_filter));
                        $query->whereIn('AuctionType', $auction_types);
                    }
                });

                if ($trade_organizer = data_get($filter_data, 'trade_organizer', null)) {            
                    $sphinxArbitrIds = SphinxService::getSphinxSearchMulti('baids_trades_index', $trade_organizer, 'id', 'arbitr_name');
                    $sphinxOrganizerIds = SphinxService::getSphinxSearchMulti('baids_lot_index', $trade_organizer, 'id', 'organizer_name');

                    if ($sphinxArbitrIds === false || $sphinxOrganizerIds === false) {
                        $query->where(function($subQuery) use ($trade_organizer) {
                            $subQuery->whereHas('trade', function ($query) use ($trade_organizer) { 
                                $query->whereIn('arbitr_name', $trade_organizer);
                            });
                        });
                    } else {
                        $query->where(function($subQuery) use ($sphinxArbitrIds, $sphinxOrganizerIds) {
                            $subQuery->whereHas('trade', function ($query) use ($sphinxArbitrIds) {
                                $query->whereIn('id', $sphinxArbitrIds);
                            });
                            $subQuery->orWhereIn('id', $sphinxOrganizerIds);
                        });
                    }
                };

                return $query;
            }

            if (isset($filter_data['debitor'])) {
                $search_string = $filter_data['debitor'];
                $sphinxIds = SphinxService::getSphinxSearchField('baids_lot_index', '*'.$search_string.'*', 'id', 'TradeObjectHtml');

                if ($sphinxIds === false) {
                    return $query->whereLike('TradeObjectHtml', "%$search_string%");
                }
                return $query->whereIn('id', $sphinxIds);
            }

            $filter_data = Arr::except($filter_data, ['price']);
            foreach ($filter_data as $key => $filter_item) {
                if (empty($filter_item)) {
                    continue;
                }
                $param = TradeLotParam::where('code', $key)->first();
                if (!$param) {
                    continue;
                }
                switch ($param->type) {
                    case 'checkbox':
                    case 'checkbox-vehicle-brands':
                    case 'checkbox-vehicle-models':
                        $query->whereHas('params', function ($query) use ($filter_item, $param) {
                            $query->where('trade_lot_param_id', $param->id);
                            if (is_array($filter_item)) {
                                $query->whereIn('value', $filter_item);
                            } else {
                                $query->where('value', $filter_item);
                            }
                        });
                        break;
                    case 'numeric-range':
                        if (!is_array($filter_item)) {
                            continue 2;
                        }
                        $query->when(!empty($filter_item['from']) || !empty($filter_item['to']), function ($query) use ($filter_item, $param) {
                            return $query->whereHas('params', function ($query) use ($filter_item, $param) {
                                $query->where('trade_lot_param_id', $param->id);

                                if (isset($filter_item['from'])) {
                                    $from = (int)$filter_item['from'];
                                    if ($from) {
                                        $query->where('value', '>=', $from);
                                    }
                                }

                                if (isset($filter_item['to'])) {
                                    $to = (int)$filter_item['to'];
                                    if ($to) {
                                        $query->where('value', '<=', $to);
                                    }
                                }

                            });
                        });
                        break;
                    default:
                        dump("Undefined filter type: {$param->type}, for code: {$param->code}", $filter_data);
                        break;
                }
            }
            return $query;
        });
    }

    public function scopeOnlyPublic($query)
    {
        return $query->where('published', 1);
        // @developer: сначало добавили фильтрацию по статусу, потом убрали
        //->whereNotIn('status', ['Finished', 'BiddingCanceled', 'BiddingFail']);
        // 54532 101654
    }

    public function scopeOnlyOpened($query)
    {
        return $query->whereNotIn('trade_lots.status', ['finished', 'BiddingCanceled', 'BiddingFail', 'ApplicationSessionEnd', 'BiddingPaused']);
    }

    public function scopeOnlyFinished($query)
    {
        return $query->whereIn('trade_lots.status', ['finished', 'BiddingCanceled', 'BiddingFail', 'ApplicationSessionEnd', 'BiddingPaused']);
    }

    public function scopeOrderByPrice($query, $orderType = 'DESC')
    {
        return $query->orderByRaw(
            "CASE WHEN `NewPrice` IS NULL THEN `StartPrice` ELSE `NewPrice` END {$orderType}"
        );
    }

    public function scopeOnlyHotLots($query, $last_days = 7)
    {
        return $query
            ->where('published', 1)
            ->whereHas('trade', function ($query) use ($last_days) {
                return $query
                    ->where('status', 'ApplicationSessionStarted')
                    ->where('ApplicationTimeBegin', '<', now())
                    ->where('ApplicationTimeEnd', '>', now())
                    ->where('ApplicationTimeEnd', '<', now()->addDays($last_days));
            });
    }

    public function scopeCompleted($query)
    {
        return $query->where('status', 'Finished');
    }

    public function scopeCatalogLocationFilter($query)
    {
        if (!$filter_data = CatalogLocation::getFromSession()) {
            return $query;
        }

        $locations = data_get($filter_data, 'locations', []);
        if (!$locations) {
            return $query;
        }

        $query->where(function ($query) use ($locations) {
            foreach ($locations as $location) {
                $query->orWhere(function ($query) use ($location) {
                    if ($location['region_with_type']) {
                        $query->where('region_with_type', $location['region_with_type']);
                    }
                    if ($location['city_with_type']) {
                        $query->where('city_with_type', $location['city_with_type']);
                    }
                });
            }
        });
        return $query;
    }

    /**
     * Информация о статусе
     * @return object
     */
    public function getStatusInfoAttribute()
    {
        if (!isset(Trade::$status_list[$this->status])) {
            return (object)Trade::$status_list['unknown'];
        }
        return (object)Trade::$status_list[$this->status];
    }

    /**
     * Заголовок лота
     * @param int $limit
     * @return string
     */
    public function getTitleAttribute($limit = 250)
    {
        return Str::ucfirst(Str::limit($this->LotTitle ?: $this->TradeObjectHtml, $limit ?: 210));
    }

    public function getDescriptionAttribute()
    {
        return $this->TradeObjectHtml;
    }

    public function getShortDescriptionAttribute($limit = null)
    {
        return Str::limit($this->TradeObjectHtml, $limit ?: 990);
    }

    /**
     * Текущая цена, руб.
     * @return float|null
     */
    public function getCurrentPriceAttribute()
    {
        return $this->NewPrice ?: $this->StartPrice;
    }

    /**
     * выводит название категории лота
     * @return string|null
     */
    public function getCategoryNameAttribute()
    {
        $category = $this->getFirstCategoryAttribute();
        return $category ? $category->title : null;
    }

    public function getFirstCategoryAttribute()
    {
        return $this->categories()
            ->orderBy('parent_id')
            ->orderBy('id')
            ->first();
    }

    public function getLastCategoryAttribute()
    {
        return $this->categories()
            ->orderBy('id', 'desc')
            ->orderBy('parent_id', 'desc')
            ->first();
    }

    public function getLastCategoryLotImgAttribute()
    {
        $category = $this->getLastCategoryAttribute();
        $other_category_img = '/img/other.jpeg';
        $no_img = '/img/box-noimg.svg';
        return $category ? ($category->lot_image_url ?: $no_img) : $other_category_img;
    }

    public function getWinnerTextAttribute()
    {
        if (!$this->SuccessTradeResult) {
            return 'Empty';
        }

        // WinnerPerson
        $winner = data_get($this->SuccessTradeResult, 'WinnerPerson.@attributes');
        if ($winner) {
            return sprintf("%s %s %s", $winner['FirstName'], $winner['LastName'], $winner['MiddleName']);
        }
        $winner = data_get($this->SuccessTradeResult, '0.WinnerPerson.@attributes');
        if ($winner) {
            return sprintf("%s %s %s", $winner['FirstName'], $winner['LastName'], $winner['MiddleName']);
        }

        // WinnerCompany
        $winner = data_get($this->SuccessTradeResult, 'WinnerCompany.@attributes');
        if ($winner) {
            return $winner['FullName'];
        }
        $winner = data_get($this->SuccessTradeResult, '0.WinnerCompany.@attributes');
        if ($winner) {
            return $winner['FullName'];
        }
        return 'Error';
    }

    /**
     * Обоснование итогового решения
     * @return mixed|string
     */
    public function getWinnerSubstantiationTextAttribute()
    {
        if (!$this->SuccessTradeResult) {
            return 'Empty';
        }
        $substantiation = data_get($this->SuccessTradeResult, 'Substantiation');
        return $substantiation;
    }

    public function getKadasterAttribute()
    {
        $param = $this->params()->whereHas('field', function ($query) {
            return $query->where('code', '=', 'kad_number');
        })->first();
        if (!$param) {
            return null;
        }
        return $param->value;
    }

    public function getParamsFillingAttribute()
    {
        return [
            'count' => TradeLotParam::where('published', '=', 1)
                ->whereHas('categories', function ($query) {
                    return $query->whereIn('id', $this->categories->pluck('id')
                    );
                })->count(),

            'fill' => $this->params()->whereHas('field', function ($query) {
                return $query->where('published', '=', 1);
            })->count(),
        ];
    }

    # actions

    /**
     * Начат прием заявок
     * @return $this
     */
    public function applicationSessionStart()
    {
        $this->status = 'ApplicationSessionStarted';
        return $this;
    }

    /**
     * Прием заявок закончен
     * @return $this
     */
    public function applicationSessionEnd()
    {
        $this->status = 'ApplicationSessionEnd';
        return $this;
    }

    /**
     * Отмена торгов
     * @param string $reason
     * @return $this
     */
    public function biddingCancel(string $reason)
    {
        $this->status = 'BiddingCanceled';
        $this->BiddingInProcess = false;
        $this->BiddingCancelReason = $reason;
        return $this;
    }

    public function biddingStart()
    {
        $this->status = 'BiddingInProcess';
        $this->BiddingInProcess = true;
        return $this;
    }

    public function biddingPause(string $reason)
    {
        $this->status = 'BiddingPaused';
        $this->BiddingPaused = true;
        return $this;
    }

    public function biddingResume(string $reason)
    {
        $this->status = $this->status_log[count($this->status_log) - 2];
        $this->BiddingPaused = false;
        return $this;
    }

    public function biddingEnd()
    {
        $this->status = 'Finished';
        $this->BiddingInProcess = false;
        return $this;
    }

    public function biddingFail(string $reason)
    {
        $this->status = 'BiddingFail';
        $this->BiddingInProcess = false;
        $this->BiddingFailReason = $reason;
        return $this;
    }

    public function biddingResult()
    {
        $this->status = 'Finished';
        return $this;
    }

    public function inFinalStatus()
    {
        return in_array($this->status, [
            'BiddingCanceled', // Отмена торгов
            'BiddingFail', // Торги не состоялись
            'Finished', // Торги завершены
        ]);
    }

    /**
     * Завршает тогри по данному лоту
     * @return bool
     */
    public function finished()
    {
        $this->status = 'Finished';
        return $this->save();
    }

    public function parseParameters($debug = false)
    {
        $this->params()->delete();
        $content = $this->TradeObjectHtml;
        $params_arr = [];

        $params = TradeLotParam::whereHas('categories', function ($query) {
            return $query->whereIn('id', $this->categories()->pluck('id'));
        })->get();

        if ($debug) {
            dump('Классификаторы: ' . $this->classificators->pluck('text')->join(', '));
            dump('Категории: ' . $this->categories->pluck('title')->join(', '));
            dump('Параметры: ' . $params->pluck('title')->join(', '));
        }

        // 3 - Спецтехника
        $categories_ids = $this->categories()->pluck('id');
        $categories_parent_ids = $this->categories()->pluck('parent_id');
        $is_spec_vehicle = in_array(3, $categories_ids->toArray()) || in_array(3, $categories_parent_ids->toArray());

        foreach ($params as $param) {
            if (key_exists($param->code, $params_arr)) {
                continue;
            }
            $options = (array)$param->options;

            if (in_array('replace_table', $options) && !empty($param->replace_table)) {
                $replace_table_raw = explode("\n", $param->replace_table);
                $replace_table = [];
                foreach ($replace_table_raw as $item) {
                    $item = explode('=', $item);
                    $replace_table[$item[0]] = $item[1];
                }
            }

            // brand list
            $class_model_name = $is_spec_vehicle ? VehicleSpecialModel::class : VehicleModel::class;
            $class_model_variant_name = $is_spec_vehicle ? VehicleSpecialModelVariant::class : VehicleModelVariant::class;
            $class_maker_name = $is_spec_vehicle ? VehicleSpecialMaker::class : VehicleMaker::class;
            $class_maker_variant_name = $is_spec_vehicle ? VehicleSpecialMakerVariant::class : VehicleMakerVariant::class;

            $brands = null;
            if (in_array('methodVehicleMaker', $options)) {
                $class_maker_name::select('name')
                    ->distinct()
                    ->orderByRaw('CHAR_LENGTH(name) DESC')
                    ->get()->chunk(100)
                    ->each(function ($brands) use (&$param, &$text) {
                        $text = [];
                        foreach ($brands as $brand) {
                            $text[] = $brand->name;
                            $text = array_merge($text, $brand->variants()->pluck('text')->toArray());
                        }
                        $text = implode('|', $text);
                        $text = str_replace(['/'], [' '], $text);
                        $param->regulars->push((object)[
                            'id' => '0',
                            'regex' => "/(?P<car_brand>({$text}))/iu",
                        ]);
                    });
            }

            $models = null;
            if (in_array('methodVehicleModel', $options) && isset($params_arr['car_brand'])) {
                $car_brand = $params_arr['car_brand']['value'];

                $models = $class_model_name::whereHas('maker', function ($query) use ($car_brand) {
                    return $query->where('name', $car_brand);
                })->select('name')
                    ->distinct()
                    ->orderByRaw('CHAR_LENGTH(name) DESC')
                    ->get();

                $models->chunk(100)->each(function ($models) use (&$param) {
                    $text = [];
                    foreach ($models as $model) {
                        $text[] = $model->name;
                        $text = array_merge($text, $model->variants()->pluck('text')->toArray());
                    }
                    $text = implode('|', $text);
                    $text = str_replace(['/'], [' '], $text);
                    $param->regulars->push((object)[
                        'id' => '0',
                        'regex' => "/(?P<car_model>({$text}))/iu",
                    ]);
                });
            }

            foreach ($param->regulars as $regular_model) {

                $result = $this->detectParameters_regex($regular_model->regex, $content, $regular_model->id);

                if ($result !== false) {
                    $result[$param->code]['_title'] = $param->title;
                    if (in_array('methodVehicleMaker', $options)) {
                        $maker_variant = $class_maker_variant_name::searchText($result[$param->code]['value'])->first();
                        if ($maker_variant) {
                            $result[$param->code]['value'] = $maker_variant->maker->name;
                        }
                    }
                    if (in_array('methodVehicleModel', $options) && isset($result['car_brand'])) {
                        $model_variant = $class_model_variant_name::whereHas('model.maker', $result['car_brand']['value'])
                            ->searchText($result[$param->code]['value'])->first();
                        if ($model_variant) {
                            $result[$param->code]['value'] = $model_variant->model->name;
                        }
                    }
                    if (in_array('clear_quota', $options)) {
                        $result[$param->code]['value'] = str_replace(["\"", "'"], null, $result[$param->code]['value']);
                    }
                    if (in_array('mb_strtoupper', $options)) {
                        $result[$param->code]['value'] = mb_strtoupper($result[$param->code]['value']);
                    }
                    if (in_array('mb_strtolower', $options)) {
                        $result[$param->code]['value'] = mb_strtolower($result[$param->code]['value']);
                    }
                    // replace_table
                    if (isset($replace_table) && key_exists($result[$param->code]['value'], $replace_table)) {
                        $result[$param->code]['value'] = $replace_table[$result[$param->code]['value']];
                    }

                    // types
                    if (in_array($param->type, ['numeric-range', 'numeric'])) {
                        $val = preg_replace("/[^\d,\.]/ui", null, $result[$param->code]['value']);
                        $val = floatval($val);
                        $result[$param->code]['value'] = mb_strtolower($val);
                    }

                    $params_arr = array_merge($params_arr, $result);
                    break;
                }
            }
        }
        if (empty($params_arr)) {
            Log::channel('lotcategory')->info('No params detected for id: ' . $this->id);
        }
                
        $this->setParams($params_arr);
        return $params_arr;
    }

    /**
     * парсинг текстов лота и запись параметров
     * @deprecated use parseParameters()
     */
    public function detectParameters()
    {
        // clear params of lot
        $this->params()->delete();

        $title = $this->TradeObjectHtml;
        $params_arr = [];

        # адрес
        if ($params = $this->detectParameter_location($title)) {
            $params_arr = array_merge($params_arr, $params);
        };

        # недвижимость
        $categories = $this->categories()->with('params')
            ->whereHas('params', function (Builder $query) {
                $query->where('code', 'realty_meters');
            })
            ->get();
        if ($categories->isNotEmpty()) {
            $regex_arr = [ // param > reqex list
                [ // realty_type
                    401 => '/(?P<realty_type>квартира|нежилое помещение|земельный участок|нежилое сооружение|нежилое здание|дом|коттедж|гараж|машиноместо|Коммерческая недвижимость)/iu',
                ],
                [ // realty_meters
                    // пл.198,6 кв.м
                    301 => '/(площадью|площадь(:)?|площадь: общая|квартира,|пл\.)\s?(-)?(?P<realty_meters>[\d\.,]+) (кв\.(\s)?м|м\.(\s)?кв|м\.2|\()/iu',
                    302 => '/(-|квартира|дом|комната|Картира|помещение) (?P<realty_meters>[\d\.,]+) кв\.(\s)?м/iu',
                ],
            ];
            foreach ($regex_arr as $reqex_list) {
                foreach ($reqex_list as $make => $reqex) {
                    $params = $this->detectParameters_regex($reqex, $title, $make);
                    if ($params !== false) {
                        if (isset($params['realty_type'])) {
                            $params['realty_type']['value'] = mb_strtolower($params['realty_type']['value']);
                        }
                        $params_arr = array_merge($params_arr, $params);
                        break;
                    }
                }
            }
        } else {
            Log::channel('lotcategory')->info('Categories list is empty for id: ' . $this->id);
        }


        # автомобили
        $categories = $this->categories()->with('params')
            ->whereHas('params', function (Builder $query) {
                $query->whereIn('code', ['car_brand', 'car_model', 'car_year', 'car_type', 'car_body_type']);
            })
            ->get();
        if ($categories->isNotEmpty()) {

            $brands = VehicleMaker::all()->pluck('name');
            $brands = $brands->merge(VehicleMakerVariant::all()->pluck('text'));
            $brands = implode('|', $brands->toArray());

            $regex_arr = [ // param > reqex list
                [ // car_kpp
                    701 => '/(?P<car_kpp>АКПП|МКПП)/iu',
                ],
                [ // car_type
                    501 => '/(?P<car_type>легковой|Грузовой фургон|грузовой|автобус|автокран|прицеп|трактор)[^\w\d]/iu',
                ],
                [ // car_body_type
                    601 => '/(?P<car_body_type>универсал|седан|кроссовер|внедорожник|хетчбек|родстер|пикап|фургон|купе|суперкар|кабриолет)[^\w\d]/iu',
                ],
                [ // car_brand, car_model
                    201 => '/автомобиль, марка: (?P<car_brand>[^,]+), модель: (?P<car_model>[^,]{2,20}+),/iu',
                    202 => '/(легковой автомобиль|автомобиль марки|автомобиль) (?P<car_brand>[^,\s]+) (?P<car_model>[^,]{2,20}+), ([1-2]{1}[0-9]{3})/iu',
                    203 => '/(?P<car_brand>Камаз|LADA|ВАЗ|ЛиАЗ|ГАЗ|УАЗ|МАЗ)[\s\-](\-)?(?P<car_model>[0-9]{3,6}(-[0-9]{2})?)/iu',
                    204 => '/(?P<car_brand>ЗИЛ)[\s\-](?P<car_model>[0-9]{3,6})/iu',
                    205 => '/(автомобиль легковой|автомобиль|автобус) (?P<car_brand>[^,\s]+)(\s(?P<car_model>[^,]{2,20}+))?, регистрационный знак/iu',
                    206 => '/(А\/М) (?P<car_brand>[^,\s]+) (?P<car_model>[^,]{2,20}+)/iu',
                    207 => '/автомобиль (?P<car_brand>(ГАЗ))[\s\-,]{0,2}(?P<car_model>\w{1}[0-9]{1,3}\w{1}[0-9]{1,3})/iu',
                    208 => '/автомобиль (?P<car_brand>[^,\s]+) (?P<car_model>[^,]{2,20}+), VIN/iu',
                    209 => '/(?P<car_brand>(' . $brands . '))[,]? (?P<car_model>[^,\s]{2,20}+)/iu',
                ],
                [ // car_year
                    101 => '/(г\.в\.|год изготовления:|год выпуска|г\/в) (?P<car_year>[1-2]{1}[0-9]{3})[\s\.,]/iu',
                    102 => '/\s(?P<car_year>[1-2]{1}[0-9]{3})\s(г\.в\.|года выпуска|)/iu',
                    103 => '/\s(?P<car_year>[1-2]{1}[0-9]{3})(г\.в\.|года выпуска)/iu',
                    104 => '/(?P<car_year>[1-2]{1}[0-9]{3})(гв[\.,](птс)?)/iu',
                ],
            ];
            foreach ($regex_arr as $reqex_list) {
                foreach ($reqex_list as $make => $reqex) {
                    $params = $this->detectParameters_regex($reqex, $title, $make);
                    if ($params !== false) {
                        // replace car maker name (car_brand)
                        if (isset($params['car_brand'])) {
                            $maker_variant = VehicleMakerVariant::searchText($params['car_brand']['value'])->first();
                            if ($maker_variant) {
                                $params['car_brand']['value'] = $maker_variant->maker->name;
                            }
                        }
                        /*if (isset($params['car_body_type']) && !isset($params['car_type'])) {
                            $params['car_type']['value'] = 'легковой';
                        }*/
                        $params_arr = array_merge($params_arr, $params);
                        break;
                    }
                }
            }
        } else {
            Log::channel('lotcategory')->info('Categories list is empty for id: ' . $this->id);
        }

        // save
        $this->setParams($params_arr);
        return false;
    }

    /**
     * парсим адрес
     * @param $content
     * @return array|null
     * @deprecated
     */
    private function detectParameter_location($content)
    {
        // адресу: Ростовская область, Константиновский район, ст. Николаевская, ул. Пролетарская, д. 14.
        $categories = $this->categories()->with('params')
            ->whereHas('params', function (Builder $query) {
                $query->where('code', 'address');
            })
            ->get();
        if ($categories->isEmpty()) {
            return null;
        }

        $params_arr = [];
        $regex_arr = [ // param > reqex list
            [ // address
                801 => '/(адрес[у]?[:]?)\s?(?P<address>([\w\-\s]+ (область|обл\.|край), )?[\w\-\s\.,]+, (улица|ул|пер|пр|ш|мкр)[\.]? [\w\-\s\.]+, (дом|д)\.?\s?[№\d\/-]+([А-Д]{1})?)/iu',
                802 => '/(?P<address>((р\-н) ([А-я\s\-]+), |([А-я\s\-]+\s(район),\s))?(город|г|ст|посёлок|пос|село|деревня|дер|д|с)\. ([\w\-\s]+)([А-я\s\-]+ район)?, (улица|ул|пер|пр|ш|мкр)[\.]? [А-я\d\-\s\.]+,\s?((дом|д)\.?)?\s?[№\d\/-]+([А-Д]{1})?)/iu',
                803 => '/адрес: (?P<address>([А-я\s\-]+ Республика,) ([А-я\s\-]+ район,) ([^;:,]+,) уч\. ([\d]+))/iu',
            ],
        ];
        foreach ($regex_arr as $reqex_list) {
            foreach ($reqex_list as $make => $reqex) {
                $params = $this->detectParameters_regex($reqex, $content, $make);
                if ($params !== false) {
                    $params_arr = array_merge($params_arr, $params);
                    break;
                } else {
                    Log::channel('lotcategory')->info('Error in function detectParameter_location for id: ' . $this->id);
                }
            }
        }
        return $params_arr;
    }

    /**
     * Поиск параметков по массиву регулярок
     * '/автомобиль, марка: (?P<car_brand>[^,]+), модель: (?P<car_model>[^,]+),/iu'
     * - car_brand, car_model - являются кодами TradeLotParam
     * @param $regex
     * @param string $text
     * @param null $make
     * @return array|bool
     */
    private function detectParameters_regex($regex, string $text, $make = null)
    {
        $result = [];
        if (preg_match($regex, $text, $matches)) {
            foreach ($matches as $code => $value) {
                if (is_numeric($code)) {
                    continue;
                }
                $param = TradeLotParam::where(compact('code'))->firstOrFail();
                switch ($param->type) {
                    case 'numeric':
                        $value = floatval(str_replace(',', '.', $value));
                        break;
                }
                $result[$code] = [
                    'value' => trim($value),
                    'mark' => $make,
                ];
            }
        }
        return empty($result) ? false : $result;
    }

    private function setParams($params_arr)
    {
        $categories = $this->categories()->with('params')
            ->whereHas('params', function (Builder $query) use ($params_arr) {
                $query->whereIn('code', array_keys($params_arr));
            })
            ->get();

        foreach ($categories as $category) {
            foreach ($params_arr as $key => $fields) {
                if (!is_array($fields)) {
                    $fields = ['value' => $fields];
                }

                $fields = array_map(function ($item) use ($key) {
                    $item = str_replace(['«', '»', '"'], null, $item);

                    // replace car model name (car_model)
                    if ($key === 'car_model') {
                        $model_variant = VehicleModel::searchText($item)->first();
                        if ($model_variant) {
                            $item = $model_variant->name;
                        }
                    }

                    return $item;
                }, $fields);

                $param = $category->params()->where('code', $key)->first();
                if (!$param) {
                    continue;
                }
                $this->params()->updateOrCreate(['trade_lot_param_id' => $param->id], $fields);
            }
        }
    }

    /**
     * Определить категории и прявязать лот
     */
    public function detectCategories()
    {
        if (!empty($this->updated_user_id)) {
            Log::channel('lotcategory')->info('Updated user id is not empty for id: ' . $this->id);
            return;
        }
        // todo: оптимизировать
        // get categories by classificators
        $categories = TradeLotCategory::with('regulars')
            ->whereHas('classifications', function ($query) {
                return $query->whereIn('code', $this->Classification);
            })->orderBy('id')->get()->keyBy('id');

        //dump($categories->pluck('id')->toJson());

        $categories_for_del = [];
        foreach ($categories as $category) {
            if (!$category->regulars->count()) {
                Log::channel('lotcategory')->info('No regulars in category id ' . $category->id . '(' . $category->slug .')' .' when parsing for id: ' . $this->id);
                continue;
            }
            foreach ($category->regulars as $regular) {
                try {
                    $boundariesDefault = ['(',')'];
                    $boundariesUpdated = ['\b(',')\b'];
                    $regularFixed = str_replace($boundariesDefault, $boundariesUpdated, $regular->regex);

                    if (preg_match($regularFixed, $this->TradeObjectHtml, $m)) {
                        // options
                        if (is_array($category->options)) {
                            if (in_array('only_self_lot', $category->options) && count($categories_for_del)) {
                                $categories_for_del = $categories
                                    ->where('id', '!=', $category->id)
                                    ->pluck('id')->toArray();
                            }
                        }
                        continue(2);
                    }
                } catch (\Throwable $err) {
                    Log::channel('lotcategory')->info('Category detect error for id: ' . $this->id . ' >>> regular->id.'.$regular->id.' ' . $err->getMessage());
                    Log::warning($err->getMessage(), ['$category->regulars[]->id' => $regular->id]);
                }
            }
            $categories_for_del[] = $category->id;
        }
        //dump($categories_for_del);
        $categories->forget($categories_for_del);
        //dump($categories->pluck('id')->toJson());
        if (empty($categories) && empty($this->categories())) {            
            Log::channel('lotcategory')->info('Categories not found for id: ' . $this->id);
        }

        // attach lot to categories
        $this->categories()->sync($categories);
    }

    public function updateAddressByDadata($cached = true)
    {
        $address_param_id = 1;
        $address = $this->params()->where('trade_lot_param_id', $address_param_id)->first();
        $save_address_param = true;
        if (!$address || empty($address->value)) {
            if (empty($this->dadata_suggest) && $this->trade->debtor) {
                $debtor = $this->trade->debtor;
                $address_str = $debtor->Address ?? $debtor->LegalAddress;
                $save_address_param = false;
            } else {
                return null;
            }
        } else {
            $address_str = $address->value;
        }

        if (!$cached || empty($this->dadata_suggest)) {
            try {
                $result = DadataSuggest::suggest("address", ["query" => $address_str, "count" => 2]);
                if (count($result) !== 3) {
                    $result = $result[0];
                }
                $this->dadata_suggest = $result;

            } catch (\Throwable $err) {
                Log::channel('lotcategory')->info('No Dadata address "' . $address_str . '" for id: ' . $this->id . ' >>> ' . $err->getMessage());

                Log::warning($err->getMessage(), ['from address' => $address_str]);
                return false;
            }
        } else {
            $result = $this->dadata_suggest;
        }

        if ($save_address_param) {
            $address->value = $result['unrestricted_value'];
            $address->save();
        }


        $this->fill(Arr::only($result['data'], ['postal_code', 'federal_district', 'region_with_type', 'area_with_type']));
        $this->city_with_type = $result['data']['city_with_type'] ?: $result['data']['settlement_with_type'];
        $this->dadata_suggest = $result;
        return $this->save();
    }

    /**
     * После добавдления лота
     */
    public function detectAllProps()
    {
        $this->detectCategories();
        if (empty($this->categories())) {
            Log::channel('lotcategory')->info('detectAllProps-detectCategories no result for id: ' . $this->id);
        }
        //$this->detectParameters();
        $this->parseParameters();
        if (empty($this->params())) {
            Log::channel('lotcategory')->info('detectAllProps-parseParameters no result for id: ' . $this->id);
        }
        $this->refresh();
        $this->updateAddressByDadata();
    }

    public function firstImage()
    {
        if ($image = $this->images->first()) {
            return Storage::disk('public')->url($image->file);
        };
        return null;
    }

    public function firstImagesArray($count = 1)
    {
        $images = $this->images
            ->sortBy('sort')
            ->sortBy('id')
            ->take($count);
        return $images->toArray();
    }

    public function latLngArray()
    {
        $data = $this->dadata_suggest;
        if (empty($data) || $data['data']['geo_lat'] === null) {
            return null;
        }
        if ($this->params()->where('trade_lot_param_id', 1)->count() < 1) {
            return null;
        }
        return [(float)$data['data']['geo_lat'], (float)$data['data']['geo_lon']];
    }

    public function similar()
    {
        $category_ids = $this->categories->pluck('id');
        return self::whereHas('categories', function ($query) use ($category_ids) {
            return $query->whereIn('id', $category_ids);
        });
    }

    public function getOptionsForMap()
    {
        $result = [];
        if ($coordinates = $this->latLngArray()) {
            $check_view_categories = $this->categories()->where('options', 'like', '%view_map%')->count();
            if ($check_view_categories > 0) {
                list($lat, $lng) = $coordinates;
                $result['coordinates'] = compact('lat', 'lng');
            }
        };
        return $result;
    }

    /**
     * Сохраняет фотографии лота
     * @param $image
     * @return string|bool
     */
    public function saveFileImage($image)
    {
        $path = Storage::disk('public')->put("lot-images/{$this->id}", $image);
        $this->images()->save(new TradeLotImage(['file' => $path]));
        return $path;
    }

    /**
     * Опрятные лоты
     * @param $query
     * @return mixed
     */
    public function scopeNeatLots($query)
    {
        return $query;//->whereHas('categories');
    }

    public function scopeByUserMailings($query, $mailings, $only_new = false)
    {
        if (!$mailings || $mailings->count() === 0) {
            return $query->where('id', -1);
        }
        if ($mailings instanceof UserMailingLot) {
            /** @var UserMailingLot[]|\Illuminate\Support\Collection $mailings */
            $mailings = \Illuminate\Support\Collection::make([$mailings]);
        }
        foreach ($mailings as $mailing) {
            $query->orWhere(function ($query) use ($mailing, $only_new) {
                return $query
                    ->when($only_new, function ($query) use ($mailing) {
                        if (empty($mailing->last_report)) {
                            return $query;
                        }
                        return $query->where('created_at', '>', Carbon::parse($mailing->last_report));
                    })
                    ->when($mailing->price_from !== null, function ($query) use ($mailing) {
                        return $query->where('StartPrice', '>', $mailing->price_from);
                    })
                    ->when($mailing->price_to !== null, function ($query) use ($mailing) {
                        return $query->where('StartPrice', '<', $mailing->price_to);
                    })
                    ->hasCategory($mailing->category_id)
                    ->where(function ($query) use ($mailing) {
                        foreach ($mailing->filters()->get()->groupBy('trade_lot_param_id') as $trade_lot_param_id => $filters) {
                            $regionFilterQuery = TradeLotParam::where('code','region_with_type')->first();
                            $regionFilterQuery->id = $trade_lot_param_id;
                            if ($regionFilterQuery->id) {
                                $searchRegions = $filters->where('trade_lot_param_id', $regionFilterQuery->id)->pluck('value');
                                $query->whereIn('region_with_type', $searchRegions);
                            } else {   
                                $query->whereHas('params', function ($query) use ($trade_lot_param_id, $filters) {
                                    $query->where(function ($query) use ($trade_lot_param_id, $filters) {
                                        return $query
                                            ->where('trade_lot_param_id', $trade_lot_param_id)
                                            ->where(function ($query) use ($filters) {
                                                $filter = $filters->first();
                                                return $query
                                                    ->when($filter->param->type === 'numeric-range', function ($query) use ($filter) {
                                                        if ($filter->value_from !== null) {
                                                            $query->where('value', '>=', $filter->value_from);
                                                        }
                                                        if ($filter->value_to !== null) {
                                                            $query->where('value', '<=', $filter->value_to);
                                                        }
                                                        return $query;
                                                    }, function ($query) use ($filters) {
                                                        return $query->whereIn('value', $filters->pluck('value'));
                                                    });
                                            });
                                    });
                                });
                            }
                        
                        }
                        return $query;
                    });
            });
        }

        // @todo: debug
        /*$query->dump();
        dump('Count: ' . $query->count());
        $lots = Collection::make();
        foreach ($query->limit(100)->get() as $lot) {
            $lots->push([
                'id' => $lot->id,
                'price' => json_encode([$lot->StartPrice, $lot->CurrentPrice]),
                'categories' => $lot->categories()->get()->pluck('title')->toJson(JSON_UNESCAPED_UNICODE),
                'params' => $lot->params()->get()->map(function ($param) {
                    return json_encode([
                        'id' => $param->trade_lot_param_id,
                        'title' => $param->field->title,
                        'value' => Str::limit($param->value, 40),
                    ], JSON_UNESCAPED_UNICODE);
                })->toArray(),
            ]);
        }
        $lots->dd();*/

        return $query;
    }

    /**
     * @param $query
     * @param TradeLotCategory|int $category
     * @param $with_children
     * @return mixed
     */
    public function scopeForCategory($query, $category, bool $with_children)
    {
        $ids = [];
        $ids[] = $category instanceof TradeLotCategory ? $category->id : $category;
        if ($with_children) {
            if (!$category instanceof TradeLotCategory) {
                $category = TradeLotCategory::find($category);
            }
            $ids = array_merge($ids, $category->children()->get()->pluck('id')->toArray());
        }

        return $query->whereHas('categories', function ($query) use ($ids) {
            return $query->whereIn('id', $ids);
        });
    }

    public function seo()
    {
        return $this->morphOne('App\Models\SeoConfiguration', 'object');
    }

    public function testSignDocs()
    {
        ProcessSignAndSendOrder::dispatch($this);
    }
}

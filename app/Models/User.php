<?php

namespace App\Models;

use App\Events\PanelEvent;
use App\Models\Service;
use App\Traits\firePanelModelEvent;
use App\Traits\HasRoles;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use MongoDB\Driver\Query;

/**
 * @property int id
 * @property string name
 * @property string middle_name
 * @property string last_name
 * @property string email
 * @property string avatar
 * @property string password
 * @property string phone
 * @property string appointment
 * @property string department
 * @property array|mixed roles
 * @property Collection|UserFavoriteLot[] favorite_lots
 * @property Collection|UserMailingLot[] mailings
 * @property Collection|UserMailingLot[] active_mailings
 * @property UserOrgTrade|null org_profile
 */
class User extends Authenticatable
{
    use Notifiable, HasRoles, SoftDeletes, firePanelModelEvent;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'middle_name', 'phone', 'email', 'password',
    ];

    protected $appends = [
        'url_avatar', 'full_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'avatar'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'roles' => 'array',
    ];

    public function attributes()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
            'password' => 'Пароль',
        ];
    }

    /**
     * Email to lowercase
     * @param $value
     */
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function setPhoneAttribute($value)
    {
        $this->attributes['phone'] = preg_replace('/[^0-9]/', '', $value);
    }

    public function setRolesAttribute($value)
    {
        if (is_array($value) && count($value) === 0) {
            $this->attributes['roles'] = null;
        } else {
            $this->attributes['roles'] = json_encode($value);
        }
    }

    public function getUrlAvatarAttribute()
    {
        if (strpos($this->avatar, 'https://') === 0) {
            return $this->avatar;
        }
        return !empty($this->avatar) ? Storage::url($this->avatar) : null;
    }

    public function getFullNameAttribute()
    {
        return implode(' ', [$this->last_name, $this->name, $this->middle_name]);
    }

    public function getTimezoneAttribute()
    {
        // todo: fix me
        return 'Asia/Yekaterinburg';
    }

    public function org_profile()
    {
        return $this->hasOne(UserOrgTrade::class);
    }

    public function notifications()
    {
        return $this->hasMany(UserNotification::class);
    }

    public function notifications_with_system()
    {
        $user = Auth::user();
        return $this->hasMany(UserNotification::class)->where(function ($query) use ($user) {
            return $query->where('user_id', '=', $user->id)->orWhereNull('user_id');
        });
    }

    public function payments()
    {
        return $this->hasMany(UserPayment::class, 'user_id');
    }

    public function lots()
    {
        return $this->hasMany(TradeLot::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function active_orders()
    {
        return $this->hasMany(Order::class)
            ->whereDate('service_begin', '<=', now())
            ->whereDate('service_end', '>=', now())->whereHas('payment', function($query){
                return $query->where('status', UserPayment::STATUS_SUCCESS);
            });
    }

    public function favorite_lots()
    {
        return $this->belongsToMany(TradeLot::class, 'user_favorite_lots');
    }

    public function mailings()
    {
        return $this->hasMany(UserMailingLot::class);
    }

    public function active_mailings()
    {
        return $this->hasMany(UserMailingLot::class)
            ->whereHas('active_order');
    }

    /**
     * Информация об организаторе торгов
     * @return array
     */
    public function getArbitrDataArray()
    {
        if (!$this->org_profile) {
            return [];
        }
        return array_merge($this->org_profile->toArray(), [
            'email' => $this->email,
            'phone' => $this->phone
        ]);
    }

    /**
     * Информация, оплачен ли тариф у пользователя
     * @return bool
     */
    public function isTariffActive()
    {
        //тарифные планы на 1/3/12 месяцев
        $tariffServicesIds = Service::TARIFF_IDS;

        return $this->active_orders()->whereIn('service_id', $tariffServicesIds)->count();
    }

    /**
     * Информация, оплачен ли тариф у пользователя
     * @return bool
     */
    public function isTrialMailingUnused()
    {
        $mailingService = Service::where('title', 'Подбор ликвидных предложений (рассылка лотов)')->first();
        $trialServiceIds = $this->orders()->where('service_id', $mailingService->id)->where('price', 0)->count();

        return !$trialServiceIds;
    }
}


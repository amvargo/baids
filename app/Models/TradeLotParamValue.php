<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property TradeLotParam field
 */
class TradeLotParamValue extends Model
{
    public $timestamps = false;

    protected $fillable = ['trade_lot_param_id', 'value', 'mark'];

    public function field()
    {
        return $this->belongsTo(TradeLotParam::class, 'trade_lot_param_id');
    }

    public function lots()
    {
        return $this->hasMany(TradeLot::class, 'id', 'trade_lot_id');
    }

    public function getTitleAttribute()
    {
        if (!$this->field) {
            return 'Undefined';
        }
        return $this->field()->first()->title;
    }
}

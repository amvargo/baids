<?php

namespace App\Models;

use App\Models\FedResurs\Message;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * @property mixed CaseNumber
 */
class TradeLegalCase extends Model
{
    protected $fillable = [
        'CaseNumber', 'TradeId', 'CourtName', 'Base'
    ];

    public function trade()
    {
        return $this->belongsTo(Trade::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'CaseNumber', 'CaseNumber');
    }

}

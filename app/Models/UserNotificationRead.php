<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

/**
 * @property int id
 * @property int|null user_id
 * @property int|null user_notification_id
 * @property bool is_read
 */
class UserNotificationRead extends Model
{
    use SoftDeletes;

    protected $casts = [
        'is_read' => 'boolean'
    ];

    protected $fillable = ['user_id'];

}

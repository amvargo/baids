<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Traits\modelHasImage;
use App\Traits\modelHasImage2;
use App\Traits\PanelModelRights;

class News extends Model
{
    use SoftDeletes;
    use modelHasImage, modelHasImage2;
    use PanelModelRights;

    protected $fillable = ['title', 'description', 'published'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $appends = [
        'url_image',
    ];

    public static function getNews() {
        $news = News::where('published', 1)->whereNotNull('image')->get();
        $sorted = $news->sortByDesc('created_at');
        return $sorted;
    }

    public function getUrlImageAttribute()
    {
        if (strpos($this->image, 'https://') === 0) {
            return $this->image;
        }
        return !empty($this->image) ? Storage::url($this->image) : null;
    }
}
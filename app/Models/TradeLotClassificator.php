<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TradeLotClassificator extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|TradeLotCategory[]
     */
    public function categories()
    {
        return $this->belongsToMany(
            TradeLotCategory::class,
            'trade_lot_category_trade_lot_classificator',
            'trade_lot_category_id',
            'trade_lot_classificator_id'
        );
    }

}

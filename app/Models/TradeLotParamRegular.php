<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string type
 * @property string code
 * @property string title
 */
class TradeLotParamRegular extends Model
{
    protected $fillable = ['regex', 'trade_lot_param_id'];
    public $timestamps = false;

}

<?php

namespace App\Models;

use App\Traits\firePanelModelEvent;
use App\Traits\UnitellerTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use \App\Models\UserNotification;

/**
 * @property int id
 * @property int|null user_id
 * @property string title
 * @property string status
 * @property float amount
 * @property array|null info
 */
class UserPayment extends Model
{
    use Notifiable, firePanelModelEvent, UnitellerTrait;

    const
        STATUS_WAITING = 'waiting',
        STATUS_SUCCESS = 'success',
        STATUS_FAIL = 'fail';

    public static $status_names = [
        self::STATUS_WAITING => ['Ожидание оплаты', 'info'],
        self::STATUS_SUCCESS => ['Оплачено успешно', 'success'],
        self::STATUS_FAIL => ['Платеж не выполнен', 'danger'],
    ];

    public $un_status_waiting = 'waiting';
    public $un_status_canceled = 'fail';
    public $un_status_paid = 'success';
    public $un_status_authorized = 'success';
    public $un_status_not_authorized = 'waiting';

    protected $fillable = ['title', 'amount', 'status', 'info', 'user_id', 'service_id', 'order_id'];

    protected $casts = [
        'info' => 'array'
    ];

    protected $appends = [
        'status_name', 'status_color'
    ];

    public function getStatusNameAttribute()
    {
        return isset(self::$status_names[$this->status])
            ? self::$status_names[$this->status][0]
            : $this->status;
    }

    public function getStatusColorAttribute()
    {
        return isset(self::$status_names[$this->status])
            ? self::$status_names[$this->status][1]
            : null;
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id')->withTrashed();
    }


    public function setNotification(){

        if( ($this->wasChanged('status'))) {

            $service = $this->service;
            $title = "Изменение статуса оплаты услуги {$service->title}";
            $body = "Статус оплаты услуги «{$service->title}» изменен на «{$this->status_name}»";

            UserNotification::setNotification($title, $body, $this->user_id);
        }
    }


    public function scopeSuccessPayment($query) {
        return $query->where('status', 'success');
    }
    public function scopeId($query, $id) {
        return $query->where('id', $id);
    }

    protected static function boot()
    {
        parent::boot();
        static::updated(function ($payment) {
            $payment->setNotification();
        });
    }

}

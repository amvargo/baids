<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TradeLotInfo extends Model
{
    public $timestamps = false;

    protected $table = 'trade_lot_info';

    protected $fillable = ['trade_lot_id', 'SaleAgreement', 'PaymentInfo', 'PriceReduction', 'Concours', 'PriceReductionAuction'];

    public function trade_lot()
    {
        return $this->belongsTo(TradeLot::class);
    }


    public function getPriceReductionAuctionRowsAttribute() {

        return explode("\n", $this->PriceReductionAuction ?? '');

    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class SeoConfiguration extends Model
{
	protected $table = 'seo_configurations';

	protected $fillable = ['url', 'name', 'title', 'description', 'keywords', 'object_id', 'object_type'];

    const CLASSES = [
        'lot' => TradeLot::class,
        'category' => TradeLotCategory::class
    ];


	public function object() {
	    return $this->morphTo();
    }



    public static function getSeoByUrl() {
        $current_route = \Route::current();
    	$result = null;
        $model = null;

        $model_name = Arr::first($current_route->parameterNames());
        $model_with_seo = null;

        if($model_name) {
            $model =  $current_route->parameter($model_name);
            if(self::hasSeoRelation($model)) {
                $model_with_seo = $model;

                if(!empty($model_with_seo->seo)) {
                    $result = $model_with_seo->seo->getResult($model_with_seo);
                }
            }
        }
        if(empty($result)) {

            $split_route = explode('/', $current_route->uri());
            $urls_for_query = ['/'];

            while($split_route) {
                $urls_for_query[] = implode('/', $split_route);
                array_pop($split_route);
            }

            $seo_default = self::byUrl($urls_for_query)->orderByRaw('CHAR_LENGTH(url) DESC')->first();

            if(!empty($seo_default)) {
                $result = $seo_default->getResult($model);
            }
        }
    	return $result;
    }


    public function scopeByUrl($query, $url){
    	if(is_string($url)) {
    		return $query->where('url', $url);
    	}
		elseif(is_array($url)) {
			return $query->whereIn('url', $url);
		}
    }


    public function getResult($object = null) {

        if(!is_object($object)) {
            $object = $this->object;
        }

    	$result = [];
        $fields = ['name', 'title', 'description', 'keywords'];

        foreach($fields as $field) {
            $result[$field] = preg_replace_callback('/{(.*?)}/i', function ($matches) use($object){
                if($matches) {
                    $field = Arr::last($matches);
                    return $object->$field ?? Arr::first($matches);
                }
            }, $this->$field);
        }
    	return $result;
    }


    public static function hasSeoRelation($object) {
        $key = 'seo';
        if($object->relationLoaded($key)) {
            return true;
        }

        if (method_exists($object, $key)) {
            //Uses PHP built in function to determine whether the returned object is a laravel relation
            return is_a($object->$key(), "Illuminate\Database\Eloquent\Relations\Relation");
        }

        return false;
    }
}

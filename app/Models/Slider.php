<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Slider extends Model
{
    protected $fillable = ['color', 'content', 'active', 'order'];

    protected $attributes = [
        'active' => false
    ];

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function image()
    {
        return $this->morphOne('App\Models\Image', 'imageable');
    }

    public function deleteImage()
    {
        $image = $this->image;
        if ($image) {
            $image->delete();
        }
    }



    protected static function boot()
    {
        parent::boot();
        static::deleted(function ($slider) {

            $slider->deleteImage();
        });
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    public $discs = [
        Slider::class => 'sliders',
        Banner::class => 'banners'
    ];

    protected $fillable = ['url', 'imageable_id', 'imageable_type', 'name', 'size'];

    protected $appends = ['absolute_url'];

    public function imageable()
    {
        return $this->morphTo();
    }

    public function getAbsoluteUrlAttribute(){
        $domain = request()->root();
        if(!empty($domain) && !empty($this->url)) {
            return "{$domain}/{$this->url}";
        }

    }

    public function getDiscName(){
        $parent = $this->imageable_type;
        if(empty($parent)){
            $parent = $this->attributes['imageable_type'];
        }

        return $this->discs[$parent];
    }

    public function deleteFile()
    {
        Storage::disk($this->getDiscName())->delete($this->name);
    }

    public function setImageAttribute($value)
    {
        $extension = explode(".", $value['upload']['filename']);
        $name = str_random(16) . "." . array_pop($extension);
        Storage::disk($this->getDiscName())->put($name, $value['dataURL']);

        $this->attributes['url'] = Storage::disk($this->getDiscName())->url($name);
        $this->attributes['name'] = $name;
    }

    public function setSize() {
        if(empty($this->size)) {
            $size = Storage::disk($this->getDiscName())->size($this->name);
            $this->size = $size;
        }
    }

    public function setUrl() {
        if(empty($this->url)) {
            $url = Storage::disk($this->getDiscName())->url($this->name);
            $this->url = $url;
        }
    }






    protected static function boot()
    {
        parent::boot();
        static::deleted(function ($image) {
            $image->deleteFile();
        });

        static::saving(function($image){
            $image->setSize();
//            $image->setUrl();
        });
    }
}

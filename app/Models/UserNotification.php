<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

/**
 * @property int id
 * @property int|null user_id
 * @property string title
 * @property string|null body
 * @property UserNotificationRead|null notification_read
 * @property UserNotificationRead|null reads
 */
class UserNotification extends Model
{
    use Notifiable, SoftDeletes;

    protected $fillable = ['user_id', 'author_id', 'title', 'body', 'order_id'];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function author() {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function order() {
        return $this->belongsTo(Order::class, 'order_id');
    }


    /**
     * Выводит прочитано ли уведомление пользователем.
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function notification_read()
    {
        $user = auth()->user();
        return $this->hasOne(UserNotificationRead::class)
            ->where('user_id', '=', $user->id)
            ->withTrashed();
    }

    public function reads()
    {
        return $this->hasMany(UserNotificationRead::class);
    }

    public function notifications_system_reads()
    {
        return $this->hasOne(UserNotificationRead::class)
            ->whereNull('user_id');
    }

    public function notifications_with_system_reads()
    {
        $user = auth()->user();
        return $this->hasOne(UserNotificationRead::class)
            ->where('user_id', '=', $user->id)
            ->orWhereNull('user_id');
    }

    public function getIsReadAttribute()
    {
        return $this->notification_read ? $this->notification_read->is_read : false;
    }

    public function setBodyAttribute($value)
    {
        $this->attributes['body'] = addslashes($value);
    }

    public static function setNotification($title = '', $body = '', $user_id = null, $author_id = 0)
    {

        self::create([
            'title' => $title,
            'body' => $body,
            'user_id' => $user_id,
            'author_id' => $author_id,
        ]);
    }

    public function scopeNotUsers($query)
    {
        return $query->whereNull('user_id');
    }

}

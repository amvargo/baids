<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsultingCategory extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title'
    ];

    public function attributes()
    {
        return [
            'title' => 'Название',
        ];
    }

}

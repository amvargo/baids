<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * @property int id
 * @property int trade_lot_id
 * @property string file
 * @property int sort
 */
class TradeLotImage extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'file', 'sort', 'id'
    ];

    protected $appends = ['url'];

    public function getUrlAttribute()
    {
        return Storage::disk('public')->url($this->file);
    }

    public function delete()
    {
        Storage::disk('public')->delete($this->file);
        return parent::delete();
    }
}

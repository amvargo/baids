<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleMakerVariant extends Model
{

    protected $table = 'vehicle_maker_variants';

    //protected $primaryKey = 'text';

    protected $fillable = ['text'];
    protected $hidden = ['vehicle_maker_id'];

    public $timestamps = false;

    //protected $with = ['maker'];

    public function maker()
    {
        return $this->belongsTo(VehicleMaker::class, 'vehicle_maker_id');
    }

    public function scopeSearchText($query, $text)
    {
        return $query->where('text', '=', $text)->with('maker')
            ->orWhereHas('maker', function ($query) use ($text) {
                return $query->where('name', '=', $text);
            });
    }
}

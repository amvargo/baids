<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleSpecialMaker extends Model
{
    protected $fillable = ['id', 'name', 'vehicle_special_type_id'];

    protected $appends = ['models_count', 'variants_count'];

    public function models()
    {
        return $this->hasMany(VehicleSpecialModel::class);
    }

    public function variants()
    {
        return $this->hasMany(VehicleSpecialMakerVariant::class);
    }

    public function getVariantsCountAttribute()
    {
        return $this->variants()->count();
    }

    public function getModelsCountAttribute()
    {
        return $this->models()->count();
    }

    public function scopeSearchText($query, $text)
    {
        return $query->leftJoin('vehicle_special_maker_variants', 'vehicle_special_maker_variants.vehicle_maker_id', '=', 'vehicle_special_makers.id')
            ->where('name', '=', $text)
            ->orWhere('text', '=', $text);
    }
}

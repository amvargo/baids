<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMailingLotFilter extends Model
{
    public $timestamps = null;

    protected $fillable = [
        'trade_lot_param_id', 'value', 'value_from', 'value_to'
    ];

    public function param()
    {
        return $this->belongsTo(TradeLotParam::class, 'trade_lot_param_id');
    }
}

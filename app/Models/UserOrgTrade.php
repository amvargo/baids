<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\FedResurs\CompanyTradeOrganizer;
use App\Models\FedResurs\ArbitrManager;



/**
 * Class UserOrgTrade
 * @package App\Models
 * @property integer id
 * @property integer user_id
 * @property string reg_number
 * @property string inn
 * @property string sro
 * @property boolean confirmed
 * @property string created_at
 * @property string updated_at
 * @property int status
 */
class UserOrgTrade extends Model
{
    protected $appends = ['type', 'full_name'];

    public $allowed_types = [
        1 => 'А/У',
        2 => 'Инные организации'
    ];

    const MODERATE_STATUS = 1;
    const ALLOWED_STATUS  = 2;
    const DENY_STATUS     = 3;


    const STATUSES = [
        self::MODERATE_STATUS  => 'На модерации',
        self::ALLOWED_STATUS   => 'Подтверждено',
        self::DENY_STATUS      => 'Отклонено'
    ];

    const MODERATE_FIELDS = ['inn', 'org_profile_type', 'org_profile_first_name', 'org_profile_last_name',
        'org_profile_middle_name', 'org_profile_full_name'
        ];

    protected $fillable = ['inn', 'org_profile_type', 'org_profile_first_name',
        'org_profile_last_name', 'org_profile_middle_name', 'org_profile_full_name', 'status', 'exist'];

    protected $casts = [
        'exist' => 'boolean'
    ];

    protected $attributes = [
        'status' => self::MODERATE_STATUS,
        'exist' => false
    ];

    /* Relationship */

    public function user(){
        return $this->belongsTo(User::class);
    }


    public function scopeAllowed($query){
        return $query->where('status', self::ALLOWED_STATUS);
    }


    /* Getters */

    public function getTypeAttribute(){
        if(!is_null($this->org_profile_type)){
            return $this->allowed_types[$this->org_profile_type];
        }
    }

    public function getFullNameAttribute(){

        $arr = [$this->org_profile_first_name, $this->org_profile_last_name, $this->org_profile_last_name];

        return ucwords(join(' ', $arr));

    }

    public function isAllowed()
    {
        return $this->status === self::ALLOWED_STATUS;
    }





    public function isModerate() {
        $modifiedFields = $this->getDirty();
        foreach($modifiedFields as $field => $val) {
            if(in_array($field, self::MODERATE_FIELDS)) {
                return true;
            }
        }
        return false;
    }

    public function checkModerate() {
        if($this->isModerate()) {
            $this->attributes['status'] = self::MODERATE_STATUS;
        }
    }

    public function searchDataAndConfirm(){
        if($this->isDirty('org_profile_type') || $this->isDirty('inn')) {

            $type = $this->attributes['org_profile_type'];

            $model = null;

            if($type == 1) {
                $model = ArbitrManager::byInn($this->attributes['inn'])->first();
            }
            elseif($type == 2) {
                $model = CompanyTradeOrganizer::byInn($this->attributes['inn'])->first();
            }

            if($model) {
                $this->attributes['exist'] = true;
            }

        }
    }


    public function scopeByType($query, $type) {
        return $query->where('org_profile_type', $type);
    }

    public function scopeByStatus($query, $status) {
        return $query->where('status', $status);
    }

    public function setNotification(){
        if( ($this->wasChanged('status'))) {
            $title = "Изменение статуса";
            $status = self::STATUSES[$this->status];
            $body = "Статус организаторов торгов №{$this->id} изменен на '{$status}'";
           // $body = "Изменение статуса организаторов торгов № {$this->id}";

            UserNotification::setNotification($title, $body, $this->user_id);
        }
    }


    protected static function boot()
    {
        parent::boot();
        static::saving(function ($user_org_trade) {
            $user_org_trade->checkModerate();
            $user_org_trade->searchDataAndConfirm();

        });
        static::saved(function($user_org_trade){
            $user_org_trade->setNotification();
        });
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TradeSite extends Model
{
    protected $table = 'trade_sites';
    protected $fillable = ['IdTradePlace', 'TradeSite', 'url', 'search_for_text'];

    public $attributes = [
        'search_for_text' => false
    ];

    /**
     * Связь с трейдом
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function trade()
    {
        return $this->hasMany(Trade::class, 'trade_site_id');
    }
}

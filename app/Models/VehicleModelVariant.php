<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleModelVariant extends Model
{

    protected $table = 'vehicle_model_variants';

    protected $fillable = ['text'];
    protected $hidden = ['vehicle_model_id'];

    public $timestamps = false;

    public function model()
    {
        return $this->belongsTo(VehicleModel::class, 'vehicle_model_id');
    }

    public function scopeSearchText($query, $text)
    {
        return $query->where('text', '=', $text)->with('model')
            ->orWhereHas('model', function ($query) use ($text) {
                return $query->where('name', '=', $text);
            });
    }
}

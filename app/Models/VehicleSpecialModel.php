<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleSpecialModel extends Model
{
    protected $fillable = ['id', 'name', 'vehicle_special_type_id', 'vehicle_special_maker_id', 'created_at', 'updated_at'];

    public function maker()
    {
        return $this->belongsTo(VehicleSpecialMaker::class, 'vehicle_special_maker_id');
    }

    public function variants()
    {
        return $this->hasMany(VehicleSpecialModelVariant::class);
    }
}

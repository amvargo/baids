<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TradeDocument extends Model
{
    protected $table = 'trade_documents' ;

    protected $fillable = ['name', 'url', 'size', 'hash'];


    public function trade() {
        return $this->belongsTo(Trade::class, 'trade_id');
    }


}

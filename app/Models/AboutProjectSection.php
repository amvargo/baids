<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Traits\modelHasImage;
use App\Traits\modelHasImage2;
use App\Traits\PanelModelRights;

class AboutProjectSection extends Model
{
    use SoftDeletes;
    use modelHasImage, modelHasImage2;
    use PanelModelRights;

    protected $fillable = ['order', 'title', 'description', 'link', 'published'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public static function getSections() {
        $sections = AboutProjectSection::where('published', 1)->get();
        return $sections->sortBy('order')->values()->all();
    }
}

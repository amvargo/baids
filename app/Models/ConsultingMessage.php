<?php

namespace App\Models;

use App\Traits\firePanelModelEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

/**
 * @property int user_id
 * @property int category_id
 * @property string full_name
 * @property string email
 * @property string phone
 * @property string message
 * @property string answer
 */
class ConsultingMessage extends Model
{
    use SoftDeletes, Notifiable, firePanelModelEvent;

    protected $fillable = [
        'full_name', 'email', 'phone', 'message', 'category_id', 'answer', 'user_id'
    ];

    public function attributes()
    {
        return [
            'full_name' => 'ФИО',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'message' => 'Сообщение',
            'answer' => 'Ответ',
            'category_id' => 'Категория',
            'user_id' => 'Пользователь',
        ];
    }

    /**
     * Email to lowercase
     * @param $value
     */
    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\Panel\ConsultingCategory::class, 'category_id');
    }
}

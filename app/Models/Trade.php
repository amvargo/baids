<?php

namespace App\Models;

use App\Events\TradeCreated;
use App\Console\Commands\ParserFedResurs;
use App\Models\FedResurs\CompanyTradeOrganizer;
use App\Models\FedResurs\DebtorCompany;
use App\Models\FedResurs\DebtorPerson;
use App\Models\FedResurs\Message;
use App\Models\FedResurs\Organization;
use Awobaz\Compoships\Compoships;
use Carbon\Carbon;
use Carbon\Traits\Date;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * @property string status
 * @property string|\DateTime EventTime
 * @property string TradeId
 * @property string|null IDEFRSB
 * @property string|null DebtorType
 * @property string|null DebtorINN
 * @property array|null DebtorData
 * @property TradeLegalCase|null LegalCase
 * @property array|null ArbitrManager
 * @property string|null ArbitrManagerName
 * @property array|null CompanyBankrCommis
 * @property string|null TradeOrganizerType
 * @property array|null TradeOrganizer
 * @property string|null AuctionType Тип торгов.
 * @property string|null FormPrice Вид предложения о цене.
 * @property Date|null DatePublishSMI Дата публикации сообщения о проведении открытых торгов в официальном издании
 * @property Date|null DatePublishEFIR Дата размещения сообщения о проведении открытых торгов на сайте данного
 *                     официального издания в сети "Интернет" и Едином федеральном реестре сведений о банкротстве
 * @property \DateTime|null TimeBegin Дата и время начала торгов.
 * @property \DateTime|null TimeEnd Дата и время окончания торгов
 * @property \DateTime|null TimeResult Дата и время объявления результатов торгов.
 * @property \DateTime|null ApplicationTimeBegin Дата и время начала подачи заявок.
 * @property \DateTime|null ApplicationTimeEnd Дата и время окончания подачи заявок
 * @property string ApplicationRules Порядок, место, срок и время представления заявок
 * @property bool ApplicationSessionStarted
 * @property bool ApplicationSessionEnded
 * @property null|Date DateBegin Дата начала представления заявок
 * @property int id
 * @property int user_id
 * @property array attaches
 * @property string TradePlaceINN
 * @property string DateRenewal Дата и время возобновления работы электронной площадки
 * @property string TradeOrganizerText Организатор торгов
 * @property string FailureInformation Сведения о сбое и возобновлении работы электронной площадки
 * @property bool ISRepeat Признак «повторные торги». Установлен (true или 1), если торги повторные
 * @property Collection|TradeLot[] lots
 * @property string CaseNumber
 * @property Organization|null place
 * @property DebtorCompany|DebtorPerson|null debtor
 * @property null|DebtorCompany debtor_company
 * @property null|DebtorPerson debtor_person
 * @property mixed TradeOrganizerINN
 * @method static Builder|static timeBeginFilter() только активные лоты
 */
class Trade extends Model
{

    use Compoships;

    protected $fillable = [
        'TradeId', 'EventTime', 'TradePlaceINN', 'DateRenewal', 'user_id', 'AuctionType', 'FormPrice',
        'ApplicationTimeBegin', 'ApplicationTimeEnd', 'status', 'DebtorType', 'DebtorINN', 'CaseNumber',
        'ArbitrManager', 'ApplicationRules', 'TradeOrganizerINN', 'Text', 'arbitr_name'
    ];

    protected $casts = [
        'EventTime' => 'datetime',
        'DebtorData' => 'array',
        'ArbitrManager' => 'array',
        'CompanyBankrCommis' => 'array',
        'TradeOrganizer' => 'array',
        'DatePublishSMI' => 'date',
        'DatePublishEFIR' => 'date',
        'TimeBegin' => 'datetime',
        'TimeEnd' => 'datetime',
        'TimeResult' => 'datetime',
        'DateRenewal' => 'datetime',
        'ApplicationTimeBegin' => 'datetime',
        'ApplicationTimeEnd' => 'datetime',
        'ISRepeat' => 'boolean',
    ];

    public static $status_list = [
        'unknown' => [
            'text' => 'НЕИЗВЕСТНЫЙ СТАТУС!',
            'color_variant' => 'danger',
        ],
        'BiddingDeclared' => [
            'text' => 'Объявлены торги',
            'color_variant' => 'success',
        ],
        'ApplicationSessionStarted' => [
            'text' => 'Открыт прием заявок',
            'color_variant' => 'success',
        ],
        'BiddingInProcess' => [
            'text' => 'Идут торги',
            'color_variant' => 'success',
        ],
        'BiddingPaused' => [
            'text' => 'Торги приостановлены',
            'color_variant' => 'warning',
        ],
        'BiddingCanceled' => [
            'text' => 'Торги отменены',
            'color_variant' => 'danger',
        ],
        'Finished' => [
            'text' => 'Завершенные',
            'color_variant' => 'info',
        ],
        'BiddingFail' => [
            'text' => 'Торги не состоялись',
            'color_variant' => 'danger',
        ],
        'Annul' => [
            'text' => 'Аннулированные',
            'color_variant' => 'danger',
        ],
        'ApplicationSessionEnd' => [
            'text' => 'Прием заявок завершен',
            'color_variant' => 'info',
        ],
    ];

    /** @var array Тип торгов. */
    public static $auction_types = [
        'OpenAuction' => [
            'text' => 'Открытый аукцион'
        ],
        'OpenConcours' => [
            'text' => 'Открытый конкурс'
        ],
        'PublicOffer' => [
            'text' => 'Публичное предложение'
        ],
        'CloseAuction' => [
            'text' => 'Закрытый аукцион'
        ],
        'CloseConcours' => [
            'text' => 'Закрытый конкурс'
        ],
        'ClosePublicOffer' => [
            'text' => 'Закрытое публичное предложение'
        ]
    ];
    public static $auction_types_for_filter = [
        'OpenAuction' => 'Открытый аукцион',
        'OpenConcours' => 'Открытый конкурс',
        'PublicOffer' => 'Публичное предложение',
    ];

    /** @var array Вид предложения о цене. */
    public static $price_forms = [
        'OpenForm' => [
            'text' => 'Открытая форма предложения о цене'
        ],
        'CloseForm' => [
            'text' => 'Закрытая форма предложения о цене'
        ],
    ];

    public static function boot()
    {
        parent::boot();

        self::saving(function ($model) {
            if ($model->isDirty('ArbitrManager')) {
                $model->arbitr_name = sprintf(
                    "%s %s %s",
                    trim(data_get($model->ArbitrManager, 'LastName', '')),
                    trim(data_get($model->ArbitrManager, 'FirstName', '')),
                    trim(data_get($model->ArbitrManager, 'MiddleName', '')),
                ) 
                ?? sprintf(
                    "%s %s",
                    trim(data_get($model->ArbitrManager, 'LastName', '')),
                    trim(data_get($model->ArbitrManager, 'FirstName', ''))
                )
                ?? $model->ArbitrManager['ShortName']
                ?? $model->ArbitrManager['FullName']
                ?? null;
            }
            
            if ($model->isDirty('TradeOrganizer')) {
                $organizer_name = $model->TradeOrganizer['ShortName']
                ?? sprintf(
                    "%s %s %s",
                    trim(data_get($model->TradeOrganizer, 'LastName', '')),
                    trim(data_get($model->TradeOrganizer, 'FirstName', '')),
                    trim(data_get($model->TradeOrganizer, 'MiddleName', '')),
                ) 
                ?? sprintf(
                    "%s %s",
                    trim(data_get($model->TradeOrganizer, 'LastName', '')),
                    trim(data_get($model->TradeOrganizer, 'FirstName', ''))
                )
                ?? $model->TradeOrganizer['FullName']
                ?? null;

                if ($organizer_name !== null) {
                    $model->lots()->update(['organizer_name' => $organizer_name]);
                }
            }
        });
    }

    public function legal_case()
    {
        return $this->hasOne(TradeLegalCase::class, 'CaseNumber', 'CaseNumber');
    }

    public function debtor_company()
    {
        return $this->hasOne(DebtorCompany::class, 'INN', 'DebtorINN');
    }

    public function debtor_person()
    {
        return $this->hasOne(DebtorPerson::class, 'INN', 'DebtorINN');
    }

    public function getDebtorAttribute()
    {
        switch ($this->DebtorType) {
            case 'DebtorCompany':
                $relative = $this->debtor_company;
                break;
            case 'DebtorPerson':
                $relative = $this->debtor_person;
                break;
            default:
                $relative = null;
        }
        return $relative;
    }

    /**
     * Площадка
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function place()
    {
        return $this->belongsTo(CompanyTradeOrganizer::class, 'TradePlaceINN', 'INN');
    }

    public function lots()
    {
        return $this->hasMany(TradeLot::class, ['TradeId', 'TradePlaceINN'], ['TradeId', 'TradePlaceINN']);
    }

    public function trade_site()
    {
        return $this->belongsTo(TradeSite::class, 'trade_site_id');
    }

    public function getArbitrManagerNameAttribute()
    {
        if (!$this->ArbitrManager) {
            return null;
        }
        return implode(' ', [
            data_get($this->ArbitrManager, 'LastName'),
            data_get($this->ArbitrManager, 'FirstName'),
            data_get($this->ArbitrManager, 'MiddleName'),
        ]);
    }

    public function getAttachesAttribute()
    {
        return Storage::disk('trade')->files($this->id, true);
    }

    /**
     * Связь с документами
     */
    public function documents() {
        return $this->hasMany(TradeDocument::class, 'trade_id');
    }


    public function getStatusInfoAttribute()
    {
        if (!isset(self::$status_list[$this->status])) {
            return (object)self::$status_list['unknown'];
        }
        return (object)self::$status_list[$this->status];
    }

    public function getAuctionTypeTextAttribute()
    {
        if (empty($this->AuctionType)) {
            return null;
        }
        return self::$auction_types[$this->AuctionType]['text'];
    }

    public function getFormPriceTextAttribute()
    {
        return $this->FormPrice ? self::$price_forms[$this->FormPrice]['text'] : null;
    }

    public function getTradeOrganizerTextAttribute()
    {
        if (!$this->TradeOrganizer) {
            return '';
        }
        if ($this->TradeOrganizerType === 'TradeOrganizerCompany') {
            return data_get($this->TradeOrganizer, 'FullName');
        }
        if ($this->TradeOrganizerType === 'TradeOrganizerPerson') {
            $org = (array)$this->TradeOrganizer;
            return sprintf("%s %s %s", $org['LastName'], $org['FirstName'], $org['MiddleName'] ?? '');
        }
        return 'Error';
    }

    public function getDebtorFullNameAttribute()
    {
        if (!$this->DebtorData) {
            return '';
        }
        if ($this->TradeOrganizerType === 'TradeOrganizerCompany') {
            return data_get($this->TradeOrganizer, 'FullName');
        }
        if ($this->TradeOrganizerType === 'TradeOrganizerPerson') {
            $org = (array)$this->TradeOrganizer;
            return sprintf("%s %s %s", $org['LastName'], $org['FirstName'], $org['MiddleName'] ?? '');
        }
        return 'Error';
    }

    public static function AnnulmentMessage($body)
    {
        if ($body instanceof \SimpleXMLElement) {
            $xml = $body;
            $ID_Annulment = (string)$xml->AnnulmentMessage->ID_Annulment;
            $Reason = (string)$xml->AnnulmentMessage->Reason;
            $IsHide = (string)$xml->AnnulmentMessage->IsHide === 'true';
        } else {
            $TradeId = $body['@TradeId'];
            $ID_Annulment = $body['ID_Annulment'];
            $Reason = $body['Reason'];
            $IsHide = isset($body['IsHide']) && $body['IsHide'] === 'true';
        }
        // todo: ОБЯЗАТЕЛЬНО ОБРАБОТАТЬ
    }

    /**
     * Аннулирование торгов
     * @return $this
     */
    public function annulment()
    {
        $this->lots()->delete();
        $this->status = 'Annul';
        return $this;
    }

    /**
     * Обновляет статус торгов по статусам его толов
     * @return int|string|null
     */
    public function updateStatusByLots()
    {
        $common_status = null;
        $this->fresh('lots');
        $statuses = $this->lots()
            ->get(['id', 'LotNumber', 'status'])
            ->groupBy('status')
            ->toArray();

        if (empty($statuses)) {
            return null;
        }

        if (count($statuses) === 1) {
            $common_status = key($statuses);
            $this->status = $common_status;
            $this->save();
            return $common_status;
        }

        // count($statuses) > 0
        // status logic...
        // Если хотя бы один из лотов находится в статусе: (status), то сами
        // торги находятся в статусе (status)
        $abc_statuses = [
            'BiddingInProcess', // Идут торги
            'BiddingPaused', // Торги приостановлены
            'ApplicationSessionStarted', // Открыт прием заявок
            'ApplicationSessionEnd', // Прием заявок завершен
            'BiddingDeclared', // Объявлены торги
            'Finished', // Завершенные
            'BiddingCanceled', // Торги отменены
            'BiddingFail', // Торги не состоялись
        ];
        foreach ($abc_statuses as $status) {
            if (in_array($status, array_keys($statuses))) {
                $this->status = $status;
                $this->save();
                return $status;
            }
        }

        return false;
    }

    public function scopeFilterByDate(Builder $query, $columnName, $from, $to)
    {
        if (!empty($from)) {
            $validFrom = Carbon::parse($from)->format('Y-m-d');
            $query->whereNotNull($columnName)->where($columnName, '>=', $validFrom);
        }
        if (!empty($to)) {
            $validTo = Carbon::parse($to)->format('Y-m-d');
            $query->whereNotNull($columnName)->where($columnName, '<=', $validTo);
        }

        return $query;
    }

    public function scopeTimeBeginFilter(Builder $query, $from, $to)
    {
        $query->filterByDate("TimeBegin", $from, $to);

        return $query;
    }

    public function scopeTimeEndFilter(Builder $query, $from, $to) {
        $query->filterByDate("TimeEnd", $from, $to);

        return $query;
    }

    public function scopeApplicationTimeBeginFilter(Builder $query, $from, $to)
    {
        $query->filterByDate("ApplicationTimeBegin", $from, $to);

        return $query;
    }

    public function scopeApplicationTimeEndFilter(Builder $query, $from, $to) {
        $query->filterByDate("ApplicationTimeEnd", $from, $to);

        return $query;
    }

    public function getMessagesFileArchive()
    {
        $storage = Storage::disk('trade_messages');
        $files = $storage->files($this->TradePlaceINN . '/' . $this->TradeId);
        return Collection::make($files)->transform(function ($file) use ($storage) {
            $name = substr($file, strpos($file, '/') + 1);
            $path = $file;
            $full_path = $storage->path($file);
            $created_at = Carbon::parse(filectime($full_path));
            try {

                // clearing
                $xml_str = str_replace(array_keys(ParserFedResurs::$fix_xml), array_values(ParserFedResurs::$fix_xml), $storage->get($path));
                $xml = simplexml_load_string($xml_str, 'SimpleXMLElement', LIBXML_COMPACT | LIBXML_PARSEHUGE);

                //$xml = simplexml_load_file($full_path, 'SimpleXMLElement', LIBXML_COMPACT | LIBXML_PARSEHUGE);
                $msg_type = key($xml);
            } catch (\Exception $err) {
                $msg_type = $err->getMessage();
            }
            return (object)compact('name', 'path', 'full_path', 'created_at', 'msg_type');
        });
    }

    /**
     * Сохраняет документы лота
     * @param $doc
     * @return string|bool
     * @throws \Exception
     */
    public function saveFileDoc($doc)
    {
        $file_name = $doc->getClientOriginalName();
        $path = Storage::disk('trade')->put("{$this->id}/docs/{$file_name}", $doc);
        return $path;
    }

    /**
     * удаление документов
     */
    public function deleteDocs()
    {
        $attaches = $this->attaches;
        if( $attaches ) {
            Storage::disk('trade')->delete( $this->attaches );
        }
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'CaseNumber', 'CaseNumber');
    }

}

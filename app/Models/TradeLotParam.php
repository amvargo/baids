<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string type
 * @property string code
 * @property string title
 * @property string validator
 * @property string replace_table
 */
class TradeLotParam extends Model
{
    protected $fillable = ['code', 'type', 'title', 'options', 'replace_table', 'sort', 'published', 'validator'];
    public $timestamps = false;

    protected $casts = [
        'options' => 'array',
        'published' => 'boolean',
    ];

    public function values()
    {
        return $this->hasMany(TradeLotParamValue::class);
    }

    /*public function params()
    {
        return $this->belongsToMany(TradeLotParam::class);
    }*/

    public function categories()
    {
        return $this->belongsToMany(TradeLotCategory::class);
    }

    public function regulars()
    {
        return $this->hasMany(TradeLotParamRegular::class);
    }

    /*public function listForSelect()
    {
        return $this->values()->select('value')->distinct()->orderBy('value')->get();
    }*/

    public function listForSelectVue()
    {
        return $this->values()
            ->whereHas('lots', function ($query) {
                return $query->catalogLocationFilter();
            })
            ->select('value')
            ->distinct()
            ->orderBy('value')
            ->get()
            ->pluck('value');
    }

}

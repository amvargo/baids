<?php

namespace App\Models;

use App\Events\OrderUpdated;
use App\Events\PanelEvent;
use App\Http\Controllers\Site\Cabinet\NotificationController;
use App\Traits\firePanelModelEvent;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

/**
 * @property integer id
 * @property integer service_id
 * @property integer|null user_id
 * @property integer|null staff_id
 * @property integer|null trade_lot_id
 * @property Service|null service
 * @property User|null user
 * @property mixed status
 * @property string|null service_begin
 * @property string|null service_end
 */
class Order extends Model
{
    use SoftDeletes, Notifiable, firePanelModelEvent;

    const
        STATUS_NEW = 'new',
        STATUS_IN_WORK = 'in_work',
        STATUS_DONE = 'done';

    public static $status_list = [
        self::STATUS_NEW => ['Новый', 'success'],
        self::STATUS_IN_WORK => ['В работе', 'info'],
        self::STATUS_DONE => ['Завершен', 'default'],
    ];

    protected $fillable = [
        'service_id', 'user_id', 'staff_id', 'payment_id', 'stage', 'status', 'service_begin', 'service_end',
        'payment_link', 'price', 'assessment', 'comment', 'trade_lot_id'
    ];

    protected $attributes = [
        'service_begin' => 'Начало услуги',
        'service_end' => 'Завершение услуги',
    ];

    protected $casts = [
        'payment' => 'boolean',
    ];

    protected $appends = [
        'status_name', 'status_color', 'date_start', 'date_end'
    ];

    protected function fireCustomModelEvent($event, $method)
    {
        if ($event === 'updated') {
            event(new OrderUpdated($this));
        }

        if (in_array($event, ['created', 'updated', 'deleted', 'restored'])) {
            info('* ' . $event . ' - ' . $method);
            event(new PanelEvent(class_basename($this), $event, $this));
        }

        // default
        if (!isset($this->dispatchesEvents[$event])) {
            return;
        }

        $result = static::$dispatcher->$method(new $this->dispatchesEvents[$event]($this));

        if (!is_null($result)) {
            return $result;
        }
    }

    public function getStatusNameAttribute()
    {
        return isset(self::$status_list[$this->status])
            ? self::$status_list[$this->status][0]
            : $this->status;
    }

    public function getStatusColorAttribute()
    {
        return isset(self::$status_list[$this->status])
            ? self::$status_list[$this->status][1]
            : null;
    }

    public function getDateStartAttribute() {
        if(!empty($this->service_begin)) {
            return Carbon::parse($this->service_begin)->format('d.m.Y');
        }
    }


    public function getDateEndAttribute() {
        if(!empty($this->service_end)) {
            return Carbon::parse($this->service_end)->format('d.m.Y');
        }
    }

    public function setServiceBeginAttribute($value) {
        $date = Carbon::parse($value);

        if($date) {
            $this->attributes['service_begin'] = $date->setTime(00, 00, 00)->subHour(2)->format('Y-m-d H:i');
        }

    }

    public function setServiceEndAttribute($value) {
        $date = Carbon::parse($value);
        if($date) {
            $this->attributes['service_end'] = $date->setTime(21, 59, 59)->format('Y-m-d H:i');
        }

    }

    public function service()
    {
        return $this->belongsTo(Service::class)->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function staff()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function lot()
    {
        return $this->belongsTo(TradeLot::class, 'trade_lot_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function payment()
    {
        return $this->hasOne(UserPayment::class)->orderByDesc('id');
    }

    /**
     * @return UserPayment
     */
    public function createPayment()
    {
        $payment = new UserPayment([
            'amount' => $this->service->price,
            'status' => UserPayment::STATUS_WAITING,
            'title' => $this->service->title,
        ]);
        $payment->info = [
            "type" => "Visa Classic"
        ];
        $payment->service()->associate($this->service->id);
        $payment->user()->associate($this->user->id);
        $payment->order()->associate($this);
        $payment->save();
        $this->update(['payment_id' => $payment->id]);
        return $payment;
    }

    public function createUnitellerPayment(){
        $payment = new UserPayment();
        $payment->fill([
            'amount' => $this->price,
            'status' => UserPayment::STATUS_WAITING,
            'title' => $this->service->title,
            'order_id' => $this->id,
            'service_id' => $this->service_id,
            'user_id' => $this->user_id

        ]);
        $payment->save();

        $this->update(['payment_id' => $payment->id]);
        return $payment;
    }


    /**
     * @param $new_status
     * @return $this
     */
    public function setStatus($new_status)
    {
        $this->status = $new_status;
        return $this;
    }

    public function onEndServiceTime()
    {
        if($this->status === Order::STATUS_IN_WORK && Carbon::parse($this->service_end) <= now()){
            $this->setStatus(Order::STATUS_DONE)->save();
        }
    }
}

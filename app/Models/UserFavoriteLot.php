<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFavoriteLot extends Model
{
    protected $appends = ['flag'];

    public function getFlagAttribute()
    {
        return 123;
    }
}

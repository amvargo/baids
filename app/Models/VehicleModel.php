<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class VehicleModel extends Model
{


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vehicle_models';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'code', 'make_id', 'created_at', 'updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->code = Str::slug($model->name);
        });
    }

    public function variants()
    {
        return $this->hasMany(VehicleModelVariant::class);
    }

    public function maker()
    {
        return $this->belongsTo(VehicleMaker::class, 'vehicle_maker_id');
    }

    public function scopeSearchText($query, $text)
    {
        return $query->leftJoin('vehicle_model_variants', 'vehicle_model_variants.vehicle_model_id', '=', 'vehicle_models.id')
            ->where('name', '=', $text)
            ->orWhere('text', '=', $text);
    }

}

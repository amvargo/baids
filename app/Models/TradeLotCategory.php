<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * @property int id
 * @property int|null parent_id
 * @property int index
 * @property string slug
 * @property string title
 * @property string|null lot_image
 * @property string|null lot_image_url
 * @property string|null image
 * @property string|null image_url
 * @property boolean published
 * @property string url
 * @property array options|null
 * @property TradeLotCategory|null parent
 * @property Collection|TradeLotCategoryRegular[] regulars
 */
class TradeLotCategory extends Model
{
    use SoftDeletes;

    protected $casts = [
        'published' => 'boolean',
        'options' => 'json',
    ];

    protected $fillable = [
        'title', 'slug', 'published', 'parent_id', 'options'//, 'lot_image'
    ];

    protected $appends = ['url'];

    protected $hidden = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|TradeLot[]
     */
    public function lots()
    {
        return $this->belongsToMany(TradeLot::class);
    }

    public function params()
    {
        return $this->belongsToMany(TradeLotParam::class);
    }

    public function classifications()
    {
        return $this->belongsToMany(TradeLotClassificator::class);
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id')
            ->where('published', '=', 1);
    }

    public function regulars()
    {
        return $this->hasMany(TradeLotCategoryRegular::class);
    }

    public function getPageTitle()
    {
        return $this->title;
    }

    public function resolveRouteBinding($value, $field = null)
    {
        return $this->where('slug', $value)->firstOrFail();
    }

    public function getUrlAttribute()
    {
        // sorry
        if (isset(request()->route()->action['as']) && strpos(request()->route()->action['as'], 'site.hot-lots') !== false) {
            return route('site.hot-lots.category.show', $this->slug);
        }

        return route('site.catalog.category.show', $this->slug);
    }

    public function getLotImageUrlAttribute()
    {
        if (empty($this->lot_image)) {
            return null;
        }
        return Storage::disk('public')->url($this->lot_image);
    }

    public function getImageUrlAttribute()
    {
        if (empty($this->image)) {
            return null;
        }
        return Storage::disk('public')->url($this->image);
    }

    /**
     * @param int $limit
     * @param bool $with_children
     * @param bool $not_empty
     * @return Collection
     */
    public static function getGeneralCategories($limit = 6, $with_children = false, $not_empty = false)
    {
        $query = TradeLotCategory::onlyPublic()
            ->whereNull('parent_id')
            ->where('published', '=', 1);

        if ($with_children) {
            $query->with('children');
        }

        if ($not_empty) {
            $query = $query->whereHas('lots', function ($query) {
                return $query->catalogLocationFilter();
            });
        }

        return $query->orderBy('index')->limit($limit)->get();
    }

    public static function getGeneralCategoriesWithActive($limit = 6, $with_children = false, $not_empty = false)
    {
        $items = self::getGeneralCategories($limit, $with_children, $not_empty);

        $items->each(function ($item) use ($with_children) {
            $has_active = false;
            if ($with_children) {
                $item->children->each(function ($item) use (&$has_active) {
                    if ($item->url === url()->current()) {
                        $has_active = true;
                        $item->active = $item->url === url()->current();
                    }
                });
            }
            $item->active = $has_active || $item->url === url()->current();
        });

        return $items;
    }

    /**
     * @param bool $with_children
     * @param bool $not_empty
     * @return Collection
     */
    public function getChildrenCategories($with_children = false, $not_empty = false)
    {
        $query = TradeLotCategory::where('parent_id', $this->id)
            ->where('published', '=', 1);

        if ($with_children) {
            $query->with('children');
        }

        if ($not_empty) {
            $query = $query->whereHas('lots', function ($query) {
                return $query->catalogLocationFilter();
            });
        }

        return $query->orderBy('index')->get()->load('children');
    }

    /**
     * @param bool $with_children
     * @return Collection
     */
    public static function getAllCategories($with_children = false)
    {
        return TradeLotCategory::getGeneralCategories(100)
            ->load('children');
    }

    public function getBreadcrumbArray()
    {
        $result = [];
        $parent = $this;
        while ($parent) {
            $result[] = (object)[
                'id' => $parent->id,
                'title' => $parent->title,
                'url' => $parent->url,
                'active' => $parent === $this,
            ];
            $parent = self::find($parent->parent_id);
        };
        return array_reverse($result);
    }

    public function scopeOnlyPublic($query)
    {
        return $query->where('published', 1);
    }

    public function seo()
    {
        return $this->morphOne('App\Models\SeoConfiguration', 'object');
    }

    public function scopeRootCategories($query) {
        return $query->whereNull('parent_id');
    }

    public static function getRootCategories() {
        return self::rootCategories()->get();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TradeLotSale extends Model
{
    protected $fillable = [
        'trade_lot_id', 'TradePlaceINN', 'TradeId', 'LotNumber', 'DateContract', 'Price',
        'ContractNumber', 'AdditionalInfo', 'ContractParticipantList'
    ];

    protected $casts = [
        'DateContract' => 'date',
        'ContractParticipantList' => 'array',
    ];
}

<?php

namespace App\Models;

use App\Events\PanelEvent;
use App\Traits\firePanelModelEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use function Psy\debug;

/**
 * @method static published()
 */
class Question extends Model
{
    use SoftDeletes, Notifiable, firePanelModelEvent;

    protected $fillable = [
        'author', 'email', 'question'
    ];

    protected $casts = [
        'published' => 'boolean',
    ];

    public function attributes()
    {
        return [
            'author' => 'Автор',
            'email' => 'E-mail',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'priority' => 'Приоритет',
            'published' => 'Опубликовать на сайте',
        ];
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('published', '=', 1);
    }
}

<?php

namespace App\Models\FedResurs;

use App\Interfaces\PlaceOfResidenceInterface;
use App\Models\Trade;
use App\Traits\PlaceOfResidenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * Class DebtorCompany
 * @package App\Models\FedResurs
 * @method static search(string $search)
 */
class DebtorCompany extends Model implements PlaceOfResidenceInterface
{
    use PlaceOfResidenceTrait;

    protected $table = 'fr_debtor_company';
    //protected $primaryKey = 'BankruptId';

    protected $placeOfResidenceField = 'LegalAddress';

    protected $fillable = [
        'BankruptId', 'Category', 'CategoryCode', 'Region', 'INN', 'FullName', 'ShortName',
        'OGRN', 'LegalAddress', 'LastReportDate', 'LastMessageDate', 'DateLastModif'
    ];


    public function fillFromObj($obj)
    {
        return $this->forceFill(Arr::except((array)$obj, ['LegalCaseList']));
    }

    public function fillFromXml(\SimpleXMLElement $xml)
    {
        return $this;
    }

    public function trades()
    {
        return $this->hasMany(Trade::class, 'DebtorINN', 'INN');
    }

    /**
     * @method search
     * @param $query
     * @param $search
     * @return mixed
     */
    public function scopeSearch($query, $search)
    {
        $query->where('FullName', 'like', "%{$search}%")
            ->orWhere('ShortName', 'like', "%{$search}%");
        return $search ? $query->orWhere('INN', '=', $search) : $query;
    }
}

<?php

namespace App\Models\FedResurs;

use App\Interfaces\PlaceOfResidenceInterface;
use App\Models\Trade;
use App\Traits\PlaceOfResidenceTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

/**
 * Class DebtorPerson
 * @package App\Models\FedResurs
 * @method static search(string $search)
 */
class DebtorPerson extends Model implements PlaceOfResidenceInterface
{
    use PlaceOfResidenceTrait;

    protected $table = 'fr_debtor_person';
    //protected $primaryKey = 'BankruptId';
    protected $placeOfResidenceField = 'Address';

    protected $fillable = [
        'BankruptId', 'Category', 'CategoryCode', 'Region', 'INN', 'Birthdate', 'Birthplace',
        'DateLastModif', 'SNILS', 'LastName', 'FirstName', 'MiddleName', 'Address', 'OGRNIP',
        'LastMessageDate', 'LastReportDate',
    ];

    public function getFullNameAttribute()
    {
        return sprintf('%s %s %s', $this->FirstName, $this->LastName, $this->MiddleName);
    }

    public function fillFromObj($obj)
    {
        $obj = (array)$obj;
        return $this->forceFill(Arr::except($obj, ['LegalCaseList', 'NameHistory']));
    }

    public function fillFromXml(\SimpleXMLElement $xml)
    {
        $attrs = $xml->children()->attributes();
        $this->FirstName = (string)$attrs["FirstName"];
        $this->LastName = (string)$attrs["LastName"];
        $this->MiddleName = (string)$attrs["MiddleName"];
        $this->INN = (string)$attrs["INN"];
        return $this;
    }

    public function trades()
    {
        return $this->hasMany(Trade::class, 'DebtorINN', 'INN');
    }

    /**
     * @param $query
     * @param $search
     * @return mixed
     */
    public function scopeSearch($query, $search)
    {
        $query->orWhere(DB::raw('CONCAT(`FirstName`, " ", `LastName`, " ", `MiddleName`)'), 'like', "%{$search}%");
        return $search ? $query->orWhere('INN', '=', $search) : $query;
    }
}

<?php

namespace App\Models\FedResurs;

use App\Models\Trade;
use App\Models\TradeLot;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use mysql_xdevapi\Collection;
use mysql_xdevapi\Exception;
use Nathanmac\Utilities\Parser\Facades\Parser;
use function foo\func;

/**
 * @property mixed PublisherType
 * @property mixed PublisherID
 * @property mixed BankruptType
 * @property mixed BankruptInfoID
 * @property string MessageType
 * @property array MessageInfo
 * @property mixed id
 * @property null|Trade trade
 */
class Message extends Model
{
    protected $table = 'fr_messages';

    protected $appends = ['Publisher', 'Bankrupt'];

    protected $casts = [
        'MessageInfo' => 'array',
        'PublisherInfo' => 'array',
        'BankruptInfo' => 'array',
        'FileInfoList' => 'array',
        'MessageURLList' => 'array',
    ];

    public function fillFromObj($msg)
    {
        if (!isset($msg['PublisherInfo'])) {
            //throw new \Exception('Message not contain PublisherInfo');
        }
        $this->forceFill([
            'id' => $msg['Id'],
            'Number' => $msg['Number'],
            'CaseNumber' => isset($msg['CaseNumber']) ? trim($msg['CaseNumber']) : null,
            'BankruptId' => $msg['BankruptId'],
            'MessageGUID' => $msg['MessageGUID'],
            'PublishDate' => Carbon::parse($msg['PublishDate']),
            'MessageType' => $msg['MessageInfo']["@MessageType"],
            'MessageInfo' => $msg['MessageInfo'],
        ]);
        if (isset($msg['PublisherInfo'])) {
            $this->forceFill([
                'PublisherType' => $msg['PublisherInfo']["@PublisherType"],
                'PublisherID' => $msg['PublisherInfo'][$msg['PublisherInfo']["@PublisherType"]]["@Id"],
                'PublisherInfo' => $msg['PublisherInfo'],
            ]);
        }
        if (isset($msg['BankruptInfo'])) {
            $this->forceFill([
                'BankruptType' => $msg['BankruptInfo']["@BankruptType"],
                'BankruptInfoID' => $msg['BankruptInfo'][($msg['BankruptInfo']["@BankruptType"] == 'Organization') ? 'BankruptFirm' : 'BankruptPerson']["@Id"],
                'BankruptCategory' => $msg['BankruptInfo']["@BankruptCategory"],
                'BankruptInfo' => $msg['BankruptInfo'],
            ]);
        }
        if (isset($msg['FileInfoList'])) {
            $this->forceFill([
                'FileInfoList' => $msg['FileInfoList'],
            ]);
        }
        if (isset($msg['MessageURLList'])) {
            $this->forceFill([
                'MessageURLList' => $msg['MessageURLList'],
            ]);
        }
        return $this;
    }

    public function getPublisherAttribute()
    {
        $model_path = "\\App\\Models\\FedResurs\\" . $this->PublisherType;
        return $model_path::find($this->PublisherID);
    }

    public function getBankruptAttribute()
    {
        $model_path = "\\App\\Models\\FedResurs\\" . $this->BankruptType;
        return $model_path::find($this->BankruptInfoID);
    }

    public function trade()
    {
        return $this->hasMany(Trade::class, 'CaseNumber', 'CaseNumber');
    }

    // обработка сообщений от федресурса

    public function applyMessage($args)
    {
        $method = "applyMessageType{$this->MessageType}";
        if (method_exists($this, $method)) {
            $this->$method($args);
        }
    }

    public function hasActionMethod($action)
    {
        $method = "applyMessageType{$this->MessageType}";
        return method_exists($this, $method);
    }

    public function applyMessageTypeAuction($args)
    {
        if ($this->MessageType !== 'Auction') {
            return false;
        }

        $list = data_get($this->MessageInfo, 'Auction.LotTable.AuctionLot');
        if (isset($list['Order'])) {
            $list = [$list];
        };
        foreach ($list as $AuctionLot) {
            /** @var TradeLot $lot */
            $lot = $this->trade()->where('TradeId', '=', $args['TradeId'])->first()
                ->lots()->where('LotNumber', '=', $AuctionLot['Order'])->first();
            if (!$lot) {
                Log::warning("Lot not found: TradeId: {$args['TradeId']}, LotNumber:{$AuctionLot['Order']}");
                continue;
            }
            $AuctionStepColumn = $AuctionLot['AuctionStepUnit'] === 'Percent' ? 'StepPricePercent' : 'StepPrice';
            $AdvanceStepColumn = $AuctionLot['AdvanceStepUnit'] === 'Percent' ? 'AdvancePercent' : 'Advance';

            $fields = [
                'StartPrice' => $AuctionLot['StartPrice'],
            ];
            if (is_string($AuctionLot['Step'])) {
                $fields[$AuctionStepColumn] = $AuctionLot['Step'];
            }
            if (is_string($AuctionLot['Advance'])) {
                $fields[$AdvanceStepColumn] = $AuctionLot['Advance'];
            }

            try {
                $lot->update($fields);
            } catch (\Exception $err) {
                Log::error($err->getMessage());
            }
            return true;
        };
    }
}

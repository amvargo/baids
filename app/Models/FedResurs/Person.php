<?php

namespace App\Models\FedResurs;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'fr_persons';

    public function fillFromObj($obj)
    {
        return $this->forceFill([
            'id' => $obj['@Id'],
            'InsolventCategoryName' => $obj['@InsolventCategoryName'],
            'FirstName' => $obj['@FirstName'],
            'MiddleName' => $obj['@MiddleName'],
            'LastName' => $obj['@LastName'],
            'Address' => $obj['@Address'],
            'OGRNIP' => isset($obj['OGRNIP']) ? $obj['OGRNIP'] : null,
            'INN' => $obj['INN'],
            'SNILS' => isset($obj['SNILS']) ? $obj['SNILS'] : null,
            'Birthdate' => Carbon::parse($obj['Birthdate']),
            'Birthplace' => $obj['Birthplace'],
            // NameHistory
        ]);
    }
}

<?php

namespace App\Models\FedResurs;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property int trade_message_id
 */
class TradeMessageLot extends Model
{
    protected $table = 'fr_trade_message_lot_list';

    protected $casts = [
        'Classification' => 'array',
        'Participants' => 'array',
        'SuccessTradeResult' => 'array',
        'FailureTradeResult' => 'array',
    ];

    public function fillFromObj($obj)
    {
        dd(key($obj));
        return $this->forceFill([
            'LotNumber' => $obj['@LotNumber'],
            'SuccessTradeResult' => data_get($obj, 'ns1:SuccessTradeResult', null),
            'FailureTradeResult' => data_get($obj, 'ns1:FailureTradeResult', null),
            'StartPrice' => data_get($obj, 'ns1:StartPrice', null),
            //'StepPrice' => data_get($obj, 'ns1:StepPrice', null),
            'TradeObjectHtml' => data_get($obj, 'ns1:TradeObjectHtml', null),
            'PriceReduction' => data_get($obj, 'ns1:PriceReduction', null),
            'Concours' => data_get($obj, 'ns1:Concours', null),
            'PaymentInfo' => data_get($obj, 'ns1:PaymentInfo', null),
            'SaleAgreement' => data_get($obj, 'ns1:SaleAgreement', null),
            'Classification' => data_get($obj, 'ns1:Classification', null),
            'Participants' => data_get($obj, 'ns1:Participants', null),
        ]);
    }
}

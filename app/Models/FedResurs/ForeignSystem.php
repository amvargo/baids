<?php

namespace App\Models\FedResurs;

use Illuminate\Database\Eloquent\Model;

class ForeignSystem extends Model
{
    protected $table = 'fr_foreign_system';

    public function fillFromObj($obj)
    {
        return $this->forceFill([
            'id' => $obj['@Id'],
            'Name' => $obj['@Name'],
        ]);
    }
}

<?php

namespace App\Models\FedResurs;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $table = 'fr_organizations';

    protected $guarded = [];

    public function fillFromObj($obj)
    {
        return $this->forceFill([
            'id' => $obj['@Id'],
            'InsolventCategoryName' => $obj['@InsolventCategoryName'],
            'INN' => $obj['INN'],
            'FullName' => $obj['@FullName'],
            'ShortName' => $obj['@ShortName'],
            'PostAddress' => $obj['@PostAddress'],
            'LegalAddress' => $obj['@LegalAddress'],
            'OGRN' => $obj['@OGRN'],
            'OKPO' => isset($obj['@OKPO']) ? $obj['@OKPO'] : null,
        ]);
    }
}

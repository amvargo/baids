<?php

namespace App\Models\FedResurs;

use App\Models\Trade;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed site
 */
class CompanyTradeOrganizer extends Model
{
    protected $table = 'fr_company_trade_organizer';
    protected $primaryKey = 'INN';
    protected $fillable = ['INN', 'OGRN', 'FullName', 'ShortName', 'DateLastModif'];

    protected $casts = [
        'DateLastModif' => 'datetime'
    ];

    public function fillFromObj($obj)
    {
        return $this->forceFill([
            'INN' => data_get($obj, 'INN'),
            'FullName' => data_get($obj, 'FullName'),
            'ShortName' => data_get($obj, 'ShortName'),
            'OGRN' => data_get($obj, 'OGRN'),
            'DateLastModif' => Carbon::parse(data_get($obj, 'DateLastModif')),
        ]);
    }

    public function scopeByInn($query, $inn = ''){
        if($inn) {
            return $query->where('INN', $inn);
        }
    }

    public function getFullSiteUrlAttribute()
    {
        if (empty($this->site)) {
            return null;
        }
        return strpos($this->site, 'http') === 0 ? $this->site : "https://{$this->site}";
    }

    public function trades()
    {
        return $this->hasMany(Trade::class, 'TradeOrganizerINN', 'INN');
    }
}

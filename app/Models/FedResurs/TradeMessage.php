<?php

namespace App\Models\FedResurs;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 */
class TradeMessage extends Model
{
    protected $table = 'fr_trade_message';

    protected $casts = [
        'TradeOrganizer' => 'array',
        'EventTime' => 'datetime',
    ];

    public static function getLotsFromObj($obj)
    {
        $obj = head(head($obj));
        if (isset($obj['ns1:LotList'])) {
            $lot_list = $obj['ns1:LotList'];
        } else if (isset($obj['ns1:TradeInfo']['ns1:LotList'])) {
            $lot_list = $obj['ns1:TradeInfo']['ns1:LotList'];
        } else {
            $lot_list = null;
        }
        return $lot_list;
    }

    public function fillFromObj($obj)
    {
        $method = substr(key($obj), 4);
        $obj = head($obj);
        $message = substr(key($obj), 4);
        $obj = head($obj);

        return $this->forceFill([
            'method' => $method,
            'message' => $message,
            'TradeId' => $obj['@TradeId'],
            'Reason' => isset($obj['@Reason']) ? $obj['@Reason'] : null,
            'TradeOrganizer' => isset($obj['ns1:TradeOrganizer']) ? $obj['ns1:TradeOrganizer'] : null,
            'EventTime' => Carbon::parse($obj['@EventTime']),
        ]);
    }
}

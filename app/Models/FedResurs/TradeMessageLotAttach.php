<?php

namespace App\Models\FedResurs;

use Illuminate\Database\Eloquent\Model;

class TradeMessageLotAttach extends Model
{
    protected $table = 'fr_trade_message_lot_list_attach';
}

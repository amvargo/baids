<?php

namespace App\Models\FedResurs;

use Illuminate\Database\Eloquent\Model;

class ArbitrManager extends Model
{
    protected $table = 'fr_arbitr_manager';
    protected $primaryKey = 'ArbitrManagerID';

    protected $guarded = [];

    protected $casts = [
        'Sro' => 'array'
    ];

    public function fillFromObj($obj)
    {
        return $this->forceFill((array)$obj);
    }

    public function scopeByInn($query, $inn = ''){
        if($inn) {
            return $query->where('INN', $inn);
        }
    }
}

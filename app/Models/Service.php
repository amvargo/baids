<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * @method static published()
 * @method static withBanners()
 * @property string|null image
 * @property string|null image2
 * @property string title
 * @property string|null annotation
 * @property string description
 * @property integer|null priority
 * @property boolean published
 * @property integer id
 * @property float price
 */
class Service extends Model
{
    //ID услуг по платной подписке на 1/3/12 месяцев
    const TARIFF_IDS = [18,19,20];
    
    use SoftDeletes;

    protected $fillable = [
        'title'/*, 'annotation', 'image'*/, 'description', 'priority', 'published', 'price', 'system_key'
    ];

    protected $casts = [
        'published' => 'boolean',
    ];

    protected $appends = [
        'url_image',
        'url_image2',
    ];

    public function attributes()
    {
        return [
            'title' => 'Название',
            'annotation' => 'Анонс',
            'image' => 'Изображение',
            'image2' => 'Баннер',
            'description' => 'Текст услуги',
            'priority' => 'Приоритет',
            'published' => 'Опубликовать на сайте',
        ];
    }

    protected $advertising = ['advertising_general', 'advertising_catalog', 'advertising_lk'];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished($query)
    {
        return $query->where('published', '=', 1);
    }

    public function scopeWithBanners($query)
    {
        return $query->whereNotNull('image2');
    }

    /**
     * @return string|null
     */
    public function getUrlImageAttribute()
    {
        if (strpos($this->image, 'https://') === 0) {
            return $this->image;
        }
        return !empty($this->image) ? Storage::url($this->image) : null;
    }

    public function getUrlImage2Attribute()
    {
        if (strpos($this->image2, 'https://') === 0) {
            return $this->image2;
        }
        return !empty($this->image2) ? Storage::url($this->image2) : null;
    }

    public function getSlugAttribute()
    {
        return Str::slug($this->title . '-' . $this->id);
    }

    public function getIdBySlug($slug)
    {
        if (preg_match('/\-(\d+)$/', $slug, $res)) {
            return $res[1];
        }
        return false;
    }

    public function resolveRouteBinding($value, $field = null)
    {
        $id = $this->getIdBySlug($value);
        return $this->findOrFail($id);
    }

    public function scopeAdvertising($query) {
        return $query->whereIn('system_key', $this->advertising);
    }
}

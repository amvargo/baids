<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TradeLotFavorite extends Model
{
    protected $table = 'trade_lot_favorites';
}

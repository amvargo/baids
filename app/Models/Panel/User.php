<?php


namespace App\Models\Panel;

use App\Role\UserRole;
use App\Traits\PanelModelRights;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

/**
 * Class User
 * @package App\Models\Panel
 */
class User extends \App\Models\User
{
    use PanelModelRights;

    protected $appends = [
        'url_avatar', 'panel_rights', 'full_name'
    ];

    protected function panelRights()
    {
        return [
            'view' => UserRole::ROLE_SUPPORT,
            'create' => UserRole::ROLE_ACCOUNT_MANAGER,
            'update' => UserRole::ROLE_ACCOUNT_MANAGER,
            'delete' => UserRole::ROLE_ADMIN,
        ];
    }

    protected $fillable = [
        'name', 'last_name', 'middle_name', 'email', 'password', 'roles', 'phone', 'appointment', 'department'
    ];

    public function delete()
    {
        $this->email .= '-del-' . $this->id;
        $this->save();
        return parent::delete();
    }

    public function deleteAvatarFile($save = false)
    {
        if (empty($this->avatar)) {
            return null;
        }
        if (!Storage::disk('public')->delete($this->avatar)) {
            info('Cannot delete avatar: ' . $this->avatar);
        }
        $this->avatar = null;
        if ($save) {
            return $this->save();
        }
    }

    public function uploadAvatarFile($image, $save = false)
    {
        $path = Storage::disk('public')
            ->put("avatars/{$this->id}", $image);

        info('upload avatar: ' . $path);
        $this->avatar = $path;

        if ($save) {
            return $this->save();
        }
    }
}

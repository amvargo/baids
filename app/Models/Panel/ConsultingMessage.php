<?php

namespace App\Models\Panel;

use App\Events\ConsultingUpdated;
use App\Events\PanelEvent;

class ConsultingMessage extends \App\Models\ConsultingMessage
{
    protected $fillable = [
        'full_name', 'email', 'phone', 'message', 'category_id', 'answer',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    protected function fireCustomModelEvent($event, $method)
    {
        // if the answer was changed, send an email
        if ($event === 'updated' && (string)$this->answer !== $this->getOriginal('answer')) {
            info('ConsultingUpdated* ' . $event . ' - ' . $method);
            event(new ConsultingUpdated($this));
        }

        if (in_array($event, ['created', 'updated', 'deleted', 'restored'])) {
            info('* ' . $event . ' - ' . $method);
            event(new PanelEvent(class_basename($this), $event, $this));
        }

        // default
        if (!isset($this->dispatchesEvents[$event])) {
            return;
        }

        $result = static::$dispatcher->$method(new $this->dispatchesEvents[$event]($this));

        if (!is_null($result)) {
            return $result;
        }

    }
}

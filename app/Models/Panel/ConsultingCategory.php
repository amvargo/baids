<?php

namespace App\Models\Panel;

class ConsultingCategory extends \App\Models\ConsultingCategory
{

    protected $fillable = [
        'title'
    ];

}

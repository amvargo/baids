<?php

namespace App\Models\Panel;

use App\Role\UserRole;
use App\Traits\modelHasImage;
use App\Traits\modelHasImage2;
use App\Traits\PanelModelRights;

/**
 * Class Services
 * @package App\Models\Panel
 */
class Service extends \App\Models\Service
{
    use modelHasImage, modelHasImage2;
    use PanelModelRights;

    protected $appends = [
        'url_image',
        'url_image2',
        'panel_rights'
    ];

    protected function panelRights()
    {
        return [
            'view' => UserRole::ROLE_SUPPORT,
            'create' => UserRole::ROLE_CONTENT,
            'update' => UserRole::ROLE_CONTENT,
            'delete' => UserRole::ROLE_CONTENT,
        ];
    }
}

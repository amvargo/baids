<?php

namespace App\Models\Panel;

class Question extends \App\Models\Question
{
    protected $fillable = [
        'author', 'email', 'question', 'answer', 'priority', 'published'
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }
}

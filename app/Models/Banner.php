<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{

    const MODERATE_STATUS = 1;
    const ALLOWED_STATUS  = 2;
    const DENY_STATUS     = 3;


    const STATUSES = [
        self::MODERATE_STATUS  => 'На модерации',
        self::ALLOWED_STATUS   => 'Подтверждено',
        self::DENY_STATUS      => 'Отклонено'
    ];

    const VARIANTS = [
        'vertical' => 'Вертикальный',
        'horizontal' => 'Горизонтальный',
    ];

    const MODERATE_FIELDS = ['category_id', 'link', 'variant'];



    protected $fillable = ['user_id', 'variant', 'link', 'order_id', 'category_id', 'status', 'service_id'];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $appends = ['status_name', 'variant_name'];


    public function image()
    {
        return $this->morphOne('App\Models\Image', 'imageable');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order() {
        return $this->belongsTo(Order::class);
    }

    public function service() {
        return $this->belongsTo(Service::class);
    }

    public function category() {
        return $this->belongsTo(TradeLotCategory::class);
    }

    public function getStatusNameAttribute() {
        if(!empty($this->status)) {
            return self::STATUSES[$this->status];
        }
    }

    public function getVariantNameAttribute() {
        if($this->variant) {
            return self::VARIANTS[$this->variant];
        }
    }

    public function setImage($new_image) {
        $old_image = $this->image;
        $name = $new_image->store( null, 'banners');
        $path = "/storage/banners/{$name}";
        $result = $this->image()->create(['name' => $name, 'url' => $path]);
        if($old_image) {
            $old_image->delete();
        }
        return $result;
    }

    public function setModerateFields() {
        foreach(self::MODERATE_FIELDS as $field) {
            if($val = request()->post($field)) {
                $this->{$field} = $val;
            }
        }
    }



    public static  function getPrice($date_start = "", $date_end = "", $price = 0) {
        $date_start = Carbon::parse($date_start);
        $date_end = Carbon::parse($date_end);
        if($date_start == $date_end){
            $diff = 1;
          }else{
            $diff  = $date_end->diffInDays($date_start)+1;
        }
        return $diff * $price;

    }


    public function scopeActive($query) {
        $now = Carbon::now()->format('Y-m-d H:i:s');
        return $query->where('created_at', '<=', $now);
    }

    public function scopeHorizontal($query){
        return $query->where('variant', 'horizontal');
    }

    public function scopeVertical($query){
        return $query->where('variant', 'vertical');
    }

    public function scopeVariant($query, $variant = 'horizontal') {
        return $query->where('variant', $variant);
    }

    public function scopeService($query, $service = 22) {
        return $query->where('service_id', $service);
    }
    public function scopeCategory($query, $category = 0) {
        if($category) {
            return $query->where('category_id', $category);
        }
    }

    public function deleteImage()
    {
        $image = $this->image;
        if ($image) {
            $image->delete();
        }
    }

    public function scopeAllowed($query){
        return $query->where('status', self::ALLOWED_STATUS);
    }

    public function isAllowed()
    {
        return $this->status === self::ALLOWED_STATUS;
    }

    public function isDeny() {
        return $this->status == self::DENY_STATUS;
    }

    public function scopeByUser($query, $user_id) {
        return $query->where('user_id', $user_id);
    }

    public function isModerate() {
        $modifiedFields = $this->getDirty();
        foreach($modifiedFields as $field => $val) {
            if(in_array($field, self::MODERATE_FIELDS)) {
                return true;
            }
        }
        return false;
    }

    public function checkModerate() {
        if($this->isModerate()) {
            $this->attributes['status'] = self::MODERATE_STATUS;
        }
    }

    public function scopeByStatus($query, $status) {
        return $query->where('status', $status);
    }

    public function setModerateStatus() {
        $this->attributes['status'] = self::MODERATE_STATUS;
    }

    public function setNotification(){

        if( ($this->wasChanged('status'))) {
            $title = "Изменение статуса";
            $status = self::STATUSES[$this->status];
            $body = "Изменение статуса рекламного банера № {$this->id} изменен на '{$status}'";

            UserNotification::setNotification($title, $body, $this->user_id);
        }
    }


    protected static function boot()
    {
        parent::boot();
        static::creating(function($banner){
            $banner->setModerateStatus();
        });

        static::saving(function ($banner) {
            $banner->checkModerate();
        });

        static::saved(function($banner){
            $banner->setNotification();
        });

        static::deleted(function ($banner) {
            $banner->deleteImage();
        });
    }
}

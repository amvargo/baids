<?php

namespace App\Console\Commands;

use App\Events\PanelEvent;
use App\Models\TradeLot;
use App\Models\TradeLotClassificator;
use App\Models\VehicleMaker;
use App\Models\VehicleMakerVariant;
use App\Models\VehicleModel;
use App\Models\VehicleSpecialMaker;
use Fomvasss\Dadata\Facades\DadataClean;
use Fomvasss\Dadata\Facades\DadataSuggest;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Sunrise\Vin\Vin;

class TestCommand extends Command
{
    protected $signature = 'test:task {task} {--id=0}';

    protected $description = 'Test description';

    public function handle()
    {
        $task = Str::ucfirst((string)$this->argument('task'));
        $method_name = "task{$task}";
        $this->$method_name();
    }

    /**
     * php artisan test:task specVehicle
     */
    public function taskSpecVehicle()
    {
        VehicleSpecialMaker::query()
            ->select(DB::raw('name, COUNT(vehicle_special_type_id) AS type_len'))
            ->groupBy('name')
            ->having('type_len','>',1)
            //->where(['name' => 'Toyota']) // @todo: temporary <<<
            ->orderBy('name')
            ->each(function ($maker){
                /** @var Collection|VehicleSpecialMaker[] $makers */
                $makers = VehicleSpecialMaker::where(['name' => $maker->name])->get();
                $this->line("Maker: {$maker->name}, count: {$makers->count()}");
                if($makers->count() > 0){
                    $first_maker = $makers->first();

                    foreach ($makers as $maker) {
                        if($first_maker === $maker){
                            continue;
                        }
                        $models_arr = $first_maker->models()->select('name')->distinct()->get()->pluck('name');
                        $this->line("Maker: {$maker->name}, models: {$maker->models->count()}");
                        $maker->models()->whereIn('name', $models_arr)->delete();
                        $maker->models()->update(['vehicle_special_maker_id' => $first_maker->id]);
                        $maker->delete();
                    }
                }
            });
    }

    /**
     * РАЗОВО, тримит должника и организатора в лотах
     */
    public function taskTrimLotData()
    {
        TradeLot::onlyPublic()->chunk(1000, function ($lots) {
            foreach ($lots as $lot) {
                if(!empty($lot->debtor_name)){
                    $lot->debtor_name = trim(preg_replace("/\s+/", " ", $lot->debtor_name));
                }
                if(!empty($lot->organizer_name)){
                    $lot->organizer_name = trim(preg_replace("/\s+/", " ", $lot->organizer_name));
                }
                $lot->save();
            }
        });
    }

    public function taskTwo()
    {
        foreach (TradeLot::all() as $lot) {
            $classificators = TradeLotClassificator::whereIn('code', $lot->Classification)->get();
            $lot->classificators()->sync($classificators);
        }
    }

    /**
     * parse addresses for all lots
     */
    public function taskFour()
    {
        $address_codes = [
            'postal_code', 'federal_district', 'region_with_type', 'area_with_type',
            // 'city_with_type', settlement_with_type
        ];
        $cache = [];

        $lots = TradeLot::whereNull('postal_code')
            ->whereHas('params.field', function ($query) {
                return $query->where('code', 'address');
            })->get();
        $this->info('Lot count: ' . $lots->count());
        $n = 0;
        /** @var TradeLot $lot */
        foreach ($lots as $lot) {
            $address = $lot->params()->where('trade_lot_param_id', 1)->first();
            $this->line($address->value);

            // dadata
            $address_md5 = md5($address->value);
            if (isset($cache[$address_md5])) {
                $result = $cache[$address_md5];
            } else {
                try {
                    $result = DadataSuggest::suggest("address", ["query" => $address->value, "count" => 1]);
                } catch (\Throwable $err) {
                    $this->error($err->getMessage());
                    $this->ask('Next?');
                    continue;
                }
                $cache[$address_md5] = $result;
            }
            $address->value = $result['unrestricted_value'];
            $this->info($address->value);
            $address->save();
            $lot->fill(Arr::only($result['data'], $address_codes));
            $lot->city_with_type = $result['data']['city_with_type'] ?: $result['data']['settlement_with_type'];
            $lot->dadata_suggest = $result;
            $lot->save();

            if (empty($lot->city_with_type)) {
                dump($result);
                $this->line('pause');
                $this->ask('Next?');
            }

            if (++$n % 10 === 0) {
                sleep(2);
                //$this->ask('Next?');
            }
        }
    }

    public function taskSign()
    {
        /** @var TradeLot $lot */
        $lot = TradeLot::orderByDesc('id')->first();
        return $lot->testSignDocs();
    }
}

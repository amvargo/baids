<?php

namespace App\Console\Commands;

use App\Events\PanelEvent;
use App\Events\ParserEvent;
use App\Events\TradeLotsCreated;
use App\Models\FedResurs\ArbitrManager;
use App\Models\FedResurs\CompanyTradeOrganizer;
use App\Models\FedResurs\DebtorCompany;
use App\Models\FedResurs\DebtorPerson;
use App\Models\FedResurs\Message;
use App\Models\FedResurs\Organization;
use App\Models\Option;
use App\Models\Trade;
use App\Models\TradeLot;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use MetaSyntactical\GoogleDirections\Polyline;
use MetaSyntactical\GoogleDirections\RouteFactory;
use MetaSyntactical\Http\Transport\Guzzle\GuzzleTransport;
use MetaSyntactical\GoogleDirections\Client as GoogleApiClient;
use GuzzleHttp\Client as HttpClient;
use Nathanmac\Utilities\Parser\Facades\Parser;
use function GuzzleHttp\Psr7\str;

class ParserFedResurs extends Command
{
    use ParserFedResursEvents, ParserFedResursSoap, ParserFedResursServiceMessages;

    protected $signature = 'parser:fedresurs {task} {--id=0} {--dateMin=0} {--dateMax=0} {--idMin=0} {--idMax=0} {--TradeId=0} {--TradePlaceINN=0} {--n=0} {--c=0} {--file=0} {--debug=0} {--lastHours=0}';
    protected $description = 'fedresurs.ru parser https://bankrot.fedresurs.ru/help/Service_1.30.pdf';

    const
        //WSDL_FILE = __DIR__ . '/ParserFedResurs/WebService_test.xml',
//        WSDL_FILE = __DIR__ . '/ParserFedResurs/WebService.xml',
        WSDL_FILE = __DIR__ . '/ParserFedResurs/WebServiceNew.xml',
        DISK_ARCHIVE_NAME = 'fedresurs_archives';

    /** @var \SoapClient */
    private $client;

    // исравление проблем с xml площадок
    static public $fix_xml = [
        'SOAP-ENV:' => 'soap:',
        /*'soap12:' => 'soap:',
        'soap12=' => 'soap=',*/
        ':SOAP-ENV' => ':soap',
        'http://schemas.xmlsoap.org/soap/envelope/' => 'http://www.w3.org/2003/05/soap-envelope',
        '<ns1:' => '<',
        '</ns1:' => '</',
        //'env:Body>' => 'Body>',
        'xmlns="http://bankrot.fedresurs.ru/BiddingService2"' => '',
        '<env:Header />' => '',
        'xsi:nil="true"' => 'nil="true"',
    ];

    public function handle()
    {
        $start_time = now();
        $task = $this->argument('task');
        $method_name = 'task' . ucfirst($task);
        try {
            if (method_exists($this, $method_name)) {
                $this->info("Method '{$method_name}'");
                $this->{$method_name}();
            } else {
                $this->error("Method '{$method_name}' not found");
            }
        } catch (\Exception $err) {
            event(new ParserEvent('fail', ['message' => $err->getMessage()]));
            var_dump($err->getMessage());
        }
        $this->info($start_time->diff(now())->format('Task time: %hh %imin %ssec'));
    }


    # tasks
    protected function taskTest()
    {
        Trade::orderBy('id', 'DESC')->chunk(1000, function ($trades) {
            /** @var Trade $trade */
            foreach ($trades as $trade) {
                $this->line('---');
                $this->info('Trade: ' . $trade->TradeId . ' [' . $trade->id . ']');
                $trade->messages()->each(function (Message $msg) use ($trade) {
                    $msg->id;
                    $style = $msg->hasActionMethod($msg->MessageType) ? 'fg=yellow' : 'fg=red';
                    $this->line('Message: ' . $msg->id . ': ' . $msg->MessageType, $style);
                    try {
                        $msg->applyMessage(['TradeId' => $trade->TradeId]);
                    } catch (\Exception $err) {
                        $this->error($err->getMessage());
                        $this->warn($err->getTraceAsString());
                        dump($msg->MessageInfo, $msg->trade->TradeId);
                        $this->ask('Continue?');
                    }
                });
            }
        });
    }

    public function taskTmp()
    {
        $start = '16.11.2020 10:33';
        $end = '16.11.2020 10:35';
        //$this->taskCheckMessages($start, $end);

        $start = '16.11.2020 9:05';
        $end = '16.11.2020 18:12';
        $this->taskCheckTradeMessages($start, $end);
    }

    /**
     * Кнопка запустить парсер
     */
    protected function taskRun()
    {
        $this->updateAllLists();

        $startTime = Carbon::now();
        event(new ParserEvent('started'));
        event(new ParserEvent('status', 'started at ' . $startTime . '...'));
        event(new ParserEvent('progress', ['progress' => 10]));

        try {
            event(new ParserEvent('status', 'updating messages...'));
            $this->taskCheckMessages();

            event(new ParserEvent('status', 'updating trade messages...'));
            $this->taskCheckTradeMessages();

            event(new ParserEvent('status', 'updating statuses...'));
            $this->taskAllParseTradeMessagesByTrade($startTime->format('Y-m-d H:i:s'));

            event(new ParserEvent('status', 'updating categories...'));
            Log::channel('lotcategory')->info('PARSER CRON-LAUNCHED for time '.$startTime->format('Y-m-d H:i:s'));
            Artisan::call('parser:params', ['--dateMin' => $startTime->format('Y-m-d H:i:s')]);

            event(new ParserEvent('progress', ['progress' => 100]));
            event(new ParserEvent('ended', "Done"));
        } catch (\Exception $err) {
            $this->error($err->getMessage());
            $this->error($err->getTraceAsString());
            event(new ParserEvent('fail', $err->getMessage()));
        }
    }

    protected function taskCron()
    {
        $startTime = Carbon::now();
        event(new ParserEvent('started'));
        event(new ParserEvent('status', 'started at ' . $startTime->format('Y-m-d H:i:s') . '...'));
        event(new ParserEvent('progress', ['progress' => 10]));

        $this->updateAllLists();

        event(new ParserEvent('status', 'updating messages...'));
        $this->taskCheckMessages();

        event(new ParserEvent('status', 'updating trade messages...'));
        $this->taskCheckTradeMessages();

        event(new ParserEvent('status', 'updating statuses...'));
        $this->taskAllParseTradeMessagesByTrade($startTime->format('Y-m-d H:i:s'));

        event(new ParserEvent('status', 'updating categories...'));
        Log::channel('lotcategory')->info('PARSER CRON-LAUNCHED for time '.$startTime->format('Y-m-d H:i:s'));
        Artisan::call('parser:params', ['--dateMin' => $startTime->format('Y-m-d H:i:s')]);

        event(new ParserEvent('progress', ['progress' => 100]));
        event(new ParserEvent('ended', "Done"));

        event(new TradeLotsCreated());
    }

    /**
     * Обновление всех данных
     */
    protected function taskUpdateLists()
    {
        $this->updateAllLists();
    }

    /**
     * Парсинг всех лотов, дату вписывае ручками, все проверки соблюдены, использовать в крайних случаях
     * @throws \SoapFault
     */
    protected function taskAllParseTradeMessagesByTrade($startTime = null) {

        $this->initClient();

        $query = Trade::where('status', '!=','BiddingCanceled');

        if ($id = $this->option('id')) {
            $query->where('id', $id);
        }
        if ($idMin = $this->option('idMin')) {
            $query->where('id', '>=', $idMin);
        }
        if ($idMax = $this->option('idMax')) {
            $query->where('id', '<=', $idMax);
        }
        if ($TradeId = $this->option('TradeId')) {
            $query->where('TradeId', $TradeId);
        }
        if ($TradePlaceINN = $this->option('TradePlaceINN')) {
            $query->where('TradePlaceINN', $TradePlaceINN);
        }

        if ($dateMax = $this->option('dateMax')) {
            $query->where('created_at', '<=', $dateMax);
        }

        $this->line("\n\nRUN: taskAllParseTradeMessagesByTrade " . now()->toString());
        if ($startTime) {
            $this->line("From startTime: " . $startTime);
            $query->where('created_at', '>=', $startTime);
        } elseif ($dateMin = $this->option('dateMin')) {
            $this->line("From dateMin: " . $dateMin);
            $query->where('created_at', '>=', $dateMin);
        } else {
            $this->line("From default date: 2020-08-19");
            $query->where('created_at', '>=', '2020-08-19');
        }


        $query->orderBy('id')
            ->chunk(100, function($models){

            foreach($models as $trade) {
                $res = $this->getTradeMessagesByTrade($trade->TradeId, $trade->TradePlaceINN, $trade->EventTime->format('Y-m-d'));
                
                $message_arr = json_decode(json_encode($res), true);

                $trade_place =  data_get($message_arr, 'GetTradeMessagesByTradeResult.TradePlace');
                $message_arr = data_get($message_arr, 'GetTradeMessagesByTradeResult.TradePlace.TradeList.Trade.MessageList.TradeMessage');
                if(!empty($message_arr['ID'])) {
                    $message_arr = [ $message_arr ];
                }

                $message_arr = data_get($message_arr, '*.ID', []);

                sort($message_arr);

                if (empty($message_arr)) {
                    $this->warn("No trade messages for id: {$trade->id}. TradeId: {$trade->TradeId} TradePlaceINN: {$trade->TradePlaceINN}");
                    Log::channel('lotstatus')->info("Error. No trade messages for id: {$trade->id}. TradeId: {$trade->TradeId} TradePlaceINN: {$trade->TradePlaceINN}");
                }

                foreach($message_arr as $TradeMessage_ID) {

                    try {
                        $xml_body_message = $this->getTradeMessageContentXml($TradeMessage_ID);
                        if (empty($xml_body_message)) {
                            $this->error('$xml_body_message is empty');
                            $this->askContinue($TradeMessage_ID . "  TradeId: {$trade->TradeId}  TradePlaceINN: {$trade->TradePlaceINN}");
                        }
                        $this->parseTradeMessageBody($TradeMessage_ID, $xml_body_message, $trade->TradePlaceINN, $trade_place['Name'], true, $trade);
                    } catch (\Exception $err) {
                        $this->error($err->getMessage() . $err->getTraceAsString());
                        $this->askContinue($err->getTraceAsString());
                    } catch (\Throwable $err) {
                        Log::channel('lotstatus')->info('Error. Lot id: ' . $trade->id . ' Message: '. $err->getMessage());

                        dump($err, $message_arr, $res);
                    }

                }
                $this->info("id:{$trade->id}:Получилось ----------");
            }

        });




    }

    /**
     * Менее трудозатратный парсер, парсит по от даты по 10 дней, такжедата забивается ручками
     */
    protected function taskNewParseTradeMessagesByDate() {

        $start_time = now();

        $inception_date = Carbon::create(2020, 8, 1);

        while(!$inception_date->isFuture()) {

            try {

                $end_date = $inception_date->copy()->addDays(10);
                $inception_format = $inception_date->format('Y-m-d');
                $end_format = $end_date->format('Y-m-d');

                $this->taskCheckTradeMessages($inception_format, $end_format);

                $this->info("Пройдена дата от: {$inception_format} по {$end_format}");

                $inception_date = $end_date;


            } catch (\Exception $err) {

                var_dump($err->getMessage());
            }
            $this->info($start_time->diff(now())->format('Task time: %hh %imin %ssec'));
        }



    }

    /**
     * получить и обработать сообщения о торгах
     * @param null $start
     * @param null $end
     */
    protected function taskCheckTradeMessages($start = null, $end = null)
    {
        $endDate = null;
        try {
            $this->initClient();

            if(!$start){
                $lastHours = (int)$this->option('lastHours');
                /** @var Option $lastTime */
                $lastTime = Option::get('parser.taskCheckTradeMessages.lastTime');
                if ($lastHours) { // --lastHours=24
                    $startFrom = now()->addHours($lastHours * -1);
                } else {
                    // last sync time
                    if (!empty($lastTime->value)) {
                        $startFrom = Carbon::parse($lastTime->value);
                    } else {
                        $startFrom = Carbon::parse('-239 hours'); // не более 10 дней назад
                    }
                }
                $lastTime->setValue(now()->toIso8601String()); // save time sync
            } else {
                $startFrom = Carbon::parse($start);
                if($end){
                    $endDate = Carbon::parse($end)->toIso8601String();
                }
            }

            $this->line("\n\nRUN: taskCheckTradeMessages " . now()->toString());
            $this->line("From time: " . $startFrom->toString());
            $res = $this->getTradeMessages($startFrom->toIso8601String(), $endDate);

            if (isset($res->TradePlace)) foreach (self::getListElements($res->TradePlace) as $TradePlace) {
                $TradePlaceINN = (string)$TradePlace->INN;

                $Name = (string)$TradePlace->Name;

//                if( $Name !== 'Межрегиональная Электронная Торговая Система') {
//                    continue;
//                }

                $Site = (string)$TradePlace->Site;
                $OwnerName = (string)$TradePlace->OwnerName;
                $this->info("----------\nTrade INN: {$TradePlaceINN} - {$OwnerName}\n{$Name} ({$Site})");

                if ($place_model = CompanyTradeOrganizer::find($TradePlaceINN)) {
                    $place_model->site = $Site;
                    $place_model->save();
                };

                $messages_arr = $this->getTradePlaceMessageArr($TradePlace);
                foreach ($messages_arr as $message_item) {
                    $TradeMessage_ID = $message_item->TradeMessage_ID;
                    $TradePlace_INN = $message_item->TradePlace_INN;
                    $TradePlace_Name = $message_item->TradePlace_Name;
                    try {
                        $xml_body_message = $this->getTradeMessageContentXml($TradeMessage_ID);
                        if (empty($xml_body_message)) {
                            $this->error('$xml_body_message is empty');
                            $this->askContinue($TradeMessage_ID);
                        }
                        $this->parseTradeMessageBody($TradeMessage_ID, $xml_body_message, $TradePlace_INN, $TradePlace_Name, true);
                    } catch (\Exception $err) {
                        $this->error($err->getMessage());
                        $this->askContinue($err->getTraceAsString());
                    }
                }

            }

            //$this->askContinue($res);
            $this->info("----------");
        } catch (\Exception $err) {
            $this->error($err->getMessage());
            $this->askContinue($err->getTraceAsString());
        }
    }

    /**
     * получить и обработать сообщения от сервиса
     * @param null $start период с
     * @param null $end период до
     * @throws \SoapFault
     */
    protected function taskCheckMessages($start = null, $end = null)
    {
        $endDate = null;
        $this->initClient();

        if(!$start){
            $lastHours = (int)$this->option('lastHours');
            /** @var Option $lastTime */
            $lastTime = Option::get('parser.taskCheckMessages.lastTime');
            if ($lastHours) { // --lastHours=24
                $startFrom = now()->addHours($lastHours * -1);
            } else {
                // last sync time
                if (!empty($lastTime->value)) {
                    $startFrom = Carbon::parse($lastTime->value);
                } else {
                    $startFrom = Carbon::parse('-99 days');
                }
            }
            $lastTime->setValue(now()->toIso8601String()); // save time sync
        } else {
            $startFrom = Carbon::parse($start);
            if($end){
                $endDate = Carbon::parse($end)->toIso8601String();
            }
        }

        $this->line("\n\nRUN: taskCheckTradeMessages " . now()->toString());
        $this->line("From time: " . $startFrom->toString());
        $message_ids = (array)$this->getMessageIds($startFrom->toIso8601String(), $endDate);

        $model_ns = '\\App\\Models\\FedResurs\\';
        foreach ($message_ids as $message_id) {
            try {
                $xml_msg = $this->getMessageContentXml($message_id);
                $msg_content = Parser::xml($xml_msg->asXML());
                /** @var \SimpleXMLElement $MessageInfo */
                $MessageInfo = current($xml_msg->xpath('MessageInfo'));
                $MessageInfoAttrs = $MessageInfo->attributes();
                $MessageType = (string) $MessageInfoAttrs["MessageType"];
                $PublishDate = (string) $xml_msg->PublishDate;
                $this->output->write("{$PublishDate}: CaseNumber: {$xml_msg->CaseNumber}, Number: {$xml_msg->Number}, MessageType: ");

                $method_name = "processMessage{$MessageType}";
                if (method_exists($this, $method_name)) {
                    $this->info("{$MessageType}");
                } else {
                    $this->warn("{$MessageType}");
                }

                $this->parseServiceMessageContentXml($xml_msg);

                if ($MessageType === 'Auction') { // save to file archive
                    $msg_number = (int) $xml_msg->Number;
                    Storage::disk('messages')
                        ->put("msg_no_{$msg_number}.xml", $xml_msg->asXML());
                }
                if (false) { // save to file archive
                    Storage::disk('messages')
                        ->put("{$msg_content['BankruptId']}/msg_{$message_id}.xml", $xml_msg->asXML());
                }
            } catch (\Exception $err) {
                $this->error($err->getMessage());
                $this->askContinue($err->getTraceAsString());
                $this->askContinue($message_id);
            }

            // todo: переписать на xml
            /*$msg_content_xml = $this->getMessageContentXml($message_id);
            dd($msg_content, $msg_content_xml);*/
            try {
                /** @var Message $msg */
                $msg = Message::findOrNew($msg_content['Id']);
                $msg->fillFromObj($msg_content);
                if ($msg->save()) {

                    // bankrupt
                    if (isset($msg_content['BankruptInfo'])) {
                        $BankruptType = $msg_content['BankruptInfo']["@BankruptType"];
                        $BankruptContent = $msg_content['BankruptInfo'][($BankruptType == 'Organization' ? 'BankruptFirm' : 'BankruptPerson')];
                        $BankruptInfoID = $BankruptContent['@Id'];
                        $model_path = $model_ns . $BankruptType;
                        $bankrupt = $model_path::findOrNew($BankruptInfoID);
                        $bankrupt->fillFromObj($BankruptContent);
                        $bankrupt->save();
                    }

                    //$msg->applyMessage(['TradeId' => $TradeId]);
                } else {
                    $this->error('Message model do not saved');
                    $this->askContinue($msg_content);
                }

            } catch (\Exception $err) {
                $this->error($err->getMessage());
                $this->error($err->getTraceAsString());
                $this->askContinue($msg_content);
            }

        }
    }

    /**
     * распаковывает архив старых сообщений из архива
     */
    protected function taskArchiveUnpack()
    {
        try {

            # 1.  Нарезка архива на сообщения
            $files = $this->getFileListXmlArchiveMessages();

            $this->info("Нарезка архива на сообщения ...");
            $bar = $this->output->createProgressBar(count($files));
            $bar->start();

            foreach ($files as $file) {
                //$this->info("File: \"{$file}\"");
                try {
                    $full_path = Storage::disk(self::DISK_ARCHIVE_NAME)->path($file);
                    $TradePlaceList = simplexml_load_file($full_path, 'SimpleXMLElement', LIBXML_COMPACT | LIBXML_PARSEHUGE);
                } catch (\Exception $err) {
                    $this->error($err->getMessage());
                    $this->askContinue($err->getTraceAsString());
                    continue;
                }
                /** @var \SimpleXMLElement $TradePlace */
                foreach ($TradePlaceList->xpath('.//TradePlace') as $TradePlace) {
                    // площадки
                    $TradePlaceAttrs = $TradePlace->attributes();
                    $TradePlaceINN = $TradePlaceAttrs['INN'];
                    //$this->line($TradePlace->getName() . ': ' . $TradePlaceINN);
                    foreach ($TradePlace->xpath('.//TradeList/Trade') as $Trade) {
                        $TradeAttrs = $Trade->attributes(); // ID_EXTERNAL, ID_EFRSB
                        $ID_EXTERNAL = $TradeAttrs['ID_EXTERNAL'];
                        // сообщения тогров
                        foreach ($Trade->xpath('.//Message') as $Message) {
                            $MessageAttrs = $Message->attributes();
                            $message_id = (int)$MessageAttrs['ID'];
                            Storage::disk('trade_messages')
                                ->put("{$TradePlaceINN}/{$ID_EXTERNAL}/msg_{$message_id}.xml", $Message->asXML());
                        }
                    }
                }
                $bar->advance();
            }

            $bar->finish();
            $this->line('');

        } catch (\Exception $err) {
            $this->error($err->getMessage());
            $this->error($err->getTraceAsString());
        }
    }

    protected function taskArchiveParseAM(){
        $directories = Storage::disk(self::DISK_ARCHIVE_NAME)->allDirectories('archive');
        foreach ($directories as $dir_name) {
            $file = Storage::disk(self::DISK_ARCHIVE_NAME)->path($dir_name.DIRECTORY_SEPARATOR.'AMList.xml');
            $xml = simplexml_load_file($file, 'SimpleXMLElement', LIBXML_COMPACT | LIBXML_PARSEHUGE);
            foreach ($xml->children() as $item) {
                /** @var ArbitrManager $man */
                ArbitrManager::updateOrCreate([
                    'INN' => (string) $item['INN'],
                ], [
                    'FirstName' => (string) $item['FirstName'],
                    'MiddleName' => (string) $item['MiddleName'],
                    'LastName' => (string) $item['LastName'],
                    'RegNum' => (string) $item['RegNum'],
                    'SRORegNum' => (string) $item['SRORegNum'],
                    'DateLastModif' => Carbon::parse((string) $item['DateLastModif']),
                    'Region' => (string) $item['Region'],
                    'DateReg' => Carbon::parse((string) $item['DateReg']),
                ]);
            }
        }
    }

    /**
     * парсинг сообщений из архива
     */
    protected function taskArchiveParse()
    {
        $this->taskArchiveParseAM();
        $this->initClient();
        try {
            $storage = Storage::disk('trade_messages');
            $dirs = scandir($storage->path(''));
            unset($dirs[0]); // .
            unset($dirs[1]); // ..

            // todo: temporary
            /*$dirs = ['10631', '048097', 'ПП-4394', 'ПП-13007', 'ПП-12942'];
            $dirs = [last($dirs)];*/

            $this->info("Обработка архива сообщений ...");
            $bar = $this->output->createProgressBar(count($dirs));
            $bar->start();

            $n = 0;
            foreach ($dirs as $dir) {
                if ($this->option('debug') === '0') {
                    $bar->advance();
                }
                /*if(++$n < 18404){ // 32000
                    continue;
                }*/
                $this->processArchiveDirTradeMsgs($dir);

                #sub dirs
                $sub_dirs = $storage->allDirectories($dir);
                foreach ($sub_dirs as $sub_dir) {
                    $this->processArchiveDirTradeMsgs($sub_dir);
                }
            }

            $bar->finish();
            $this->line('');

        } catch (\Exception $err) {
            $this->error($err->getMessage());
            $this->error($err->getTraceAsString());
        }
    }

    /**
     * обработка всех сообщений торгов из директории
     * @param $dir
     * @return null
     */
    public function processArchiveDirTradeMsgs($dir)
    {
        $storage = Storage::disk('trade_messages');
        $files = $storage->files($dir);
        if (empty($files)) {
            return null;
        }

        $trade_messages = [];
        $annulment_messages = [];
        //$this->warnDebug("Trade dir: " . $dir);
        foreach ($files as $file) {
            // "003852/7/msg_8727023_5262258084.xml"
            if (preg_match("/^(?P<place>\d+)\/(?P<trade>.*)\/msg_(?P<msg>\d+)\.xml$/", $file, $res)) {
                //$this->infoDebug(sprintf("Trade: %s, Msg: %s Place: %s", $res['trade'], $res['msg'], $res['place']));

                $xml = simplexml_load_file($storage->path($file), 'SimpleXMLElement', LIBXML_COMPACT | LIBXML_PARSEHUGE);

                // clearing
                $xml_str = str_replace(array_keys(self::$fix_xml), array_values(self::$fix_xml), $xml->asXML());
                $xml = simplexml_load_string($xml_str, 'SimpleXMLElement', LIBXML_COMPACT | LIBXML_PARSEHUGE);

                try {
                    $ns = self::registerXPathNamespace($xml);
                    $body = $xml->xpath('.//' . $ns . ':Body');
                } catch (\Throwable $err){
                    $this->error($err->getMessage());
                    continue;
                } catch (\Exception $err){
                    $this->error($err->getMessage());
                    continue;
                }
                if (empty($body)) {
                    $this->error('Body is empty');
                    $this->askContinue($xml->asXML());
                    continue;
                }
                //$soap_method = $body[0]->children()->getName();
                try {
                    if (empty($body[0]->children()) || empty($body[0]->children()->children())) {
                        $this->error('$body[0] empty');
                        $this->askContinue($xml);
                    }
                    $soap_event = $body[0]->children()->children()->getName();
                } catch (\Exception $err){
                    $this->error('$body[0][0] empty');
                    continue;
                }
                $trade_messages[] = (object)[
                    'PlaceINN' => $res['place'],
                    'TradeId' => $res['trade'],
                    'MessageId' => $res['msg'],
                    'ns' => $ns,
                    //'soap_method' => $soap_method,
                    'soap_event' => $soap_event,
                    'xml_body_str' => $body[0]->children()->asXML(),
                ];
                if ($soap_event === 'AnnulmentMessage') {
                    $ID_Annulment = (string)$body[0]->children()->children()->AnnulmentMessage->ID_Annulment;
                    $annulment_messages[] = $res['msg'];
                    $annulment_messages[] = $ID_Annulment;
                }
            } else {
                $this->warn('Bad file mask: ' . $file);
                $this->askContinue();
            }
        }

        # filtering annulment messages
        $trade_messages = array_filter($trade_messages, function ($item) use ($annulment_messages) {
            return !in_array($item->MessageId, $annulment_messages);
        });

        foreach ($trade_messages as $trade_message) {
            $this->parseTradeMessageBody($trade_message->MessageId, $trade_message->xml_body_str, $trade_message->PlaceINN);
            //$this->askContinue();
        }

    }


    /**
     * обновление всех реестров
     */
    protected function updateAllLists()
    {
        event(new ParserEvent('started'));
        try {
            $this->initClient();
            event(new ParserEvent('progress', ['progress' => 10]));

            event(new ParserEvent('status', 'updating list company trade organizer'));
            $this->updateListCompanyTradeOrganizer();
            event(new ParserEvent('progress', ['progress' => 50]));

            event(new ParserEvent('status', 'updating list arbitr manager'));
            $this->updateListArbitrManager();
            event(new ParserEvent('progress', ['progress' => 70]));

            event(new ParserEvent('status', 'updating list debtor'));
            $this->updateListDebtor();
            event(new ParserEvent('progress', ['progress' => 90]));

            //...

            event(new ParserEvent('progress', ['progress' => 100]));
            event(new ParserEvent('ended', "Done"));
        } catch (\Exception $err) {
            $this->error($err->getMessage());
            $this->error($err->getTraceAsString());
            event(new ParserEvent('fail', $err->getMessage()));
        }
    }

    protected function updateListCompanyTradeOrganizer()
    {
        $lastHours = (int)$this->option('lastHours');
        /** @var Option $lastTime */
        $lastTime = Option::get('parser.updateListCompanyTradeOrganizer.lastTime');
        if ($lastHours) { // --lastHours=24
            $startFrom = now()->addHours($lastHours * -1);
        } else {
            // last sync time
            if (!empty($lastTime->value)) {
                $startFrom = Carbon::parse($lastTime->value);
            } else {
                $startFrom = now()->addDays(-100000);
            }
        }

        $this->line("\n\nRUN: updateListCompanyTradeOrganizer " . now()->toString());
        $this->line("From time: " . $startFrom->toString());

        // todo: develop
        $lastTime->setValue(now()->toIso8601String()); // save time sync
        $res = $this->getCompanyTradeOrganizerRegister($startFrom->toIso8601String());
        $organizations = data_get($res, 'TradeOrganizerList.TradeOrganizer');
        if (is_object($organizations)) {
            $organizations = [$organizations];
        } elseif (is_null($organizations)) {
            $organizations = [];
        }

        $bar = $this->output->createProgressBar(count($organizations));
        $bar->start();

        foreach ($organizations as $item) {
            $org = CompanyTradeOrganizer::findOrNew($item->INN);
            $org->fillFromObj($item)->save();
            //$this->line(($org->INN ? 'Updated' : 'Added')." item INN: {$item->INN}");
            $bar->advance();
        }
        $bar->finish();
        $this->line('');
    }

    protected function updateListArbitrManager()
    {
        $lastHours = (int)$this->option('lastHours');
        /** @var Option $lastTime */
        $lastTime = Option::get('parser.updateListArbitrManager.lastTime');
        if ($lastHours) { // --lastHours=24
            $startFrom = now()->addHours($lastHours * -1);
        } else {
            // last sync time
            if (!empty($lastTime->value)) {
                $startFrom = Carbon::parse($lastTime->value);
            } else {
                $startFrom = now()->addMonths(-6);
            }
        }

        $this->line("\n\nRUN: updateListArbitrManager " . now()->toString());
        if ($startFrom !== null) {
            $this->line("From time: " . $startFrom);
        }

        $lastTime->setValue(now()->toIso8601String()); // save time sync
        $res = $this->getArbitrManagerRegister($startFrom->toISOString());
        $managers = (array)data_get($res, 'AMList.ArbitrManager');

        $bar = $this->output->createProgressBar(count($managers));
        $bar->start();

        $n = 0;
        $count = count($managers);
        foreach ($managers as $item) {
            if (is_object($item) && isset($item->ArbitrManagerID)) {
                /** @var ArbitrManager $man */
                $man = ArbitrManager::findOrNew($item->ArbitrManagerID);
                $man->fillFromObj($item)->save();
                $bar->advance();
                if ((++$n) % 10 === 0) {
                    event(new ParserEvent('status', "updating list arbitr manager: {$n}/{$count}"));
                }
            }
        }
        $bar->finish();
        $this->line('');
    }

    protected function updateListDebtor()
    {

        $lastHours = (int)$this->option('lastHours');
        /** @var Option $lastTime */
        $lastTime = Option::get('parser.updateListDebtor.lastTime');
        if ($lastHours) { // --lastHours=24
            $startFrom = now()->addHours($lastHours * -1);
        } else {
            // last sync time
            if (!empty($lastTime->value)) {
                $startFrom = Carbon::parse($lastTime->value);
            } else {
                $startFrom = now()->addDays(-100);
            }
        }

        $this->line("\n\nRUN: updateListDebtor " . now()->toString());
        $this->line("From time: " . $startFrom->toString());


        $lastTime->setValue(now()->toIso8601String()); // save time sync
        $res = $this->getDebtorRegister($startFrom->toIso8601String());

        $DebtorList = data_get($res, 'DebtorList');

        if (isset($DebtorList->DebtorPerson)) {
            if (!is_array($DebtorList->DebtorPerson)) {
                $DebtorPersonList = [];
                $DebtorPersonList[] = $DebtorList->DebtorPerson;
            } else {
                $DebtorPersonList = (array)$DebtorList->DebtorPerson;
            }
            $bar = $this->output->createProgressBar(count($DebtorPersonList));
            $bar->start();
            foreach ($DebtorPersonList as $item) {
                if (empty($item->INN)) {
                    continue;
                }
                try {
                    unset($item->Guid);

                    /** @var DebtorPerson $debtor */
                    $debtor = DebtorPerson::firstOrNew(['BankruptId' => data_get($item, 'BankruptId')]);
                    $debtor->fillFromObj($item)->save();
                    $bar->advance();
                } catch (\Exception $err) {
                    $this->error($err->getMessage());
                    $this->askContinue($err->getTraceAsString());
                    $this->askContinue($item);
                }
            }
            $bar->finish();
            $this->line('');
        }


        if (isset($DebtorList->DebtorCompany)) {
            if (!is_array($DebtorList->DebtorCompany)) {
                $DebtorCompanyList = [$DebtorList->DebtorCompany];
            } else {
                $DebtorCompanyList = (array)$DebtorList->DebtorCompany;
            }
            $bar = $this->output->createProgressBar(count($DebtorCompanyList));
            $bar->start();

            foreach ($DebtorCompanyList as $item) {
                if (empty($item->INN) || $item->INN == '0000000000') {
                    continue;
                }
                try {
                    unset($item->Guid);

                    /** @var DebtorCompany $debtor */
                    $debtor = DebtorCompany::firstOrNew(['BankruptId' => data_get($item, 'BankruptId')]);
                    $debtor->fillFromObj($item)->save();
                    $bar->advance();
                } catch (\Exception $err) {
                    $this->error($err->getMessage());
                    $this->askContinue($err->getTraceAsString());
                    $this->askContinue($item);
                }
            }
            $bar->finish();
            $this->line('');
        }
    }

    # parsing

    /**
     * регистрация прастранства имен xml
     * @param \SimpleXMLElement $message
     * @return string пространство имен тега Body
     */
    private static function registerXPathNamespace(\SimpleXMLElement &$message)
    {
        $str = $message->asXML();
        preg_match("/<(?P<soap_ns>[^:<>\s]+):Envelope (?P<str>[^>]+)>/", $str, $envelope_str);
        $soap_ns = $envelope_str['soap_ns'];
        preg_match_all("/xmlns:(?P<ns>[^=\s]+)=\"(?P<url>[^\"]+)\"/", $envelope_str['str'], $xmlns_ns);
        for ($n = 0; $n < count($xmlns_ns[0]); $n++) {
            $message->registerXPathNamespace($xmlns_ns[1][$n], $xmlns_ns[2][$n]);
            //dump('NS: ' . $xmlns_ns[1][$n] . " = " . $xmlns_ns[2][$n]);
        }
        //dump($soap_ns);
        return $soap_ns;
    }

    /**
     * Обработка тела сообщения о тограх
     * @param int $message_id
     * @param string|\SimpleXMLElement $xml_body_message
     * @param null $TradePlaceINN
     * @param null $TradePlace_Name
     * @param bool $save_xml_body зохраняет сообщение как файл
     */
    private function parseTradeMessageBody(int $message_id, $xml_body_message, $TradePlaceINN = null, $TradePlace_Name = null, $save_xml_body = false, $trade_passed = null)
    {
        if ($xml_body_message instanceof \SimpleXMLElement) {
            /** @var \SimpleXMLElement $xml */
            $xml = $xml_body_message;
        } else {
            try {
                $xml_body_message = str_replace('xsi:nil="true"', 'nil="true"', $xml_body_message);
                $xml = new \SimpleXMLElement($xml_body_message);
            } catch (\Exception $err) {
                Log::channel('lotstatus')->info('Error load Message '. ($trade_passed? ' for id: '.$trade_passed->id :'') . ': ' . $err->getMessage());

                $this->error("Error load xml: " . $err->getMessage());
                $this->askContinue($err->getTraceAsString());
                $this->askContinue($xml_body_message);
                return;
            }
        }

        $method = $xml->getName();
        if (empty($method)) {
            $this->error(" Method is empty");
            dump($xml_body_message);
            return;
        }
        $event_name = $xml->children()->getName();
        $coll_method = "event{$event_name}";
        $TradeId = (string)$xml->children()->attributes()['TradeId'];
        /**
         * Возможно уже передали модель
         */
        if (!$trade_passed) {
            $trade_model = Trade::firstOrNew([
                'TradeId' => $TradeId,
                'TradePlaceINN' => $TradePlaceINN,
            ]);
        } else {
            $trade_model = $trade_passed;
        }

        if (!method_exists($this, $coll_method)) {
            Log::channel('lotstatus')->info('Method ' . $coll_method . ' not exists for id: ' . $trade_model->id);

            $this->error(" Method '{$coll_method}' not exists");
            $this->askContinue($xml_body_message);
        } else {
            $EventTime = Carbon::parse((string)$xml->children()->attributes()['EventTime']);

            /** @var Trade $trade_model */

            /**
             * Новая проверка, чтобы сообщения записывались у которых время эвента больше
             */
            //старая версия проверки
            //if($trade_model->EventTime > $EventTime) {
            //    return;
            //}

            //Более точная проверка, лучше для обновления уже существующих лотов
            if ($trade_passed) {
                $carbonTradeModelEventTime = Carbon::createFromFormat('Y-m-d H:i:s', $trade_model->EventTime);
                $carbonEventTime = Carbon::createFromFormat('Y-m-d H:i:s', $EventTime);
                if($carbonTradeModelEventTime && $carbonTradeModelEventTime > $carbonEventTime) {
                    //Log::channel('lotstatus')->info('Wrong eventTime for id: ' . $trade_model->id);
                    return;
                }
            }

            if (!in_array($event_name, ['AnnulmentMessage'])) {
                $trade_model->EventTime = $EventTime;
            }

            // Правила
            if ($event_name !== 'BiddingInvitation' && !$trade_model->status) {
                Log::channel('lotstatus')->info('No BiddingInvitation for TradeId: ' . $TradeId . ' for TradePlaceINN: ' . $TradePlaceINN . (($trade_model && $trade_model->id)?' for id: ' . $trade_model->id:''));

                // todo: develop
                $this->warn("TradeId: {$TradeId}, Message: {$message_id}, Event:'{$event_name}' - нет сообщения о начале торгов");
                //$this->askContinue($xml_body_message);
                //$this->loadAllTradeMessage($TradeId, $TradePlaceINN);
                return;
            }
            $this->output->write($EventTime.': Trade: ' . $TradeId . ', Message: ' . $message_id . ', Event:' . $event_name . ' - ');
            if ($trade_model->status === 'BiddingCanceled') {
                Log::channel('lotstatus')->info('BiddingCanceled for id: ' . $trade_model->id);

                $this->warn("{$EventTime}: Trade: {$TradeId}, Message: {$message_id}, Event:'{$event_name}' - Торги уже закрыты !!!");
                return;
            }
            /*if ($trade_model->status === 'BiddingDeclared' && $event_name = 'AnnulmentMessage') {
                $trade_model->annulment()->save();
                return;
            }*/

            // save xml to file archive
            if ($save_xml_body && $xml_body_message instanceof \SimpleXMLElement) {
                Storage::disk('trade_messages')
                    ->put("{$TradePlaceINN}/{$TradeId}/msg_{$message_id}.xml", $xml->asXML());
            }


            // events, post update
            $this->$coll_method($trade_model, $xml, $TradePlace_Name);
            $trade_model->save();

            //$this->askContinue($xml);
        }
    }


    // tools

    /**
     * ставит на паузу и ждет реакции, может вывести данные для дебага
     */
    private function askContinue()
    {
        if ($this->option('c') === '1') {
            return;
        }
        if ($this->option('debug') === '0') {
            return;
        }

        if (empty(func_get_args())) {
            $input = $this->anticipate('Continue?', ['Yes', 'No'], 'Yes');
            if ($input === 'No') {
                dd('stopped');
            }
        } else {
            $input = $this->anticipate('Show details?', ['No, go to next', 'Yes'], 'No, go to next');

            if ($input === 'Yes') {
                $args = func_get_args();
                if (count($args)) {
                    foreach ($args as $arg) {
                        dump($arg);
                    }
                }
                if (!$this->confirm('Do you wish to continue?', true)) dd('stopped');
            }
        }


    }

    private function getFileListXmlArchiveMessages()
    {
        $all_files = [];
        $directories = Storage::disk(self::DISK_ARCHIVE_NAME)
            ->allDirectories('archive');
        foreach ($directories as $dir_name) {
            $files = Storage::disk(self::DISK_ARCHIVE_NAME)->files($dir_name);
            $files = array_filter($files, function ($file) {
                return preg_match("/trade-mes-\d{1,2}-\d{4}_\d{2}.xml/", $file);
            });
            $all_files = array_merge($all_files, $files);
        }
        return $all_files;
    }

    public function infoDebug($string, $verbosity = null)
    {
        if ($this->option('debug') == '0') {
            return null;
        }
        parent::info($string, $verbosity = null);
    }

    public function lineDebug($string, $style = null, $verbosity = null)
    {
        if ($this->option('debug') == '0') {
            return null;
        }
        parent::line($string, $style = null, $verbosity = null);
    }

    public function warnDebug($string, $verbosity = null)
    {
        if ($this->option('debug') == '0') {
            return null;
        }
        parent::warn($string, $verbosity = null);
        //sleep(2);
    }

    public static function getListElements($object)
    {
        if (gettype($object) === 'array') {
            return $object;
        }
        return [$object];
    }

    /**
     * Позвращает массив с информацией о номере сообщения и данные о площадке
     * @param $TradePlace_obj
     * @return array
     */
    private function getTradePlaceMessageArr($TradePlace_obj)
    {
        $result = [];
        foreach (self::getListElements($TradePlace_obj) as $TradePlace) {
            $TradePlace_INN = (string)$TradePlace->INN;
            $TradePlace_Name = (string)$TradePlace->Name;
            foreach (self::getListElements($TradePlace->TradeList) as $TradeList) {
                foreach (self::getListElements($TradeList->Trade) as $Trade) {
                    $Trade_ID_External = $Trade->ID_External;
                    $Trade_ID_EFRSB = $Trade->ID_EFRSB;
                    foreach (self::getListElements($Trade->MessageList) as $MessageList) {
                        foreach (self::getListElements($MessageList->TradeMessage) as $TradeMessage) {
                            $TradeMessage_ID = (string)$TradeMessage->ID;
                            $result[] = (object)compact(
                                'TradeMessage_ID',
                                'TradePlace_INN',
                                'TradePlace_Name',
                                'Trade_ID_External',
                                'Trade_ID_EFRSB'
                            );
                        }
                    }
                }
            }
        }
        return $result;
    }


}

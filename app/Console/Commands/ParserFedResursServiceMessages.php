<?php


namespace App\Console\Commands;

use App\Models\Trade;
use App\Models\TradeLot;
use App\Services\MessageAuctionService;
use Carbon\Carbon;


/**
 * Trait ParserFedResursServiceMessages
 * @package App\Console\Commands
 * @doc Сервис передачи реестров, торгов, отчетов АУ и опубликованных сообщений
 */
trait ParserFedResursServiceMessages
{

    // 5.1.7.1 Типы сообщений
    public static $message_types = [
        'Auction' => 'Объявление о проведении торгов',
        'TradeResult' => 'Сведения о результатах торгов',
        'TransferAssertsForImplementation' => 'Сведения о передаче имущества на реализацию',
        'Annul' => 'Сведения об аннулировании ранее опубликованных сообщений',
        'SaleContractResult' => 'Сведения о заключении договора купли-продажи',
        'SaleContractResult2' => 'Сведения о заключении договора купли-продажи (версия 2)',
        'SaleOrderPledgedProperty' => 'Об определении начальной
                                        продажной цены, утверждении
                                        порядка и условий проведения
                                        торгов по реализации предмета
                                        залога, порядка и условий
                                        обеспечения сохранности предмета
                                        залога',
    ];


    /**
     * Запуск методов обработки сообщений
     * @param \SimpleXMLElement $msg_xml
     * @return bool
     */
    private function parseServiceMessageContentXml(\SimpleXMLElement $msg_xml)
    {
        // $msg_xml->getName() => "MessageData"
        // $msg_xml => Id, Number, CaseNumber, PublishDate, BankruptId, MessageGUID, PublisherInfo, MessageInfo
        // BankruptInfo, FileInfoList, MessageURLList
        $MessageType = $msg_xml->MessageInfo->attributes()['MessageType'];
        $method_name = "processMessage{$MessageType}";
        if (!method_exists($this, $method_name)) {
            //$this->error("Method {$method_name} not exists!");
            $this->askContinue();
            return false;
        }
        /*$this->info(sprintf(
            "Number: %s, CaseNumber: %s, MessageType: %s",
            $msg_xml->Number,
            $msg_xml->CaseNumber,
            $msg_xml->MessageInfo->attributes()['MessageType']
        ));*/
        $this->askContinue($msg_xml->getName(), $msg_xml);
        $this->$method_name($msg_xml);
    }

    /**
     * 5.1.11 <Auction>
     * Объявление о проведении торгов
     * @param \SimpleXMLElement $xml
     */
    private function processMessageAuction(\SimpleXMLElement &$xml)
    {
        // @todo: development
        /** @var Trade $trade */
        // Закоментировал чтобы по 2 раза логика не отрабатывала
//        $trade = Trade::where('IDEFRSB', (int) $xml->IDEFRSB)->orderByDesc('id')->first();
//        if(!$trade) {
//            return;
//        }
//        try {
//            (new MessageAuctionService($trade, $xml))->updateTrade();
//        } catch (\Exception $err) {
//            $this->error($err->getMessage());
//            dump($err->getTraceAsString());
//            $this->askContinue($err->getTraceAsString());
//        } catch (\Throwable $err) {
//            $this->error($err->getMessage());
//            dump($err->getTraceAsString());
//        }
    }
}

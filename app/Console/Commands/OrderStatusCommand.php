<?php

namespace App\Console\Commands;

use App\Models\Order;
use Illuminate\Console\Command;

class OrderStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the order status before end service time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Order::whereIn('status', [Order::STATUS_IN_WORK])
            ->where('service_end', '<=', now())
            ->chunk(100, function ($orders){
                foreach ($orders as $order) {
                    $this->info("Order {$order->id}: onEndServiceTime");
                    $order->onEndServiceTime();
                }
            });
    }
}

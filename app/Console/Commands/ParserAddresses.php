<?php

namespace App\Console\Commands;

use App\Events\PanelEvent;
use App\Models\TradeLot;
use Illuminate\Console\Command;

class ParserAddresses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:addresses {--id=0} {--debug=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parsing lot addresses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $debug = $this->option('debug') === '1';
        if ($id = $this->option('id')) {
            $lots = \Illuminate\Support\Collection::make();
            $lots_query = TradeLot::where('id', $id);
        } else {
            /*$lots = TradeLot::whereHas('params.field', function ($query) {
                return $query->where('code', 'address');
            })->get();*/
            $lots_query = TradeLot::where('published', 1)
                ->whereNull('region_with_type')
                ->orderBy('id', 'desc');
        }

        $count = $lots_query->count();
        $this->info('Lot count: ' . $count);

        $bar = $this->output->createProgressBar($count);
        $bar->start();

        event(new PanelEvent('TradeLot', 'parsing-start', compact('count')));

        $n = 0;
        $lots_query->chunk(100, function ($lots) use (&$n, &$bar, &$count, $debug) {

            /** @var TradeLot $lot */
            foreach ($lots as $lot) {
                $lot->updateAddressByDadata();
                $bar->advance();
                if (++$n % 50 === 0) {
                    event(new PanelEvent('TradeLot', 'parsing-progress', compact('count', 'n')));
                }
                if($debug){
                    $this->info("DEBUG for lot: {$lot->id}");
                    $input = $this->anticipate('Continue?', ['Yes', 'No'], 'Yes');
                    if ($input === 'No') {
                        dd('stopped');
                    }
                }
            }
            event(new PanelEvent('TradeLot', 'parsing-complete', compact('count')));
        });

        $bar->finish();
        $this->line('');
        return 0;
    }
}

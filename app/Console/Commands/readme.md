
- загрузка сообщений с федресурса
```php artisan parser:fedresurs cron```

- загрузка сообщений с федресурса за последние 24 часа
```php artisan parser:fedresurs cron --lastHours=24```


- парсинг категорий и параметров для лотов, если нужно отдельно от предыдущек команды запустить
```php artisan parser:params```

- ...для конкретного лота
```php artisan parser:params --id=70866```

- ...с подробностями
```php artisan parser:params --id=70866 --debug=1```

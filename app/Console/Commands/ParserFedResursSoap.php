<?php


namespace App\Console\Commands;


use Nathanmac\Utilities\Parser\Facades\Parser;

trait ParserFedResursSoap
{

    # soap functions and client
    /**
     * Create soap client
     * @throws \SoapFault
     */
    private function initClient()
    {
        $this->client = new \SoapClient(self::WSDL_FILE, config('services.fedresurs'));
    }

    /**
     * Список идентификаторов сообщений за период
     * @param \DateTime $startDate Дата и время начала периода
     * @param \DateTime|null $endDate \DateTime Дата и время окончания периода
     * @return mixed
     */
    private function getMessageIds($startDate, $endDate = null)
    {
        $params = compact('startDate', 'endDate');
        $res = $this->client->GetMessageIds($params);
        return data_get($res, 'GetMessageIdsResult.int');
    }

    /**
     * Сообщение по идентификатору сообщения
     * @param int $id Идентификатор сообщения в ЕФРСБ
     * @return mixed
     */
    private function getMessageContent($id)
    {
        $res = $this->client->GetMessageContent(['id' => $id]);
        //return new \SimpleXMLElement($res->GetMessageContentResult);
        return Parser::xml($res->GetMessageContentResult);
    }

    private function getMessageContentXml($id)
    {
        $res = $this->client->GetMessageContent(['id' => $id]);
        return simplexml_load_string($res->GetMessageContentResult);
    }

    /**
     * Список идентификаторов торгов и сообщений ЭТП за период
     * @param \DateTime $startFrom Дата и время начала периода
     * @param \DateTime|null $endTo \DateTime Дата и время окончания периода
     * @return mixed
     */
    private function getTradeMessages($startFrom, $endTo = null)
    {
        $params = compact('startFrom', 'endTo');
        $res = $this->client->GetTradeMessages($params);
        return $res->GetTradeMessagesResult;
    }

    /**
     * Список сообщений по идентификатору торгов на ЭТП
     * @param string $id Идентификатор торгов (ЭТП)
     * @param string $tradePlaceInn ИНН площадки
     * @param \DateTime $startFrom Дата и время начала периода
     * @param \DateTime|null $endTo \DateTime Дата и время окончания периода
     * @return mixed
     */
    private function getTradeMessagesByTrade($id, $tradePlaceInn, $startFrom, $endTo = null)
    {
        // todo: develop
        $params = compact('id', 'tradePlaceInn', 'startFrom', 'endTo');
        $res = $this->client->GetTradeMessagesByTrade($params);
        return $res;
    }

    /**
     * Сообщение по идентификатору от ЭТП
     * @param int $idTradeMessage
     * @return mixed
     */
    private function getTradeMessageContentXml($idTradeMessage)
    {

        $params = compact('idTradeMessage');
        $namespace = null;
        $res= null;
        try {
            $res = $this->client->GetTradeMessageContent($params);
            //$this->askContinue(substr($res->GetTradeMessageContentResult, 0, 1024));

            // clearing
            $xml_str = str_replace(array_keys(self::$fix_xml), array_values(self::$fix_xml), $res->GetTradeMessageContentResult);
            #$xml = simplexml_load_string($xml_str, 'SimpleXMLElement', LIBXML_COMPACT | LIBXML_PARSEHUGE);
            //$this->askContinue(substr($xml_str, 0, 1024));

            $xml = simplexml_load_string($xml_str, 'SimpleXMLElement', LIBXML_COMPACT | LIBXML_PARSEHUGE | LIBXML_NSCLEAN);
            $namespace = $xml->getNamespaces(true);

            $keys = array_filter(array_keys($namespace));
            $ns = array_pop($keys );

            if($ns) {
                $ns = $namespace[$ns];
            }

//            Виталина реализация, пока оставим
//            $ns = null;
//            foreach (['soap', 'SOAP-ENV', 'S', 's', 'soapenv', 'soap12', 'env'] as $item) {
//                if (isset($namespace[$item])) {
//                    $ns = $namespace[$item];
//                }
//            }
            //$this->askContinue($ns);
            if ($ns === null) {
                $this->askContinue($res->GetTradeMessageContentResult, $namespace);
            }
            $soap = $xml->children( $ns ); // ->xpath('//nameOfElement')
            //$this->askContinue($xml, $soap, $soap->xpath('//Body'));

            return current($soap->children());
        } catch (\Exception $err) {
            $this->error($err->getMessage());
            $this->askContinue($err->getTraceAsString());
            $this->askContinue($namespace);
            $this->askContinue($res);
        }
    }

    /**
     * GetArbitrManagerRegister. Список арбитражных управляющих
     * @param \DateTime|null $date Дата последнего изменения записей
     * @return mixed
     */
    private function getArbitrManagerRegister($date = null)
    {
        $params = compact('date');
        $res = $this->client->GetArbitrManagerRegister($params);
        return $res->GetArbitrManagerRegisterResult;
    }

    /**
     * GetDebtorRegister. Список должников
     * @param \DateTime|null $date Дата последнего изменения записей
     * @return mixed
     */
    private function getDebtorRegister($date = null)
    {
        $params = compact('date');
        $res = $this->client->GetDebtorRegister($params);
        return $res->GetDebtorRegisterResult;
    }

    /**
     * GetCompanyTradeOrganizerRegister. Список организаторов торгов
     * @param \DateTime|null $date Дата последнего изменения записей
     * @return mixed
     */
    private function getCompanyTradeOrganizerRegister($date = null)
    {
        $params = compact('date');
        $res = $this->client->GetCompanyTradeOrganizerRegister($params);
        return $res->GetCompanyTradeOrganizerRegisterResult;
    }

    /**
     * GetSroRegister. Список СРО АУ
     * @param \DateTime|null $date Дата последнего изменения записей
     * @return mixed
     */
    private function getSroRegister($date = null)
    {
        $params = compact('date');
        $res = $this->client->GetSroRegister($params);
        return $res->GetSroRegisterResult;
    }

    /**
     * GetDebtorsByLastPublicationPeriod. Список должников с последней публикацией в указанный период
     * @param \DateTime|null $date Дата последнего изменения записей
     * @return mixed
     */
    private function getDebtorsByLastPublicationPeriod($date = null)
    {
        $params = compact('date');
        $res = $this->client->GetDebtorsByLastPublicationPeriod($params);
        return $res;
    }

    /**
     * GetDebtorByIdBankrupt. Данные по должнику по его идентификатору
     * @param $idBankrupt
     * @return mixed
     */
    private function getDebtorByIdBankrupt($idBankrupt)
    {
        $params = compact('idBankrupt');
        $res = $this->client->GetDebtorByIdBankrupt($params);
        return $res;
    }

    /**
     * GetDebtorMessagesContentForPeriodByIdBankrupt. Список сообщений по должнику
     * @param string $idBankrupt Идентификатор сообщения в  ЕФРСБ
     * @return mixed
     */
    private function getDebtorMessagesContentForPeriodByIdBankrupt($idBankrupt)
    {
        $params = compact('idBankrupt');
        $res = $this->client->GetDebtorMessagesContentForPeriodByIdBankrupt($params);
        return Parser::xml($res->GetDebtorMessagesContentForPeriodByIdBankruptResult);
    }

    /**
     * GetDebtorReportsContentForPeriodByIdBankrupt. Список отчетов по должнику за период
     * @param string $id Идентификатор должника
     *
     *
     * @param \DateTime $startDate Начало периода отбора отчетов
     * @return mixed
     */
    private function getDebtorReportsContentForPeriodByIdBankrupt($id, $startDate)
    {
        $params = compact('id', 'startDate');
        $res = $this->client->GetDebtorReportsContentForPeriodByIdBankrupt($params);
        return $res;
    }

    /**
     * SearchDebtorByCode. Данные по должнику по коду (ИНН, ОГРН, ОГРНИП, СНИЛС)
     * @param string $codeType
     * @param string $codeValue
     * @return
     */
    private function searchDebtorByCode($codeType, $codeValue)
    {
        $params = compact('codeType', 'codeValue');
        $res = $this->client->SearchDebtorByCode($params);
        return current($res);
    }
}

<?php


namespace App\Console\Commands;


use App\Models\Trade;
use App\Models\TradeLegalCase;
use App\Models\TradeLot;
use App\Models\TradeLotClassificator;
use App\Models\TradeLotSale;
use App\Services\MessageAuctionService;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Nathanmac\Utilities\Parser\Facades\Parser;

/**
 * events, post update
 * Trait ParserFedResursEvents
 * @package App\Console\Commands
 */
trait ParserFedResursEvents
{

    /**
     * Начат прием заявок
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventApplicationSessionStart(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->info("Начат прием заявок ...");
        $trade_model->status = 'ApplicationSessionStarted';

        $lots = self::lotsByLotList($trade_model, $xml->ApplicationSessionStart->LotList);
        /** @var TradeLot $lot */
        foreach ($lots as $lot) {
            if ($lot->inFinalStatus()) {
                continue;
            }
            $lot->applicationSessionStart()->save();
        }

        $trade_model->updateStatusByLots();
    }

    /**
     * Сведения о ходе проведения торгов
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventApplicationSessionStatistic(Trade $trade_model, \SimpleXMLElement $xml)
    {
        $this->info("Сведения о ходе проведения торгов ...");

        $trade_model->DateBegin = Carbon::parse($xml->ApplicationSessionStatistic->DateBegin);

        foreach ($xml->ApplicationSessionStatistic->LotList->children() as $LotStatistic) {
            $LotStatistic_attrs = $LotStatistic->attributes();
            $LotNumber = $LotStatistic_attrs['LotNumber'];
            $EntryCount = $LotStatistic_attrs['EntryCount']; // Количество поступивших заявок
            $AcceptCount = $LotStatistic_attrs['AcceptCount']; // принятых
            $lot = $trade_model->lots()
                ->where('LotNumber', '=', $LotNumber)
                ->first();
            if ($lot) {
                $lot->EntryCount = $EntryCount;
                $lot->AcceptCount = $AcceptCount;
                $lot->ApplicationList = (array)$LotStatistic->ApplicationList;
                $lot->save();
            } else {
                $this->warn("\$lot not found!");
                $this->askContinue($xml);
            }
        }

        // Файл протокола определения участников торгов.
        if ($xml->ApplicationSessionStatistic->Attach) {
            $file_path = "{$trade_model->id}/docs";
            self::saveAttach($xml->ApplicationSessionStatistic->Attach, 'trade', $file_path);
        }
    }

    /**
     * Прием заявок закончен
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventApplicationSessionEnd(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->info("Прием заявок закончен ...");
        $trade_model->status = 'ApplicationSessionEnd';

        $lots = self::lotsByLotList($trade_model, $xml->ApplicationSessionEnd->LotList);
        /** @var TradeLot $lot */
        foreach ($lots as $lot) {
            if ($lot->inFinalStatus()) {
                continue;
            }
            $lot->applicationSessionEnd()->save();
        }
        $trade_model->updateStatusByLots();
    }

    /**
     * Предложение о цене
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventBiddingProcessInfo(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->info("Предложение о цене ...");
        if ($xml->BiddingProcessInfo->PriceInfo) {
            $attrs = $xml->BiddingProcessInfo->PriceInfo->attributes();
            $LotNumber = (string)$attrs['LotNumber'];
            $lot = $trade_model->lots()
                ->where('LotNumber', '=', $LotNumber)
                ->first();
            if ($lot) {
                $lot->NewPrice = (float)$attrs['NewPrice'];
                $lot->save();
            } else {
                $this->warn("Lot {$LotNumber} of trade {$trade_model->TradeId} not found");
            }
        }
    }

    /**
     * 5.1.1.13 Сообщение «Торги не состоялись» - BiddingFail
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventBiddingFail(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->info("Торги не состоялись ...");
        $trade_model->status = 'BiddingFail';

        // Reason
        $Reason = (string)$xml->BiddingFail->Reason;

        // Attach - Файл с протоколом результатов торгов
        if ($xml->BiddingFail->Attach) {
            self::saveAttach($xml->BiddingFail->Attach, 'trade', "{$trade_model->id}/docs");
        }

        // LotList
        if ($xml->BiddingFail->LotList && count($xml->BiddingFail->LotList->children())) {
            foreach ($xml->BiddingFail->LotList->children() as $BiddingStateLotInfo) {
                $BiddingStateLotInfoAttrs = $BiddingStateLotInfo->attributes();
                $LotNumber = (string)$BiddingStateLotInfoAttrs['LotNumber'];
                $Reason = (string)$BiddingStateLotInfoAttrs['Reason'];
                /** @var TradeLot $lot */
                $lot = $trade_model->lots()->where('LotNumber', '=', $LotNumber)->first();
                if ($lot) {
                    $lot->biddingFail($Reason)->save();
                }
            }
            $trade_model->updateStatusByLots();
        } else {
            /** @var TradeLot $lot */
            foreach ($trade_model->lots()->get() as $lot) {
                if ($lot->inFinalStatus()) {
                    continue;
                }
                $lot->biddingFail($Reason)->save();
            }
        }
    }

    /**
     * Сообщение о продаже
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventBiddingInvitation(Trade &$trade_model, \SimpleXMLElement $xml, $TradePlace_Name = null)
    {
        $this->info("Сообщение о продаже ...");
        $trade_model->status = 'BiddingDeclared';
        $trade_model->IDEFRSB = (string)$xml->children()->children()->IDEFRSB;


        // debtor info
        if ($debtor = $xml->BiddingInvitation->Debtor->children()) {
            $DebtorPersonArr = (array)data_get($debtor, $debtor->getName());
            $trade_model->DebtorType = $debtor->getName();
            $trade_model->DebtorINN = (string)$debtor->attributes()->INN;
            $trade_model->DebtorData = $DebtorPersonArr['@attributes'];
        };

        // legalCase info
        if ($LegalCase = $xml->BiddingInvitation->LegalCase) {
            $LegalCaseArr = (array)$LegalCase;
            $trade_model->CaseNumber = $LegalCaseArr['@attributes']['CaseNumber'];
        };

        // ArbitrManager
        if ($ArbitrManager = $xml->BiddingInvitation->ArbitrManager) {
            $ArbitrManagerArr = (array)$ArbitrManager;
            $trade_model->ArbitrManager = $ArbitrManagerArr['@attributes'];
        }

        // CompanyBankrCommis
        if ($CompanyBankrCommis = $xml->BiddingInvitation->CompanyBankrCommis) {
            $CompanyBankrCommisArr = (array)$CompanyBankrCommis;
            $trade_model->CompanyBankrCommis = $CompanyBankrCommisArr['@attributes'];
        }

        // TradeOrganizer
        if ($xml->BiddingInvitation->TradeOrganizer) {
            $TradeOrganizer = $xml->BiddingInvitation->TradeOrganizer->children();
            $TradeOrganizerArr = (array)data_get($TradeOrganizer, $TradeOrganizer->getName());
            $trade_model->TradeOrganizerType = $TradeOrganizer->getName();
            $trade_model->TradeOrganizerINN = $TradeOrganizerArr['@attributes']['INN'];
            $trade_model->TradeOrganizer = $TradeOrganizerArr['@attributes'];
        }

        // TradeInfo
        $TradeInfo = $xml->BiddingInvitation->TradeInfo;
        $TradeInfoAttrs = $TradeInfo->attributes();
        $trade_model->AuctionType = (string)$TradeInfoAttrs->AuctionType;
        $trade_model->FormPrice = (string)$TradeInfoAttrs->FormPrice;
        $trade_model->ISRepeat = !!((string)$TradeInfoAttrs->ISRepeat);
        $trade_model->DatePublishSMI = Carbon::parse((string)$TradeInfo->DatePublishSMI);
        $trade_model->DatePublishEFIR = Carbon::parse((string)$TradeInfo->DatePublishEFIR);
        $TimeEnd = (string)$TradeInfo->OpenForm->TimeEnd;
        if ($TradeInfo->OpenForm) {
            $trade_model->TimeBegin = Carbon::parse((string)$TradeInfo->OpenForm->attributes()['TimeBegin']);
            $trade_model->TimeEnd = $TimeEnd ? Carbon::parse($TimeEnd) : null;
        }
        if ($TradeInfo->CloseForm) {
            $trade_model->TimeResult = Carbon::parse((string)$TradeInfo->CloseForm->attributes()['TimeResult']);
        }
        $trade_model->ApplicationTimeBegin = Carbon::parse((string)$TradeInfo->Application->attributes()['TimeBegin']);
        $trade_model->ApplicationTimeEnd = Carbon::parse((string)$TradeInfo->Application->attributes()['TimeEnd']);
        $trade_model->ApplicationRules = (string)$TradeInfo->Application->Rules;

        $trade_model->save();

        // LotList
        if ($TradeInfo->LotList) {
            foreach ($TradeInfo->LotList->children() as $Lot) {
                $this->eventBiddingInvitation_createLot($trade_model, $Lot, $TradePlace_Name);
            }
        }

        // LegalCase
        if ($LegalCase = $xml->BiddingInvitation->LegalCase) {
            $LegalCaseArr = (array)$LegalCase;
            $LegalCaseFill = $LegalCaseArr['@attributes'];
            $LegalCaseFill['TradeId'] = $trade_model->TradeId;
            $trade_model->legal_case()->create($LegalCaseFill);
        };

        // Debtor
        if ($debtor = $xml->BiddingInvitation->Debtor->children()) {
            $debtor_type = $debtor->getName(); // DebtorCompany, DebtorPerson
            $debtor_obj = ($debtor_type === 'DebtorCompany') ? $trade_model->debtor_company : $trade_model->debtor_person;
            if (!$debtor_obj) {
                // not found, search debtor
                $this->eventBiddingInvitation_createDebtor($trade_model, $debtor);
            }
        };

        // Attach
        if ($TradeInfo->Attach) {
            self::saveAttach($TradeInfo->Attach, 'trade', "{$trade_model->id}/docs");
        }

        // обработка сохраненного Объявления о проведении торгов
        try {
            $xml_msg = null;
            if(Storage::disk('messages')->exists("msg_no_{$trade_model->IDEFRSB}.xml")){
                $xml_msg = Storage::disk('messages')->get("msg_no_{$trade_model->IDEFRSB}.xml");
                $xml_msg = simplexml_load_string($xml_msg, 'SimpleXMLElement', LIBXML_NOCDATA);
            }
            else {
                $xml_msg = $this->getMessageContentXml( $trade_model->IDEFRSB );
                Storage::disk('messages')
                    ->put("msg_no_{$trade_model->IDEFRSB}.xml", $xml_msg->asXML());
            }

            (new MessageAuctionService($trade_model, $xml_msg))->updateTrade();
        } catch (\Exception $err) {
            $this->error($err->getMessage());
            $this->askContinue($err->getTraceAsString());

            Log::error('Id: '. $trade_model->id . " ". 'TradeId: '. $trade_model->TradeId. "\n" . $err->getMessage() . "\n\n" . $err->getTraceAsString());

        } catch (\Throwable $err) {
            $this->error($err->getMessage());
            $this->askContinue($err->getTraceAsString());
            Log::error('Id: '. $trade_model->id . " ". 'TradeId: '. $trade_model->TradeId. "\n" . $err->getMessage() . "\n\n" . $err->getTraceAsString());
        }

    }

    /**
     * @param $trade_model
     * @param \SimpleXMLElement $Lot лот из Сообщение о продаже
     * @param null $TradePlace_Name
     */
    private function eventBiddingInvitation_createLot(&$trade_model, \SimpleXMLElement $Lot, $TradePlace_Name = null)
    {
        $lot_number = (int)$Lot->attributes()['LotNumber'];
        /** @var TradeLot $trade_lot */
        $trade_lot = TradeLot::firstOrNew([
            'TradeId' => $trade_model->TradeId,
            'TradePlaceINN' => $trade_model->TradePlaceINN,
            'LotNumber' => $lot_number,
        ]);

        $trade_lot->TradeOrganizerINN = $trade_model->TradeOrganizerINN;
        $trade_lot->organizer_name = trim($trade_model->trade_organizer_text);
        $trade_lot->place_name = $TradePlace_Name;
        $trade_lot->debtor_name = $trade_model->DebtorType === 'DebtorCompany'
        ? trim($trade_model->DebtorData['FullName'])
        : sprintf(
            "%s %s %s",
            trim($trade_model->DebtorData['FirstName']),
            trim($trade_model->DebtorData['LastName']),
            trim($trade_model->DebtorData['MiddleName'] ?? '')
        );

        $TradeObjectHtml = strip_tags((string)$Lot->TradeObjectHtml);
        $TradeObjectHtml = preg_replace('/([а-я])([А-Я])/u', '$1 $2', $TradeObjectHtml);
        $TradeObjectHtml = str_replace('&amp;#189;', '½', $TradeObjectHtml);

        $trade_lot->status = 'BiddingDeclared';
        $trade_lot->status_log = [];
        $trade_lot->StartPrice = (float)$Lot->StartPrice;
        $trade_lot->StepPrice = (float)$Lot->StepPrice;
        $trade_lot->StepPricePercent = (float)$Lot->StepPricePercent;
        $trade_lot->TradeObjectHtml = trim($TradeObjectHtml);
        $trade_lot->Advance = (float)$Lot->Advance;
        $trade_lot->AdvancePercent = (float)$Lot->AdvancePercent;
        $trade_lot->Participants = (string)$Lot->Participants;
        $classificators_arr = (array)data_get($Lot->Classification, 'IDClass');
        $trade_lot->Classification = $classificators_arr;
        try {
            $trade_lot->save();
            $trade_lot->PriceReduction = (string)$Lot->PriceReduction;
            $trade_lot->PaymentInfo = trim((string)$Lot->PaymentInfo);
            $trade_lot->SaleAgreement = trim((string)$Lot->SaleAgreement);
            $trade_lot->Concours = (string)$Lot->Concours;
            $classificators = TradeLotClassificator::whereIn('code', $classificators_arr)->get();
            $trade_lot->classificators()->sync($classificators);
            $trade_lot->refresh();

            $trade_lot->detectAllProps();
        } catch (\Exception $err) {
            $this->error($err->getMessage());
            $this->askContinue($err->getTraceAsString());
        }
    }

    private function eventBiddingInvitation_createDebtor(&$trade_model, \SimpleXMLElement $debtor)
    {
        $debtorAttrs = $debtor->attributes();
        $INN = (string)$debtorAttrs->INN;
        $debtor_type = $debtor->getName(); // DebtorCompany / DebtorPerson
        $codeType = ($debtor_type === 'DebtorPerson') ? 'PersonInn' : 'CompanyInn';
        try {
            $debtor_res = $this->SearchDebtorByCode($codeType, $INN);
            $debtor_list = self::getListElements($debtor_res->DebtorList);
        } catch (\Exception $err){
            $this->error($err->getMessage());
            $this->askContinue($err->getTraceAsString());
            return;
        }
        $debtor_obj = null;
        if (!empty($debtor_list)) {
            $debtor_obj = current(current($debtor_list));
            if ($debtor_obj === false) {
                $this->error('Debtor not found');
                $this->askContinue(compact('INN', 'debtor_type'));
                $this->askContinue($debtor_obj, $debtor_res);
                return;
            }
            $except_fields = ['LegalCaseList', 'NameHistory'];
            $fields = Arr::except((array)$debtor_obj, $except_fields);
            try {
                switch ($debtor_type) {
                    case 'DebtorCompany':
                        $trade_model->debtor_company()->create($fields);
                        break;
                    case 'DebtorPerson':
                        $trade_model->debtor_person()->create($fields);
                        break;
                }
            } catch (\Exception $err) {
                $this->error($err->getMessage());
                $this->askContinue($err->getTraceAsString());
                $this->askContinue($debtor_obj, $debtor_res);
            }
        } else {
            // not exists
            switch ($debtor_type) {
                case 'DebtorCompany':
                    $trade_model->debtor_company()->firstOrCreate([
                        'INN' => $INN,
                        'FullName' => $debtorAttrs->FullName,
                        'ShortName' => $debtorAttrs->ShortName,
                        'OGRN' => $debtorAttrs->OGRN,
                    ]);
                    break;
                case 'DebtorPerson':
                    $trade_model->debtor_person()->firstOrCreate([
                        'INN' => $INN,
                        'FirstName' => $debtorAttrs->FirstName,
                        'MiddleName' => $debtorAttrs->MiddleName ?? '',
                        'LastName' => $debtorAttrs->LastName,
                        'SNILS' => $debtorAttrs->SNILS,
                    ]);
                    break;
            }
        }
    }

    /**
     * Результаты торгов
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventBiddingResult(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->info("Результаты торгов ...");

        foreach ($xml->BiddingResult->LotList->children() as $LotTradeResult) {
            $LotNumber = $LotTradeResult->attributes()['LotNumber'];
            /** @var TradeLot $lot */
            $lot = $trade_model->lots()
                ->where('LotNumber', '=', $LotNumber)
                ->first();
            if ($lot) {
                $lot->ParticipantsArr = (array)data_get($LotTradeResult, 'Participants');
                $lot->FailureTradeResult = (array)data_get($LotTradeResult, 'FailureTradeResult');
                if ($LotTradeResult->SuccessTradeResult) {
                    $lot->NewPrice = $LotTradeResult->SuccessTradeResult->attributes()['Price'];
                    $lot->SuccessTradeResult = (array)data_get($LotTradeResult, 'SuccessTradeResult');
                }
                $lot->biddingResult()->save();
            } else {
                $this->error('$lot not found. $LotNumber = ' . $LotNumber . ' of TradeId ' . $trade_model->TradeId);
                $this->askContinue(compact('LotNumber'), $xml);
            }
        }
        $trade_model->status = 'Finished';
        $trade_model->updateStatusByLots();

        // Attach - Файл с протоколом результатов торгов
        self::saveAttach($xml->BiddingResult->Attach, 'trade', "{$trade_model->id}/docs");
    }

    /**
     * Торги завершены
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventBiddingEnd(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->info("Торги завершены ...");
        //$trade_model->status = 'Finished';

        $lots = self::lotsByLotList($trade_model, $xml->BiddingEnd->LotList);
        /** @var TradeLot $lot */
        foreach ($lots as $lot) {
            if ($lot->inFinalStatus()) {
                continue;
            }
            $lot->biddingEnd()->save();
        }
        $trade_model->updateStatusByLots();
    }

    /**
     * 5.1.1.18 Сообщение «Сообщение о заключении договора купли-продажи» - ContractSale
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventContractSale(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->info("Сообщение о заключении договора купли-продажи ...");

        // Сведения о результатах заключения договоров.
        foreach ($xml->ContractSale->LotContractSaleList->children() as $LotContractSale) {
            $LotContractSaleAttrs = $LotContractSale->attributes();
            $LotNumber = (string)$LotContractSaleAttrs['LotNumber'];
            /** @var TradeLot $lot */
            $lot = $trade_model->lots()->where('LotNumber', '=', $LotNumber)->first();
            $lot_sale = TradeLotSale::firstOrNew(['trade_lot_id' => $lot->id]);
            $lot_sale->fill([
                'TradePlaceINN' => $trade_model->TradePlaceINN,
                'TradeId' => $lot->TradeId,
                'LotNumber' => $LotNumber,
            ]);

            // Сведения о договоре
            if ($ContractInfo = $LotContractSale->ContractInfo) {
                // Дата заключения договора на приобретение лота
                $DateContract = Carbon::parse((string)$ContractInfo->DateContract);

                // Цена по договору, руб.
                $Price = (float)$ContractInfo->Price;

                $ContractNumber = null;
                if ($ContractInfo->ContractNumber) {
                    // Номер договора на приобретение лота.
                    $ContractNumber = (string)$ContractInfo->ContractNumber;
                };
                $lot_sale->fill([
                    'DateContract' => $DateContract,
                    'Price' => $Price,
                    'ContractNumber' => $ContractNumber,
                ]);
            }

            if ($LotContractSale->AdditionalInfo) {
                // Дополнительная информация.
                $lot_sale->fill([
                    'AdditionalInfo' => (string)$LotContractSale->AdditionalInfo,
                ]);
            };

            // LotContractSale - Сведения об участниках
            $ContractParticipantList = [];
            foreach ($LotContractSale->ContractParticipantList->children() as $ContractParticipant) {
                $ContractParticipantAttrs = (array)$ContractParticipant->attributes();
                // О  - Name, IsWinner, IsBuyer
                // Н  - OGRN, INN, IsWinner
                $ContractParticipantList[] = $ContractParticipantAttrs['@attributes'];
            }

            $lot_sale->fill([
                'ContractParticipantList' => $ContractParticipantList,
            ]);

            $lot->biddingResult()->save();

            $lot_sale->save();

        }

        $trade_model->updateStatusByLots();
    }

    /**
     * 5.1.1.5 Сообщение «Начаты торги» - BiddingStart
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventBiddingStart(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->info("Начаты торги  ...");
        $trade_model->status = 'BiddingInProcess';

        $lots = self::lotsByLotList($trade_model, $xml->BiddingStart->LotList);
        /** @var TradeLot $lot */
        foreach ($lots as $lot) {
            $lot->biddingStart()->save();
        }
        $trade_model->updateStatusByLots();
    }

    /**
     * Отмена торгов
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventBiddingCancel(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->info("Отмена торгов  ...");
        $trade_model->status = 'BiddingCanceled';
        $Reason = (string)$xml->BiddingCancel->attributes()['Reason'];

        if ($xml->BiddingCancel->LotList && count($xml->BiddingCancel->LotList->children())) {
            foreach ($xml->BiddingCancel->LotList->children() as $LotInfo) {
                $LotInfo_attrs = $LotInfo->attributes();
                $LotNumber = (string)$LotInfo_attrs['LotNumber'];
                $Reason = (string)$LotInfo_attrs['Reason'];
                /** @var TradeLot $lot */
                if ($lot = $trade_model->lots()->where('LotNumber', '=', $LotNumber)->first()) {
                    $lot->biddingCancel($Reason)->save();
                };
            }
            $trade_model->updateStatusByLots();
        } else {
            /** @var TradeLot $lot */
            foreach ($trade_model->lots()->get() as $lot) {
                if ($lot->inFinalStatus()) {
                    continue;
                }
                $lot->biddingCancel($Reason)->save();
            }
        }
    }

    /**
     * 5.1.1.19 Сообщение «Сообщение о завершении торгов вследствие оставления
     * конкурсным кредитором предмета залога за собой» - BiddingEndBankruptcyCreditor
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventBiddingEndBankruptcyCreditor(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->warnDebug("О завершении торгов вследствие оставления конкурсным кредитором предмета залога за собой ...");
        $trade_model->status = 'Finished';
        /** @var TradeLot $lot */
        foreach ($trade_model->lots()->get() as $lot) {
            if ($lot->inFinalStatus()) {
                continue;
            }
            $this->warnDebug('TODO: eventBiddingEndBankruptcyCreditor - обработать входящие данные');
            $lot->finished();
        }
        // TODO: eventBiddingEndBankruptcyCreditor - обработать входящие данные
        /*$this->askContinue($xml);
        $this->askContinue($xml->asXML());*/
    }

    /**
     * 5.1.1.14 Сообщение «Приостановление торгов» - BiddingPause
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventBiddingPause(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->info("Торги приостановлены ...");
        $trade_model->status = 'BiddingPaused';
        $Reason = (string)$xml->BiddingPause->attributes()['Reason'];

        if ($xml->BiddingPause->LotList && count($xml->BiddingPause->LotList->children())) {
            foreach ($xml->BiddingPause->LotList->children() as $LotInfo) {
                $LotInfo_attrs = $LotInfo->attributes();
                $LotNumber = (string)$LotInfo_attrs['LotNumber'];
                $Reason = (string)$LotInfo_attrs['Reason'];
                /** @var TradeLot $lot */
                if ($lot = $trade_model->lots()->where('LotNumber', '=', $LotNumber)->first()) {
                    $lot->biddingPause($Reason)->save();
                };
            }
            $trade_model->updateStatusByLots();
        } else {
            /** @var TradeLot $lot */
            foreach ($trade_model->lots()->get() as $lot) {
                if ($lot->inFinalStatus()) {
                    continue;
                }
                $lot->biddingPause($Reason)->save();
            }
        }
    }

    /**
     * 5.1.1.15 Сообщение «Возобновление торгов» - BiddingResume
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventBiddingResume(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->info("Возобновление торгов ...");
        $Reason = (string)$xml->BiddingResume->attributes()['Reason'];

        if ($xml->BiddingResume->LotList && count($xml->BiddingResume->LotList->children())) {
            foreach ($xml->BiddingResume->LotList->children() as $LotInfo) {
                $LotInfo_attrs = $LotInfo->attributes();
                $LotNumber = (string)$LotInfo_attrs['LotNumber'];
                $Reason = (string)$LotInfo_attrs['Reason'];
                /** @var TradeLot $lot */
                if ($lot = $trade_model->lots()->where('LotNumber', '=', $LotNumber)->first()) {
                    $lot->biddingResume($Reason)->save();
                };
            }
        } else {
            /** @var TradeLot $lot */
            foreach ($trade_model->lots()->get() as $lot) {
                if ($lot->inFinalStatus()) {
                    continue;
                }
                $lot->biddingResume($Reason)->save();
            }
        }

        // ставится предыдущий статус
        $trade_model->updateStatusByLots();
    }

    // TODO: eventAnnulmentMessage - ОБЯЗАТЕЛЬНО ОБРАБОТАТЬ
    private function eventAnnulmentMessage(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->error("Сообщение «Аннулирование» - AnnulmentMessage ...");

        // todo: devepor
        Trade::AnnulmentMessage($xml);

        /*$ID_Annulment = (string)$xml->AnnulmentMessage->ID_Annulment;
        $Reason = (string)$xml->AnnulmentMessage->Reason;
        $IsHide = (string)$xml->AnnulmentMessage->IsHide === 'true';
        $this->askContinue($ID_Annulment, $IsHide, $Reason, $xml);*/
        //$this->askContinue($xml);
        // ???  $trade_model->updateStatusByLots();
    }

    /**
     * Принимает системные сообщения и сообщения о возникновении технического сбоя
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventSystemInfo(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->warnDebug('SystemInfo - Системное сообщение');
        $this->askContinue($xml);
    }

    /**
     * 5.1.1.17 Сообщение «Сообщение об установлении новых сроков»
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventBiddingNewTerm(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        $this->info("Сообщение об установлении новых сроков ...");

        // Дата и время возобновления работы электронной площадки
        $trade_model->DateRenewal = (string)$xml->BiddingNewTerm->attributes()['DateRenewal'];

        // Дата и время возобновления представления заявок на участие в торгах в связи с техническим сбоем.
        if ($TimeBeginRequest = (string)$xml->BiddingNewTerm->TimeBeginRequest) {
            $trade_model->ApplicationTimeBegin = Carbon::parse($TimeBeginRequest);
        }

        // Дата и время окончания представления заявок на участие в торгах в связи с техническим сбоем.
        if ($TimeEndRequest = (string)$xml->BiddingNewTerm->TimeEndRequest) {
            $trade_model->ApplicationTimeEnd = Carbon::parse($TimeEndRequest);
        }

        // Дата и время начала торгов в связи с техническим сбоем.
        if ($TimeBeginTrade = (string)$xml->BiddingNewTerm->TimeBeginTrade) {
            $trade_model->TimeBegin = Carbon::parse($TimeBeginTrade);
        }

        // Дата и время окончания торгов в связи с техническим сбоем.
        if ($TimeEndTrade = (string)$xml->BiddingNewTerm->TimeEndTrade) {
            $trade_model->TimeEnd = Carbon::parse($TimeEndTrade);
        }

        // Дата и время объявления результатов торгов в связи с техническим сбоем
        if ($TimeResult = (string)$xml->BiddingNewTerm->TimeResult) {
            $trade_model->TimeResult = Carbon::parse($TimeResult);
        }

        // Сведения о сбое и возобновлении работы электронной площадки.
        $trade_model->FailureInformation = (string)$xml->BiddingNewTerm->Information;


    }

    /**
     * 5.1.1.20 Сообщение «Сообщение о смене организатора торгов»
     * @param Trade $trade_model
     * @param \SimpleXMLElement $xml
     */
    private function eventNewTradeOrganizer(Trade &$trade_model, \SimpleXMLElement $xml)
    {
        // TradeOrganizer
        if ($xml->NewTradeOrganizer->TradeOrganizer) {
            $TradeOrganizer = $xml->NewTradeOrganizer->TradeOrganizer->children();
            $TradeOrganizerArr = (array)data_get($TradeOrganizer, $TradeOrganizer->getName());
            $trade_model->TradeOrganizerType = $TradeOrganizer->getName();
            $trade_model->TradeOrganizer = $TradeOrganizerArr['@attributes'];
        }
    }

    /**
     * @param \SimpleXMLElement $xml Attach element
     * @param string $storage_name
     * @param $file_path
     * @return bool
     */
    static function saveAttach($xml, $storage_name, $file_path)
    {
        if ($xml instanceof \SimpleXMLElement) {
            $Blob = (string)$xml->Blob;
            $file_type = (string)$xml->Type;
            $file_name = (string)$xml->FileName;
        } else {
            $arr = $xml;
            $Blob = $arr['Blob'];
            $file_type = $arr['Type'];
            $file_name = $arr['FileName'];
        }
        $file_name = preg_replace('/["]/', '', $file_name);
        $file_name = Str::limit($file_name, 60, '_');
        if (substr($file_name, strlen($file_type) * -1) !== $file_type) {
            $file_name .= '.' . $file_type;
        }
        $file_path = "{$file_path}/{$file_name}";
        return Storage::disk($storage_name)->put($file_path, base64_decode($Blob));
    }

    /**
     * @param Trade $trade_model
     * @param \SimpleXMLElement $LotList
     * @return \Illuminate\Database\Eloquent\Collection
     */
    static protected function lotsByLotList(Trade &$trade_model, \SimpleXMLElement &$LotList)
    {
        if ($LotList && count($LotList->children())) {
            $LotNumberArr = [];
            foreach ($LotList->children() as $LotInfo) {
                $LotInfo_attrs = $LotInfo->attributes();
                $LotNumberArr[] = (string)$LotInfo_attrs['LotNumber'];
            }

            return $trade_model->lots()
                ->whereIn('LotNumber', $LotNumberArr)
                ->get();
        }
        return $trade_model->lots()->get();
    }
}

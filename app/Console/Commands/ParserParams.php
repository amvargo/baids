<?php

namespace App\Console\Commands;

use App\Events\PanelEvent;
use App\Models\TradeLot;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ParserParams extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:params {--id=0} {--dateMin=0} {--dateMax=0} {--idMin=0} {--idMax=0} {--debug=0} {--lastHours=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parsing params';

    protected $debug = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startTime = Carbon::now();
        $startTimeTotal = Carbon::now()->subHours(4);

        $this->debug = (boolean)$this->option('debug');

        if ($id = $this->option('id')) {
            $lots = \Illuminate\Support\Collection::make();
            $lots->push(TradeLot::findOrFail($id));
        } else {
            $lots = TradeLot::query();
            
            if ($dateMin = $this->option('dateMin')) {
                $lots->where('created_at', '>=', $dateMin);
            }
            if ($dateMax = $this->option('dateMax')) {
                $lots->where('created_at', '<=', $dateMax);
            }
            if ($idMin = $this->option('idMin')) {
                $lots->where('id', '>=', $idMin);
            }
            if ($idMax = $this->option('idMax')) {
                $lots->where('id', '<=', $idMax);
            }
            if ($lastHours = $this->option('lastHours')) {
                $startFrom = now()->addHours($lastHours * -1);

                $lots->where('created_at', '>=', $startFrom->toDateTimeString());
            }

            $lots = $lots->orderBy('id', 'desc')->get();  
        };

        $count = $lots->count();

        if (!$this->debug) {
            $bar = $this->output->createProgressBar($count);
            $bar->start();
        }
        Log::info('Start TradeLotParam parsing');
        event(new PanelEvent('TradeLotParam', 'parsing-start', compact('count')));
        try {
            $n = 0;
            /** @var \Illuminate\Support\Collection|TradeLot[] $lots */
            foreach ($lots as $lot) {

                if (!$this->debug) {
                    $lot->detectAllProps();
                    $bar->advance();
                } else {
                    $this->info('Lot: ' . $lot->id);
                    $this->info('Content: ' . $lot->TradeObjectHtml);
                    $lot->detectCategories();
                    $params = $lot->parseParameters(true);
                    dump($params);
                    $this->ask('Next?');
                }
                if (++$n % 50 === 0) {
                    event(new PanelEvent('TradeLotParam', 'parsing-progress', compact('count', 'n')));
                }
            }
            event(new PanelEvent('TradeLotParam', 'parsing-complete', compact('count')));
        } catch (\Throwable $err) {
            $this->error($err->getMessage());
            if ($this->debug) {
                $this->warn($err->getTraceAsString());
            }
            $message = $err->getMessage();
            Log::error($message);
            event(new PanelEvent('TradeLotParam', 'parsing-error', compact('count', 'message')));
        }

        if (!$this->debug) {
            $bar->finish();
        }
        $this->line('');

        //компоновка данных о лотах
        //storage_path('logs/lot_category/' . Carbon::now()->format('Y-m') . '/lotcategory.log'
        $countError = 0;
        $countErrorCategory = 0;
        $countNoRegularCategory = 0;
        $logFilePath = storage_path().'/logs/lot_category/' . Carbon::now()->format('Y-m') . '/lotcategory-'.Carbon::now()->format('Y-m-d').'.log';
        if (!file_exists($logFilePath)) {
            Log::channel('lotcategory')->info('No Log File for date '.$startTime->format('Y-m-d'));
            return 0;
        }
        $logFile = file($logFilePath);
        $logCollection = [];
        // Loop through an array, show HTML source as HTML source; and line numbers too.
        foreach ($logFile as $line_num => $line) {
            $logCollection[] = array('line'=> $line_num, 'content'=> htmlspecialchars($line));
            if (preg_match('/\[(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})\]/',$line, $matches)) {
                $lineTime = Carbon::create($matches[1],$matches[2],$matches[3],$matches[4],$matches[5],$matches[6]);

                if($lineTime->gt($startTime)) {
                    if (strpos($line, 'Category detect error') !== false) {
                        $countError++;
                        $countErrorCategory++;                
                    }
                    if (strpos($line, 'No regulars in category') !== false) {
                        $countError++;
                        $countNoRegularCategory++;                
                    }
                }
            }
        }  
        Log::channel('lotcategory')->info('PARSER FINISHED for time '.$startTime->format('Y-m-d H:i:s').': Lots:['.$count.'] - Errors:['. $countError . '] - Category detect errors:['.$countErrorCategory.'] - Empty regular lists:[' .$countNoRegularCategory.']');

        //подсчет за последние 4 часа
        $countTotal = 0;
        $countErrorTotal = 0;
        $countErrorCategoryTotal = 0;
        $countNoRegularCategoryTotal = 0;
        $logFile = file(storage_path().'/logs/lot_category/' . Carbon::now()->format('Y-m') . '/lotcategory-'.Carbon::now()->format('Y-m-d').'.log');
        $logCollection = [];
        
        foreach ($logFile as $line_num => $line) {
            $logCollection[] = array('line'=> $line_num, 'content'=> htmlspecialchars($line));
            if (preg_match('/PARSER FINISHED for time (\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/',$line, $matches)) {
                $lineTime = Carbon::create($matches[1],$matches[2],$matches[3],$matches[4],$matches[5],$matches[6]);
            
                if($lineTime->gt($startTimeTotal)) {
                    if (strpos($line, 'TOTAL PARSER FINISHED') !== false) {
                        return 0;
                    }

                    if (preg_match('/(Lots)\:\[(\d+)\]/',$line,$matches)) {
                        $countTotal += $matches[2];
                    }
                    if (preg_match('/(Errors)\:\[(\d+)\]/',$line,$matches)) {
                        $countErrorTotal += $matches[2];
                    }
                    if (preg_match('/(Category detect errors)\:\[(\d+)\]/',$line,$matches)) {
                        $countErrorCategoryTotal += $matches[2];
                    }
                    if (preg_match('/(Empty regular lists)\:\[(\d+)\]/',$line,$matches)) {
                        $countNoRegularCategoryTotal += $matches[2];
                    }
                }
            }
        } 

        Log::channel('lotcategory')->info('TOTAL PARSER FINISHED for time '.$startTime->format('Y-m-d H:i:s').': Lots:['.$countTotal.'] - Errors:['. $countErrorTotal . '] - Category detect errors:['.$countErrorCategoryTotal.'] - Empty regular lists:[' .$countNoRegularCategoryTotal.']');

        return 0;
    }
}

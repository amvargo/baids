<?php

namespace App\Console;

use App\Console\Commands\ParserFedResurs;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $file = 'command_fedresurs_output.log';
        $support_email = config('mail.support_address');

        $schedule->command('parser:fedresurs cron')
            ->everyFiveMinutes()
            ->emailOutputOnFailure($support_email);

        $schedule->command('parser:fedresurs updateLists')
            ->everyTenMinutes()
            ->emailOutputOnFailure($support_email);

        $schedule->command('order:status')
            ->everyTenMinutes()
            ->emailOutputOnFailure($support_email);

        $schedule->call(function(){
            Artisan::call('uniteller:get');
        })->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

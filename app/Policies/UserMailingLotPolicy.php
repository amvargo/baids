<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UserMailingLot;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserMailingLotPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }

    public function view(User $user, $mailing)
    {
        return $mailing->user_id === $user->id;
    }

    public function create(User $user)
    {
        return $user->active_orders()->where('service_id', UserMailingLot::ACCESS_FOR_SERVICE_ID)->count() > 0;
    }

    public function update(User $user, $mailing)
    {
        return $mailing->user_id === $user->id;
    }

    public function delete(User $user, $mailing)
    {
        return $mailing->user_id === $user->id;
    }
}

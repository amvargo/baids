<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UserNotificationRead;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserNotificationReadPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any user notification reads.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the user notification read.
     *
     * @param \App\Models\User $user
     * @param \App\Models\UserNotificationRead $userNotificationRead
     * @return mixed
     */
    public function view(User $user, UserNotificationRead $userNotificationRead)
    {
        return $userNotificationRead->user_id === $user->id || $user->isAdmin();
    }

    /**
     * Determine whether the user can create user notification reads.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the user notification read.
     *
     * @param \App\Models\User $user
     * @param \App\Models\UserNotificationRead $userNotificationRead
     * @return mixed
     */
    public function update(User $user, UserNotificationRead $userNotificationRead)
    {
        return true;
    }

    /**
     * Determine whether the user can delete the user notification read.
     *
     * @param \App\Models\User $user
     * @param \App\Models\UserNotificationRead $userNotificationRead
     * @return mixed
     */
    public function delete(User $user, UserNotificationRead $userNotificationRead)
    {
        return $userNotificationRead->user_id === $user->id || $user->isAdmin();
    }

    /**
     * Determine whether the user can restore the user notification read.
     *
     * @param \App\Models\User $user
     * @param \App\Models\UserNotificationRead $userNotificationRead
     * @return mixed
     */
    public function restore(User $user, UserNotificationRead $userNotificationRead)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can permanently delete the user notification read.
     *
     * @param \App\Models\User $user
     * @param \App\Models\UserNotificationRead $userNotificationRead
     * @return mixed
     */
    public function forceDelete(User $user, UserNotificationRead $userNotificationRead)
    {
        return $user->isAdmin();
    }
}

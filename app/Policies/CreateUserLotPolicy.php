<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CreateUserLotPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }

    public function view(User $user, $lot)
    {
        return true;
    }

    public function create(User $user)
    {
        return $user->org_profile && $user->org_profile->isAllowed();
    }

    public function update(User $user, $lot)
    {
        return $lot->user_id === $user->id;
    }

    public function delete(User $user, $lot)
    {
        return $lot->user_id === $user->id;
    }

    public function restore(User $user, $lot)
    {
        return false;
    }

    public function forceDelete(User $user, $lot)
    {
        return false;
    }
}

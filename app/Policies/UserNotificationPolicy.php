<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UserNotification;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserNotificationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any user notifications.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the user notification.
     *
     * @param \App\Models\User $user
     * @param \App\Models\UserNotification $userNotification
     * @return mixed
     */
    public function view(User $user, UserNotification $userNotification)
    {
        return $userNotification->user_id === $user->id;
    }

    /**
     * Determine whether the user can create user notifications.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the user notification.
     *
     * @param \App\Models\User $user
     * @param \App\Models\UserNotification $userNotification
     * @return mixed
     */
    public function update(User $user, UserNotification $userNotification)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the user notification.
     *
     * @param \App\Models\User $user
     * @param \App\Models\UserNotification $userNotification
     * @return mixed
     */
    public function delete(User $user, UserNotification $userNotification)
    {
        return $userNotification->user_id === $user->id || $user->isAdmin();
    }

    /**
     * Determine whether the user can restore the user notification.
     *
     * @param \App\Models\User $user
     * @param \App\Models\UserNotification $userNotification
     * @return mixed
     */
    public function restore(User $user, UserNotification $userNotification)
    {
        return $userNotification->user_id === $user->id || $user->isAdmin();
    }

    /**
     * Determine whether the user can permanently delete the user notification.
     *
     * @param \App\Models\User $user
     * @param \App\Models\UserNotification $userNotification
     * @return mixed
     */
    public function forceDelete(User $user, UserNotification $userNotification)
    {
        return $user->isAdmin();
    }
}

<?php

namespace App\Providers;

use App\Models\Trade;
use App\Models\TradeLot;
use App\Models\UserMailingLot;
use App\Models\UserNotification;
use App\Models\UserNotificationRead;
use App\Policies\CreateUserLotPolicy;
use App\Policies\UserMailingLotPolicy;
use App\Policies\UserNotificationPolicy;
use App\Policies\UserNotificationReadPolicy;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\View;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        TradeLot::class => CreateUserLotPolicy::class,
        Trade::class => CreateUserLotPolicy::class,
        UserNotification::class => UserNotificationPolicy::class,
        UserNotificationRead::class => UserNotificationReadPolicy::class,
        UserMailingLot::class => UserMailingLotPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}

<?php

namespace App\Providers;

use App\Events\ConsultingUpdated;
use App\Events\OrderCreated;
use App\Events\PanelEvent;
use App\Events\ParserCommandEvent;
use App\Events\TradeLotsCreated;
use App\Listeners\MailingLotsSender;
use App\Listeners\ParserListener;
use App\Listeners\SendEmailConsultationAnswer;
use App\Listeners\SendEmailOrderCreated;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Events\OrderUpdated;
use App\Listeners\SendOrderEvaluationNotification;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        OrderCreated::class => [
            SendEmailOrderCreated::class,
        ],
        ConsultingUpdated::class => [
            SendEmailConsultationAnswer::class,
        ],
        TradeLotsCreated::class => [
            MailingLotsSender::class,
        ],
        ParserCommandEvent::class => [
            ParserListener::class
        ],
        OrderUpdated::class => [
            SendOrderEvaluationNotification::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}

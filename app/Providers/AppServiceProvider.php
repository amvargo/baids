<?php

namespace App\Providers;

use App\Http\Middleware\CheckUserRole;
use App\Http\Requests\CatalogLocation;
use App\Models\User;
use App\Role\RoleChecker;
use App\ViewComposers\GlobalComposer;
use Carbon\Carbon;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Models\SeoConfiguration;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CheckUserRole::class, function (Application $app) {
            return new CheckUserRole(
                $app->make(RoleChecker::class)
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Schema::defaultStringLength(191);
        Blade::directive('money', function ($amount) {
            return "<?php echo number_format($amount, 2, ',', ' '); ?>";
        });
        Blade::directive('money_round', function ($amount) {
            return "<?php echo number_format($amount, 0, ',', ' '); ?>";
        });

        view()->composer('*', GlobalComposer::class);

        view()->composer('layouts.site', function($view)
        {
            $view->with('result_seo', SeoConfiguration::getSeoByUrl());
        });
    }
}

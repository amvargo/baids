<?php

namespace App\Interfaces;

/**
 * Для физ и юр дебиторов, чтобы обращаться одинаково из трейда
 * Interface PlaceOfResidenceInterface
 * @package App\Interfaces
 */
interface PlaceOfResidenceInterface
{
    public function setPlaceOfResidenceAttribute(string $address);
    public function getPlaceOfResidenceAttribute();
}

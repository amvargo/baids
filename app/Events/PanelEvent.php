<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PanelEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $model = null;
    public $event = null;
    public $obj = null;

    /**
     * Create a new event instance.
     *
     * @param string $model
     * @param string $event
     * @param null $obj
     */
    public function __construct($model, $event, $obj = null)
    {
        $this->model = $model;
        $this->event = $event;
        if (isset($obj->id)) {
            $this->obj = ['id' => $obj->id];
        } else {
            $this->obj = $obj;
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('panel');
    }
}

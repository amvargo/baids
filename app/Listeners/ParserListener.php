<?php

namespace App\Listeners;

use App\Events\ParserCommandEvent;
use App\Events\ParserEvent;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;

class ParserListener
{
    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        Log::info('ParserListener handle', [
            'type' => $event->type,
            'command' => $event->command,
        ]);
        //Artisan::queue("parser:fedresurs", ['task' => $event->command]);

        switch ($event->type) {
            case 'params':
                Artisan::queue("parser:params");
                break;
        }
    }
}

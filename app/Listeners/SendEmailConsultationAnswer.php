<?php

namespace App\Listeners;

use App\Models\UserNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailConsultationAnswer
{
    public function handle($event)
    {
        $model = &$event->model;
        $user = &$event->model->user;
        if ($event->model->user) {
            Mail::to($model->email ?: $user)
                ->queue(new \App\Mail\ConsultationAnswer($model));

            $title = 'Ответ на онлайн консультацию';
            $body = "Тема: {$model->category->title}\n\nВопрос: {$model->message}\n\nОтвет: {$model->answer}";
            UserNotification::setNotification($title, $body, $user->user_id);
        }
    }
}

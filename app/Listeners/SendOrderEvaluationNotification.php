<?php

namespace App\Listeners;

use App\Events\OrderUpdated;
use App\Models\Order;
use App\Models\UserNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class SendOrderEvaluationNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderUpdated  $event
     * @return void
     */
    public function handle(OrderUpdated $event)
    {
        $order = $event->order;

        if ($order->status === 'done' && $order->assessment === null) {
            $serviceTitle = $order->service->title;

            $title = "Вам была оказана услуга \"$serviceTitle\". Оцените качество оказания услуги.";
            $body = ""; //"someText...";
            $author = Auth::user();
            if(!$author){
                return;
            }

            $mess = new UserNotification([
                'title' => $title,
                'body' => $body,
                'author_id' => $author->id,
                'user_id' => $order->user_id,
                'order_id' => $order->id
            ]);
            $mess->save();
        }
    }
}

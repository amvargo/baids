<?php

namespace App\Listeners;

use App\Models\TradeLot;
use App\Models\User;
use App\Models\UserMailingLot;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailingLotsSender
{

    const BEGIN_MAILING_TIME = 'today 8am';
    const END_MAILING_TIME = 'today 9pm';

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        $this->checkMailings();
    }

    /**
     * отобрать пользоателей для отправки по времени
     */
    public function checkMailings()
    {
        User::whereHas('active_mailings')->get()->each(function ($user) {

            /*$timezone = $user->time_zone;
            $begin_mailing = Carbon::parse(self::BEGIN_MAILING_TIME, $timezone);
            $end_mailing = Carbon::parse(self::END_MAILING_TIME, $timezone);
            $now = Carbon::now($timezone);

            // if between
            if ($now->gte($begin_mailing) && $now->lte($end_mailing)) {
                // тут ограничение по времени
            }*/
            try {
                $this->sendMailingToUser($user);
            } catch (\Exception $err) {
                Log::error('Cannot send mailing to user: ' . $user->id . ', email: ' . $user->email);
            }
        });
    }

    public function sendMailingToUser(User $user)
    {
        $user->active_mailings->each(function (UserMailingLot $mailing) use ($user) {
            $lots = TradeLot::byUserMailings($mailing, true)->onlyPublic()
                ->limit(20)->get();

            if ($lots->count() > 0) {

                $mailing->last_report = now();
                $mailing->save();

                Log::debug("Send mail to user ID: {$user->id}, {$user->email}");
                Mail::to($user)->queue(new \App\Mail\UserMailingLots($mailing, $lots));
            }
        });
    }
}

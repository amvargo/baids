<?php

namespace App\Listeners;

use App\Events\OrderCreated;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailOrderCreated
{
    /**
     * Handle the event.
     * @param OrderCreated $event
     * @return void
     */
    public function handle(OrderCreated $event)
    {
        Mail::to($event->order->user)
            ->queue(new \App\Mail\OrderCreated($event->order));
    }
}

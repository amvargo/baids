<?php

namespace App\Traits;


use App\Http\Controllers\Panel\UserController;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Trait PageTableController
 * @package App\Traits
 */
trait PageTableController
{
    public function index()
    {
        $this->checkResourceAccess('list');

        $query = $this->getCollectionQuery();

        # filter
        $filter = \request()->get('filter');
        if ($filter) {
            $query = $this->filterTable($query, $filter);
        }

        if ($table_with = $this->tableModelWith()) {
            $query->with($table_with);
        }

        return JsonResource::collection($query->paginate($this->getPaginate()));
    }

    public function getCollectionQuery()
    {
        return $this->sortedTableModel()->withTrashed();
    }


    public function sortedTableModel()
    {
        $sort_by = 'id';
        $sort_dir = 'asc';

        if($sort = \request()->query('sort')){
            if(strpos($sort, '-') === 0){
                $sort_dir = 'desc';
                $sort_by = substr($sort, 1);
            } else {
                $sort_by = $sort;
            }
        }

        $query = $this->tableModel();
        $query = $query::orderBy($sort_by, $sort_dir);
        return $query;
    }

    public function show($id)
    {
        $this->checkResourceAccess('item');
        $item = $this->getShowQuery();
        if ($with = $this->tableModelDetailWith()) {
            $item->with($with);
        }
        return response()->json($item->find($id));
    }

    public function getShowQuery()
    {
        return $this->tableModel()::withTrashed();
    }


    public function update(request $request)
    {
        $this->checkResourceAccess('update');

        $this->checkUpdatingValidation();

        $id = $request->get('id');
        if ($with = $this->tableModelDetailWith()) {
            $item = $this->tableModel()::with($with)->find($id);
        } else {
            $item = $this->tableModel()::find($id);
        };

        $item->fill($request->all());

        if (!$item->save()) {
            throw new HttpException(400, 'Error updating model');
        }

        return response()->json($item);
    }

    public function checkUpdatingValidation()
    {
        if (!empty($rules = $this->getUpdateValidation())) {
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                throw new HttpException(400, $validator->errors()->first());
            }
        }
    }

    public function store()
    {
        $this->checkResourceAccess('create');
        if (!empty($rules = $this->geCreateValidation())) {
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                throw new HttpException(400, $validator->errors()->first());
            }
        }

        // store
        $class_name = $this->tableModel();
        $model = new $class_name;
        $model->fill(Input::all());
        if ($pass = Input::get('password')) {
            $model->password = Hash::make($pass);
        }
        $model->save();

        return response()->json($model);
    }

    public function destroy($id, request $request)
    {
        $this->checkResourceAccess('delete');
        $this->tableModel()::find($id)->delete();
        if (method_exists($this, 'deleteEvent')) {
            $this->deleteEvent($id);
        }
        return response()->json('ok');
    }

    function getPaginate()
    {
        return 20;
    }

    /**
     * @return mixed
     */
    function tableModelWith()
    {
        return null;
    }

    /**
     * @return mixed
     */
    function tableModelDetailWith()
    {
        return null;
    }

    /**
     * Check roles.
     * @param $roles string|array who has access
     */
    public function checkRoles($roles)
    {
        /** @var User $current_user */
        $current_user = Auth::user();
        if (!$current_user->hasAnyRoles((array)$roles)) {
            throw new HttpException(403, 'Forbidden');
        };
    }

    public function resourceAccess()
    {
        return [
            'list' => null,
            'item' => null,
            'create' => null,
            'update' => null,
            'delete' => null,
        ];
    }

    public function checkResourceAccess($action)
    {
        /** @var User $current_user */
        $current_user = Auth::user();
        $access = $this->resourceAccess();
        if (isset($access[$action]) && !$current_user->hasAnyRoles((array)$access[$action])) {
            throw new HttpException(403, 'Forbidden');
        };
    }

    public function geCreateValidation()
    {
        return [];
    }

    public function getUpdateValidation()
    {
        return [];
    }

    /**
     * @param Builder $query
     * @param $search
     * @return Builder
     */
    function filterTable($query, $search)
    {
        foreach ($search as $key => $value) {
            if (empty($value)) {
                continue;
            }
            if ($key === 'role') {
                if ($value === 'ROLE_USER') {
                    $query = $query->where('roles', '=', null);
                } else {
                    $query = $query->where('roles', 'like', '%' . $value . '%');
                }
            } elseif (in_array($key, ['TradeId', 'TradeObjectHtml'])) { // search
                $query = $query->where($key, 'like', "%{$value}%");
            } else {
                $query = $query->where($key, '=', $value);
            }
        }
        return $query;
    }

}

<?php

namespace App\Traits;

use App\Role\UserRole;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

trait modelHasImage2
{

    public function deleteImage2($save = false)
    {
        if (empty($this->image2)) {
            return null;
        }
        if (!Storage::disk('public')->delete($this->image2)) {
            info('Cannot delete image2: ' . $this->image2);
        }
        $this->image2 = null;
        if ($save) {
            return $this->save();
        }
        return $this;
    }

    public function uploadImage2($image, $save = false)
    {
        $class = strtolower(class_basename($this));
        $path = Storage::disk('public')
            ->put("images_{$class}/{$this->id}", $image);

        info('upload image2: ' . $path);
        $this->image2 = $path;

        if ($save) {
            return $this->save();
        }
        return $this;
    }
}

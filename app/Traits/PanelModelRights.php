<?php

namespace App\Traits;

use App\Role\UserRole;

trait PanelModelRights
{

    /**
     * rights: view, create, update, delete
     * @return array
     */
    protected function panelRights()
    {
        return [
            'view' => UserRole::ROLE_SUPPORT,
            'update' => UserRole::ROLE_MANAGEMENT,
            'delete' => UserRole::ROLE_ADMIN,
        ];
    }

    public function getPanelRightsAttribute()
    {
        $user = auth()->user();
        if (!$user) {
            return [];
        }
        $rights = $this->panelRights();
        foreach (['view', 'create', 'update', 'delete'] as $key) {
            if (isset($rights[$key])) {
                $rights[$key] = $user->hasAnyRoles($key);
            } else {
                $rights[$key] = false;
            }
        }
        return $rights;
    }

}

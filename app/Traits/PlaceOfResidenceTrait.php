<?php

namespace App\Traits;

/**
 * Для физ и юр дебиторов, чтобы обращаться одинаково из трейда
 * необходимо свойство в классе - placeOfResidenceField
 * Trait PlaceOfResidenceTrait
 * @package App\Traits
 */
trait PlaceOfResidenceTrait
{

    /**
     * Заполнение адреса места жительства
     * @param string $address
     */
    public function setPlaceOfResidenceAttribute($address = '') {

        $field = $this->placeOfResidenceField;

        $this->attributes[$field] = $address;
    }

    /**
     * Получение места жительства
     * @return mixed
     */
    public function getPlaceOfResidenceAttribute() {
        $field = $this->placeOfResidenceField;
        return $this->{$field};

    }



}

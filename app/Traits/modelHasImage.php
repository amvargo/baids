<?php

namespace App\Traits;

use App\Role\UserRole;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

trait modelHasImage
{

    public function deleteImage($save = false)
    {
        if (empty($this->image)) {
            return null;
        }
        if (!Storage::disk('public')->delete($this->image)) {
            info('Cannot delete image: ' . $this->image);
        }
        $this->image = null;
        if ($save) {
            return $this->save();
        }
        return $this;
    }

    public function uploadImage($image, $save = false)
    {
        $class = strtolower(class_basename($this));
        $path = Storage::disk('public')
            ->put("images_{$class}/{$this->id}", $image);

        info('upload image: ' . $path);
        $this->image = $path;

        if ($save) {
            return $this->save();
        }
        return $this;
    }
}

<?php

namespace App\Traits;

/**
 * Trait PageTableController
 * @package App\Traits
 */
trait PageTableImageController
{

    public function imageUpload($id)
    {
        $request = request();
        $item = $this->tableModel()::findOrFail($id);
        $item->uploadImage($request->file('image'), true);
        return $item;
    }

    public function imageDelete($id)
    {
        $item = $this->tableModel()::findOrFail($id);
        $item->deleteImage(true);
        return $item;
    }

}

<?php

namespace App\Traits;

use App\Role\UserRole;

trait HasRoles
{
    /***
     * @param string $role
     * @return $this
     */
    public function addRole(string $role)
    {
        $roles = $this->getRoles();
        $roles[] = $role;

        $roles = array_unique($roles);
        $this->setRoles($roles);

        return $this;
    }

    /**
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles)
    {
        $this->setAttribute('roles', $roles);
        return $this;
    }

    /***
     * @param $role
     * @return mixed
     */
    public function hasRole($role)
    {
        return in_array($role, $this->getRoles()) || in_array(UserRole::ROLE_ADMIN, $this->getRoles());
    }

    /***
     * @param $roles
     * @return mixed
     */
    public function hasRoles($roles)
    {
        $currentRoles = $this->getRoles();
        foreach ($roles as $role) {
            if (!in_array($role, $currentRoles)) {
                return false;
            }
        }
        return true;
    }

    public function hasAnyRoles($roles)
    {
        if ($this->hasRoleAdmin()) {
            return true;
        }
        $currentRoles = $this->getRoles();
        foreach ((array)$roles as $role) {
            if (in_array($role, $currentRoles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRoleAdmin()
    {
        return in_array(UserRole::ROLE_ADMIN, $this->getRoles());
    }

    public function isAdmin()
    {
        return $this->hasRoleAdmin();
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        $roles = $this->getAttribute('roles');

        if (is_null($roles)) {
            $roles = [];
            return $roles;
        }

        $i = 0;
        while ($i < count($roles)) {
            $user_role = $roles[$i++];
            $children_roles = UserRole::getAllowedRoles($user_role);
            foreach ($children_roles as $children_role) {
                if (!in_array($children_role, $roles)) {
                    $roles[] = $children_role;
                }
            }
        }
        return $roles;
    }
}

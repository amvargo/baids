<?php


namespace App\Traits;


use App\Events\PanelEvent;

trait firePanelModelEvent
{

    protected function fireCustomModelEvent($event, $method)
    {

        if (in_array($event, ['created', 'updated', 'deleted', 'restored'])) {
            info('* ' . $event . ' - ' . $method);
            event(new PanelEvent(class_basename($this), $event, $this));
        }

        // default
        if (!isset($this->dispatchesEvents[$event])) {
            return;
        }

        $result = static::$dispatcher->$method(new $this->dispatchesEvents[$event]($this));

        if (!is_null($result)) {
            return $result;
        }

    }
}

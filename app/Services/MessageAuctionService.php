<?php

namespace App\Services;


use App\Models\FedResurs\ArbitrManager;
use App\Models\FedResurs\CompanyTradeOrganizer;
use App\Models\FedResurs\DebtorCompany;
use App\Models\FedResurs\DebtorPerson;
use App\Models\Trade;
use App\Models\TradeLotInfo;
use App\Models\TradeSite;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Log;
use Nathanmac\Utilities\Parser\Facades\Parser;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Fomvasss\Dadata\Facades\DadataSuggest;
use Illuminate\Support\Arr;


/**
 * Сервис для парсинга "Объявления о проведении торгов"
 * Class MessageAuctionService
 * @package App\Services
 */
class MessageAuctionService {

    protected $msg;             // XML сообщение
    protected $trade;           // Модель трейда
    protected $match_url;       // Найденный урл в тексте
    protected $mess_content;    // Распарсенное сообщение
    protected $auction;         // Часть сообщения к которой нужно часто обращаться
    protected $suggest;         // Ddata Адрес дебитора
    protected $debtor;          // Должник, заполняется в процессе


    public function __construct(Trade $trade, \SimpleXMLElement $msg)
    {
        $this->msg = $msg;
        $this->trade = $trade;
        $this->mess_content = Parser::xml( preg_replace( '/xsi:nil="true"/', '', $this->msg->asXML()) );                  // Парсим найденное сообщение
        $this->auction = data_get($this->mess_content, 'MessageInfo.Auction', []); // Данные о торгах
        $this->match_url = $this->matchUrl();
    }


    /**
     * Перевод Вида торгов (для того чтобы не нарушить логику)
     * @var array
     */
    public static $translate_trade_types = [
        'OpenedAuction' => 'OpenAuction',
        'ClosedAuction' => 'CloseAuction',
        'OpenedConcours' => 'OpenConcours',
        'ClosedConcours' => 'CloseConcours',
        'PublicOffer'    => 'PublicOffer',
        'ClosePublicOffer' => 'ClosePublicOffer'
    ];

    /**
     * Перевод формы подачи предложения о цене (для того чтобы не нарушить логику)
     * @var array
     */
    public static $translate_price_types = [
        'Public'  => 'OpenForm',
        'Private' => 'CloseForm'
    ];

    /**
     * Перевод для типа компании
     * @var array
     */
    public static $translate_debtor_types = [
        'Bankrupt.Company.v2' => 'DebtorCompany',
        'Bankrupt.Person.v2'  => 'DebtorPerson'
    ];

    /**
     * Организаторы торгов, компании
     * @var array
     */
    public static $translate_publisher_companies = [
        'Publisher.ArbitrManagerSro.v2',
        'Publisher.FirmTradeOrganizer.v2',
        'Publisher.Company.v2',
        'Publisher.CentralBankRf.v2',
        'Publisher.Asv.v2',
        'Publisher.FnsDepartment.v2',
        'Publisher.Mfc.v2'
    ];

    /**
     * Организаторы торгов люди
     * @var array
     */
    public static $translate_publisher_persons = [
        'Publisher.PersonTradeOrganizer.v2',
        'Publisher.Person.v2',
        'Publisher.Efrsb.v2',
        'Publisher.ArbitrManager.v2'
    ];


    /**
     * Вся логика;
     */
    public function updateTrade() {

        Log::info("Сообщение об объявлении торгов...");

        // Пытаемся вытянуть урл площадки и записать его, если уже не записан
        if( !empty( $this->trade->place ) && empty( $this->trade->place->full_site_url ) ) {
            if( $this->match_url ) {
                $this->trade->place->full_site_url = $this->match_url;
                $this->trade->place->save();
            }
        }

        // создаем ЭТП
        $this->saveElectronicTradingPlatform();


        // Данные для трейда, поля без проверок - обязательные
        $this->trade->isRepeat = (bool) data_get($this->auction, 'IsRepeat');                // Повторность

        if($TradeType =  data_get($this->auction, 'TradeType') ) {
            $this->trade->AuctionType = self::$translate_trade_types[ $this->auction['TradeType'] ];  // Вид торгов
        }

        if($PriceType =  data_get($this->auction, 'PriceType') ) {
            $this->trade->FormPrice   = self::$translate_price_types[ $PriceType ];  // Форма
        }


        if( !empty($this->auction['Date']) ) {
            $this->trade->TimeBegin   = Carbon::parse( $this->auction['Date'] );                  // Дата начала торгов
        }
        $this->trade->Text      = data_get($this->auction, 'Text', '');              // Заполнение текста


        if( $TimeBegin = data_get($this->auction, 'Application.TimeBegin') ) {
            $this->trade->ApplicationTimeBegin = Carbon::parse($TimeBegin);                   // Дата начала приема заявок
        }

        if($TimeEnd = data_get($this->auction, 'Application.TimeEnd')) {
            $this->trade->ApplicationTimeEnd = Carbon::parse($TimeEnd);                 // Дата окончания приема заявок
        }


        // Заполнение Должника, если он пришел

        try {
            if ($BankruptMessage =  data_get($this->mess_content, 'Bankrupt')) {
                $this->debtor = $this->updateOrCreateBankrupt($BankruptMessage, $this->mess_content['BankruptId']);   // Создаем или обновляем банкрота
                $this->trade->DebtorType = self::$translate_debtor_types[ $BankruptMessage['@xsi:type'] ];            // Тип должника

            }
            else {
                $this->debtor = $this->trade->debtor;
            }
        } catch( \ErrorException $e) {
            Log::error( $e->getMessage() . "\n" . $e->getTraceAsString() );
        } catch (\Throwable $e) {
            Log::error( $e->getMessage() . "\n" . $e->getTraceAsString() );
        }



        if ( $this->debtor ) {
            $this->getDadataAdress( $this->debtor->place_of_residence );                                        // Получаем распарсенный адрес
            $this->trade->DebtorINN = $this->debtor->INN;                                                       // Заполняем ИНН

            // Заполнение данных
            if( $this->trade->DebtorType === 'DebtorCompany' ) {
                $this->trade->DebtorData = [
                    'FullName'  => $this->debtor->FullName,
                    'INN'       => $this->debtor->INN,
                    'OGRN'      => $this->debtor->OGRN,
                    'ShortName' => $this->debtor->ShortName
                ];
            }
            else {
                $this->trade->DebtorData = [
                    'FirstName'  => $this->debtor->FirstName,
                    'MiddleName' => $this->debtor->MiddleName,
                    'LastName'   => $this->debtor->LastName,
                    'INN'        => $this->debtor->INN
                ];

            }
        }

        try {
            $this->fillTradeOrganizer();    // Пытаемся заполнить компанию органитазора торгов или арбитражного управляющего, если в трейде пусто, то записываем их как организаторов
        } catch( \ErrorException $e) {
            Log::error( $e->getMessage() . "\n" . $e->getTraceAsString() );
        } catch (\Throwable $e) {
            Log::error( $e->getMessage() . "\n" . $e->getTraceAsString() );
        }

        $this->trade->save();
        $this->saveDocuments();         // Создание документов
        $this->updateLots();            // Обновление лотов

    }


    /**
     * Поиск урл в тексте
     * @return mixed|string|null
     */
    public function matchUrl()
    {
        preg_match('/(((https|http):\/\/)|(www\.))[0-9a-zA-Zа-я-А-Я\.-]*\.[a-z]*\b/', $this->auction['Text'] ?? '', $matches_url);

        $url = null;

        if(!empty($matches_url[0])) {
            $url = $matches_url[0];
        }

        if(!empty($url) && strpos($url, 'http') === false) {
            $url = "https://{$url}";
        }

        return $url;
    }


    /**
     * Находим или создаем ЭТП
     */
    protected function saveElectronicTradingPlatform()
    {
        $TradeSite    = trim($this->auction['TradeSite'] ?? '');
        $IdTradePlace = data_get($this->auction, 'IdTradePlace');
        $query = TradeSite::query();

        if(empty($TradeSite) && empty($IdTradePlace) ) {
            return;
        }

        if($IdTradePlace) {
            $query->orWhere('IdTradePlace', $IdTradePlace);
        }

        if($TradeSite) {
            $query->where('TradeSite', $TradeSite);
        }

        $ExistsTradeSite = $query->first();


        if($IdTradePlace && $ExistsTradeSite && empty($ExistsTradeSite->IdTradePlace)) {

            $ExistsTradeSite->IdTradePlace = $IdTradePlace;
            $ExistsTradeSite->save();

        }

        if( empty($ExistsTradeSite) && $this->match_url) {

            $data = [
                'IdTradePlace' => $IdTradePlace,
                'TradeSite'    => $TradeSite,
                'url'          => $this->match_url
            ];

            $ExistsTradeSite = TradeSite::create( $data );
        }

        if($ExistsTradeSite) {
            $this->trade->trade_site()->associate( $ExistsTradeSite );
        }

    }


    /**
     * Сохранение документов
     * @param $this->mess_content
     */
    protected function saveDocuments() {

        try {
            if($doc_url_list = data_get($this->mess_content, 'MessageURLList.MessageURL')) {

                // если 1 элемент, то объект
                if( !empty($doc_url_list['@URLName']) ) {
                    $doc_url_list = [ $doc_url_list ];
                }

                $FileInfoList = $this->mess_content['FileInfoList']['FileInfo'];

                if( !empty($FileInfoList['Hash']) ) {
                    $FileInfoList = [ $FileInfoList ];
                }

                $doc_url_list = collect( $doc_url_list );
                $exists_hash = [];
                $data_for_create = [];

                foreach($doc_url_list as $index => $doc_data) {

                    $hash = $FileInfoList[$index]['Hash'];
                    $exists_hash[] = $hash;

                    $data_for_create[] = [
                        'name' => $doc_data['@URLName'],
                        'url'  => preg_replace('/^(http|https)/', 'https', $doc_data['@URL']),
                        'size' => $doc_data['@DownloadSize'],
                        'hash' => $hash
                    ];
                }

                $this->trade->documents()->whereIn('hash', $exists_hash)->delete();
                $this->trade->documents()->createMany($data_for_create);
            }
        } catch (\ErrorException $e) {
            Log::error( $e->getMessage() . "\n" . $e->getTraceAsString() );
            return;
        }
    }


    /**
     * Обновление или создание дебитора
     * @param $data
     * @param $bankrupt_id
     */
    private function updateOrCreateBankrupt( $data, $bankrupt_id) {

        $debtor = null;

        $bankruptType = self::$translate_debtor_types[ $data['@xsi:type'] ];

        switch ($bankruptType) {
            case 'DebtorCompany':
                $debtor = DebtorCompany::firstOrNew([ 'BankruptId' => $bankrupt_id ]);
                $debtor->fill([
                    'INN' => trim(data_get($data, 'Inn')),
                    'OGRN' => trim(data_get($data, 'Ogrn')),
                    'ShortName' => trim(data_get($data, 'Name')),
                    'LegalAddress'   => trim(data_get($data, 'Address')),
                    'Category'  => data_get($data, 'Category.Description'),
                    'CategoryCode'  => data_get($data, 'Category.Code'),
                ]);

                if( empty($debtor->FullName) ) {
                    $debtor->FullName = $debtor->ShortName;
                }

                $debtor->save();

                break;
            case 'DebtorPerson':

                $fields_for_save = [
                    'SNILS' => 'Snils',
                    'Address' => 'Address',
                    'Birthplace' => 'Birthplace'
                ];

                $debtor = DebtorPerson::firstOrNew([ 'BankruptId' => $bankrupt_id ]);

                $data_for_update = [
                    'INN' => trim($data['Inn']),
                    'FirstName' => trim($data['Fio']['FirstName']),
                    'MiddleName' => trim(data_get($data, 'Fio.MiddleName', '')),
                    'LastName' => trim(data_get($data, 'Fio.LastName', '')),
                    'Category'  => data_get($data, 'Category.Description'),
                    'CategoryCode'  => data_get($data, 'Category.Code'),
                    'Birthdate' => Carbon::parse(data_get($data, 'Birthdate'))
                ];


                foreach($fields_for_save as $key => $field) {

                    if( !empty( $data[$field] ) ) {
                        $data_for_update[$key] = trim($data[$field]);
                    }

                }

                $debtor->update( $data_for_update );
                break;
        }



        return $debtor;

    }


    /**
     * Заполнение организатора торгов
     */
    private function fillTradeOrganizer() {

        $publisher = data_get($this->mess_content, 'Publisher');
        if( empty($publisher) ) {
            return;
        }

        if( in_array($publisher['@xsi:type'], self::$translate_publisher_companies)) {

            $company = CompanyTradeOrganizer::firstOrNew([
                'INN' => $publisher['Inn'],
                'OGRN' => $publisher['Ogrn'] ?? $publisher['Ogrnip']
            ]);

            // Заполняем полное имя
            if( empty($company->FullName) && !empty($publisher['Name']) ) {
                $company->FullName = $publisher['Name'];
            }

            // Заполняем короткое имя
            if( empty($company->ShortName) && !empty($publisher['Name'])) {
                $company->ShortName = $publisher['Name'];
            }

            // Пытаемся заполнить урл
            if( !empty($this->match_url) && empty($company->site) ) {
                $company->site = $this->match_url;
            }

            $company->save();

            if( empty( $this->trade->TradeOrganizer ) ) {
                $this->trade->TradeOrganizerINN = $company->INN;
                $this->trade->TradeOrganizerType = 'TradeOrganizerCompany';
                $this->trade->TradeOrganizer = [
                    'FullName' => $company->FullName,
                    'INN' => $company->INN,
                    'OGRN' => $company->OGRN,
                ];
            }
        }

        elseif(in_array($publisher['@xsi:type'], self::$translate_publisher_persons)) {

            $data = [
                'FirstName' => data_get($publisher, 'Fio.FirstName'),
                'LastName'  => data_get($publisher, 'Fio.LastName'),
                'MiddleName' => data_get($publisher, 'Fio.MiddleName'),
                'INN' => data_get($publisher, 'Inn')
            ];

            if($Snils = data_get($publisher, 'Snils')) {
                $data['SNILS'] = $Snils;
            }


            if( empty( $this->trade->TradeOrganizer ) ) {
                $this->trade->TradeOrganizerINN = $data['INN'];
                $this->trade->TradeOrganizerType = 'TradeOrganizerPerson';
                $this->trade->TradeOrganizer = $data;
            }

            if( $publisher['@xsi:type'] === 'Publisher.ArbitrManager.v2') {

                ArbitrManager::firstOrCreate( $data );

                if( empty( $this->trade->ArbitrManager ) ) {
                    $data = $this->trade->TradeOrganizer;
                    $data['RegNum'] = null;
                    $this->trade->ArbitrManager = $data;
                }

            }

        }

    }

    /**
     * Обновление лотов
     */
    protected function updateLots() {

        // Инфа по лотам
        $lots_inf = data_get($this->auction, 'LotTable.AuctionLot');

        if( !$lots_inf ) {
            Log::warn( "Сообщение 'Объявление о проведении торгов', Лотов нет" );
            return;
        }


        // Если 1 элемент, то приходит объектом - исправляем
        if( !empty($lots_inf['Order']) ) {
            $lots_inf = [ $lots_inf ];
        }

        $lots_inf = new Collection($lots_inf);

        $lots_inf_order = $lots_inf->keyBy('Order'); // ключи номеров лотов для быстрого доступа
        $replace = '/[^a-zа-я0-9]/ui';

        $check_same_count_lots = $lots_inf->count() === $this->trade->lots->count();
        $check_same_lots = false;

        if ($check_same_count_lots &&  $lots_inf->count() !== 1 ) {
                $lot_numbers_message = $this->trade->lots->pluck('LotNumber');
                $lot_numbers_trade   = $lots_inf->pluck('Order');
                $diff = $lot_numbers_message->diff( $lot_numbers_trade );
                $check_same_lots = $diff->isEmpty();
        }

        // бежим по всем лотам и проставляем им инфу
        foreach($this->trade->lots as $lot) {

            // Заполнение имени дебитора для поиска
            if( !empty($this->trade->debtor) ) {
                $lot->debtor_name = $this->trade->debtor->FullName;
            }

            if( $organizer_name = $this->trade->trade_organizer_text ) {
                $lot->organizer_name = $organizer_name;  // Заполнение имени организатора для поиска
            }
            // конфликт с логикой Витали
            if( empty($lot->place_name) &&  !empty($this->auction['TradeSite'])) {
                $lot->place_name = $this->auction['TradeSite'];
            }

            // Заолняем дату если она есть
            if( !empty($this->suggest) ) {
                $lot->fill(Arr::only($this->suggest['data'], ['postal_code', 'federal_district', 'region_with_type', 'area_with_type']));
                $lot->city_with_type = $this->suggest['data']['city_with_type'] ?: $this->suggest['data']['settlement_with_type'];
                $lot->dadata_suggest = $this->suggest;
            }


            /**
             * Для этого обновления необходимо идентифицировать лот (лот сообщение о торгах = лот объявление о продаже)
             */

            $lot_inf = null;

            if($check_same_count_lots && $lots_inf->count() === 1) {
                $lot_inf = $lots_inf->first();
            }
            elseif( $check_same_lots ) {
                $lot_inf = $lots_inf_order[ $lot->LotNumber ];
            }
            elseif( $this->trade->trade_site ) {
                if($this->trade->trade_site->search_for_text) {

                    $TradeObjectHtml = preg_replace($replace, '', $lot->TradeObjectHtml);
                    $lot_inf = $lots_inf->filter(function($value, $key) use($replace, $TradeObjectHtml, $lot){
                        $Description = preg_replace($replace, '', $value['Description']);
                        $result = (mb_stripos($TradeObjectHtml, $Description) !== false) ||
                            (mb_stripos($TradeObjectHtml, "лот{$value['Order']}") !== false);
                        return $result;
                    })->first();

                } elseif( !empty( $lots_inf_order[ $lot->LotNumber ] ) ) {
                    $lot_inf = $lots_inf_order[ $lot->LotNumber ];
                }
            }

            /**
             * если нашли или не нашли трейд
             */
            if( empty( $lot_inf ) ) {
                Log::warn( "Сообщение 'Объявление о проведении торгов', CaseNumber: {$this->trade->CaseNumber},
                TradeId: {$this->trade->TradeId} - Лот с номером {$lot->LotNumber} не найден" );
            }
            else {

                $lot->StartPrice = !empty($lot->StartPrice) ? $lot->StartPrice : (float)$lot_inf['StartPrice']; // Начальная цена, заполняем только тогда, когда нет
                $lot->TradeObjectHtml = $lot_inf['Description'] ?? '';                // Общая информация о лоте
                $AdvanceStepUnit = data_get($lot_inf, 'AdvanceStepUnit');
                $Advance = (float) $lot_inf['Advance'];

                // Заполнение задатка всегда в процентах
                if( $AdvanceStepUnit && $Advance ) {
                    $lot->AdvancePercent = $AdvanceStepUnit === 'Percent' ? $Advance : round($Advance / $lot->StartPrice * 100);
                }


                // заполнение шага аукциона, всегда в процентах
                $auctionStepUnit = data_get($lot_inf, 'AuctionStepUnit');
                $Step = (float) $lot_inf['Step'];

                if($auctionStepUnit && $Step ) {
                    $lot->StepPricePercent = $auctionStepUnit === 'Percent' ? $Step : round($Step / $lot->StartPrice * 100);
                }

                if($PriceReductionAuction = data_get($lot_inf, 'PriceReduction')) {
                    TradeLotInfo::updateOrCreate(
                        [ 'trade_lot_id' => $lot->id ],
                        [ 'PriceReductionAuction' => trim($PriceReductionAuction, "\n ") ]
                    );
                }

            }

            $lot->save();
            Log::info("Сообщение 'Объявление о проведении торгов' - обновлен лот - {$lot->LotNumber}, CaseNumber: {$this->trade->CaseNumber}, TradeId: {$this->trade->TradeId}");
        }

    }


    /**
     * Получение адресса для дебитора
     * @return bool
     */
    protected function getDadataAdress($address_str) {

        try {
            $result = DadataSuggest::suggest("address", ["query" => $address_str, "count" => 2]);
            if (count($result) !== 3) {
                $result = $result[0];
            }
            $this->suggest = $result;

        } catch (\Throwable $err) {
            Log::warning($err->getMessage(), ['from address' => $address_str]);
            return false;
        }

    }
}

<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SphinxService {
    //'baids_lot_index'
    //'baids_trades_index' для поиска по arbitr_name


    public static function getSphinxSearch($table, $search_string, $output, $match = '*')
    {
        $result = [];
        try {
            $conn = DB::connection('sphinx');

            $result = $conn->table($table)->match($match, $search_string)->limit(999999)->pluck($output);
            return $result;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            //throw new \ErrorException($e->getMessage());
            return false;
        }
    }

    public static function getSphinxSearchMulti($table, $search_array, $output, $match = '') {
        $result = [];

        try {
            $conn = DB::connection('sphinx');
            $search_string =  'MATCH(\''. ($match?"@{$match} ":"") .'(' . implode(')|(', $search_array).')\')';

            $result = $conn->table($table)->whereRaw($search_string)->limit(999999)->pluck($output);
            return $result;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }
}
